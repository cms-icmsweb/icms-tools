[![pipeline status](https://gitlab.cern.ch/cms-icmsweb/icms-tools/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-icmsweb/icms-tools/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cms-icmsweb/icms-tools/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-icmsweb/icms-tools/-/commits/master)

# icms-tools

This project provides a REST-alike API, a reverse proxy for the icms-piggyback and an identity provider service used by the old iCMS.

Once a standalone web application, since its inception `icms-tools` has bifurcated into [a separate frontend application](https://gitlab.cern.ch/cms-icmsweb/icms-tools-front) and the [API](https://icms.cern.ch/tools-api/restplus) itself.
This transition notwithstanding, some vestiges of Jinja-based templates, JavaScript code or related plumbing remain.

## Getting started

While setups may vary and trends change, the steps summarized below have once been useful to get things off the ground:

1. Check the code out, ideally after having forked the repository.
2. Create a `virtualenv`, activate it and, just before installing the dependencies (`pip install -r requirements/development.txt`):
   - make sure to have a local version of [icms-common](https://gitlab.cern.ch/cms-icmsweb/icms-common) installed to satisfy `pip`'s dependency: `-e file:///${PWD}/../icms-common#egg=icms_common`
   - do the same for [icms-orm](https://gitlab.cern.ch/cms-icmsweb/icms-orm), satisfying `-e file:///${PWD}/../icms-orm#egg=icms_orm`
   - make sure to apt-get upgrade/install the necessary packages described in the .gitlab.ci.yml file. If a psycopg2 issue appears during requirements installation then you may need to install the libpq-dev package too.
3. Create an [instance config](https://flask.palletsprojects.com/en/1.1.x/config/#instance-folders) file `instance/config.py` resembling the following:

```python
import icms_orm
from util import constants as const
import logging

icms_orm.toolkit_schema_name = lambda: 'toolkit'
icms_orm.epr_schema_name = lambda: 'epr'

TESTING = False

SQLALCHEMY_BINDS = {
const.OPT_NAME_BIND_LEGACY: 'mysql+pymysql://icms_legacy:@127.0.0.1/CMSPEOPLE?charset=utf8mb4',
const.OPT_NAME_BIND_TOOLKIT: 'postgresql+psycopg2://toolkit:icms@localhost/icms',
const.OPT_NAME_BIND_EPR: 'postgresql+psycopg2://epr:icms@localhost/icms',
const.OPT_NAME_BIND_COMMON: 'postgresql+psycopg2://icms:icms@localhost/icms',
}

SQLALCHEMY_DATABASE_URI = SQLALCHEMY_BINDS[const.OPT_NAME_BIND_TOOLKIT]
LOGIN_IGNORE_PASSWORD = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
LOGGING_LEVEL = logging.DEBUG
SECRET_KEY = 'NothingFancy'
ENVIRONMENT_VARIABLES = {'NLS_LANG': 'AMERICAN_AMERICA.UTF8'}
ICMS_PIGGYBACK_URL = 'http://localhost:7070'
AL_FILES_DIR = '/home/bawey/Desktop/alfiles'

exec('%s=\'%s\'' % (const.OPT_NAME_INDICO_TOKEN, 'foobar'))
exec('%s=\'%s\'' % (const.OPT_NAME_INDICO_SECRET, 'FOOBAR'))
```

4. Try verifying your setup with `TOOLKIT_CONFIG=DEV python manage.py run --host 0.0.0.0` (or any other command exposed through `manage.py`). It shouldn't fully work yet, due to missing databases, but all the software dependencies should be satisfied by now.
5. You can spin up the necessary database containers using the commands below (you might want to add `--rm` and/or `--tmpfs` for throw-away, in-memory instances):
   - `docker run /var/lib/postgresql/data:rw,noexec,nosuid --name pg -e POSTGRES_PASSWORD=postgres -d -p 127.0.0.1:5432:5432 postgres:9.5`
   - `docker run /var/lib/mysql:rw,noexec,nosuid --name mdb -e MYSQL_ROOT_PASSWORD='' -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -d -p 127.0.0.1:3306:3306 mariadb:latest`
6. At this point you'll need the database dump files (they cannot be publicly shared though so ask your colleagues for directions). Once you obtain the files, they should be importable like so:
   - for MySQL: `gunzip <SOMEFILE.dump.gz> -c | mysql -u root -h 127.0.0.1`
   - for Postgres: `psql -U postgres -h 127.0.0.1 -d icms -f <SOMEFILE.sql>`
     - actually the `icms` database will need to be created beforehand, e.g. with `create database icms;` executed from `psql` console
7. You should be all set by now, try:
   - running the webapp and opening the Swagger GUI (typically `http://localhost:5000/restplus/`)
   - making an API call, e.g. with `httpie`: `http localhost:5000/restplus/icms_public/user_info User:tbawej`

### Alternative setup with fake test DBs

This is a minimal setup without dumps, as an alternative to step 6 above. Both DBs will be set up (or reset) and populated with fake data, in the same way as for CI testing.

**DB bootstrap**

If you haven't performed this already, bootstrap the DBs with the right schemata and users:

```bash
echo "import icms_orm" | PYTHONPATH=./ python -m icms_orm.configuration.db_bootstrap_script mysql | mysql -u root -h ${MYSQL_HOST}
echo "import icms_orm" | PYTHONPATH=./ python -m icms_orm.configuration.db_bootstrap_script postgres | psql -U ${POSTGRES_USER} -d ${POSTGRES_DB} -h ${POSTGRES_HOST}
```

Notes:

- You many need to modify the DB connection details in line with your individual setup.
- You need root / superuser access to both DBs.
- Check if databases' names in dump files are correct. Your colleague might have used another ones for testing purposes.
- Check the schema used in Postgres dump file and make sure you have access to it. `ALTER SCHEMA <schema> OWNER TO <user>` if needed.
- The schema name may be different from `public`. `SET search_path TO <schema>` before listing the tables created by the script.

**DB setup / reset and population with fake data**

With the venv activated, run the following:

```bash
ICMS_LEGACY_USER=root PYTHONPATH=./ pytest tests/reset_populate_test_dbs.py -p tests.prestart_plugin
```

Notes:

- This script will start by dropping all tables in both the Postgres `icms` and MySQL `CMSPEOPLE` DBs! Only a subset of the tables will be populated afterwards (just enough for a minimal development / testing environment).
- Since the minimally set-up DB doesn't include your personal details, you probably need to set the `User` HTTP header to a username that exists in the DB, e.g. `User: tbawej`. When using a browser, the [Modify Header Value](https://addons.mozilla.org/en-US/firefox/addon/modify-header-value/) add-on can be handy; just make sure to `Modify` the Header instead of `Add`ing it.

## DB migrations

The right tool for performing the migrations depends on the affected schema(ta). It is currently recommended that migrations are split in a way such that each individual alembic version affects only one schema. The migration tool provided in this repo is meant for performing migrations only to the `toolkit` schema. For the `public` schema, see the `icms-orm` repo and for the `epr` schema, see the `iCMS-EPR` repo.

Flask-Migrate is used for the migrations, which acts as an interface between Flask and alembic. It adds a flask script subcommand db which sets up the Flask app context and acts as an entrypoint to the alembic CLI commands. Alembic migration files are stored in `./migrations/versions/` and the post-migration script is `./migrations/env.py`.

The regular alembic commands can be executed via a `flask db` prefix, e.g.

- `flask db current` for getting the current DB version
- `flask db history` for the available versions
- `flask db upgrade` for migrating to a newer version
- `flask db downgrade` for migrating to a previous version

Check out the [alembic](https://alembic.sqlalchemy.org/en/latest/) and [Flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/index.html) official docs for more information.

## What about the tests...

One way of launching them would be to mimic [what happens in CI](https://gitlab.cern.ch/cms-icmsweb/icms-tools/-/blob/master/.gitlab-ci.yml) but that might very well obliterate your databases if they're configured as above.
If you're comfortable enough setting up your containers so that the tests execute against different DB backends, you can read no further. Otherwise you might find the following useful:

```bash
cd ${ORM_DIR}
source ${ORM_VNV_DIR}/bin/activate

export POSTGRES_HOST=127.0.0.1
export MYSQL_HOST=127.0.0.1
export ICMS_OLD_PEOPLE_SCHEMA=TEST_CMSPEOPLE
export ICMS_OLD_NOTES_SCHEMA=TEST_Portal_notes
export ICMS_OLD_WF_NOTES_SCHEMA=TEST_Portal_wf_notes
export ICMS_OLD_NEWS_SCHEMA=TEST_Portal_news
export ICMS_OLD_CADI_SCHEMA=TEST_CMSAnalysis
export ICMS_OLD_METADATA_SCHEMA=TEST_MetaData
export ICMS_COMMON_ROLE=icms_test
export ICMS_EPR_ROLE=epr_test
export ICMS_TOOLKIT_ROLE=toolkit_test
export ICMS_READER_ROLE=icms_test_reader
export ICMS_LEGACY_USER=icms_legacy_test

PYTHONPATH=./ python icms_orm/configuration/db_bootstrap_script.py postgres | psql -U postgres -h 127.0.0.1
PYTHONPATH=./ python icms_orm/configuration/db_bootstrap_script.py mysql | mysql -u root -h 127.0.0.1
```

The snippet above requires the `icms-orm` to be checked out into the `${ORM_DIR}` directory and the corresponding virtualenv to reside at `${ORM_VNV_DIR}`. What follows defines several configuration variables before launching a DB-bootstrapping script and piping the resulting SQL to respective DB clients.

As long as the environment variables are defined for the current terminal session, the tests can be launched like so:

```bash
PYTHONPATH=./ pytest tests -v -o junit_family=xunit1 -p tests.prestart_plugin
```

Depending on how you launch your tests, the abovementioned configuration options might need to be set accordingly in order to be effective (e.g. for the IDE of your choice).

## Various details

The following sections aim to briefly summarize various aspects of `icms-tools`'s inner workings, at least the most important ones.

### Application settings

#### Profiles and configs

There are several profiles predefined in the [`app_profiles`](./app_profiles.py) module. Each contains a template of the application's config object that can be further refined through the overrides specified in the `/instance/config.py` file (as described above).
Profiles can be activated at startup using the `TOOLKIT_CONFIG` environment variable (which is a bit of a misnomer and probably merits renaming to `TOOLKIT_PROFILE`).

#### CORS settings

A `Webapp` instance takes care of comparing the `origin` (from request headers) against the regular expression from the application's config (`CORS_ALLOWED_ORIGINS`) and setting the response headers accordingly.

#### Logging database changes

Auditing can be enabled using the `DB_CHANGELOG_ENABLED` option. It is currently disabled due to excessive amounts of data it would write. You can search for the option name to check how it has been implemented (it could use some refinements to only audit a configurable subset of tables).

#### Those other settings...

Some are commented on directly in the [`app_profiles.py`](./app_profiles.py), others might require some string searching to figure out.

#### Formatting in VSCode

For automatic formatting and ordering of import statements on save in VSCode we use `Black Formatter` and `isort` extensions.
After install and enable them add in your settings.json the following:

```
"black-formatter.showNotifications": "always",
"[python]": {
  "editor.defaultFormatter": "ms-python.black-formatter",
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.organizeImports": true
  },
  "editor.formatOnType": true
},
"isort.args": ["--profile", "black"],
"black-formatter.interpreter": ["/home/asamanta/miniconda3/bin/python3"],
```

_Don't forget to change the black-formatter.interpreter path accordignly_

### Authenticating users

Grep hint: `@login_manager.request_loader`.

The username will be looked for in the headers (the SSO puts it there and thwarts spoofing efforts).
For local development, a browser extension like [Simple Modify Headers](https://addons.mozilla.org/en-US/firefox/addon/simple-modify-header/) might be handy.
CLI utilities can be fed a header's value directly, like:

- `curl -L localhost:5000/restplus/icms_public/user_info -H 'User: tbawej'`
- or (using `httpie`): `http localhost:5000/restplus/icms_public/user_info User:tbawej`

The name of the header to take into account can be configured using the `AUTH_HEADER_NAME` option.

### Identity provider

Exposes two endpoints:

1. `/idProvider/demand/<path:source_url>`
2. `/idProvider/redeem/<string:token>`

The latter, as of Nov 2020, needs to be exempt from the SSO so that the client application can retrieve the credentials set aside when opening the first, SSO-protected endpoint. Certain aspects can be controlled through application settings:

```python
IDENTITY_PROVIDER_TTL = 120
IDENTITY_PROVIDER_ENFORCE_HTTPS = False
IDENTITY_PROVIDER_TOKEN_DELIVERY_ENDPOINT = '/iCMS/analysisadmin/loginsso'
```

### Vote delegations

References:

- [merge request for the REST-based delegations](https://gitlab.cern.ch/cms-icmsweb/icms-tools/-/merge_requests/919)

## API endpoints and alike

Central to the project's purpose, the API's development has undergone several shifts in direction, leaving behind a confusingly heterogeneous landscape of varying naming conventions, inconsistent use of HTTP methods and a few alternative approaches to structuring the endpoints' logic.

The most recent approach involves:

- naming endpoints using plural nouns
- exposing the following operations:
- `POST` on a collection to create an instance (id gets assigned by the backend, return object representation is returned right-away)
- `GET` on a collection to list instances
- `PUT` on a `collection/id` to update an instance
- `DELETE` on a `collection/id` to delete an instance
- `GET` on a `collection/id` to fetch an instance

The operations outlined above fall slightly short of the RESTful standards, most notably in the following aspects:

- instance creation should be idempotent and performed using `PUT` with and id assigned by the client, e.g. `uuid`
- returning onbject's representation rather than id/location is debatable as well
- `POST` on a collection should be used to create a collection, however rarely that would actually happen
- response codes could use some refinement, e.g. `204` for successful deletions, `201` for creations

While the shortcomings summarized below might foreshadow another impending shift, the current coding practices include the following:

- creating new instances of [`IcmsApiNamespace`](blueprints/api_restplus/icms_api_namespace.py) to create new API namespaces, e.g. [`permissions_endpoints.py`](blueprints/api_restplus/endpoints/permissions_endpoints.py)
- subclassing [`AbstractCRUDUnit`](blueprints/api_restplus/api_units/basics/crud.py) to create new endpoints, e.g. [`booking_request_units.py`](blueprints/api_restplus/api_units/booking_units/booking_request_units.py)
- registering the new endpoints using `IcmsApiNamespace`'s `register_crud_handler_class` method.

### Working with `AbstractCRUDUnit`'s subclasses

The [`AbstractCRUDUnit`](blueprints/api_restplus/api_units/basics/crud.py) class leverages the template design pattern to defer some implementation steps to subclasses. Scrutinizing the existing subclasses should provide a decent picture of what's what (to find those classes in VS Code the following regex search can be used: `class [\w]+\(AbstractCRUDUnit`).
In the most basic of cases (e.g. [`RoomApiCall`](blueprints/api_restplus/api_units/booking_units/room_units.py)) only two methods are required to generate the corresponding CRUD functionality:

- `get_model_fields_list`: returning a list of `ModelFieldDefinition` instances (most notably responsible for mapping API model fields to DB columns)
- `get_model_name`: returning a string name of the corresponding API model

`AbstractCRUDUnit`'s remaining methods can be overridden to refine certain aspects of the new endpoint's behavior. In order to prevent certain methods from being generated, specific mix-ins can be extended by the class (e.g. `Listless`, `Getless`, `Putless` etc. - as they are taken into account by the [`IcmsApiNamespace`](blueprints/api_restplus/icms_api_namespace.py) class when registering handler classes).

`AbstractCRUDUnit` class might require future refinements should a need arise to control new aspects of its behavior.

### Defining API models

As hinted above, the binding between an API model's field and a database column is expressed through the [`ModelFieldDefinition`](blueprints/api_restplus/api_units/basics/builders.py)'s `name` property, like so:

```python
FIELD_ID = ModelFieldDefinition.builder(type=Integer).column(
PaperAuthor.authorID).name('id').readonly(True).build()
```

### Customizing query string parser for `AbstractCRUDUnit`-based units

Using [`BookingSlotApiUnit`](./blueprints/api_restplus/api_units/booking_units/booking_slot_units.py) as an example:

1. It specifies the model fields that should be included in the parser (`is_model_field_eligible_for_parser`).
2. It modifies the generated parser by marking the `weekId` as a required parameter (`get_args`).

## Handling permissions

The present permissions management solution aimed to address the issues of its predecessors by:

- decoupling the business logic from permissions checks
- involving relationships between concerned entities in the process of making decisions, e.g. _is X a supervisor of Y?_
- exposing an interface to modify access rights without changing the code

The production instance's interface can be found [here](https://icms.cern.ch/tools/admin/permissions) and it also explains how to configure permissions.
From the implementation's perspective, it might be worth looking at the [`check_permissions`](blueprints/api_restplus/decorators.py) decorator (applied uniformly across all API endpoints) to see how [`PermissionsManager`](webapp/permissions/permissions_manager.py) is involved to make the decision and how all the necessary components are loaded from the database:

- [`RestrictedResourcesManager`](webapp/permissions/restricted_resources_manager.py) looks the resource up or, that failing, instantiates a resource with `None` key (which is important for applying application's defaults)
- [`ToolkitAccessRulesProvider`](webapp/permissions/access_rules.py) fetches the rules for the given resource or, that failing, retrieves the application's defaults.
- Finally `AbstractPermissionsManager`'s `can_perform` method is invoked to make the call.

In line with the values defined in [`app_profiles.py`](./app_profiles.py) the defaults allow any non-writing API endpoint to be accessed by any user while writing and deleting is exclusively available to `ICMS_root` flag holders. All the configured permissions are effectively exceptions from those defaults.

### Generating resource links

As permissions can be evaluated without an actual request being made, access rights for various endpoints can be determined ahead of any attempts to access them. This in turn allows for a richer communication with API clients that can be informed about the actions performable on given resource, e.g.:

```json
{
  "id": 5,
  "name": "root",
  "rules": {
    "ICMS_root": "u.flags"
  },
  "_links": {
    "update": {
      "url": "/restplus/admin/permissions/classes/5",
      "method": "PUT"
    },
    "delete": {
      "url": "/restplus/admin/permissions/classes/5",
      "method": "DELETE"
    }
  }
}
```

Adding links to response happens within the (not exactly glorious) [`_add_hypermedia_to_row_dict` method of `AbstractCRUDUnit`](blueprints/api_restplus/api_units/basics/crud.py). The latter in turn relies on the possible hyperlink relationships being pre-registered with the API unit class (through `register_hyperlink_wrapper` method).

- In case of `AbstractCRUDUnit` subclasses this is done by the `IcmsApiNamespace` when registering the CRUD handler class.
- Prior to that some custom code would be added to establish the necessary link (i.e. call `register_hyperlink_wrapper`).

#### Why is it so convoluted?

Generating links requires a subclass of `restplus`' (`restx`') `Resource` being registered with the API instance, usually through an API namespace object, like in the following snippet:

```python
@ns.route('/assignments')
class AssignmentsCollection(Resource):
	@api.expect(AssignmentsCalls.get_args())
	@api.marshal_list_with(AssignmentsCalls.get_model())
	def get(self):
		return AssignmentsCalls.get_many()
```

Such is the standard approach and it's still widespread in `icms-tools`. Initially the API unit classes would only provide the necessary functionality that would still require the boilerplate so as to establish the necessary wiring. `IcmsApiNamespace` was added to eliminate the boilerplate and under the hood it creates the `Resource` subclasses that delegate the work to `AbstractCRUDUnit` subclasses.

Perhaps having `AbstractCRUDUnit` subclass `Resource` could have led to a cleaner implementation.
