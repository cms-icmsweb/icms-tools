from typing import Optional
from webapp.webapp import Webapp
from blueprints.voting.voting_lists.info_providers import VotingInfoProvider
from blueprints.voting.voting_lists.abstract_voting_list_model import VotingParticipant
from blueprints.voting.voting_lists.persisted_voting_list_model import PersistedVotingListModel
from blueprints.voting.voting_lists.abstract_voting_list_model import AbstractVotingListModel
from blueprints.voting.voting_lists.proportional_election_voting_model import ProportionalElectionVotingModel
from blueprints.voting.voting_lists.inst_voting_list_model import InstVotingListModel
from icms_orm.toolkit import Voting, VotingListEntry
from datetime import date
import logging
import abc

log: logging.Logger = logging.getLogger(__name__)


class VotingListModel(abc.ABC):
    """
    The class is named like this because it's already used further down the road. Now it will act more like a factory
    that tries to determine which kind of a VoLiMo to instantiate and return
    """
    @classmethod
    def get_instance(cls, voting_date: Optional[date] = None, is_election: bool = False, voting_code: Optional[str] = None) -> AbstractVotingListModel:
        """
        Method that determines which VoLiMo to instantiate and return
        :param voting_date:
        :param is_election:
        :param voting_code:
        :return:
        """
        ssn = Voting.session()
        voting: Voting = ssn.query(Voting).filter(Voting.code == voting_code).one_or_none()
        has_persisted = ssn.query(VotingListEntry).filter(VotingListEntry.code == voting_code).count() > 0
        if has_persisted:
            return PersistedVotingListModel(voting_code=voting_code)
        else:
            voting_date = voting_date or voting.start_time.date()
            is_election = is_election or (voting.type or '').upper() == Voting.Type.ELECTION
            if is_election and voting_code in Webapp.current_app().config['ELECTIONS_WITH_PROPORTIONAL_MODEL']:
                return ProportionalElectionVotingModel(voting_date=voting_date, voting_code=voting_code, is_election=is_election, voting=voting)
        return InstVotingListModel(voting_date=voting_date, voting_code=voting_code, is_election=is_election, voting=voting)
