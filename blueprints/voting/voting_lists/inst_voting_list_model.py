import logging
from datetime import date, datetime, timedelta
from typing import List, Set, Tuple

import sqlalchemy as sa
from icms_orm.cmspeople import Institute as OldInst
from icms_orm.cmspeople import InstStatusValues, MoData
from icms_orm.cmspeople import Person as OldPerson
from icms_orm.common import Country
from icms_orm.common import Institute as NewInst
from icms_orm.common import Person as NewPerson
from icms_orm.toolkit import VoteDelegation, Voting, VotingMerger, VotingMergerMember
from icmsutils.domain_object_model import (
    Institute,
    Person,
    VotingCluster,
    VotingCountry,
    VotingEntity,
    VotingInstitute,
    VotingParticipant,
)

from blueprints.voting.voting_lists import AbstractVotingListModel
from blueprints.voting.voting_lists.info_providers import VotingInfoProvider


class VotingMoPool(object):
    """
    Can use a vast optimisation when it comes to counting things, finding by country etc.
    """

    def __init__(self, year, one_year_as_of):
        assert isinstance(year, int)
        assert isinstance(one_year_as_of, date)
        ssn = MoData.session()
        self._pool = {}
        (
            mo_col,
            phd_mo_col,
            free_mo_col,
            phd_ic_col,
            phd_fa_col,
        ) = MoData.get_columns_for_year(year)
        rows = (
            ssn.query(phd_ic_col, sa.func.count(MoData.cmsId), OldInst)
            .join(OldInst, phd_ic_col == OldInst.code)
            .group_by(phd_ic_col)
            .filter(phd_mo_col.ilike("yes"))
            .filter(OldInst.cmsStatus == InstStatusValues.YES)
            .filter(
                sa.func.coalesce(OldInst.dateCmsIn, OldInst.dateCreate)
                < one_year_as_of - timedelta(365)
            )
            .all()
        )
        for ic, count, db_inst in rows:
            _inst = Institute.from_data_object(db_inst)
            self.insert(_inst, count)

    def get_insts(self, country=None) -> List[Institute]:
        return [
            _i for _i in self._pool.keys() if country is None or _i.country == country
        ]

    def get_countries(self) -> Set[str]:
        return {_i.country for _i in self._pool.keys()}

    def insert(self, inst: Institute, count: int):
        assert isinstance(inst, Institute)
        self._pool[inst] = count

    def get_value(self, inst: Institute) -> int:
        return self._pool.get(inst, 0)

    def get_total(self):
        return sum([v for v in self._pool.values()])

    def withdraw_inst(self, inst: Institute):
        assert isinstance(inst, Institute)
        return self._pool.pop(inst) if inst in self._pool else None

    def withdraw_country(self, country: str):
        _total = 0
        for _inst in self.get_insts():
            assert isinstance(_inst, Institute)
            if _inst.country == country:
                _total += self.withdraw_inst(_inst)
        return _total

    def is_empty(self):
        return len(self._pool) == 0


class DelegationsInfoProvider(object):
    def __init__(self, voting_info: VotingInfoProvider):
        self._voting_info = voting_info
        self._data = {}
        self._init_data()

    def _prepare_db_query(self):
        ssn = Voting.session()
        query = (
            ssn.query(VoteDelegation, NewPerson)
            .join(NewPerson, VoteDelegation.cms_id_to == NewPerson.cms_id)
            .filter(VoteDelegation.voting_code == self._voting_info.code)
        )
        if not self._voting_info.is_election:
            query2 = (
                ssn.query(VoteDelegation, NewPerson)
                .join(NewPerson, VoteDelegation.cms_id_to == NewPerson.cms_id)
                .filter(
                    sa.and_(
                        VoteDelegation.is_long_term == True,
                        VoteDelegation.time_created
                        < datetime.combine(
                            self._voting_info.list_closing_date, datetime.min.time()
                        ),
                    )
                )
            )
            query = query.union(query, query2)
        query = query.filter(VoteDelegation.status.ilike("%active%"))
        return query

    def _init_data(self):
        data = self._prepare_db_query().all()
        logging.debug(
            "Found {0} active delegations for {1}".format(
                len(data), self._voting_info.code
            )
        )
        for _delegation, _delegate in data:
            assert isinstance(_delegation, VoteDelegation)
            assert isinstance(_delegate, NewPerson)
            self._data[
                (_delegation.cms_id_from, _delegation.specific_inst_code or None)
            ] = Person.from_data_object(_delegate)

    def get_delegate_for(self, person: Person, inst: Institute = None):
        assert isinstance(person, Person)
        assert inst is None or isinstance(inst, Institute)
        # inst-specific delegation needs to take precedence over general delegation from person A to B
        return (
            self._data.get((person.cms_id, inst.code if inst else None))
            or self._data.get((person.cms_id, None))
            or None
        )


class IgnoreDelegationsInfoProvider:
    """Replaces DelegationsInfoProvider when delegations have to be ignored."""

    def __init__(self, *_, **__):
        pass

    def get_delegate_for(self, *_, **__):
        return None


class ClustersInfoProvider(object):
    def __init__(self, valid_as_of):
        ssn = Voting.session()

        q = (
            ssn.query(VotingMerger, VotingMergerMember, NewPerson, NewInst, Country)
            .join(
                VotingMergerMember,
                VotingMergerMember.merger_id == VotingMerger.id,
                isouter=True,
            )
            .join(NewPerson, VotingMerger.representative_cms_id == NewPerson.cms_id)
            .join(NewInst, NewInst.code == VotingMergerMember.inst_code, isouter=True)
            .join(Country, VotingMerger.country == Country.name, isouter=True)
            .filter(
                sa.and_(
                    sa.or_(
                        VotingMerger.valid_from == None,
                        VotingMerger.valid_from <= valid_as_of,
                    ),
                    sa.or_(
                        VotingMerger.valid_till == None,
                        VotingMerger.valid_till >= valid_as_of,
                    ),
                )
            )
            .order_by(VotingMergerMember.merger_id)
        )
        self._leaders = {}
        self._members = {}
        self._countries = {}
        for merger, member, person, inst, country in q.all():
            assert member is None or isinstance(member, VotingMergerMember)
            assert inst is None or isinstance(inst, NewInst)
            assert isinstance(merger, VotingMerger)
            assert isinstance(person, NewPerson)
            assert country is None or isinstance(country, Country)
            if country is None:
                logging.warning(
                    "Country name {0} not found in countries table. Issues are looming!".format(
                        merger.country
                    )
                )
                self._countries[merger.id] = merger.country
            else:
                self._countries[merger.id] = country.name
            logging.debug(
                "Storing merger info from {0}".format(self._countries[merger.id])
            )
            self._members[merger.id] = self._members.get(merger.id, [])
            if inst is not None:
                self._members[merger.id].append(
                    Institute(
                        code=inst.code,
                        name=inst.code,
                        country=self._countries[merger.id],
                    )
                )
            self._leaders[merger.id] = Person.from_data_object(person)

    def get_clusters_with_leaders(
        self, country=None
    ) -> List[Tuple[VotingCluster, Person]]:
        data = []
        for id in self._members.keys():
            members = self._members.get(id)
            leader = self._leaders.get(id)
            cluster = VotingCluster(*members)
            assert isinstance(cluster, VotingCluster)
            assert isinstance(leader, Person)
            if country is None or cluster.country == country:
                data.append((cluster, leader))
        return data

    def get_empty_country_clusters(
        self, country=None
    ) -> List[Tuple[VotingCountry, Person]]:
        data = []
        for id, members in self._members.items():
            if len(members) > 0:
                continue
            _leader = self._leaders.get(id)
            _country = self._countries.get(id)
            assert isinstance(_leader, Person)
            if country is None or country == _country:
                data.append((VotingCountry(country=_country), _leader))
        return data


class CbiInfoProvider(object):
    def __init__(self):
        self._data = {}
        ssn = OldPerson.session()
        q = (
            ssn.query(OldInst.code, OldPerson)
            .join(OldPerson, OldInst.cbiCmsId == OldPerson.cmsId)
            .filter(OldInst.cmsStatus == InstStatusValues.YES)
        )
        for db_inst_code, db_person in q.all():
            self._data[db_inst_code] = Person.from_data_object(db_person)

    def get_cbi_for(self, inst: Institute):
        assert isinstance(inst, Institute)
        return self._data.get(inst.code)


class InstVotingListModel(AbstractVotingListModel):
    def __init__(
        self,
        voting_date=None,
        is_election=False,
        voting_code=None,
        voting=None,
        votes_per_entity=1,
    ):
        super().__init__()
        self._voting_info = VotingInfoProvider(
            voting, voting_code, voting_date, is_election
        )
        self._votes_per_entity = votes_per_entity
        self._current_pool = VotingMoPool(
            self._voting_info.mo_year,
            one_year_as_of=voting.list_closing_date or voting.start_time.date(),
        )
        self._previous_pool = VotingMoPool(
            self._voting_info.mo_year - 1,
            one_year_as_of=voting.list_closing_date or voting.start_time.date(),
        )
        self._cbi_info = CbiInfoProvider()
        self._delegations_info = (
            IgnoreDelegationsInfoProvider()
            if "CB-CHAIR" in voting_code
            else DelegationsInfoProvider(self._voting_info)
        )
        self._clusters_info = ClustersInfoProvider(
            valid_as_of=self._voting_info.voting_date
        )
        self._prepare_model()

    def _prepare_model(self):
        logging.debug("Current pool's size: {0}".format(self._current_pool.get_total()))
        self._pick_voting_institutes()
        self._pick_voting_clusters()
        self._pick_voting_countries()

    def _pick_voting_institutes(self):
        # first gathering all the insts, also those having 0 PHDs now but 3 in the past (possible?)
        _insts_set = set(self._current_pool.get_insts())
        for inst in self._previous_pool.get_insts():
            _insts_set.add(inst)

        for inst in _insts_set:
            assert isinstance(inst, Institute)
            if (
                self._current_pool.get_value(inst) > 2
                or self._previous_pool.get_value(inst) > 2
            ):
                nominal_voter = self._cbi_info.get_cbi_for(inst)
                delegate = self._delegations_info.get_delegate_for(
                    person=nominal_voter, inst=inst
                )
                self._add_voter(
                    person=nominal_voter,
                    entity=VotingInstitute(institute=inst),
                    votes=self._votes_per_entity,
                    delegate=delegate,
                )
                self._current_pool.withdraw_inst(inst)
                logging.debug(
                    "{0} pulled from the pool, {1} total remaining".format(
                        inst.code, self._current_pool.get_total()
                    )
                )

    def _pick_voting_clusters(self):
        for cluster, leader in self._clusters_info.get_clusters_with_leaders():
            assert isinstance(cluster, VotingCluster)
            assert isinstance(leader, Person)
            phds_total = sum(
                [self._current_pool.get_value(inst) for inst in cluster.institutes]
            )
            logging.debug("Cluster {0} has {1} PHDs!".format(cluster, phds_total))
            if phds_total >= 3:
                delegate = self._delegations_info.get_delegate_for(
                    person=leader, inst=None
                )
                self._add_voter(
                    leader,
                    cluster,
                    self._votes_per_entity,
                    remarks=None,
                    delegate=delegate,
                )
                for inst in cluster.institutes:
                    phds_total -= self._current_pool.withdraw_inst(inst) or 0
                assert (
                    phds_total == 0
                ), "The sum of PHDs of insts withdrawn from the pool should cancel this out"

    def _pick_voting_countries(self):
        represented_countries = {
            p.voting_entity.country
            for p in self.participants
            if isinstance(p, VotingParticipant)
        }
        logging.debug(
            "Countries represented already: {0}".format(
                ", ".join([c for c in represented_countries])
            )
        )
        countries_to_vote = self._current_pool.get_countries().difference(
            represented_countries
        )
        for country in countries_to_vote:
            logging.debug("Should consider a country {0}".format(country))
            insts = self._current_pool.get_insts(country=country)
            logging.debug("Found {0} institutes from {1}".format(len(insts), country))
            top_phds, top_inst = (0, None)
            for _inst in insts:
                assert isinstance(_inst, Institute)
                phds = self._current_pool.withdraw_inst(_inst)
                if phds > top_phds:
                    top_inst, top_phds = (_inst, phds)
            logging.debug(
                "Top contributor for {0} is {1} with {2} PHDs".format(
                    country, top_inst.code, top_phds
                )
            )
            leader, entity = None, None
            # empty cluster with an explicit leader could be used as an override to designate the voter
            empty_clusters = self._clusters_info.get_empty_country_clusters(
                country=country
            )
            if len(empty_clusters) == 1:
                entity, leader = empty_clusters[0]
                assert isinstance(entity, VotingCountry)
                assert isinstance(leader, Person)
                logging.debug(
                    "There is an empty cluster for {0}, leader is {1}".format(
                        country, leader.cms_id
                    )
                )
            leader = leader or self._cbi_info.get_cbi_for(top_inst)
            entity = entity or VotingCountry(country=country)
            delegate = self._delegations_info.get_delegate_for(person=leader, inst=None)
            self._add_voter(
                person=leader,
                entity=entity,
                votes=self._votes_per_entity,
                remarks=None,
                delegate=delegate,
            )

    @property
    def voting_date(self):
        return self._voting_info.voting_date

    @property
    def list_closing_date(self):
        return self._voting_info.list_closing_date
