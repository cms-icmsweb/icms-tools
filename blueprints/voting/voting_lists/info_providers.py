from icms_orm.toolkit import Voting
from datetime import date, datetime


class VotingInfoProvider(object):
    def __init__(self, voting=None, code=None, voting_date=None, is_election=None):
        self._voting = voting or (Voting.session().query(Voting).filter(Voting.code == code).one() if code else None)
        self._voting = self._voting or Voting.from_ia_dict({
            Voting.code: code,
            Voting.list_closing_date: voting_date,
            Voting.start_time: voting_date,
            Voting.type: is_election and Voting.Type.ELECTION or Voting.Type.VOTE,
        })

    @property
    def voting_date(self) -> date:
        assert isinstance(self._voting, Voting)
        return self._voting.start_time.date()

    @property
    def code(self) -> str:
        assert isinstance(self._voting, Voting)
        return self._voting.code

    @property
    def mo_year(self) -> int:
        assert isinstance(self._voting, Voting)
        return self._voting.applicable_mo_year or self._voting.start_time.year

    @property
    def is_election(self) -> bool:
        assert isinstance(self._voting, Voting)
        return self._voting.type == Voting.Type.ELECTION

    @property
    def list_closing_date(self) -> date:
        assert isinstance(self._voting, Voting)
        return self._voting.list_closing_date or self._voting.start_time.date()

