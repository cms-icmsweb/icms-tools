from blueprints.voting.voting_lists.inst_voting_list_model import InstVotingListModel
from icms_orm.cmspeople import MoData, Person as DbPerson, Institute as DbInstitute
from icmsutils.domain_object_model import Person, VotingInstitute, Institute


class ProportionalElectionVotingModel(InstVotingListModel):
    """
    This is the model invented for the first online election. It awards each MO holder with 1 vote and Team Leaders with 4
    """

    def __init__(self, voting_date=None, is_election=False, voting_code=None, voting=None):
        super().__init__(voting_date, is_election, voting_code, voting, votes_per_entity=4)
        self._add_mo_holders_to_list()

    def _add_mo_holders_to_list(self):
        year = self.voting_date.year
        the_column = MoData.get_phd_mo_column_for_year(year)
        q = MoData.session().query(DbPerson, DbInstitute).join(MoData, DbPerson.cmsId == MoData.cmsId). \
            join(DbInstitute, DbPerson.instCode == DbInstitute.code). \
            filter(the_column.ilike('yes'))
        for db_person, db_inst in q.all():
            assert isinstance(db_person, DbPerson)
            assert isinstance(db_inst, DbInstitute)
            if not db_person.cmsId in self.get_entitled_cms_ids():
                self._add_voter(person=Person.from_data_object(db_person), entity=VotingInstitute(institute=Institute.from_data_object(db_inst)), votes=1, remarks=None, delegate=None)
