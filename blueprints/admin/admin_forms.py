from flask_wtf import Form
from wtforms.fields import SubmitField, FloatField
from wtforms.fields.html5 import IntegerField
from datetime import date


class AppCheckForm(Form):
    missing_due_tolerance = FloatField(label='Missing due tolerance', default=0.05)
    year = IntegerField(label='Year', default=date.today().year)
    submit = SubmitField('Submit')
