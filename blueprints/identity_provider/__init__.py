from blueprints.identity_provider.identity_provider_blueprint import IdentityProviderBlueprint

blueprint = IdentityProviderBlueprint.get_instance()

__all__ = ['blueprint']
