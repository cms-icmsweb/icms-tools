import uuid
import flask
import flask_login
from icms_orm.toolkit import ProvidedIdentity
import logging
from datetime import datetime
import re


class IdentityProvider():

    deslashing_pattern = re.compile(r'(?P<scheme>http(s?)):/(?P<domain>[^/]+)')

    @staticmethod
    def config_id_ttl() -> int:
        return flask.current_app.config.get('IDENTITY_PROVIDER_TTL', 60)

    @staticmethod
    def config_enforce_https() -> bool:
        return flask.current_app.config.\
            get('IDENTITY_PROVIDER_ENFORCE_HTTPS') is True

    @staticmethod
    def config_token_delivery_endpoint() -> str:
        return str(flask.current_app.config.
                   get('IDENTITY_PROVIDER_TOKEN_DELIVERY_ENDPOINT'))

    @staticmethod
    def preserve_credentials(requested_url: str) -> str:
        hr_id = flask_login.current_user.person.hrId
        username = flask_login.current_user.username
        token = str(uuid.uuid1())

        ssn = ProvidedIdentity.session()
        ssn.add(ProvidedIdentity.from_ia_dict({
            ProvidedIdentity.hr_id: hr_id,
            ProvidedIdentity.username: username,
            ProvidedIdentity.requested_url: requested_url,
            ProvidedIdentity.token: token,
            ProvidedIdentity.timestamp: datetime.utcnow()
        }))
        ssn.commit()

        return token

    @staticmethod
    def retrieve_credentials(token: str):
        ssn = ProvidedIdentity.session()
        row: ProvidedIdentity = ssn.query(ProvidedIdentity).filter(
            ProvidedIdentity.token == token).one_or_none()
        if row is None:
            flask.abort(404)
        ssn.delete(row)
        ssn.commit()
        if (datetime.utcnow() - row.timestamp).seconds > 60:
            logging.warn(
                f'Stale credentials: {(datetime.utcnow() - row.timestamp).seconds} seconds')
            flask.abort(410)
        return (row.hr_id, row.username, row.requested_url)

    @staticmethod
    def generate_token_delivery_url(requested_url: str):
        """
        Not sure about the name but it's the initial receiving endpoint of the client's application.
        The /iCMS/analysisadmin/loginsso should be configurable and stored upon the initial request.
        """
        prefix: str = requested_url[:requested_url.index('/iCMS')]
        if (IdentityProvider.config_enforce_https()):
            prefix = prefix.replace('http://', 'https://')
        delivery_endpoint = IdentityProvider.config_token_delivery_endpoint()
        return f'{prefix}{delivery_endpoint}'

    @staticmethod
    def demand_token(requested_url: str):
        if flask_login.current_user.is_authenticated is not True:
            flask.abort(403)
        requested_url = IdentityProvider.fix_url(requested_url).replace(':80/', '/')
        logging.info(f'Demanding a token for URL: {requested_url}')
        token = IdentityProvider.preserve_credentials(requested_url)
        delivery_url = IdentityProvider.generate_token_delivery_url(
            requested_url)
        return flask.redirect(f'{delivery_url}?token={token}')

    @staticmethod
    def redeem_token(token: str):
        logging.info(f'Attempting to redeem ID token {token}')
        props = ','.join(
            [str(x) for x in IdentityProvider.retrieve_credentials(token)])
        return f'<PRE>\n{props}\n</PRE>'

    @staticmethod
    def fix_url(url: str):
        """
        Motivation:
        - one of the slashes following "http(s)" gets slashed in certain setups (eg. production)
        """
        return IdentityProvider.deslashing_pattern.sub(r'\g<scheme>://\g<domain>', url)

