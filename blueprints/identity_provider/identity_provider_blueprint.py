from typing import Optional
import flask
from blueprints.identity_provider.identity_provider import IdentityProvider


class IdentityProviderBlueprint(flask.Blueprint):
    NAME = 'identity_provider'
    __instance: Optional['IdentityProviderBlueprint'] = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_url_rule('/demand/<path:requested_url>', '/demand',
                          IdentityProviderBlueprint.proxy_identity_demand)
        self.add_url_rule('/redeem/<string:token>', '/redeem/',
                          IdentityProvider.redeem_token)

    @staticmethod
    def proxy_identity_demand(requested_url: str):
        """
        Method was added to address the issue of dropped query string
        parameters without having the provider talk to the request object.
        """
        qs = flask.request.query_string.decode('utf8')
        if qs:
            return IdentityProvider.demand_token(f'{requested_url}?{qs}')
        else:
            return IdentityProvider.demand_token(requested_url)

    @classmethod
    def get_instance(cls) -> 'IdentityProviderBlueprint':
        if cls.__instance is None:
            cls.__instance = cls(cls.NAME, __name__)
        return cls.__instance



