import logging
from enum import Enum
import flask
import flask_login
from blueprints.author_lists.blueprint import al_blueprint
from pathlib import Path

log: logging.Logger = logging.getLogger(__name__)


class AuthorListFileType(Enum):
    TEX = 'authorlist.tex'
    REVTEX = 'authorlist.revtex'
    XML = 'authorlist.xml'
    # XML_2 = 'authorlist2.xml'
    # XML_N = 'authorlistN.xml'
    HTML = 'authorlist.html'
    # HTML_BODY = 'authorlist_body.html'
    TEX_C = 'public-authorlist.tex'
    XML_C = 'public-authorlist.xml'
    XML_INT = 'journal-internal-authorlist.xml'
    # PDF_C = 'authorlist.pdf'
    HTML_C = 'public-authorlist.html'


@al_blueprint.route('/getFile/<string:code>/<string:type>')
@flask_login.login_required
def route_get_al_file(code: str, type: str):
    filename = f'{code}-{type}'
    filesDir = flask.current_app.config.get('AL_FILES_DIR')
    logging.info(f'About to serve the file {filename} of type {type} from {filesDir}')
    filePath = Path(filesDir) / Path(filename)
    if not filePath.exists():
        filename = filename.replace('public-authorlist', 'authorlist').replace('journal-internal-authorlist', 'authorlistN')
        logging.info(f'... file for {type} not found updated to use old file name {filename} from {filesDir}')
        filePath = Path(filesDir) / Path(filename)
    if '.html' in type:
        with open( filePath, 'r' ) as hf:        
            return ''.join( hf.readlines() )
        # return flask.send_from_directory(filesDir, filename, 
        #                                  as_attachment=False,
        #                                  # attachment_filename=filename,
        #                                  )
    else:
        return flask.send_from_directory(filesDir, filename, 
                                         as_attachment=True,
                                         attachment_filename=filename)
