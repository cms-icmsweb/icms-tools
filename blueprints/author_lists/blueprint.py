import flask
from util import constants as const

al_blueprint = flask.Blueprint(const.BP_NAME_AUTHOR_LISTS, __name__, template_folder='templates')
