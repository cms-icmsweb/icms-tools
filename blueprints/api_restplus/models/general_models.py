from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.common import ApplicationAsset, AppNameValues
import sqlalchemy as sa


app_asset_info = api.model('Application Asset Info', {
    ApplicationAsset.name.key: fields.String(),
    ApplicationAsset.application.key : fields.String(enum=AppNameValues.values()),
    ApplicationAsset.data.key : fields.Raw(), # json, formatted
})

