from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.cmspeople import PersonHistory
from icmsutils.prehistory import PrehistoryParser


class TextHistoryField(fields.Raw):
    __schema_example__ = ['someone_10/06/2015-PeopleFlags:ICMS_secr', 'someone_10/06/2015-Projects:DAQ',
                          'someone_10/06/2015-Activity:13', ]

    def __init__(self, *args, **kwargs):
        kwargs['description'] = kwargs.get('description', 'Raw history as stored in iCMS People DB, divided into separate lines.')
        super(TextHistoryField, self).__init__(*args, **kwargs)

    def format(self, value):
        # I really don't know why this is necessary but apparently somewhere along the way our unicoded history gets
        # wrapped in a tuple sometimes (like in the prehistory patcher endpoint)
        if isinstance(value, tuple) and len(value) == 1:
            value = value[0]
        return PrehistoryParser.split_raw_history_into_lines(value)


event_fields = api.model('Pre-History Model', {
    'date': fields.DateTime(),
    'new values': fields.Raw(attribute='new_values'),
    'old values': fields.Raw(attribute='old_values')
})


patcher_info = api.model('Pre-History Patcher Info', {
    'missing events count': fields.Integer(),
    'patched history': TextHistoryField(),
    'missing events': fields.List(fields.Nested(event_fields))
})


text_history = api.model('Text History', {
    PersonHistory.history.key: TextHistoryField(required=True)
})
