from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.cmspeople import Institute, Person, MemberStatusValues, MemberActivity, Project
import sqlalchemy as sa
import datetime
import logging
from datetime import date


institute_info = api.model('Institute info - essential', {
    Institute.code.key: fields.String(required=True, description='Institute\'s code'),
    Institute.name.key: fields.String(required=True, description='Full name'),
    Institute.country.key: fields.String(required=True, description='Institute\'s country'),
    Institute.cmsStatus.key: fields.String(required=True, description='Institute\'s status')
})

institute_details = api.inherit('Institute info - exended', institute_info, {
    Institute.address.key: fields.String(required=False, description='Address')
})

person_change_model = api.model('Object describing changes to be performed on a specific Person', {
    Person.cmsId.key: fields.Integer(required=True, descripion='The CMS ID of person to be modified'),
    Person.status.key: fields.String(required=False, description='New CMS status', enum=MemberStatusValues.values()),
    MemberActivity.name.key: fields.String(required=False, description='New CMS Activity'),
    Person.isAuthorSuspended.key: fields.Boolean(required=False, description='New value of EPR work suspension flag'),
    Person.project.key: fields.String(required=False, description='New project for the person')
})