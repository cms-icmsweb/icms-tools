from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.old_notes import Note
import sqlalchemy as sa


note_info = api.model('Note Info', {
    Note.title.key: fields.String(),
    Note.cmsNoteId.key: fields.String(),
    Note.submitter.key: fields.Integer(attribute=Note.submitter.key),
    Note.submitterName.key: fields.String(),
    Note.authors.key: fields.String(),
    Note.type.key: fields.String(),
    Note.subdetector.key: fields.String(),
    Note.submitDate.key: fields.DateTime(),
    Note.status.key: fields.String()
})


note_submitter_info = api.model('Note Submitter Info', {
    'submitter_cms_id': fields.Integer(attribute=Note.submitter.key),
    Note.cmsNoteId.key: fields.String()
})