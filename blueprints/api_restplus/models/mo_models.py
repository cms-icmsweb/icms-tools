from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.common import Person, MO, MoStatusValues, FundingAgency as FA, PersonStatus, CmsActivityValues
from collections import OrderedDict


phd_list_entry = api.model('PHD List entry', OrderedDict([
    ('cms_id', fields.Integer(attribute=lambda x: x.person.get(Person.cms_id))),
    ('first_name', fields.String(attribute=lambda x: x.person.get(Person.first_name))),
    ('last_name', fields.String(attribute=lambda x: x.person.get(Person.last_name))),
    ('cms_activity', fields.String(attribute=lambda x: x.person_status.get(PersonStatus.activity), enum=CmsActivityValues.values(), title='Activity in CMS', description='Caution: returns present value!')),
    ('is_author', fields.Boolean(attribute=lambda x: x.person_status.get(PersonStatus.is_author), title='Authorship Flag', description='Caution: returns present value!')),
    ('year', fields.Integer(attribute=lambda x: x.mo.get(MO.year))),
    ('mo_status', fields.String(attribute=lambda x: x.mo.get(MO.status), enum=MoStatusValues.values())),
    ('mo_inst_code', fields.String(attribute=lambda x: x.mo.get(MO.inst_code))),
    ('mo_fa_name', fields.String(attribute=lambda x: x.fa and x.fa.get(FA.name) or None)),
    ('mo_fa_country', fields.String(attribute=lambda x: x.fa and x.fa.get(FA.country_code) or None, title='FA\'s Country Code'))
]))
