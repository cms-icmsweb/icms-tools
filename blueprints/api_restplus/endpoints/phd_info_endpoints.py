from flask_restx import Resource
from blueprints.api_restplus.api_units.phd_info_api_units import PhdInfoApiCall

from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('phd_info', description='PhD Info tool for students and thesis data collection')

@ns.route('/<int:id>')
class Phd_Info(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(PhdInfoApiCall.phd_info_model)
    def get(self, id):
        """
        Returns specific PhD info
        """
        return PhdInfoApiCall.get_phd_info_entry(id)

    @api.expect(PhdInfoApiCall.phd_info_model, validate=True)
    @api.marshal_with(PhdInfoApiCall.phd_info_model)
    def put(self, id):
        """
        Edits existing PhD info
        """
        return PhdInfoApiCall.edit_phd_info(id)

    def delete(self, id):
        """
        Deletes existing PhD info
        """
        return PhdInfoApiCall.delete_phd_info(id)


@ns.route('')
class Phd_Infos(Resource):
    skip_auto_permission_check = True

    @api.expect(PhdInfoApiCall.get_args(), validate=True)
    @api.marshal_list_with(PhdInfoApiCall.get_model())
    def get(self):
        """
        Returns information about PhD info matching given criteria
        """
        return PhdInfoApiCall.get_phd_info_list()

    @api.expect(PhdInfoApiCall.phd_info_model, validate=True)
    @api.marshal_with(PhdInfoApiCall.phd_info_model)
    def post(self):
        """
        Creates PhD info
        """
        return PhdInfoApiCall.create_phd_info()