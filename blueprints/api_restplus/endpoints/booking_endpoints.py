from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from blueprints.api_restplus.api_units import RoomApiCall
from blueprints.api_restplus.api_units import CmsWeekApiCall
from blueprints.api_restplus.api_units import BookingRequestApiCall, BookingRequestStatusApiCall
from blueprints.api_restplus.api_units import BookingSlotsApiUnit, BookingProjectApiCall


booking_ns = IcmsApiNamespace(
    'booking', description='Room booking logic', ordered=False)
booking_ns.register_crud_handler_class(api, CmsWeekApiCall, '/weeks')
booking_ns.register_crud_handler_class(api, RoomApiCall, '/rooms')
booking_ns.register_crud_handler_class(api, BookingRequestApiCall, '/requests')
booking_ns.register_crud_handler_class(
    api, BookingRequestStatusApiCall, '/requests/statuses')
booking_ns.register_crud_handler_class(api, BookingSlotsApiUnit, '/slots')
booking_ns.register_crud_handler_class(api, BookingProjectApiCall, '/projects')
