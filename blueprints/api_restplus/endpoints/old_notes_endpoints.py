from flask_restx import reqparse, inputs
from blueprints.api_restplus.api_units import OldNoteProcessesCall, OldNotesStepsCall
import os
import flask
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from blueprints.api_restplus.logic import old_notes_logic
from blueprints.api_restplus.logic.old_notes_logic import Attr
from blueprints.api_restplus.models import old_notes_models
from subprocess import Popen
import logging
import tempfile
import werkzeug
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.api_units import AutocompleteNoteIdsApiCall



ns = IcmsApiNamespace('old_notes', description='Admin endpoints for performing some tasks on old notes DB')

change_submitter_args = reqparse.RequestParser()
change_submitter_args.add_argument(Attr.cms_id, type=int, required=True)
change_submitter_args.add_argument(Attr.note_id, type=str, required=True)

change_subproject_args = reqparse.RequestParser()
change_subproject_args.add_argument(Attr.note_id, type=str, required=True)
change_subproject_args.add_argument(Attr.subproject)


filter_notes_args = reqparse.RequestParser()
filter_notes_args.add_argument(Attr.note_id, type=str, required=False)
filter_notes_args.add_argument(Attr.note_title, type=str, required=False)
filter_notes_args.add_argument(Attr.note_type, type=str, help='How do I declare this one as enum like for a model?')

get_pdf_args = reqparse.RequestParser()
get_pdf_args.add_argument(Attr.note_id, type=str, required=True, location='args')

put_pdf_args = reqparse.RequestParser()
put_pdf_args.add_argument(Attr.note_id, type=str, required=True, location='args')
put_pdf_args.add_argument(Attr.file, type=werkzeug.datastructures.FileStorage, location='files', required=True, help='PDF file')
put_pdf_args.add_argument(Attr.keep_backup, type=inputs.boolean, location='args', required=False, default=True,
                          nullable=True)


@ns.route('/note')
class NoteInfo(Resource):
    @api.expect(filter_notes_args)
    @api.marshal_list_with(old_notes_models.note_info)
    def get(self):
        """
        Returns basic information about the note
        """
        args = filter_notes_args.parse_args(flask.request)
        return old_notes_logic.get_notes_info(**args)


@ns.route('/submitter')
class NoteSubmitterInfo(Resource):
    @admin_required
    @api.expect(change_submitter_args)
    @api.marshal_with(old_notes_models.note_info)
    def put(self):
        """
        Allows changing note's submitter
        """
        args = change_submitter_args.parse_args(flask.request)
        return old_notes_logic.set_note_submitter(**args)


@ns.route('/subproject')
class NotesSubprojectInfo(Resource):
    @admin_required
    @api.expect(change_subproject_args)
    @api.marshal_with(old_notes_models.note_info)
    def put(self):
        """
        Allows changing Note's subproject
        """
        args = change_subproject_args.parse_args(flask.request)
        return old_notes_logic.set_note_subproject(**args)


@ns.route('/pdf')
class NotePdf(Resource):
    @api.expect(get_pdf_args)
    def get(self):
        """
        :param note_id:
        :return: For given CMS Note ID returns the most recent pdf file
        """
        args = get_pdf_args.parse_args(flask.request)
        return old_notes_logic.get_pdf(**args)

    @admin_required
    @api.expect(put_pdf_args)
    def post(self):
        """
        :param note_id:
        Takes a new PDF file and swaps it with whatever was there -
        """
        args = put_pdf_args.parse_args(flask.request)
        f_handle, f_destination = tempfile.mkstemp(prefix='to_stitch_', suffix='.pdf', dir='/tmp/')
        if args[Attr.file].mimetype != 'application/pdf':
            raise Exception('Uploaded file does not look like a PDF!')
        else:
            args[Attr.file].save(f_destination)
        try:
            old_notes_logic.restitch_note(cms_note_id=args.get(Attr.note_id), local_source=f_destination, keep_a_copy=args.get(Attr.keep_backup, True))
        except Exception as e:
            raise e
        finally:
            os.remove(f_destination)
        return {'status': 'OK', 'note_id': args.get(Attr.note_id), 'backed_up': args.get(Attr.keep_backup, True)}


@ns.route('/likely_actors')
class NoteProbableActors(Resource):
    @api.expect(get_pdf_args)
    def get(self):
        """
        :return:
        """
        args = get_pdf_args.parse_args(flask.request)
        return old_notes_logic.find_likely_subeditors(**args)


@ns.route('/processes')
class NoteWorkflowInfo(Resource):
    @api.expect(OldNoteProcessesCall.get_args(), validate=True)
    def get(self):
        return OldNoteProcessesCall.get_latest()


@ns.route('/process_steps')
class NoteStepInfo(Resource):
    @api.expect(OldNotesStepsCall.get_args(), validate=True)
    def get(self):
        return OldNotesStepsCall.get()


@ns.route('/step_undo')
class NoteStepReverser(Resource):
    @api.expect(OldNotesStepsCall.get_args(), validate=True)
    def post(self):
        return OldNotesStepsCall.undo_last_step()

@ns.route('/autocomplete_node_ids')
class AutocompleteNoteIds(Resource):
    @api.expect(AutocompleteNoteIdsApiCall.get_args(), validate=True)
    @api.marshal_list_with(AutocompleteNoteIdsApiCall.get_model())
    def get(self):
        return AutocompleteNoteIdsApiCall.get_cms_note_ids_suggestions()
