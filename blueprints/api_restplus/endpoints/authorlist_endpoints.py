import flask
from blueprints.api_restplus.api_units import (
    EditMemberAuthorApiCall,
    EditPendingAuthorApiCall,
    MemberAuthorApiUnit,
    PendingAuthorsApiUnit,
    ALFileApiUnit,
    ALFileSetApiCall,
    OpenDataAuthorListApiUnit,
)
from blueprints.api_restplus.api_units.authorlist_units.authorlist_api_units import (
    AuthorListUnit, AuthorListEntryUnit)
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.logic import authorlist_logic as logic
from blueprints.api_restplus.models import authorlist_models as models
from flask_restx import Resource, inputs, reqparse

ns = IcmsApiNamespace('authorlists', description='Operations on author lists')

ns.register_crud_handler_class(api, AuthorListEntryUnit, '/entries')
ns.register_crud_handler_class(api, AuthorListUnit, '/lists')
ns.register_crud_handler_class(api, ALFileApiUnit, '/files')
ns.register_crud_handler_class(api, ALFileSetApiCall, '/filesets')


al_filter_args = reqparse.RequestParser()
al_filter_args.add_argument(logic.Arg.code, type=str)
al_filter_args.add_argument(logic.Arg.al_status, choices=[
                            'open', 'closed'], help='Not a valid AL status: {error_msg}')

al_mentions_args = reqparse.RequestParser()
al_mentions_args.add_argument(logic.Arg.cms_id, type=int, required=True)


@ns.route('/al_info')
class ListInfo(Resource):
    @api.expect(al_filter_args, validate=True)
    @api.marshal_list_with(models.authorlist_info)
    def get(self):
        """
        Returns information on filtered author lists
        """
        args = al_filter_args.parse_args(flask.request)
        return logic.get_al_info(**args)


@ns.route('/al_mentions')
class ALMentions(Resource):
    @api.expect(al_mentions_args, validate=True)
    @api.marshal_list_with(models.author_mentions_info)
    def get(self):
        return logic.get_al_mentions(**al_mentions_args.parse_args(flask.request))


@ns.route('/memberAuthors')
class MemberAuthorsInfo(Resource):
    @api.marshal_list_with(MemberAuthorApiUnit.get_model())
    def get(self):
        return MemberAuthorApiUnit.get_member_authors()

    @api.expect(EditMemberAuthorApiCall.get_model(), validate=True)
    @api.marshal_list_with(EditMemberAuthorApiCall.get_model())
    def put(self):
        return EditMemberAuthorApiCall.edit_member_author()


@ns.route('/pendingAuthors')
class PendingAuthorsInfo(Resource):
    @api.marshal_list_with(PendingAuthorsApiUnit.get_model())
    def get(self):
        return PendingAuthorsApiUnit.get_pending_authors()

    @api.expect(EditPendingAuthorApiCall.get_model(), validate=True)
    @api.marshal_list_with(EditPendingAuthorApiCall.get_model())
    def put(self):
        return EditPendingAuthorApiCall.edit_pending_author()


@ns.route('/opendata')
class AwardTypes(Resource):
    @api.expect(OpenDataAuthorListApiUnit.get_args(), validate=True)
    @api.marshal_with(OpenDataAuthorListApiUnit.get_model())
    def get(self):
        """
        Returns aggregate author lists over multiple years for OpenData
        """
        return OpenDataAuthorListApiUnit.get()
