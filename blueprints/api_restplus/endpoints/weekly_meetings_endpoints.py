from flask_restx import Resource
from blueprints.api_restplus.api_units.weekly_meetings_api_units import WeeklyMeetingsApiCall

from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('weekly_meetings', description='Weekly meetings organization')

@ns.route('/<int:id>')
class Weekly_Meeting(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(WeeklyMeetingsApiCall.get_model())
    def get(self, id):
        """
        Returns information about specific Weekly Meeting
        """
        return WeeklyMeetingsApiCall.get_weekly_meeting(id)

    @api.expect(WeeklyMeetingsApiCall.get_model(), validate=True)
    @api.marshal_with(WeeklyMeetingsApiCall.get_model())
    def put(self, id):
        """
        Edits existing Weekly Meeting
        """
        return WeeklyMeetingsApiCall.edit_weekly_meeting(id)



@ns.route('')
class Weekly_Meetings(Resource):
    skip_auto_permission_check = True

    @api.expect(WeeklyMeetingsApiCall.get_args(), validate=True)
    @api.marshal_list_with(WeeklyMeetingsApiCall.get_model())
    def get(self):
        """
        Returns information about Weekly Meetings matching given criteria
        """
        return WeeklyMeetingsApiCall.get_weekly_meetings()

    @api.expect(WeeklyMeetingsApiCall.get_model(), validate=True)
    @api.marshal_with(WeeklyMeetingsApiCall.get_model())
    def post(self):
        """
        Creates Weekly Meeting
        """
        return WeeklyMeetingsApiCall.create_weekly_meeting()