from icmsutils.businesslogic import servicework as sw
from flask_restx import reqparse
import flask
from datetime import date
from blueprints.api_restplus.decorators import admin_required, engagement_office_required
from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from util.trivial import current_db_ssn
from blueprints.api_restplus.logic import servicework_logic
from blueprints.api_restplus.models import servicework_models
from icms_orm.common import ApplicationAsset, Person
import flask_login
from blueprints.api_restplus.api_units import EprInstStatsCall, EprInstTimeLinesCall, ProjectsCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


epr_ns = IcmsApiNamespace('epr', description='EPR-related commands & queries')

argn_cms_id = 'cms_id'
argn_year = 'year'
argn_inst_code = 'inst_code'
argn_correction = 'correction'
argn_remarks = 'remarks'
argn_reason = 'reason'

person_args = reqparse.RequestParser()
person_args.add_argument(argn_cms_id, type=int,
                         required=True, help='Person CMS ID')
person_args.add_argument(argn_year, type=int, required=False, help='Year')

inst_args = reqparse.RequestParser()
inst_args.add_argument(argn_inst_code, type=str,
                       required=True, help='Institute code')
inst_args.add_argument(argn_year, type=int, required=False, help='Year')


def negative_float(value):
    parsed = float(value)
    if parsed > 0:
        raise ValueError(
            'Only negative corrections of EPR due are permitted at the moment')
    return parsed


correction_args = reqparse.RequestParser()
correction_args.add_argument(
    argn_cms_id, type=int, required=True, help='Person CMS ID', location='json')
correction_args.add_argument(
    argn_year, type=int, required=True, help='Year', location='json')
correction_args.add_argument(argn_correction, type=float, required=True, location='json',
                             help='EPR due correction amount for the year, eg. -2, 1 etc.')
correction_args.add_argument(argn_inst_code, type=str, required=False, location='json',
                             help='The institute that the change should apply to (by default all within the year)')
correction_args.add_argument(
    argn_remarks, type=str, required=False, location='json')
correction_args.add_argument(
    argn_reason, type=str, required=False, location='json')


class IndividualBase(Resource):
    def _args(self):
        args = person_args.parse_args(flask.request)
        cms_id, year = [args.get(x, None) for x in [argn_cms_id, argn_year]]
        year = year or date.today().year
        return cms_id, year


@epr_ns.route('/person/worked')
class IndividualWorked(IndividualBase):
    @api.expect(person_args, validate=True)
    def get(self):
        cms_id, year = self._args()
        return sw.get_person_worked(cms_id, year, current_db_ssn())


@epr_ns.route('/person/shifts')
class IndividualShifts(IndividualBase):
    @api.expect(person_args, validate=True)
    def get(self):
        cms_id, year = self._args()
        return sw.get_person_shifts_done(cms_id, year, current_db_ssn())


@epr_ns.route('/person/contrib')
class IndividualContrib(IndividualBase):
    @api.expect(person_args, validate=True)
    def get(self):
        cms_id, year = self._args()
        return sum([f(cms_id, year, current_db_ssn()) for f in [sw.get_person_shifts_done, sw.get_person_worked]])


class InstBase(Resource):
    def _args(self):
        args = inst_args.parse_args(flask.request)
        inst_code, year = [args.get(x, None)
                           for x in [argn_inst_code, argn_year]]
        year = year or date.today().year
        return inst_code, year


@epr_ns.route('/inst/worked')
class InstWorked(InstBase):
    @api.expect(inst_args, validate=True)
    def get(self):
        inst_code, year = self._args()
        return sw.get_institute_worked(inst_code, year, current_db_ssn())


@epr_ns.route('/inst/shifts')
class InstShifts(InstBase):
    @api.expect(inst_args, validate=True)
    def get(self):
        inst_code, year = self._args()
        return sw.get_institute_shifts_done(inst_code, year, current_db_ssn())


@epr_ns.route('/inst/contrib')
class InstContrib(InstBase):
    @api.expect(inst_args, validate=True)
    def get(self):
        inst_code, year = self._args()
        return sum(
            [f(inst_code, year, current_db_ssn()) for f in [sw.get_institute_shifts_done, sw.get_institute_worked]])


@epr_ns.route('/rectify_app_due')
class AppDueRectifier(Resource):
    @engagement_office_required
    @api.expect(correction_args)
    @api.doc(responses={403: 'Not Authorized'})
    def post(self):
        cms_id, year, factor, inst = [correction_args.parse_args(flask.request).get(k, None) for k in
                                      (argn_cms_id, argn_year, argn_correction, argn_inst_code)]
        result = sw.correct_applicant_due(cms_id=cms_id, year=year, correction=factor, db_session=current_db_ssn(),
                                          inst_code=inst)
        return '%d timelines of %d corrected by %.2f applicant due' % (year, cms_id, result)


@epr_ns.route('/waive_author_due')
class AuthDueWaiver(Resource):
    @engagement_office_required
    @api.expect(correction_args)
    @api.doc(responses={403: 'User not authorized to perform action', 404: 'No match for specified parameters'})
    def post(self):
        user = flask_login.current_user
        if user.is_engagement_office_member() or user.is_admin():
            cms_id, year, factor, inst, reason, remarks = [correction_args.parse_args(flask.request).get(k, None) for k in
                                                           (argn_cms_id, argn_year, argn_correction, argn_inst_code, argn_reason, argn_remarks)]
            result = sw.correct_author_due(cms_id=cms_id, year=year, correction=factor, inst_code=inst,
                                           reason=reason, remarks=remarks, actor_cms_id=flask_login.current_user.cms_id)
            return '{year}\'s author due of {cms_id} corrected by {amount}'.format(year=year, cms_id=cms_id, amount=result)
        else:
            flask.abort(403)


@epr_ns.route('/due_corrections')
class DueCorrections(Resource):
    @engagement_office_required
    def get(self):
        _json = ApplicationAsset.retrieve(
            sw.AssetNames.EPR_AUTHOR_REDUCTIONS) or []
        _ppl = {_p.get(Person.cms_id): _p for _p in Person.session().query(Person).
                filter(Person.cms_id.in_({_r.get('author_cms_id') for _r in _json})).all()}
        for _r in _json:
            _cms_id = _r['author_cms_id']
            _r['last_name'] = _ppl.get(_cms_id).get(Person.last_name)
            _r['first_name'] = _ppl.get(_cms_id).get(Person.first_name)
        return _json


@epr_ns.route('/rectify_future_time_lines')
class FutureTimeLinesRectifier(Resource):
    @admin_required
    @api.doc(responses={403: 'Not Authorized'})
    def post(self):
        return sw.correct_future_time_lines(db_session=current_db_ssn(), write_changes=True)

    def get(self):
        return sw.correct_future_time_lines(db_session=current_db_ssn(), write_changes=False)


class TimeLineFilterArgs(object):
    _instance = None
    _args_instance = None

    def __init__(self):
        self._req_parser = reqparse.RequestParser()
        self._req_parser.add_argument('year', type=int, required=False)
        self._req_parser.add_argument('inst_code', type=str, required=False)
        self._req_parser.add_argument('cms_id', type=int, required=False)
        self._req_parser.add_argument('id', type=int, required=False)

    def parse_request(self):
        self._args_instance = self._req_parser.parse_args(flask.request)

    def __get_prop_value(self, prop_name):
        return self._args_instance.get(prop_name)

    @property
    def request_parser(self):
        return self._req_parser

    @property
    def year(self):
        return self.__get_prop_value('year')

    @property
    def id(self):
        return self.__get_prop_value('id')

    @property
    def inst_code(self):
        return self.__get_prop_value('inst_code')

    @property
    def cms_id(self):
        return self.__get_prop_value('cms_id')

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = cls()
        return cls._instance


@epr_ns.route('/timeline')
class TimeLineResource(Resource):
    @api.expect(TimeLineFilterArgs.get_instance().request_parser)
    @api.marshal_list_with(servicework_models.tlu_info)
    def get(self):
        """
        :return: list of EPR time-lines matching given search criteria
        """
        args = TimeLineFilterArgs.get_instance()
        args.parse_request()
        return servicework_logic.get_epr_time_lines(year=args.year, cms_id=args.cms_id, id=args.id, inst_code=args.inst_code)

    @api.expect(TimeLineFilterArgs.get_instance().request_parser, servicework_models.tlu_info, validate=True)
    @admin_required
    def put(self):
        """
        Updates the EPR time-line designated by the ID parameter
        :return: JSON of differences found between the record in DB and the data passed as payload
        """
        args = TimeLineFilterArgs.get_instance()
        args.parse_request()
        return servicework_logic.update_epr_time_line(year=args.year, cms_id=args.cms_id, id=args.id, inst_code=args.inst_code, params=api.payload)

    @admin_required
    @api.expect(servicework_models.tlu_info, validate=True)
    @api.marshal_with(servicework_models.tlu_info)
    def post(self):
        """
        Creates a new TimeLine
        :return: created TimeLine as JSON
        """
        return servicework_logic.create_new_epr_time_line(params=api.payload)


@epr_ns.route('/inst/stats')
class EprInstStats(Resource):
    @api.expect(EprInstStatsCall.get_args(), validate=True)
    @api.marshal_list_with(EprInstStatsCall.get_model())
    def get(self):
        return EprInstStatsCall.get_stats()


@epr_ns.route('/inst/timelines')
class EprInstTimeLines(Resource):
    @api.expect(EprInstTimeLinesCall.get_args(), validate=True)
    @api.marshal_list_with(EprInstTimeLinesCall.get_model())
    def get(self):
        return EprInstTimeLinesCall.get_timelines()


@epr_ns.route('/projects')
class ProjectsCollection(Resource):
    @api.expect(ProjectsCall.get_args(), validate=True)
    @api.marshal_list_with(ProjectsCall.get_model())
    def get(self):
        return ProjectsCall.get_many()
