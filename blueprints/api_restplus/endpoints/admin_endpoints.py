import flask

from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from blueprints.api_restplus.decorators import admin_required
from util.decorators import clearance_required
from webapp import clearance
from blueprints.api_restplus.api_units import (
    CBIFlagsCall, EmailsApiCall, EmailApiCall, AccessClassesApiCall,
    AnnouncementsApiCall, AnnouncementApiCall, AccessPermissionsApiCall,
    RestrictedResourcesApiCall, ImpersonateUserCall, ApplicantDuesCall,
    FreeAuthorshipMOCall, DateEndSignCall
)
from werkzeug.routing import Rule
from blueprints.api_restplus.decorators import ParamStoreProvider
import inspect
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace

ns = IcmsApiNamespace('admin', description='Admin related endpoints')


@ns.route('/emails')
class EmailsInfo(Resource):
    @admin_required
    @api.marshal_list_with(EmailsApiCall.get_model())
    def get(self):
        """
        Returns a list of the most recent Emails
        """
        return EmailsApiCall.get_emails()


@ns.route('/email')
class Email(Resource):
    @admin_required
    @api.expect(EmailApiCall.get_args(), validate=True)
    @api.marshal_list_with(EmailApiCall.get_model())
    def get(self):
        """
        Returns information about an Email with all the logs
        """
        return EmailApiCall.get_email()

    @admin_required
    @api.expect(EmailApiCall.get_model(), validate=True)
    @api.marshal_list_with(EmailApiCall.get_model())
    def post(self):
        """
        Creates an email
        """
        return EmailApiCall.post_email()


@ns.route('/announcements')
class AnnouncementsInfo(Resource):
    @admin_required
    @api.marshal_list_with(AnnouncementsApiCall.get_model())
    @clearance_required(clearance.SystemAdmin.can_edit)
    def get(self):
        """
        Returns all the announcements
        """
        return AnnouncementsApiCall.get_announcements()


@ns.route('/announcement')
class AnnouncementAdmit(Resource):
    @admin_required
    @api.expect(AnnouncementApiCall.get_model(), validate=True)
    @api.marshal_list_with(AnnouncementApiCall.get_model())
    @clearance_required(clearance.SystemAdmin.can_edit)
    def post(self):
        """
        Creates an announcement
        """
        return AnnouncementApiCall.post_announcement()


@ns.route('/routes')
class RoutesCollection(Resource):
    def get(self):
        """
        Returns a list of routes (endpoints) configured within the app
        """
        app: flask.Flask
        app = flask.current_app
        rule: Rule
        routes = []
        for rule in app.url_map.iter_rules():
            routes.append(dict(rule=rule.rule, args=list(
                rule.arguments), methods=list(rule.methods)))
        return routes


@ns.route('/permissions/classes')
class AccessClassesCollection(Resource):
    @api.marshal_list_with(AccessClassesApiCall.get_model())
    def get(self):
        """
        Returns a list of all access classes present in the system
        """
        return AccessClassesApiCall.find()

    @api.marshal_with(AccessClassesApiCall.get_model())
    @api.expect(AccessClassesApiCall.get_model())
    def post(self):
        return AccessClassesApiCall.post()


@ns.route('/permissions/classes/<int:id>')
class AccessClassesItem(Resource):
    @api.marshal_with(AccessClassesApiCall.get_model())
    def get(self, id: int):
        return AccessClassesApiCall.get(AccessClassesApiCall.create_filter(id))

    @api.marshal_with(AccessClassesApiCall.get_model())
    @api.expect(AccessClassesApiCall.get_model())
    def put(self, id: int):
        return AccessClassesApiCall.put(AccessClassesApiCall.create_filter(id))

    def delete(self, id: int):
        return AccessClassesApiCall.delete(AccessClassesApiCall.create_filter(id))


@ns.route('/permissions/stores')
class ParamStoresCollection(Resource):
    def get(self):
        """
        A quick reminder and an always-in-sync reference on the prefixes that can be used.
        """
        return [dict(prefix=g.prefix, source=inspect.getsource(g.getter)) for g in ParamStoreProvider.get_all()]


@ns.route('/permissions/resources')
class RestrictedResourcesCollection(Resource):
    @api.marshal_list_with(RestrictedResourcesApiCall.get_model())
    def get(self):
        """
        Lists all the restricted resources defined within the system
        """
        return RestrictedResourcesApiCall.find()

    @api.marshal_with(RestrictedResourcesApiCall.get_model())
    @api.expect(RestrictedResourcesApiCall.get_model())
    def post(self):
        """
        Creation of new restricted resource definitions.
        """
        return RestrictedResourcesApiCall.post()


@ns.route('/permissions/resources/<int:id>')
class RestrictedResourceItem(Resource):
    @api.marshal_with(RestrictedResourcesApiCall.get_model())
    def get(self, id: int):
        """
        Lists all the restricted resources defined within the system
        """
        return RestrictedResourcesApiCall.get(RestrictedResourcesApiCall.create_filter(id))

    @api.marshal_with(RestrictedResourcesApiCall.get_model())
    @api.expect(RestrictedResourcesApiCall.get_model())
    def put(self, id: int):
        """
        Edits an existing restricted resource's definition.
        """
        return RestrictedResourcesApiCall.put(RestrictedResourcesApiCall.create_filter(id))

    def delete(self, id: int):
        return RestrictedResourcesApiCall.delete(RestrictedResourcesApiCall.create_filter(id))


@ns.route('/permissions/rights')
class AccessRightsCollection(Resource):
    @api.marshal_list_with(AccessPermissionsApiCall.get_model())
    def get(self):
        """
        Lists all the access rights defined in the database
        """
        return AccessPermissionsApiCall.find()

    @api.marshal_with(AccessPermissionsApiCall.get_model())
    @api.expect(AccessPermissionsApiCall.get_model())
    def post(self):
        """
        Allows granting access rights
        """
        return AccessPermissionsApiCall.post()


@ns.route('/permissions/rights/<int:id>')
class AccessRightsItem(Resource):
    @api.marshal_with(AccessPermissionsApiCall.get_model())
    def get(self, id: int):
        return AccessPermissionsApiCall.get(AccessPermissionsApiCall.create_filter(id))

    @api.marshal_with(AccessPermissionsApiCall.get_model())
    @api.expect(AccessPermissionsApiCall.get_model())
    def put(self, id: int):
        return AccessPermissionsApiCall.put(AccessPermissionsApiCall.create_filter(id))

    def delete(self, id: int):
        return AccessPermissionsApiCall.delete(AccessPermissionsApiCall.create_filter(id))


@ns.route('/impersonate')
class ImpersonatingUser(Resource):
    @api.marshal_with(ImpersonateUserCall.get_model())
    def get(self):
        return ImpersonateUserCall.get_impersonation_info()

    @api.marshal_with(ImpersonateUserCall.get_model())
    def delete(self):
        return ImpersonateUserCall.stop_impersonating()


@ns.route('/impersonate/<int:cms_id>')
class ImpersonateUser(Resource):
    @api.marshal_with(ImpersonateUserCall.get_model())
    @admin_required
    def put(self, cms_id):
        return ImpersonateUserCall.impersonate(cms_id)


@ns.route('/cbi-flags')
class CBIFlags(Resource):
    @api.marshal_list_with(CBIFlagsCall.get_model())
    @admin_required
    def get(self):
        return CBIFlagsCall.get_cbi_flags()


@ns.route('/date-end-sign')
class DateEndSign(Resource):
    @api.marshal_list_with(DateEndSignCall.get_model())
    @admin_required
    def get(self):
        return DateEndSignCall.get_date_end_sign()


@ns.route('/applicant-dues')
class ApplicantDues(Resource):
    @api.marshal_list_with(ApplicantDuesCall.get_model())
    @admin_required
    def get(self):
        return ApplicantDuesCall.get_applicant_dues()


@ns.route('/free-authorship-mo')
class FreeMO(Resource):
    @api.marshal_list_with(FreeAuthorshipMOCall.get_model())
    @admin_required
    def get(self):
        return FreeAuthorshipMOCall.get_free_auhorship_mo()
