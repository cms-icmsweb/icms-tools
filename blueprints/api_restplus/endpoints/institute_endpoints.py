from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from flask_login.utils import login_required
from blueprints.api_restplus.api_units import OverdueGraduationsApiCall, ConfirmStudentStatusApiCall, SuspensionsApiCall, EditSuspensionStatusApiCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('inst', description='Institute related endpoints')

@ns.route('/overdue-graduations')
class LastingStudents(Resource):
    @login_required
    @api.marshal_list_with(OverdueGraduationsApiCall.get_model())
    def get(self):
        """
        Returns a list of the long lasting students
        """
        return OverdueGraduationsApiCall.get_overdue_graduations()

@ns.route('/overdue-graduations/status')
class ConfirmLastingStudentStatus(Resource):
    @login_required
    @api.expect(ConfirmStudentStatusApiCall.get_model(), validate=True)
    @api.marshal_list_with(ConfirmStudentStatusApiCall.get_model())
    def post(self):
        """
        Creates or Updated an ActivityCheck for a student.
        Used to confirm or decline the studentship status for a Person
        """
        return ConfirmStudentStatusApiCall.confirm_status()

@ns.route('/suspensions')
class SuspensionsInfo(Resource):
    @login_required
    @api.marshal_list_with(SuspensionsApiCall.get_model())
    def get(self):
        """
        Lists people of the institutes the user is a leader of along with their suspension status
        """
        return SuspensionsApiCall.get_people()

    @login_required
    @api.expect(EditSuspensionStatusApiCall.get_model(), validate=True)
    @api.marshal_list_with(EditSuspensionStatusApiCall.get_model())
    def put(self):
        """
        Lists people of the institutes the user is a leader of along with their suspension status
        """
        return EditSuspensionStatusApiCall.edit_status()
        


