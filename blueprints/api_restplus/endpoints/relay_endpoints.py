from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from blueprints.api_restplus.api_units import IcmsPiggybackApiCall
import logging
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('relay', description='Endpoints relaying the requests/responses to/from other services')


@ns.route('/piggyback/<path:path>')
class IcmsPiggybackRelayEndpoint(Resource):

    def get(self, path=None):
        logging.debug('Received a request for path %s', path)
        return IcmsPiggybackApiCall.relay_request(endpoint=path, method='GET')

    def post(self, path=None):
        return IcmsPiggybackApiCall.relay_request(endpoint=path, method='POST')

    def put(self, path=None):
        return IcmsPiggybackApiCall.relay_request(endpoint=path, method='PUT')

    def delete(self, path=None):
        return IcmsPiggybackApiCall.relay_request(endpoint=path, method='DELETE')
