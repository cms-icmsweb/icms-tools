from flask_restx import reqparse, inputs
import io
import flask
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from blueprints.api_restplus.logic import general_logic
from blueprints.api_restplus.logic.general_logic import Attr
from blueprints.api_restplus.models import general_models
from blueprints.api_restplus.api_units import MarkdownApiCall, ImageUploadApiCall, ImageApiCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('general', description='Assets of all sorts: application configs, markdown notes, images etc')

filter_app_asset_args = reqparse.RequestParser()
filter_app_asset_args.add_argument(Attr.name, type=str, required=False)
filter_app_asset_args.add_argument(Attr.application, type=str, required=False)

post_app_asset_args = reqparse.RequestParser()
post_app_asset_args.add_argument(Attr.name, type=str, required=False)
post_app_asset_args.add_argument(Attr.application, type=str, required=False)
post_app_asset_args.add_argument(Attr.data, type=str, help='JSON data')


@ns.route('/appAssets')
class AppAssetInfo(Resource):
    @api.expect(filter_app_asset_args)
    @api.marshal_list_with(general_models.app_asset_info)
    def get(self):
        """
        Returns basic information about the application assets
        """
        args = filter_app_asset_args.parse_args(flask.request)
        return general_logic.get_app_asset_info(**args)

    @admin_required
    @api.expect(general_models.app_asset_info)
    def post(self):
        """
        Update (or create if not yet existing) the content of an application asset
        """

        general_logic.post_app_asset_info( **api.payload )

        return { 'status': 'OK', 'message' : 'Application asset successfully updated (or created).' }


@ns.route('/markdown')
class MarkdownResource(Resource):
    @api.expect(MarkdownApiCall.get_args(), validate=True)
    @api.marshal_with(MarkdownApiCall.get_model())
    @api.response(404, 'Not found')
    def get(self):
        return MarkdownApiCall.get_note()

    @api.expect(MarkdownApiCall.get_model(), validate=True)
    @api.marshal_with(MarkdownApiCall.get_model())
    @admin_required
    def post(self):
        return MarkdownApiCall.post_note()


@ns.route('/markdown_list')
class MarkdownIndex(Resource):
    @api.expect(MarkdownApiCall.get_args(), validate=True)
    # @api.marshal_list_with(MarkdownApiCall.get_model())
    def get(self):
        return MarkdownApiCall.get_list()


@ns.route('/image')
class ImageResource(Resource):
    @api.expect(ImageApiCall.get_args(), validate=True)
    @api.produces(['image/png'])
    def get(self):
        """
        Returns the image matching specified criteria. Unable to identify one it throws nasty errors.
        """
        return ImageApiCall.get_image()

    @admin_required
    @api.expect(ImageUploadApiCall.get_args(), validate=True)
    @api.marshal_with(ImageUploadApiCall.get_model())
    def post(self):
        """
        Admins can upload new photos here
        """
        return ImageUploadApiCall.upload_image()


@ns.route('/images')
class ImagesIndex(Resource):
    @api.expect(ImageApiCall.get_args(), validate=True)
    def get(self):
        """
        Returns a list of images matching specified criteria (or all in the absence of criteria)
        """
        return ImageApiCall.get_images_list()