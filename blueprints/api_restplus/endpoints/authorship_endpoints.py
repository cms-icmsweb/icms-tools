from blueprints.api_restplus.api_units.authorship_units import AuthordataApiUnit
from icmsutils.businesslogic.authorship import cbi_set_author_yes
from icms_orm.cmspeople import Person
from icmsutils.businesslogic import authorship as author_logic
from flask_restx import reqparse
import flask
import logging
import sqlalchemy as sa
import re
from datetime import date
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from util.trivial import current_db_ssn
from flask_login import current_user
from flask_restx.inputs import boolean
from blueprints.api_restplus.api_units import AuthorshipStatsCall, ApplicationCheckApiCall
from blueprints.api_restplus.api_units import AuthorshipRightsCheckUnit
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace

authorship_ns = IcmsApiNamespace(
    'authorship', description='Authorship-related operations')
authorship_ns.register_crud_handler_class(
    api, AuthordataApiUnit, '/authordata')

argn_toggle = 'toggle'
argn_cms_id = 'cms_id'

form_args = reqparse.RequestParser()
form_args.add_argument(argn_cms_id, type=int, required=True,
                       help='CMS ID', location='form')
form_args.add_argument(argn_toggle, type=boolean, required=True,
                       help='New boolean value', location='form')

query_args = reqparse.RequestParser()
query_args.add_argument(argn_cms_id, type=int, required=True, help='CMS ID')


@authorship_ns.route('/author')
class Author(Resource):
    @api.expect(query_args)
    def get(self):
        cms_id = query_args.parse_args(flask.request).get(argn_cms_id)
        return current_db_ssn().query(Person.isAuthor).filter(Person.cmsId == cms_id).one()


@authorship_ns.route('/block')
class AuthorBlock(Resource):
    @api.expect(query_args)
    def get(self):
        cms_id = query_args.parse_args(flask.request).get(argn_cms_id)
        return current_db_ssn().query(Person.isAuthorBlock).filter(Person.cmsId == cms_id).one()


@authorship_ns.route('/admit')
class AuthorAdmit(Resource):
    @admin_required
    @api.expect(form_args)
    @api.doc(responses={403: 'Not Authorized'})
    def post(self):
        cms_id, toggle = [form_args.parse_args(flask.request).get(k) for k in [
            argn_cms_id, argn_toggle]]
        subject = current_db_ssn().query(Person).filter(Person.cmsId == cms_id).one()
        (toggle and author_logic.cbi_set_author_yes or author_logic.cbi_set_author_no)(
            current_db_ssn(), subject, current_user.person)
        return '%d was set as %s' % (cms_id, toggle and 'ready for authorship [AuthorBlock=0 + AuthorAllow=1]' or 'awaiting authorship [MISC_AUTHORNO + AuthorAllow=0]')


@authorship_ns.route('/author_stats')
class AuthorStats(Resource):
    @api.expect(AuthorshipStatsCall.get_args(), validate=True)
    @api.marshal_with(AuthorshipStatsCall.get_model())
    def get(self):
        return AuthorshipStatsCall.get()


@authorship_ns.route('/app_checks')
class AuthorAppChecksList(Resource):
    @api.expect(ApplicationCheckApiCall.get_args(), validate=True)
    @api.marshal_list_with(ApplicationCheckApiCall.get_model())
    def get(self):
        return ApplicationCheckApiCall.list_most_recent_checks()


@authorship_ns.route('/check_rights/<int:cms_id>')
class AuthorshipRightsCheck(Resource):
    @api.marshal_with(AuthorshipRightsCheckUnit.get_model())
    def get(self, cms_id):
        return AuthorshipRightsCheckUnit.get(AuthorshipRightsCheckUnit.make_id_filter(cms_id))

    @api.marshal_with(AuthorshipRightsCheckUnit.get_model())
    def put(self, cms_id):
        return AuthorshipRightsCheckUnit.put(AuthorshipRightsCheckUnit.make_id_filter(cms_id))
