from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from blueprints.api_restplus.api_units import PortalNewsItemApiCall, PortalNewsChannelApiCall, PortalNewsContentApiCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('portal_news', description='Endpoints for fetching news from the old iCMS database')


@ns.route('/items')
class NewsItemsInfo(Resource):
    @api.expect(PortalNewsItemApiCall.get_args())
    @api.marshal_with(PortalNewsItemApiCall.get_model())
    def get(self):
        """
        Returns information about tenures matching given criteria
        """
        return PortalNewsItemApiCall.get_news()


@ns.route('/content')
class NewsItemContnet(Resource):
    @api.expect(PortalNewsContentApiCall.get_args())
    @api.marshal_with(PortalNewsContentApiCall.get_model())
    def get(self):
        """
        Returns the content of specified news item
        """
        return PortalNewsContentApiCall.get_content()


@ns.route('/channels')
class ChannelsInfo(Resource):
    @api.marshal_with(PortalNewsChannelApiCall.get_model())
    def get(self):
        """
        Returns a list of known news channels
        """
        return PortalNewsChannelApiCall.get_channels()
