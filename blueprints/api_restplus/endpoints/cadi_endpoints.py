from blueprints.api_restplus.api_units.cadi_units import CheckLatexBuildCall
from blueprints.api_restplus import api
from flask_restx import Resource
from blueprints.api_restplus.models.cadi_api_models import (
    arc,
    arc_xl,
    arc_member,
    analysis_status_change,
)
from blueprints.api_restplus.parsers import (
    pagination_arguments,
    argn_pag_page,
    argn_pag_per_page,
    updated_analyses_args,
)
from blueprints.api_restplus.parsers import (
    cadi_arguments,
    argn_cadi_code,
    argn_cadi_awg,
    argn_cadi_year,
)
from blueprints.api_restplus.logic import cadi_logic as cadi_business
from icms_orm.cmsanalysis import CadiAnalysis, CadiHistory, CDS
from icmsutils.businesslogic import cadi as cadi_logic
from flask_restx import reqparse
import flask
import sqlalchemy as sa
from sqlalchemy import func
from datetime import date, timedelta
from icmsutils import timedate
import logging
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.api_units import (
    CadiLineExternalMetaDataUnit,
    CadiGroupNamesApiCall,
    AutocompleteAnalysisCodesApiCall,
)

log: logging.Logger = logging.getLogger(__name__)


cadi_ns = IcmsApiNamespace(
    "cadi", description="Operations on Analysis Review Committees"
)
cadi_ns.register_crud_handler_class(
    api, CadiLineExternalMetaDataUnit, "/paper_external_data"
)


@cadi_ns.route("/arcs/")
class ArcCollection(Resource):
    @api.expect(pagination_arguments, validate=True)
    @api.marshal_list_with(arc)
    def get(self):
        """Fetch info about all Analysis Review Committees in the DB"""
        pag_args = pagination_arguments.parse_args(flask.request)
        return cadi_business.get_arcs(ssn=CDS.session(), **pag_args)

    # def post(self):
    #     pass


@cadi_ns.route("/arc/<string:code>")
class ArcItem(Resource):
    @api.marshal_with(arc_xl)
    def get(self, code):
        """Fetch info on ARC for a specific analysis"""
        return cadi_business.get_arc_by_analysis_code(ssn=CDS.session(), code=code)


@cadi_ns.route("/arc_members")
class ArcMemberCollection(Resource):
    @api.expect(cadi_arguments, pagination_arguments, validate=True)
    @api.marshal_list_with(arc_member)
    def get(self):
        pag_args = pagination_arguments.parse_args(flask.request)
        cadi_args = cadi_arguments.parse_args(flask.request)
        return cadi_business.get_arc_members(
            ssn=CDS.session(),
            page=pag_args.get(argn_pag_page, 1),
            per_page=pag_args.get(argn_pag_per_page, 10),
            an_code=cadi_args.get(argn_cadi_code),
            awg=cadi_args.get(argn_cadi_awg),
            year=cadi_args.get(argn_cadi_year),
        )


xeb_report_period = "xeb_report_period"
xeb_args = reqparse.RequestParser()
xeb_args.add_argument(
    xeb_report_period,
    type=int,
    required=False,
    default=7,
    help="Reporing period (days, 7 usually and by default, sometimes 14)",
)


@cadi_ns.route("/xeb_report")
class XebReport(Resource):
    @staticmethod
    def safely_extract_journals(publi_field_value):
        value = None
        try:
            value = CadiAnalysis.get_publication_info(
                CadiAnalysis.from_ia_dict({CadiAnalysis.publi: publi_field_value})
            ).get("journals", None)
        except Exception as e:
            logging.warning(e)
        return value

    @api.expect(xeb_args)
    def get(self):
        args = xeb_args.parse_args(flask.request)
        period = args.get(xeb_report_period)
        ssn = CDS.session()
        # XEB report data - take ALL state updates that happened in the last 7 days
        xeb_data = {"period": period}
        sq_cds = (
            ssn.query(
                sa.func.max(CDS.id).label("cds_max_id"), CDS.code.label("cds_code")
            )
            .group_by(CDS.code)
            .subquery()
        )

        rows = (
            ssn.query(
                CadiAnalysis.code,
                CadiAnalysis.name,
                CadiHistory.newValue,
                func.str_to_date(CadiHistory.updateDate, "%W, %d %M %Y"),
                CadiAnalysis.status,
                CDS.title,
                func.str_to_date(CDS.stepDate, "%d/%m/%Y"),
                CadiAnalysis.publi,
            )
            .join(
                CadiHistory,
                sa.and_(
                    CadiHistory.masterId == CadiAnalysis.id,
                    CadiHistory.table == CadiAnalysis.__tablename__,
                ),
            )
            .join(
                sq_cds,
                CadiAnalysis.code.in_(
                    [sq_cds.c.cds_code, sa.func.concat("d", sq_cds.c.cds_code)]
                ),
            )
            .join(CDS, sq_cds.c.cds_max_id == CDS.id)
            .filter(
                CadiHistory.newValue.in_(
                    [
                        cadi_logic.CadiState.CWR.code,
                        cadi_logic.CadiState.ACCEPT.code,
                        cadi_logic.CadiState.SUB.code,
                    ]
                )
            )
            .filter(
                func.str_to_date(CadiHistory.updateDate, "%W, %d %M %Y")
                >= (date.today() - timedelta(days=period))
            )
            .filter(CadiHistory.field == CadiAnalysis.status.key)
            .filter(CadiHistory.table == CadiAnalysis.__tablename__)
            .order_by(sa.desc(CadiHistory.id))
            .all()
        )

        # used to pick only the most recent of entries
        last_dates = {}

        status_list = []
        for (
            code,
            title,
            status,
            day,
            current_state,
            cds_title,
            cds_step_date,
            publi,
        ) in rows:
            if code not in last_dates or last_dates[code] < day:
                # It looks like the intention was only to show the ongoing CWR records and any that reached SUB or ACCEPT
                if status != "CWR" or current_state == "CWR":
                    if status not in status_list:
                        status_list.append(status)
                    last_dates[code] = day
                    code = code[0] == "d" and code[1:] or code
                    xeb_data[status] = xeb_data.get(status, [])
                    cds_title = cds_title.replace("  ", " ")
                    item = {
                        "code": code,
                        "status": status,
                        "day": timedate.date_to_str_concise(
                            status == "CWR" and cds_step_date or day
                        ),
                        "title": cds_title,
                        "journals": XebReport.safely_extract_journals(publi),
                        "change_date": timedate.date_to_str_concise(day),
                        "cds_step_date": timedate.date_to_str_concise(cds_step_date),
                    }
                    if item not in xeb_data[status]:
                        xeb_data[status].append(item)

        # re-order the lists of dicts for each status according to the date:
        for status in status_list:
            newlist = sorted(
                xeb_data[status],
                key=lambda k: timedate.parse_date_concise(k["day"]),
                reverse=True,
            )
            xeb_data[status] = newlist

        return xeb_data


@cadi_ns.route("/check-latex-build")
class CheckLatexBuild(Resource):
    @api.marshal_with(CheckLatexBuildCall.get_model())
    @api.expect(CheckLatexBuildCall.get_model())
    def post(self):
        return CheckLatexBuildCall.check()


@cadi_ns.route("/updated-lines")
class UpdateLines(Resource):
    @api.expect(updated_analyses_args, validate=True)
    @api.marshal_list_with(analysis_status_change)
    def get(self):
        params = updated_analyses_args.parse_args(flask.request)
        return cadi_business.get_updated_analyses(**params)


@cadi_ns.route("/autocomplete_analysis_codes")
class AutocompleteAnalysisCodes(Resource):
    @api.expect(AutocompleteAnalysisCodesApiCall.get_args(), validate=True)
    @api.marshal_list_with(AutocompleteAnalysisCodesApiCall.get_model())
    def get(self):
        return AutocompleteAnalysisCodesApiCall.get_analysis_codes_suggestions()


@cadi_ns.route("/cadi_group_names")
class CadiGroupNames(Resource):
    @api.expect(CadiGroupNamesApiCall.get_args(), validate=True)
    @api.marshal_with(CadiGroupNamesApiCall.get_model())
    def get(self):
        return CadiGroupNamesApiCall.get_cadi_group_names()
