"""
Endpoints attaching to the eGroup handling
"""

from wsgiref import validate
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from blueprints.api_restplus.api_units import EgroupApiCall, EgroupMembersApiCall


ns = IcmsApiNamespace('egroups', description='Endpoints attaching to the eGroup handling')


@ns.route('/egroup')
class EgroupInfo(Resource):
    @api.expect(EgroupApiCall.get_args())
    @api.marshal_with(EgroupApiCall.get_model())
    def get(self):
        """
        Returning information about the specified eGroup
        """
        return EgroupApiCall.get_egroup_info()

    @api.expect(EgroupApiCall.get_model())
    @api.marshal_with(EgroupApiCall.get_model())
    def post(self):
        """
        Create the specified eGroup
        """
        return EgroupApiCall.post_egroup()

    @api.expect(EgroupApiCall.get_model())
    def delete(self):
        """
        Delete the specified eGroup
        """
        return EgroupApiCall.delete_egroup()

@ns.route('/members')
class EgroupMemberInfo(Resource):
    @api.marshal_with(EgroupMembersApiCall.get_model())
    @api.expect(EgroupMembersApiCall.get_args(), validate=True)
    def get(self):
        """
        Returning the member list of the specified eGroup
        """
        return EgroupMembersApiCall.get_egroup_members()

    @api.expect(EgroupMembersApiCall.get_model())
    @api.marshal_with(EgroupMembersApiCall.get_model())
    def post(self):
        """
        Updating the member list for the specified eGroup
        """
        return EgroupMembersApiCall.post_egroup_members()

    @api.expect(EgroupMembersApiCall.get_model())
    def delete(self):
        """
        Removing the specified members from the specified eGroup
        """
        return EgroupMembersApiCall.delete_mem_from_egroup()
