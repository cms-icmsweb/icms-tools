from flask_restx import Resource

from blueprints.api_restplus.api_units import (
    AwardeesApiCall,
    AwardNominationsApiCall,
    AwardProjectsApiCall,
    AwardsApiCall,
    AwardTypesApiCall,
    NomineesApiCall,
)
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace

ns = IcmsApiNamespace("awards", description="Operations on CMS Awards")


@ns.route("/types")
class AwardTypes(Resource):
    @api.expect(AwardTypesApiCall.get_args(), validate=True)
    @api.marshal_list_with(AwardTypesApiCall.get_model())
    def get(self):
        """
        Returns information about AwardTypes
        """
        return AwardTypesApiCall.list_award_types()

    @api.expect(AwardTypesApiCall.get_model(), validate=True)
    @api.marshal_with(AwardTypesApiCall.get_model())
    def post(self):
        """
        Create AwardType
        """
        return AwardTypesApiCall.create_award_type()


@ns.route("/types/<int:id>")
class AwardType(Resource):
    @api.marshal_with(AwardTypesApiCall.get_model())
    def get(self, id):
        """
        Returns information about specific AwardType
        """
        return AwardTypesApiCall.get_award_type(id)

    @api.expect(AwardTypesApiCall.get_model(), validate=True)
    @api.marshal_with(AwardTypesApiCall.get_model())
    def put(self, id):
        """
        Edits existing AwardType
        """
        return AwardTypesApiCall.update_award_type(id)


@ns.route("")
class Awards(Resource):
    skip_auto_permission_check = True

    @api.expect(AwardsApiCall.get_args(), validate=True)
    @api.marshal_with(AwardsApiCall.award_list_model)
    def get(self):
        """
        Returns information about Awards matching given criteria
        """
        return AwardsApiCall.list_awards()

    @api.expect(AwardsApiCall.get_model(), validate=True)
    @api.marshal_with(AwardsApiCall.get_model())
    def post(self):
        """
        Creates Award
        """
        return AwardsApiCall.create_award()


@ns.route("/<int:id>")
class Award(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(AwardsApiCall.get_model())
    def get(self, id):
        """
        Returns information about specific Award
        """
        return AwardsApiCall.get_award(id)

    @api.expect(AwardsApiCall.get_model(), validate=True)
    @api.marshal_with(AwardsApiCall.get_model())
    def put(self, id):
        """
        Edits existing Award
        """
        return AwardsApiCall.edit_award(id)

    def delete(self, id):
        """
        Deletes Award
        """
        return AwardsApiCall.delete_award(id)


@ns.route("/nominations")
class AwardNominations(Resource):
    skip_auto_permission_check = True

    @api.expect(AwardNominationsApiCall.get_args(), validate=True)
    @api.marshal_list_with(AwardNominationsApiCall.get_model())
    def get(self):
        """
        Returns information about AwardNominations matching given criteria
        """
        return AwardNominationsApiCall.list_nominations()

    @api.expect(AwardNominationsApiCall.get_model(), validate=True)
    @api.marshal_with(AwardNominationsApiCall.get_model())
    def post(self):
        """
        Creates AwardNomination
        """
        return AwardNominationsApiCall.create_nomination()


@ns.route("/nominations/<int:id>")
class AwardNomination(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(AwardNominationsApiCall.get_model())
    def get(self, id):
        """
        Returns information about specific AwardNomination
        """
        return AwardNominationsApiCall.get_nomination(id)

    @api.expect(AwardNominationsApiCall.get_model(), validate=True)
    @api.marshal_with(AwardNominationsApiCall.get_model())
    def put(self, id):
        """
        Edits existing AwardNomination
        """
        return AwardNominationsApiCall.edit_nomination(id)

    def delete(self, id):
        """
        Deletes AwardNomination
        """
        return AwardNominationsApiCall.delete_nomination(id)


@ns.route("/<int:award_id>/nominations_for_review")
class AwardNominationsForReview(Resource):
    """Award nominations for review"""

    skip_auto_permission_check = True

    @api.marshal_list_with(AwardNominationsApiCall.review_model)
    def get(self, award_id):
        """
        Returns enriched AwardNominations for the specified award for review purposes
        """
        return AwardNominationsApiCall.list_nominations_for_review(award_id)


@ns.route("/awardees")
class Awardees(Resource):
    skip_auto_permission_check = True

    @api.expect(AwardeesApiCall.get_args(), validate=True)
    @api.marshal_list_with(AwardeesApiCall.get_model())
    def get(self):
        """
        Returns information about Awardees matching given criteria
        """
        return AwardeesApiCall.list_awardees()

    @api.expect(AwardeesApiCall.get_model(), validate=True)
    @api.marshal_with(AwardeesApiCall.get_model())
    def post(self):
        """
        Creates Awardee
        """
        return AwardeesApiCall.create_awardee()


@ns.route("/awardees/<int:awardee_id>")
class Awardee(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(AwardeesApiCall.get_model())
    def get(self, awardee_id):
        """
        Returns information about Awardee by id
        """
        return AwardeesApiCall.get_awardee(awardee_id)

    @api.expect(AwardeesApiCall.get_model(), validate=True)
    @api.marshal_with(AwardeesApiCall.get_model())
    def put(self, awardee_id):
        """
        Edits existing Awardee
        """
        return AwardeesApiCall.edit_awardee(awardee_id)

    def delete(self, awardee_id):
        """
        Deletes (de-selects) Awardee
        """
        return AwardeesApiCall.delete_awardee(awardee_id)


@ns.route("/nominees")
class Nominees(Resource):
    @api.expect(NomineesApiCall.get_args(), validate=True)
    @api.marshal_list_with(NomineesApiCall.get_model())
    def get(self):
        """
        Returns information about Award Nominees matching given criteria
        """
        return NomineesApiCall.list_nominees()


@ns.route("/<int:id>/nominees")
class AwardNominees(Resource):
    @api.expect(NomineesApiCall.get_args(), validate=True)
    @api.marshal_list_with(NomineesApiCall.get_model())
    def get(self, id):
        """
        Returns information about Award Nominees for an Award.
        """
        return NomineesApiCall.list_nominees(award_id=id)


@ns.route("/projects")
class AwardProjects(Resource):
    skip_auto_permission_check = True

    @api.expect(AwardProjectsApiCall.get_args(), validate=True)
    @api.marshal_list_with(AwardProjectsApiCall.get_model())
    def get(self):
        """
        Returns information about Projects
        """
        return AwardProjectsApiCall.list_projects()
