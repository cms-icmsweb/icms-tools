from blueprints.api_restplus import api
from flask_restx import Resource
from flask_restx import reqparse
import flask, logging, sqlalchemy as sa, re
from sqlalchemy import func
from datetime import date, timedelta
from icmsutils import timedate
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.decorators import admin_required
from icmsutils.rectifiers import TimeLinesJanitor, ExitDateRectifier
from icms_orm.common import PersonStatus
from icmsutils.prehistory import PrehistoryPatcher, PrehistoryModel
from icms_orm.cmspeople import PersonHistory as History, Person
from blueprints.api_restplus.models import janitorial_models
from blueprints.api_restplus.logic import janitorial_logic
from blueprints.api_restplus.api_units import AccountStatusCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('janitorial', description='Cleanup methods')

departure_date = 'departure_date'
departure_date_arg = reqparse.RequestParser()
departure_date_arg.add_argument(departure_date, required=True, help='A date formatted as %s' % timedate.DATE_FMT_CONCISE)


force_patching_arg = reqparse.RequestParser()
force_patching_arg.add_argument('force', required=False, default=False, help='Enforces merging EPR events into the '
                                                                             'prehistory, even if the latter parses fine as is.')


@ns.route('/sanitize_new_time_lines/<int:cms_id>')
class PersonStatusSanitizer(Resource):
    @admin_required
    def put(self, cms_id):
        """Removes 0-length time lines, merges adjacent timelines of the same state"""
        janitor = TimeLinesJanitor(filter_dict={PersonStatus.cms_id: cms_id})
        janitor.declutter()
        janitor.defragment()
        return 'PersonStatus entries for CMS id %d have been cleaned up!' % cms_id


@ns.route('/reset_departure/<int:cms_id>')
class PersonDepartureResetter(Resource):
    @admin_required
    @api.expect(departure_date_arg)
    @api.doc(responses={403: 'Not Authorized'})
    def get(self, cms_id):
        """Cleans up all the associated stuff in all relevant databases"""
        args = departure_date_arg.parse_args(req=flask.request)
        passed_date_string = args.get(departure_date)
        day = timedate.parse_date_concise(passed_date_string)
        logging.debug('Requested CMS departure date for CMS ID %d: %s' % (cms_id, str(day)))
        rectifier = ExitDateRectifier(cms_id=cms_id, new_departure_date=day)
        rectifier.fix()
        return 'Done, the date of departure from CMS has been set as %s for CMS ID %d' % (str(day), cms_id)


@ns.route('/patch_prehistory/<int:cms_id>')
class PersonPrehistoryPatcher(Resource):
    @admin_required
    @api.doc(responses={403: 'Not Authorized'})
    @api.marshal_with(janitorial_models.patcher_info)
    def get(self, cms_id):
        """
        Tests if someone's prehistory can be patched, returns the number of missing events and, if applicable,
        the resulting patched history string.
        """
        patcher = PrehistoryPatcher.new_instance(cms_id=cms_id)
        result = {'missing events count': len(patcher.get_missing_events())}
        if patcher.get_missing_events():
            result['patched history'] = patcher.get_patched_history(actor_cms_id=0),
            result['missing events'] = patcher.get_missing_events()
        return result

    @admin_required
    @api.doc(responses={403: 'Not Authorized'})
    @api.expect(force_patching_arg)
    def post(self, cms_id):
        """
        Attempts to patch the history. Does so only if parsing the resulting history string goes smoothly.
        """
        force = force_patching_arg.parse_args(flask.request).get('force')
        person, history = Person.session().query(Person, History).join(History, Person.cmsId == History.cmsId).filter(
            Person.cmsId == cms_id).one()
        pm = PrehistoryModel(cms_id=cms_id, person=person, history=history)
        if pm.is_tainted():
            patcher = PrehistoryPatcher.new_instance(cms_id=cms_id, person=person, history=history)
            if not patcher.get_missing_events():
                return 'No missing events found through cross-checking.'
            else:
                history.set(History.history, patcher.get_patched_history())
                new_pm = PrehistoryModel(cms_id=cms_id, person=person, history=history)
                if len(new_pm.get_prehistory_time_lines()) > 0 and not new_pm.is_tainted():
                    patcher.do_patch_prehistory()
                    return 'Patcher seems to have fixed that history!'
                else:
                    return 'Patcher tried to fix that history, but to no avail.'
        else:
            return 'History parses fine as is, no need to do anything.'


@ns.route('/text_history/<int:cms_id>')
class TextHistory(Resource):
    @admin_required
    @api.marshal_with(janitorial_models.text_history)
    def get(self, cms_id):
        """
        Returns the cms_id holder's text history, split into individual lines.
        """
        return History.query.filter(History.cmsId == cms_id).one()

    @api.expect(janitorial_models.text_history)
    @api.marshal_with(janitorial_models.text_history)
    def post(self, cms_id):
        """
        Sets a new text history for designated cms_id holder. Admins-only, enforced with permissions.
        """
        return janitorial_logic.update_person_history(cms_id=cms_id, payload=api.payload)


@ns.route('/account_status')
class AccountStatusResource(Resource):
    @admin_required
    @api.expect(AccountStatusCall.get_args(), validate=True)
    @api.marshal_with(AccountStatusCall.get_model())
    def get(self):
        return AccountStatusCall.get()

    @admin_required
    @api.expect(AccountStatusCall.get_args(), validate=True)
    @api.marshal_with(AccountStatusCall.get_model())
    def post(self):
        return AccountStatusCall.post()
