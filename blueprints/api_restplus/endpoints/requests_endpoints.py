from blueprints.api_restplus.api_units import PersonStatusRequestsApiUnit
from blueprints.api_restplus.api_units import RequestsApiUnit, RequestStepsApiUnit
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.blueprint import api

ns = IcmsApiNamespace('requests', description='Endpoints for handling the processes requiring multiple approval steps')

ns.register_crud_handler_class(api, RequestsApiUnit, '')
ns.register_crud_handler_class(api, RequestStepsApiUnit, '/steps')
ns.register_crud_handler_class(api, PersonStatusRequestsApiUnit, '/person/status')