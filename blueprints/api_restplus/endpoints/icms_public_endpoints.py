"""
Endpoints attaching to the public schema if the iCMS database (the new one)
"""

from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from blueprints.api_restplus.api_units import (
    GrappaEgroupsApiCall,
    UserInfoApiCall,
    AutocompletePeopleCall,
    AutocompleteInstitutesCall,
    AnnouncementsCall,
    ParsedHistoryCall,
    CmsProjectCodesApiCall,
)


ns = IcmsApiNamespace(
    "icms_public",
    description="Endpoints attaching to the public schema if the iCMS database (the new one)",
)


@ns.route("/user_info")
class UserInfo(Resource):
    @api.expect(UserInfoApiCall.get_args())
    @api.marshal_with(UserInfoApiCall.get_model())
    def get(self):
        """
        Returning information about specified user (or the logged in user if CMS ID is not provided)
        """
        return UserInfoApiCall.get_user_info()


@ns.route("/grappa_egroups")
class GrappaEgroups(Resource):
    @api.expect(GrappaEgroupsApiCall.get_args())
    @api.marshal_with(GrappaEgroupsApiCall.get_model())
    def get(self):
        """
        Returning egroups from grappa information about specified user (or the logged in user if CMS ID is not provided)
        """
        return GrappaEgroupsApiCall.get_grappa_egroups()


@ns.route("/autocomplete_people/<string:term>")
class AutocompletePeople(Resource):
    @api.expect(AutocompletePeopleCall.get_args(), validate=True)
    @api.marshal_with(AutocompletePeopleCall.get_model())
    def get(self, term):
        return AutocompletePeopleCall.get_people_suggestions(term)


@ns.route("/autocomplete_institutes/<string:term>")
class AutocompleteInstitutes(Resource):
    @api.marshal_list_with(AutocompleteInstitutesCall.get_model())
    def get(self, term):
        return AutocompleteInstitutesCall.get_institutes_suggestions(term)


@ns.route("/announcements")
class Announcements(Resource):
    @api.marshal_with(AnnouncementsCall.get_model())
    @api.expect(AnnouncementsCall.get_args(), validate=True)
    def get(self):
        return AnnouncementsCall.get_announcements()

    @api.expect(AnnouncementsCall.get_model())
    @api.marshal_with(AnnouncementsCall.get_model())
    @admin_required
    def post(self):
        return AnnouncementsCall.post_announcement()


@ns.route("/parsed_history")
class ParsedHistoryInfo(Resource):
    @api.marshal_list_with(ParsedHistoryCall.get_model())
    @api.expect(ParsedHistoryCall.get_args(), validate=True)
    def get(self):
        return ParsedHistoryCall.get()


@ns.route("/cms_projects")
class CmsProjectCodes(Resource):
    @api.expect(CmsProjectCodesApiCall.get_args(), validate=True)
    @api.marshal_with(CmsProjectCodesApiCall.get_model())
    def get(self):
        return CmsProjectCodesApiCall.get_cms_projects()
