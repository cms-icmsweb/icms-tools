from flask_restx import Api
from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException
from blueprints.api_restplus.decorators import check_permissions, login_usually_required
from icmsutils.exceptions import IcmsException, IcmsInsufficientRightsException
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from werkzeug.exceptions import HTTPException
from functools import wraps

import psycopg2
import logging
import traceback
import requests
from util.trivial import current_db_ssn
from flask import abort

log: logging.Logger = logging.getLogger(__name__)


class SingletonApiFactory(object):
    __api = None

    @classmethod
    def get_api(cls) -> Api:
        if cls.__api is None:
            cls.__api = cls.__create_api_instance()
        return cls.__api

    @classmethod
    def __create_api_instance(cls) -> Api:
        return IcmsApi()


class IcmsApi(Api):
    def __init__(self):
        super().__init__(version='0.42', title='iCMS restplus API', description='Restplus iCMS API', 
                         decorators=[login_usually_required, check_permissions, handle_errors], 
                         ordered=False, default_mediatype='application/json'
                         )

def handle_errors(func):
    @wraps(func)
    def decorated_func(*args, **kwargs):
        _api = SingletonApiFactory.get_api()
        _logging_function = lambda x: None
        try:
            return func(*args, **kwargs)
        except HTTPException as e:
            # these should already be something meaningful, let's pass em on then
            raise e
        except IcmsInsufficientRightsException as e:
            _api.abort(403, e)
        except IcmsException as e:
            _api.abort(409, e)
        except NoResultFound as e:
            _logging_function = lambda x: log.warning(x)
            _api.abort(404, 'No results found in the database.')
        except MultipleResultsFound as e:
            _logging_function = lambda x: log.warning(x)
            _api.abort(404, 'Multiple DB results found where only one was expected.')
        except SQLAlchemyError as e:
            _logging_function = lambda x: log.error(x)
            current_db_ssn().rollback()
            _api.abort(500, 'Database error occurred.')
        except ArbitraryErrorCodeException as e:
            _logging_function = lambda x: log.warning(x)
            _api.abort(e.code, str(e))
        except requests.exceptions.ConnectionError as e:
            _logging_function = lambda x: log.error(x)
            _api.abort(500, 'Failed to load data from an external service.')
        except psycopg2.OperationalError as e:
            # catch the DB connection exception as in "msg" and then 
            # exit() the process so the emperor can restart it with 
            # a valid krb token from the pre-exec hook:
            _logging_function( f'got OperationalError: {str(e)}' )
            msg = 'connection to server at "dbod-icms-pg-prod-2.cern.ch" (188.185.5.40), port 6614 failed: could not initiate GSSAPI security context: Unspecified GSS failure.  Minor code may provide more information: Server not found in Kerberos database connection to server at "dbod-icms-pg-prod-2.cern.ch" (188.185.5.40), port 6614 failed: fe_sendauth: no password supplied'
            if 'could not initiate GSSAPI security context' in str(e):
                _logging_function( f'got OperationalError, exiting -- reason: {str(e)}' )
                sys.exit(-42)
            else:
                _logging_function = lambda x: log.error(x)
                _api.abort(500, 'Server error')
        except Exception as e:
            _logging_function = lambda x: log.error(x)
            _api.abort(500, 'Server error')
        finally:
            # Log the previous exception if logging function has been set
            _logging_function(traceback.format_exc())
    return decorated_func
