from icmsutils.exceptions import IcmsInsufficientRightsException, IcmsException


class AccessViolationException(IcmsInsufficientRightsException):
    """Raised when someone tries to act beyond their privileges"""

    def __init__(self, message):
        super().__init__(403, message=message)


class ArbitraryErrorCodeException(Exception):
    """
    Used to throw from processing code so that API can be instructed directly on what error code to return
    """

    def __init__(self, code, message=""):
        super().__init__(message)
        self._code = code

    @property
    def code(self):
        return self._code


class NotFoundException(ArbitraryErrorCodeException):
    def __init__(self, message):
        super().__init__(404, message=message)


class BadRequestException(ArbitraryErrorCodeException):
    def __init__(self, message):
        super().__init__(400, message=message)
