from flask.blueprints import Blueprint
from util import constants as const
from blueprints.api_restplus.api_factory import SingletonApiFactory


restplus_blueprint = Blueprint(const.BP_NAME_API_RESTPLUS, __name__, template_folder='templates')
api = SingletonApiFactory.get_api()
api.init_app(restplus_blueprint)
