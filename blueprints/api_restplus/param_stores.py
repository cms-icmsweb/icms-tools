from typing import List
from dataclasses import dataclass
import flask_login
import flask


@dataclass
class ParamStoreProvider():
    prefix: str
    getter: callable

    @classmethod
    def get_for_current_user(cls, getter: callable = lambda: flask_login.current_user) -> 'ParamStoreProvider':
        return ParamStoreProvider('u', getter)

    @classmethod
    def get_for_view_args(cls, getter: callable = lambda: flask.request.view_args) -> 'ParamStoreProvider':
        return ParamStoreProvider('p', getter)

    @staticmethod
    def get_all() -> List['ParamStoreProvider']:
        return [
            ParamStoreProvider.get_for_current_user(),
            # for path variables as in /resource/<int:id>
            ParamStoreProvider.get_for_view_args(),
            ParamStoreProvider('r', lambda: flask.request.get_json()),
            # for query string parameters as in /resource?cmsId=9981
            ParamStoreProvider('q', lambda: flask.request.args),
        ]
