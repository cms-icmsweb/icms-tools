import re
from datetime import datetime
from blueprints.api_restplus.exceptions import NotFoundException

import sqlalchemy as sa
from flask_login import current_user
from icms_orm.common import Affiliation as Aff
from icms_orm.common import Announcement
from icms_orm.common import Institute as Inst
from icms_orm.common import InstituteLeader as InstLead
from icms_orm.common import InstituteStatus as InstStat
from icms_orm.common import LegacyFlag, OrgUnit
from icms_orm.common import Person as Pers
from icms_orm.common import PersonStatus as PersStat
from icms_orm.common import Position, PrehistoryTimeline, Tenure
from icms_orm.toolkit import CmsProject

from util import constants as const
from webapp.webapp import Webapp
from asyncio.log import logger
import requests
from requests.auth import AuthBase
from netrc import netrc
from pathlib import Path

from .basics import BaseApiUnit, ModelFactory, ParserBuilder


class UserInfoApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "User Info",
        {
            Pers.cms_id.key: ModelFactory.Integer(),
            Pers.first_name.key: ModelFactory.String(),
            Pers.last_name.key: ModelFactory.String(),
            Aff.inst_code.key: ModelFactory.String(),
            PersStat.status.key: ModelFactory.String(),
            PersStat.is_author.key: ModelFactory.Boolean(),
            PersStat.activity.key: ModelFactory.String(),
            Pers.hr_id.key: ModelFactory.Integer(),
            Pers.login.key: ModelFactory.String(),
            "team_duties": ModelFactory.Raw(),
            "flags": ModelFactory.Raw(),
            "cms_egroups": ModelFactory.List(ModelFactory.String),
            "grappa_egroups": ModelFactory.List(ModelFactory.String),
            "managed_unit_ids": ModelFactory.Raw(),
        },
    )

    _args = (
        ParserBuilder()
        .add_argument(Pers.cms_id.key, type=ParserBuilder.INTEGER, required=False)
        .add_argument(Pers.login.key, type=ParserBuilder.STRING, required=False)
        .parser
    )

    @classmethod
    def _get_user_info_query(cls):
        s = Pers.session()
        sq_td = (
            s.query(
                InstLead.cms_id.label("cms_id"),
                sa.func.json_object_agg(
                    InstLead.inst_code,
                    sa.case([(InstLead.is_primary == True, "leader")], else_="deputy"),
                ).label("team_duties"),
            )
            .group_by(InstLead.cms_id)
            .filter(InstLead.end_date == None)
            .subquery()
        )

        sq_flags = (
            s.query(
                LegacyFlag.cms_id.label("cms_id"),
                sa.func.json_agg(LegacyFlag.flag_code).label("flags"),
            )
            .group_by(LegacyFlag.cms_id)
            .filter(LegacyFlag.start_date <= sa.func.current_date())
            .filter(
                sa.or_(
                    LegacyFlag.end_date.is_(None),
                    LegacyFlag.end_date >= sa.func.current_date(),
                )
            )
            .subquery()
        )

        sq_managed_units = (
            s.query(
                sa.func.json_agg(OrgUnit.id).label("managed_unit_ids"),
                Tenure.cms_id.label("cms_id"),
            )
            .join(
                Tenure,
                sa.or_(
                    OrgUnit.id == Tenure.unit_id,
                    OrgUnit.superior_unit_id == Tenure.unit_id,
                ),
            )
            .join(
                Position,
                sa.and_(Tenure.position_id == Position.id, Position.level <= 1),
            )
            .filter(
                sa.and_(
                    Tenure.start_date < datetime.today(),
                    sa.or_(Tenure.end_date == None, Tenure.end_date > datetime.today()),
                )
            )
            .group_by(Tenure.cms_id)
            .subquery()
        )

        cols = [
            Pers.cms_id,
            Pers.first_name,
            Pers.last_name,
            Pers.hr_id,
            Pers.login,
            Aff.inst_code,
            PersStat.status,
            PersStat.is_author,
            PersStat.activity,
            sq_td.c.team_duties,
            sq_flags.c.flags,
            sq_managed_units.c.managed_unit_ids,
        ]

        _q = (
            s.query(*cols)
            .join(
                PersStat,
                sa.and_(Pers.cms_id == PersStat.cms_id, PersStat.end_date == None),
            )
            .join(
                Aff,
                sa.and_(
                    Pers.cms_id == Aff.cms_id,
                    Aff.is_primary == True,
                    Aff.end_date == None,
                ),
            )
        )

        for _sq in [sq_td, sq_flags, sq_managed_units]:
            _q = _q.join(_sq, _sq.c.cms_id == Pers.cms_id, isouter=True)
        return _q, cols

    @classmethod
    def get_user_info(cls, api_args=None, nullable=False):
        _q, cols = cls._get_user_info_query()
        if api_args is None:
            api_args = cls.parse_args()
        if api_args.get(Pers.cms_id.key, None):
            _q = _q.filter(Pers.cms_id == api_args.get(Pers.cms_id.key))
        elif api_args.get(Pers.login.key, None):
            _q = _q.filter(Pers.login == api_args.get(Pers.login.key))
        else:
            _q = _q.filter(Pers.cms_id == current_user.cms_id)
        row = _q.one_or_none()
        if nullable is True and row is None:
            return None

        if row is None:
            raise NotFoundException(
                f"The requested user ({api_args.get(Pers.cms_id.key)}/{api_args.get(Pers.login.key)}) was not found"
            )

        cms_person_dict = cls.row_to_dict(row, cols)
        # Get egroups from ldap
        ldap_person = (
            Webapp.current_app().get_ldap_proxy().get_person_by_hrid(row.hr_id)
        )
        # ensure reasonable return if no ldap entry for the person is found
        try:
            cms_person_dict["cms_egroups"] = ldap_person.cmsEgroups
        except:
            cms_person_dict["cms_egroups"] = []

        return cms_person_dict

    @classmethod
    def get_for_cms_id(cls, cms_id, nullable=False):
        return cls.get_user_info(api_args={Pers.cms_id.key: cms_id}, nullable=nullable)

    @classmethod
    def get_for_username(cls, username, nullable=False):
        return cls.get_user_info(api_args={Pers.login.key: username}, nullable=nullable)


class GrappaEgroupsApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Grappa Egroups Info",
        {
            "grappa_egroups": ModelFactory.List(ModelFactory.String),
        },
    )

    _args = (
        ParserBuilder()
        .add_argument(Pers.cms_id.key, type=ParserBuilder.INTEGER, required=False)
        .add_argument(Pers.login.key, type=ParserBuilder.STRING, required=False)
        .parser
    )

    @classmethod
    def get_grappa_egroups(cls, api_args=None, nullable=False):
        return_dict = {}
        api_args = cls.parse_args()

        # ensure reasonable return if no row.login or no grappa info for the person is found
        try:
            return_dict["grappa_egroups"] = cls.get_grappa_egroups_for_current_user(
                current_user.username
            )
        except Exception as e:
            logger.error(
                f"Error while fetching e groups for current user ({current_user.username}) recursively from Authorization Service API Identities: {e}"
            )
            return_dict["grappa_egroups"] = []

        return return_dict

    @classmethod
    def get_grappa_egroups_for_current_user(cls, username):
        list_of_egroups = []
        auth_service_api_base_url = "https://authorization-service-api.web.cern.ch"
        group_identity_endpoint = (
            f"{auth_service_api_base_url}/api/v1.0/Identity/{username}/groups/recursive"
        )
        with AuthorizationServiceApiSession(
            api_token=cls.get_token(), timeout=5
        ) as session:
            first_page = session.get(
                group_identity_endpoint,
                params={
                    "limit": 1500,
                },
            )
            json_resp = first_page.json()

        [
            list_of_egroups.append(egroup["groupIdentifier"])
            for egroup in json_resp["data"]
        ]
        return list_of_egroups

    @classmethod
    def get_application_credentials(cls):
        grappa_netrc_host = "grappa.cern.ch"
        data_netrc_file = Path("/data/secrets/.netrc")
        netrc_entry = None
        if data_netrc_file.exists():
            netrc_entry = netrc(data_netrc_file).authenticators(grappa_netrc_host)
        if not netrc_entry:
            home_netrc_file = Path.home() / ".netrc"
            netrc_entry = netrc(home_netrc_file).authenticators(grappa_netrc_host)
        client_id, _, client_secret = netrc_entry
        return client_id, client_secret

    @classmethod
    def get_token(cls):
        api_token_endpoint = "https://auth.cern.ch/auth/realms/cern/api-access/token"
        client_id, client_secret = cls.get_application_credentials()
        token_resp = requests.post(
            api_token_endpoint,
            data={
                "grant_type": "client_credentials",
                "client_id": client_id,
                "client_secret": client_secret,
                "audience": "authorization-service-api",
            },
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            timeout=5,
        )
        token_resp.raise_for_status()
        api_token = token_resp.json()["access_token"]
        return api_token


class AuthorizationServiceApiAuth(AuthBase):
    """Bearer token auth for API access"""

    def __init__(self, token):
        self.token = token

    def __call__(self, request):
        request.headers["Authorization"] = f"Bearer {self.token}"
        return request


class AuthorizationServiceApiSession(requests.Session):
    """Authorization service API session handling auth, timeouts and errors"""

    def __init__(self, *, api_token, timeout=5):
        self.timeout = timeout
        super().__init__()
        self.auth = AuthorizationServiceApiAuth(api_token)
        self.hooks["response"].append(lambda r, *args, **kwargs: r.raise_for_status())

    def request(self, method, url, **kwargs):
        if "timeout" not in kwargs:
            kwargs["timeout"] = self.timeout
        return super().request(method, url, **kwargs)


class AutocompletePeopleCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Person Autocomplete Info",
        {
            Pers.cms_id.key: ModelFactory.Integer(),
            Pers.first_name.key: ModelFactory.String(),
            Pers.last_name.key: ModelFactory.String(),
            Aff.inst_code.key: ModelFactory.String(),
        },
    )

    _args = (
        ParserBuilder()
        .add_argument(
            "team_only",
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=False,
        )
        .parser
    )

    @classmethod
    def get_people_suggestions(cls, search_term):
        args = cls.parse_args()
        results = []
        output_cols = [Pers.last_name, Pers.first_name, Pers.cms_id, Aff.inst_code]
        search_term = search_term.strip()
        if search_term:
            input_tokens = re.findall(r"[\w]*", search_term)
            session = Pers.session()

            wildcard_match_against = [
                Pers.last_name,
                Pers.first_name,
                Pers.last_name,
                Pers.login,
                Pers.email,
            ]
            number_match_against = [
                sa.sql.expression.cast(Pers.cms_id, sa.String),
                sa.sql.expression.cast(Pers.hr_id, sa.String),
            ]

            q = (
                session.query(*output_cols)
                .filter(
                    sa.and_(
                        *[
                            sa.or_(
                                *(
                                    (
                                        []
                                        if token.isdigit()
                                        else [
                                            col.ilike("%{0}%".format(token))
                                            for col in wildcard_match_against
                                        ]
                                    )
                                    + (
                                        [token == col for col in number_match_against]
                                        if token.isdigit()
                                        else []
                                    )
                                )
                            )
                            for token in input_tokens
                        ]
                    )
                )
                .join(
                    PersStat,
                    sa.and_(PersStat.cms_id == Pers.cms_id, PersStat.end_date == None),
                )
                .join(
                    Aff,
                    sa.and_(Aff.cms_id == PersStat.cms_id, Aff.end_date == None),
                )
                .order_by(Pers.last_name)
            )
            q = cls.attach_query_filters(q, args, [])
            if args.get("team_only"):
                institutes = current_user.get_represented_institutes_codes()
                if institutes:
                    q = q.filter(Aff.inst_code.in_(institutes.keys()))
            results = q.limit(const.NUMBER_OF_SUGGESTED_RESULTS).all()
        return cls.rows_to_list_of_dicts(results, output_cols)


class AutocompleteInstitutesCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Institute Autocomplete Info",
        {
            Inst.code.key: ModelFactory.String,
            Inst.name.key: ModelFactory.String,
            Inst.country_code.key: ModelFactory.String,
            InstStat.status.key: ModelFactory.String,
        },
    )

    @classmethod
    def get_institutes_suggestions(cls, search_term):
        terms = ["%{0}%".format(_token) for _token in re.findall(r"[\w]*", search_term)]
        output_cols = [Inst.code, Inst.name, Inst.country_code, InstStat.status]
        match_against = [Inst.code, Inst.name, Inst.country_code]
        q = (
            Inst.session()
            .query(*output_cols)
            .join(
                InstStat, sa.and_(InstStat.code == Inst.code, InstStat.end_date == None)
            )
        )
        q = q.filter(
            sa.and_(
                *[
                    sa.or_(*[_col.ilike(_term) for _col in match_against])
                    for _term in terms
                ]
            )
        )
        return cls.rows_to_list_of_dicts(
            q.limit(const.NUMBER_OF_SUGGESTED_RESULTS).all(), output_cols
        )


class AnnouncementsCall(BaseApiUnit):
    __model_ingredients = {
        Announcement.id: ModelFactory.field_proxy(
            ModelFactory.Integer,
            {ModelFactory.REQUIRED: False, ModelFactory.EXAMPLE: 42},
        ),
        Announcement.type: ModelFactory.field_proxy(
            ModelFactory.String,
            {
                ModelFactory.REQUIRED: False,
                ModelFactory.DEFAULT: "INFO",
                ModelFactory.ENUM: ["INFO", "WARNING"],
            },
        ),
        Announcement.application: ModelFactory.field_proxy(
            ModelFactory.String,
            {
                ModelFactory.REQUIRED: False,
                ModelFactory.ENUM: ["epr", "common", "tools", "notes", "cadi"],
            },
        ),
        Announcement.subject: ModelFactory.field_proxy(
            ModelFactory.String,
            {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: "Bonus EPR credit"},
        ),
        Announcement.content: ModelFactory.field_proxy(
            ModelFactory.String,
            {
                ModelFactory.REQUIRED: True,
                ModelFactory.EXAMPLE: "Every user who logs in exactly thrice over the next week will receive 2 months of free EPR credit!",
            },
        ),
        Announcement.start_datetime: ModelFactory.field_proxy(
            ModelFactory.DateTime,
            {
                ModelFactory.REQUIRED: True,
            },
        ),
        Announcement.end_datetime: ModelFactory.field_proxy(
            ModelFactory.DateTime, {ModelFactory.REQUIRED: True}
        ),
    }

    _model = ModelFactory.make_model(
        "Announcement Info", {_k.key: _v for _k, _v in __model_ingredients.items()}
    )

    _args = (
        ParserBuilder()
        .add_argument(Announcement.id.key, ParserBuilder.INTEGER, required=False)
        .add_argument(
            Announcement.application.key, ParserBuilder.STRING, required=False
        )
        .add_argument(Announcement.type.key, ParserBuilder.STRING, required=False)
        .add_argument(
            "active_only", ParserBuilder.BOOLEAN, required=False, default=False
        )
        .parser
    )

    @classmethod
    def _do_get_announcements(cls, args_dict):
        ssn = Announcement.session()
        _cols = [_c for _c in cls.__model_ingredients]
        q = ssn.query(*_cols)
        q = cls.attach_query_filters(
            q,
            args_dict,
            [
                (Announcement.id, None, None),
                (Announcement.application, None, None),
                (Announcement.type, None, None),
            ],
        )
        if args_dict.get("active_only") == True:
            q = q.filter(
                sa.and_(
                    Announcement.start_datetime <= datetime.now(),
                    sa.or_(
                        Announcement.end_datetime == None,
                        Announcement.end_datetime >= datetime.now(),
                    ),
                )
            )
        return cls.rows_to_list_of_dicts(q.all(), cols_list=_cols)

    @classmethod
    def get_announcements(cls):
        return cls._do_get_announcements(cls.parse_args())

    @classmethod
    def post_announcement(cls):
        payload = cls.get_payload()
        ssn = Announcement.session()
        _sent_id = payload.get(Announcement.id.key, None)
        announcement = (
            _sent_id
            and ssn.query(Announcement).filter(Announcement.id == _sent_id)
            or Announcement.from_ia_dict({})
        )
        cls.update_db_object(
            announcement,
            payload,
            rules=[(_col, None, None) for _col in cls.__model_ingredients],
        )
        return cls._do_get_announcements(
            {Announcement.id.key: announcement.get(Announcement.id)}
        )[0]


class ParsedHistoryCall(BaseApiUnit):
    _args = (
        ParserBuilder()
        .add_argument(
            PrehistoryTimeline.cms_id.key, ParserBuilder.INTEGER, required=True
        )
        .parser
    )
    _model = ModelFactory.make_model(
        "Parsed History Line",
        {
            PrehistoryTimeline.id: ModelFactory.Integer,
            PrehistoryTimeline.cms_id: ModelFactory.Integer,
            PrehistoryTimeline.start_date: ModelFactory.Date,
            PrehistoryTimeline.end_date: ModelFactory.Date,
            PrehistoryTimeline.inst_code: ModelFactory.String,
            PrehistoryTimeline.status_cms: ModelFactory.String,
            PrehistoryTimeline.activity_cms: ModelFactory.String,
        },
    )

    @classmethod
    def get(cls):
        q_cols = PrehistoryTimeline.ia_list()
        q = PrehistoryTimeline.session().query(*q_cols)
        q = cls.attach_query_filters(
            q, cls.parse_args(), [PrehistoryTimeline.cms_id]
        ).order_by(PrehistoryTimeline.start_date)
        return cls.rows_to_list_of_dicts(q.all(), q_cols)


class CmsProjectCodesApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "CMS Project Codes",
        {
            CmsProject.code: ModelFactory.String,
            CmsProject.name: ModelFactory.String,
            CmsProject.official: ModelFactory.Boolean,
            CmsProject.status: ModelFactory.String,
        },
    )
    _args = (
        ParserBuilder()
        .add_argument(CmsProject.code.key, type=ParserBuilder.STRING, required=False)
        .add_argument(CmsProject.name.key, type=ParserBuilder.STRING, required=False)
        .add_argument(
            CmsProject.official.key, type=ParserBuilder.BOOLEAN, required=False
        )
        .add_argument(CmsProject.status.key, type=ParserBuilder.STRING, required=False)
        .parser
    )
    _args_mapping = [
        CmsProject.code,
        CmsProject.name,
        CmsProject.official,
        CmsProject.status,
    ]

    @classmethod
    def get_cms_projects(cls):
        args = cls.parse_args()
        q_cols = CmsProject.ia_list()
        q = CmsProject.session().query(*q_cols).order_by(CmsProject.code)
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        return cls.rows_to_list_of_dicts(q.all(), q_cols)
