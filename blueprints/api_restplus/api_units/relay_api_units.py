from blueprints.users.user_class import User
import flask
from flask import request as flask_request
import flask_login
from blueprints.api_restplus.api_units.basics import BaseApiUnit
import requests
import logging


class IcmsPiggybackApiCall(BaseApiUnit):

    @classmethod
    def join(cls, *path_elements):
        url = '/'.join([s.strip('/')
                        for s in [IcmsPiggybackApiCall.get_piggyback_url()] + list(path_elements)])
        logging.debug("Returning piggyback's URL of %s", url)
        return url

    @classmethod
    def get_piggyback_url(cls):
        return flask.current_app.config['ICMS_PIGGYBACK_URL']

    @classmethod
    def get_payload(cls):
        payload = super().get_payload()
        return payload

    @classmethod
    def _get_path_prefix(cls, request: flask.Request):
        return '/'.join(request.url_rule.rule.split('/')[:-1])

    @classmethod
    def get_header_prefix_value(cls, request: flask.Request):
        path_prefix = cls._get_path_prefix(request)
        script_root = request.script_root
        return '/'.join([script_root, path_prefix]).replace('//', '/')

    @classmethod
    def relay_request(cls, endpoint: str, method='GET'):
        url = IcmsPiggybackApiCall.join(endpoint)
        request = requests.Request(
            url=url,
            method=method.upper(),
            headers=cls.prepare_headers(flask_login.current_user)
        )
        if method.upper() == 'GET':
            if flask.request.query_string:
                request.url = '{url}?{qs}'.format(
                    url=request.url, qs=flask.request.query_string.decode('utf8'))
        else:
            payload = cls.get_payload()
            request.json = payload
            # the tuple could be up to 4 elements long: filename, content, content type and a dict of further headers
            request.files = {k: (f.filename, f)
                             for k, f in flask.request.files.items()}
        logging.debug(f'Relaying request to: {url}')
        prepared_request = request.prepare()
        response = requests.Session().send(prepared_request, allow_redirects=False)
        return cls.repack_response(response)

    @classmethod
    def repack_response(cls, obtained):
        assert isinstance(obtained, requests.Response)
        returned = flask.make_response(obtained.content)
        for header in ['Content-disposition', 'Content-length', 'Content-type', 'Location']:
            if header in obtained.headers.keys():
                returned.headers[header] = obtained.headers[header]
        returned.status = str(obtained.status_code)
        return returned

    @classmethod
    def prepare_headers(cls, user: User):
        headers = {
            'X-Forwarded-Proto': getattr(flask_request, 'scheme', 'https'),
            'X-Forwarded-Prefix': cls.get_header_prefix_value(flask_request),
            'X-Forwarded-Host': flask_request.host,
            'Accept-Encoding': flask.request.headers.get('Accept-Encoding', ''),
            # 'Cache-Control': flask.request.headers.get('CACHE_CONTROL', 'No-Cache'),
            # 'Content-Type': flask.request.headers.get('CONTENT_TYPE'),
            # 'Content-Length': flask.request.headers.get('CONTENT_LENGTH'),
        }
        if user.is_cms:
            headers['X-Auth-Username'] = user.username
            headers['X-Auth-CmsId'] = str(user.cms_id)
        return headers
