from venv import logger
from blueprints.api_restplus.api_units.basics.model_field_types import (
    Boolean,
    Integer,
    String,
)
from typing import Type
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.voting.voting_lists import VotingListModel, VotingParticipant
from icms_orm.toolkit import Voting
from icms_orm.cmspeople import CountryRegionMapping

import logging

log: logging.Logger = logging.getLogger(__name__)


class CountryRegionInfo:
    """Singleton class to cache institute, country, and region info"""

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(CountryRegionInfo, cls).__new__(cls)
            cls._instance._init_data()
        return cls._instance

    def _init_data(self):
        self.country_region_map = {}
        ssn2 = CountryRegionMapping.session()

        q = ssn2.query(CountryRegionMapping)

        results = q.all()

        for crm in results:
            self.country_region_map[crm.name] = crm.region_code

    def get_region_code(self, country):
        return (
            self.country_region_map[country]
            if self.country_region_map and self.country_region_map[country]
            else None
        )


class VotingParticipantsApiUnit(BaseApiUnit):

    ARG_NAME_INCLUDE_NEARLY_VOTING = "include_nearly_voting"
    ARG_NAME_VOTING_KEY = Voting.code.key

    FIELD_NAME_PERSON = "person"
    FIELD_NAME_HR_ID = "hr_id"
    FIELD_NAME_CMS_ID = "cms_id"
    FIELD_NAME_UNIT = "unit"
    FIELD_NAME_COUNTRY = "country"
    FIELD_NAME_REGION = "region"
    FIELD_NAME_VOTES = "votes"
    FIELD_NAME_IS_DELEGATED = "is_delegated"
    FIELD_NAME_REMARKS = "remarks"

    _args = (
        ParserBuilder()
        .add_argument(
            name=ARG_NAME_VOTING_KEY, required=True, type=ParserBuilder.STRING
        )
        .add_argument(
            name=ARG_NAME_INCLUDE_NEARLY_VOTING,
            required=False,
            default=False,
            type=ParserBuilder.BOOLEAN,
        )
        .parser
    )

    _model = ModelFactory.make_model(
        "Voting Participant Info",
        {
            FIELD_NAME_PERSON: String,
            FIELD_NAME_HR_ID: Integer,
            FIELD_NAME_CMS_ID: Integer,
            FIELD_NAME_UNIT: String,
            FIELD_NAME_COUNTRY: String,
            FIELD_NAME_REGION: String,
            FIELD_NAME_VOTES: Integer,
            FIELD_NAME_IS_DELEGATED: Boolean,
            FIELD_NAME_REMARKS: String,
        },
    )

    @classmethod
    def get_voting_participants(cls: Type["VotingParticipantsApiUnit"]):
        results = []
        voting_code = cls.parse_args().get(Voting.code.key)
        include_nearly_voting = cls.parse_args().get(cls.ARG_NAME_INCLUDE_NEARLY_VOTING)
        model = VotingListModel.get_instance(voting_code=voting_code)
        region_info = CountryRegionInfo()

        for p in model.participants:
            assert isinstance(p, VotingParticipant)
            region_code = (
                region_info.get_region_code(p.voting_entity.country)
                if p.voting_entity.name != "CERN"
                else "CERN"
            )

            results.append(
                {
                    cls.FIELD_NAME_PERSON: (p.delegate or p.person).label,
                    cls.FIELD_NAME_CMS_ID: (p.delegate or p.person).cms_id,
                    cls.FIELD_NAME_HR_ID: (p.delegate or p.person).hr_id,
                    cls.FIELD_NAME_UNIT: p.voting_entity.name,
                    cls.FIELD_NAME_COUNTRY: p.voting_entity.country,
                    cls.FIELD_NAME_REGION: region_code,
                    cls.FIELD_NAME_VOTES: p.votes,
                    cls.FIELD_NAME_IS_DELEGATED: p.delegate is not None,
                    cls.FIELD_NAME_REMARKS: ";".join(
                        [
                            x
                            for x in (
                                (
                                    "delegated by {0}".format(p.person.label)
                                    if p.delegate
                                    else None
                                ),
                                p.remarks,
                            )
                            if x
                        ]
                    ),
                }
            )
        if include_nearly_voting:
            for p in model.near_participants:
                assert isinstance(p, VotingParticipant)
                region_code = (
                    region_info.get_region_code(p.voting_entity.country)
                    if p.voting_entity.name != "CERN"
                    else "CERN"
                )

                results.append(
                    {
                        cls.FIELD_NAME_UNIT: p.voting_entity.name,
                        cls.FIELD_NAME_COUNTRY: p.voting_entity.country,
                        cls.FIELD_NAME_REGION: region_code,
                        cls.FIELD_NAME_REMARKS: "{0} PHDs in {2}, {1} a year before".format(
                            p.phds_now, p.phds_past, model.voting_date.year
                        ),
                        cls.FIELD_NAME_VOTES: 0,
                    }
                )
        return results
