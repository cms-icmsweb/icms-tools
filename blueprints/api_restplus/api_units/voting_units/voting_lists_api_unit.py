from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from icmsutils.domain_object_model.voting import VotingCluster, VotingCountry, VotingEntity, VotingInstitute, VotingParticipant
from blueprints.voting.voting_lists.abstract_voting_list_model import AbstractVotingListModel
from blueprints.voting.voting_lists import VotingListModel
import logging

from icmsutils.exceptions import IcmsException
from blueprints.api_restplus.api_units.basics.crud import Deleteless, Listless, Putless
from typing import Any, Dict, List, Optional, Type
from blueprints.api_restplus.api_units.basics.model_field_types import Boolean, String
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit, ModelFieldDefinition
from icms_orm.toolkit import Voting, VotingListEntry, VotingMerger, VotingMergerMember
import sqlalchemy as sa


log: logging.Logger = logging.getLogger(__name__)


class VotingListsApiUnit(AbstractCRUDUnit, Deleteless, Putless):
    FIELD_EVENT_CODE = ModelFieldDefinition.builder(
        String).name('event_code').id().required().build()
    FIELD_IS_PERSISTED = ModelFieldDefinition.builder(
        Boolean).name('is_persisted').readonly().build()

    @classmethod
    def get_model_name(cls: Type[AbstractCRUDUnit]) -> str:
        return 'Voting List'

    @classmethod
    def is_model_field_eligible_for_parser(cls: Type['VotingListsApiUnit'], field: ModelFieldDefinition) -> bool:
        # So far no need to filter the list on existing fields - disabling query string arguments
        return False

    @classmethod
    def get_model_fields_list(cls: Type['VotingListsApiUnit']) -> List[ModelFieldDefinition]:
        return [cls.FIELD_EVENT_CODE, cls.FIELD_IS_PERSISTED]

    @classmethod
    def get_by_identity(cls: Type['VotingListsApiUnit'], *params) -> Dict[str, Any]:
        '''Fetches voting list metadata for given voting code'''
        code = params[0]
        for e in cls.find(None):
            if e[cls.FIELD_EVENT_CODE.name] == code:
                return e
        raise ArbitraryErrorCodeException(
            404, f'Voting list for code {code} not found')

    @classmethod
    def find(cls: Type['VotingListsApiUnit'], needle: Optional[ResourceFilter]) -> List[Dict[str, Any]]:
        '''Lists existing voting lists'''
        ssn = VotingListEntry.session()
        query = ssn.query(Voting.code, sa.func.count(VotingListEntry.id).label('voters')).join(
            VotingListEntry, Voting.code == VotingListEntry.code, isouter=True).group_by(Voting.code)
        return [{cls.FIELD_EVENT_CODE.name: code, cls.FIELD_IS_PERSISTED.name: count > 0} for code, count in query.all()]

    @classmethod
    def post(cls: Type['VotingListsApiUnit']) -> Dict[str, object]:
        '''Persists corresponding voting list model in the DB'''

        voting_code = cls.get_payload()[cls.FIELD_EVENT_CODE.name]
        ssn = Voting.session()

        if ssn.query(VotingListEntry).filter(VotingListEntry.code == voting_code).count() > 0:
            raise IcmsException(
                'Voting list for {0} already persisted in the DB!'.format(voting_code))

        model: AbstractVotingListModel = VotingListModel.get_instance(
            voting_code=voting_code)

        ppt: VotingParticipant
        for ppt in model.participants:
            entity: VotingEntity = ppt.voting_entity

            represented_inst_code = None
            merger_id = None
            merger = None
            if isinstance(entity, VotingCluster):
                # we need to find the ID from the DB based on membership (sorry, sad, but only admins would invoke that)
                merger_ids = ssn.query(VotingMerger.id).\
                    join(VotingMergerMember, VotingMergerMember.merger_id == VotingMerger.id).\
                    filter(sa.and_(
                        VotingMergerMember.inst_code.in_(
                            [_i.code for _i in entity.institutes]),
                        VotingMerger.valid_from <= model.list_closing_date,
                        sa.or_(VotingMerger.valid_till == None,
                               VotingMerger.valid_till >= model.voting_date)
                    )).distinct(VotingMerger.id).all()
                _ids_set = set(id for (id, ) in merger_ids)
                assert len(
                    _ids_set) == 1, 'Found the following merger_id candidates: {0}'.format(_ids_set)
                merger_id = _ids_set.pop()

            if isinstance(entity, VotingInstitute):
                represented_inst_code = entity.institute.code

            if isinstance(entity, VotingCountry):
                # creating a one-off empty merger so that we can store a reference to something in our DB
                merger: VotingMerger = VotingMerger.from_ia_dict({
                    VotingMerger.valid_from: model.list_closing_date,
                    VotingMerger.valid_till: model.voting_date,
                    VotingMerger.country: entity.country,
                    VotingMerger.representative_cms_id: ppt.person.cms_id
                })

            entry: VotingListEntry = VotingListEntry.from_ia_dict({
                VotingListEntry.cms_id: (ppt.delegate or ppt.person).cms_id,
                VotingListEntry.code: voting_code,
                VotingListEntry.weight: ppt.votes,
                VotingListEntry.represented_merger_id: merger_id,
                VotingListEntry.represented_inst_code: represented_inst_code,
                VotingListEntry.delegated_by_cms_id: ppt.delegate and ppt.person.cms_id or None,
                VotingListEntry.remarks: ppt.remarks,
            })

            if merger is not None:
                logging.debug('Setting the one-off merger for VoteListEntry')
                entry.represented_merger = merger

            ssn.add(entry)

        ssn.commit()
        return {cls.FIELD_EVENT_CODE.name: voting_code, cls.FIELD_IS_PERSISTED.name: True}
