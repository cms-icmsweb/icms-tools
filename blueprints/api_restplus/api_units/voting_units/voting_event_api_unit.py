from datetime import datetime
import logging
from sqlalchemy.orm.attributes import InstrumentedAttribute
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from icms_orm.custom_classes import PseudoEnum
from sqlalchemy.orm.query import Query
from blueprints.api_restplus.api_units.basics.builders import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics.model_field_types import Boolean, Date, DateTime, Integer, String
from typing import Any, Dict, List, Optional, Type
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from icms_orm.toolkit import Voting, VotingListEntry
import sqlalchemy as sa

log: logging.Logger = logging.getLogger(__name__)


class VotingEventsApiUnit(AbstractCRUDUnit):
    class Field(PseudoEnum):
        ID = ModelFieldDefinition.builder(Integer).column(
            Voting.id).name('id').id().readonly(True).build()
        CODE = ModelFieldDefinition.builder(String).column(
            Voting.code).name('code').required().build()
        TYPE = ModelFieldDefinition.builder(String).column(
            Voting.type).name('type').enum(Voting.Type.values()).required().build()
        TITLE = ModelFieldDefinition.builder(String).column(
            Voting.title).name('title').build()
        LIST_CLOSING_DATE = ModelFieldDefinition.builder(Date).column(
            Voting.list_closing_date).name('list_closing_date').build()
        START_TIME = ModelFieldDefinition.builder(DateTime).column(
            Voting.start_time).name('start_time').required().build()
        DELEGATION_DEADLINE = ModelFieldDefinition.builder(DateTime).column(
            Voting.delegation_deadline).name('delegation_deadline').build()
        END_TIME = ModelFieldDefinition.builder(DateTime).column(
            Voting.end_time).name('end_time').build()
        FIELD_APPLICABLE_MO_YEAR = ModelFieldDefinition.builder(Integer).column(
            Voting.applicable_mo_year).name('applicable_mo_year').build()
        IS_WRITTEN_LIST_PRESENT = ModelFieldDefinition.builder(Boolean).column(sa.case([(sa.func.count(
            VotingListEntry.code) > 0, True)], else_=False)).name('is_written_list_present').readonly().build()
        IS_DELEGATION_ALLOWED = ModelFieldDefinition.builder(Boolean).name('is_delegation_allowed').readonly().build()

    @classmethod
    def get_query(cls: Type['VotingEventsApiUnit'], resource_filter: Optional[ResourceFilter]) -> Query:
        super().get_query()
        query: Query = super().get_query(resource_filter=resource_filter)
        query = query.join(VotingListEntry, VotingListEntry.code ==
                           Voting.code, isouter=True).group_by(Voting.code)
        return query

    @classmethod
    def get_model_name(cls: Type['VotingEventsApiUnit']) -> str:
        return 'Voting_Event'

    @classmethod
    def get_model_fields_list(cls: Type['VotingEventsApiUnit']) -> List[ModelFieldDefinition]:
        return cls.Field.values()

    @classmethod
    def is_model_field_eligible_for_parser(cls: Type['VotingEventsApiUnit'], field: ModelFieldDefinition) -> bool:
        return field in [cls.Field.CODE, cls.Field.ID, cls.Field.TITLE, cls.Field.TYPE, cls.Field.IS_DELEGATION_ALLOWED]

    @classmethod
    def row_to_dict(cls: Type['VotingEventsApiUnit'], row: List[Any], cols_list: List[InstrumentedAttribute]) -> Dict[str, Any]:
        retval: Dict[str, Any] = super().row_to_dict(row, cols_list)
        utcnow = datetime.utcnow()
        retval[cls.Field.IS_DELEGATION_ALLOWED.name] = retval[cls.Field.DELEGATION_DEADLINE.name] > utcnow \
            and retval[cls.Field.START_TIME.name] > utcnow
        return retval

    @classmethod
    def find(cls: Type['VotingEventsApiUnit'], needle: Optional[ResourceFilter]) -> List[Dict[str, Any]]:
        '''Finds voting events matching specified criteria.'''
        retval: List[Dict[str, Any]] = super().find(needle=needle)
        # Post-processing the results to filter on synthetic columns (if requested to)
        if needle:
            filter: ResourceFilter
            for filter in needle:
                log.debug(
                    f'DB column: {filter.column}, fallback key: {filter.fallback_key}')
                if cls.Field.IS_DELEGATION_ALLOWED.name == filter.fallback_key:
                    retval = [e for e in retval if e[cls.Field.IS_DELEGATION_ALLOWED.name]
                              == filter.value]
        return retval
