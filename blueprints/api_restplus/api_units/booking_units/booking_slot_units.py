import logging
from flask_restx.reqparse import Argument
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.general_management.general_management_models import IndicoBooking
from typing import List, Optional
from icms_orm.toolkit import CmsWeek
from flask import current_app
from blueprints.api_restplus.api_units.basics import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import ModelFactory
from util.constants import OPT_NAME_INDICO_SECRET, OPT_NAME_INDICO_TOKEN

log: logging.Logger = logging.getLogger(__name__)


class BookingSlotsApiUnit(AbstractCRUDUnit):

    FIELD_ROOM_ID = ModelFieldDefinition.builder(
        ModelFactory.Integer).name("roomId").build()
    FIELD_WEEK_ID = ModelFieldDefinition.builder(
        ModelFactory.Integer).name("weekId").required().build()
    FIELD_START_TIME = ModelFieldDefinition.builder(
        ModelFactory.DateTime).name("startTime").build()
    FIELD_END_TIME = ModelFieldDefinition.builder(
        ModelFactory.DateTime).name("endTime").build()
    FIELD_SOURCE = ModelFieldDefinition.builder(
        ModelFactory.String).name("source").build()

    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return [cls.FIELD_ROOM_ID, cls.FIELD_WEEK_ID, cls.FIELD_START_TIME, cls.FIELD_END_TIME, cls.FIELD_SOURCE]

    @classmethod
    def get_model_name(cls) -> str:
        return "Room Booking Slot"

    @classmethod
    def get_args(cls):
        parser = super().get_args()
        field: Argument
        for field in parser.args:
            if field.name == cls.FIELD_WEEK_ID.name:
                field.required = True
        return parser

    @classmethod
    def find(cls, needle: Optional[ResourceFilter] = None):
        """
        TODO: fetching indico bookings should get a dedicated component that would 
        get mocked during tests to produce a specific, Indico-compliant response
        """
        results = []
        week_id = cls.parse_args().get(cls.FIELD_WEEK_ID.name)
        ssn = current_app.db.session()
        week: CmsWeek
        week = ssn.query(CmsWeek).filter(CmsWeek.id == week_id).one()

        try:
            indico_bookings = IndicoBooking.get_bookings_between(
                current_app.db, current_app.config[OPT_NAME_INDICO_TOKEN], current_app.config[OPT_NAME_INDICO_SECRET], week.date, week.date_end)
        except Exception as e:
            indico_bookings = []
            log.error(f'Error fetching booking slots from Indico: {e}')

        fixed_bookings = IndicoBooking.get_virtual_bookings(
            current_app.db, week.id)

        for bucket, is_legit in [(indico_bookings, True), (fixed_bookings, False)]:
            entry: IndicoBooking
            for entry in bucket:
                results.append({
                    cls.FIELD_SOURCE.name: is_legit and 'indico' or 'fixed',
                    cls.FIELD_WEEK_ID.name: week_id,
                    cls.FIELD_START_TIME.name: entry.time_start,
                    cls.FIELD_END_TIME.name: entry.time_end,
                    cls.FIELD_ROOM_ID.name: entry.room.id
                })

        return results

    @classmethod
    def is_model_field_eligible_for_parser(cls, field: ModelFieldDefinition):
        return field.name == cls.FIELD_WEEK_ID.name
