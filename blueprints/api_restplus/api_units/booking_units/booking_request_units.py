import flask
from logging import Logger
import logging
from collections import defaultdict

from sqlalchemy.orm import joinedload
from blueprints.api_restplus.api_units.basics.model_field_types import DateTime
import sqlalchemy as sa
from sqlalchemy.orm.query import Query
from blueprints.api_restplus.api_units.basics.crud import Listonly
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import Any, Dict, List, Type, Optional
from flask_login import current_user
from blueprints.api_restplus.api_units.basics import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import ModelFactory
from icmsutils.businesslogic.roombookings import are_colliding
from blueprints.api_restplus.exceptions import IcmsException
from icms_orm.cmspeople import Person
from icms_orm.common import Person as Person2
from icms_orm.common import Person as IcmsPerson
from icms_orm.toolkit import CmsProject, Room
from icms_orm.toolkit import RoomRequest
from icms_orm.toolkit import RoomRequestConvener
from icms_orm.toolkit import EmailMessage
from icms_orm.toolkit import Status

log: Logger = logging.getLogger(__name__)


class BookingRequestApiCall(AbstractCRUDUnit):

    FIELD_ID = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.id).id().readonly(True).build()
    FIELD_CMS_ID_BY = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.cms_id_by).readonly(True).build()
    FIELD_CMS_ID_FOR = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.cms_id_for).required(True).build()
    FIELD_STATUS = ModelFieldDefinition.builder(ModelFactory.String).column(
        RoomRequest.status).readonly(True).enum(['done', 'pending', 'suspended', 'deleted']).build()
    FIELD_ROOM_ID = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.room_id).required(True).build()
    FIELD_ROOM_ID_PREFERRED = ModelFieldDefinition.builder(
        ModelFactory.Integer).column(RoomRequest.room_id_preferred).readonly(True).build()
    FIELD_START_TIME = ModelFieldDefinition.builder(
        DateTime).column(RoomRequest.time_start).required(True).build()
    FIELD_DURATION = ModelFieldDefinition.builder(
        ModelFactory.Integer).column(RoomRequest.duration).required(True).build()
    FIELD_WEEK_ID = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.cms_week_id).required(True).build()
    FIELD_TITLE = ModelFieldDefinition.builder(
        ModelFactory.String).column(RoomRequest.title).build()
    FIELD_PROJECT = ModelFieldDefinition.builder(
        ModelFactory.String).column(RoomRequest.project).build()
    FIELD_IS_WEBCAST = ModelFieldDefinition.builder(
        ModelFactory.Boolean).column(RoomRequest.webcast).build()

    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return [
            cls.FIELD_ID,
            cls.FIELD_CMS_ID_BY,
            cls.FIELD_CMS_ID_FOR,
            cls.FIELD_TITLE,
            cls.FIELD_PROJECT,
            cls.FIELD_IS_WEBCAST,
            cls.FIELD_START_TIME,
            cls.FIELD_DURATION,
            cls.FIELD_WEEK_ID,
            cls.FIELD_ROOM_ID_PREFERRED,
            cls.FIELD_ROOM_ID,
            ModelFieldDefinition.builder(ModelFactory.Integer).column(
                RoomRequest.capacity).required(False).build(),
            ModelFieldDefinition.builder(ModelFactory.String).column(
                RoomRequest.password).required(False).build(),
            ModelFieldDefinition.builder(ModelFactory.Boolean).column(
                RoomRequest.official).required(True).build(),
            ModelFieldDefinition.builder(ModelFactory.String).column(
                RoomRequest.agenda_url).build(),
            ModelFieldDefinition.builder(ModelFactory.String).column(
                RoomRequest.remarks).build(),
            ModelFieldDefinition.builder(ModelFactory.String).column(
                RoomRequest.reason).build(),
            cls.FIELD_STATUS,
            ModelFieldDefinition.builder(ModelFactory.DateTime).column(
                RoomRequest.time_created).readonly(True).build(),
            ModelFieldDefinition.builder(ModelFactory.DateTime).column(
                RoomRequest.time_updated).readonly(True).build(),
            ModelFieldDefinition(
                name='conveners',
                type=ModelFactory.List,
                nested_type=ModelFactory.Raw,
                required=False,
                default=[],
                example=[{'convener_cms_id': 1}],
            ),
        ]

    @classmethod
    def get_model_name(cls) -> str:
        return 'Booking Request'

    @classmethod
    def get_payload(cls):
        """
        injecting current user's CMS ID into the payload
        """
        super_payload = super().get_payload()
        super_payload[cls.FIELD_CMS_ID_BY.name] = current_user.cms_id
        super_payload[cls.FIELD_STATUS.name] = 'pending'
        super_payload[cls.FIELD_ROOM_ID_PREFERRED.name] = super_payload[cls.FIELD_ROOM_ID.name]
        return super_payload

    @classmethod
    def validate_mapper_before_persisting(cls: Type['BookingRequestApiCall'], mapper: Any):
        if cls.check_for_collisions(mapper):
            raise IcmsException(
                'Requested booking overlaps with another request for the same room.')

    @classmethod
    def check_for_collisions(cls: Type['BookingRequestApiCall'], request: RoomRequest):
        """
        TODO: narrow down the set of fetched requests for better performance, if need be
        TODO maybe: allow collisions with canceled / suspended bookings
        """
        requests: List[RoomRequest] = RoomRequest.query.filter(sa.and_(
            RoomRequest.cms_week_id == request.cms_week_id,
            RoomRequest.status != 'deleted',
            RoomRequest.status != 'suspended'
        )).all()
        for potential in requests:
            if are_colliding(potential, request) and potential.id != request.id:
                return True
        return False

    @classmethod
    def dispatch_notification(cls, request: Dict[str, Any], action, sender='Cms.Secretariat@cern.ch'):
        request_id = request.get(cls.FIELD_ID.name)
        status = request.get(cls.FIELD_STATUS.name)
        room_id = request.get(cls.FIELD_ROOM_ID.name, request.get(
            cls.FIELD_ROOM_ID_PREFERRED.name))
        owner_cms_id = request.get(cls.FIELD_CMS_ID_FOR.name,
                                   request.get(cls.FIELD_CMS_ID_BY.name))
        title = request.get(cls.FIELD_TITLE.name)
        start_time = request.get(cls.FIELD_START_TIME.name)
        duration = request.get(cls.FIELD_DURATION.name)
        # fetching conveners awkwardly so that the right role is used and privileges are not standing in the way (toolkit vs common)
        conveners: List[Person2] = [r[-1] for r in flask.current_app.db.session.query(RoomRequestConvener, Person2)
                                    .join(Person2, RoomRequestConvener.cms_id == Person2.cms_id)
                                    .filter(RoomRequestConvener.request_id == request_id).all()
                                    ]
        log.debug(f'Found {len(conveners)} conveners for booking {request_id}')

        if status == Status.SUSPENDED:
            log.debug(
                'Room request in suspended state. Suppressing the email notification.')
            return
        room = Room.query.filter(Room.id == room_id).one()
        owner = Person.query.filter(Person.cmsId == owner_cms_id).options(
            joinedload(Person.user)).one()

        args = {
            'request_owner': f'{owner.firstName} {owner.lastName}',
            'title': title,
            'request_status': status,
            'request_date': start_time.date(),
            'action': action,
            'request_time': start_time.time(),
            'request_duration': (lambda d: d % 60 == 0 and '%dh' % (d // 60) or '%dh %dm' % (d // 60, d % 60))(duration),
            'request_place': str(room),
            'conveners': [f'{p.first_name}, {p.last_name}' for p in conveners]
        }
        mail_body = flask.render_template('emails/room_request.txt', **args)
        EmailMessage.compose_message(sender=sender, bcc=sender, reply_to=sender, subject=f'Room request {action}', source_app='toolkit',
                                     to=owner.user.get_email(), body=mail_body, db_session=flask.current_app.db.session)
    
    @classmethod
    def expandQueryForConveners(cls: Type['AbstractCRUDUnit'], base_query: Query) -> Query:
        expanded_query = (
            base_query
            .add_columns(
                RoomRequestConvener.cms_id.label('convener_cms_id'),
                IcmsPerson.first_name.label('convener_first_name'),
                IcmsPerson.last_name.label('convener_last_name'),
            )
            .join(
                RoomRequestConvener,
                sa.and_(
                    RoomRequestConvener.request_id == RoomRequest.id,
                    RoomRequestConvener.active.is_(True)
                ),
                isouter=True
            )
            .join(IcmsPerson, RoomRequestConvener.cms_id == IcmsPerson.cms_id, isouter=True)
        )
        return expanded_query
    
    @classmethod
    def processConvenerResults(
        cls: Type['AbstractCRUDUnit'],
        query: Query
    ) -> List[Dict[str, Any]]:
        base_cols = super().get_db_columns()
        extra_fields = ['convener_cms_id', 'convener_first_name', 'convener_last_name']
        row_dicts = super().rows_to_list_of_dicts(query.all(), base_cols + extra_fields)
        base_fields = [col.key for col in base_cols]
        base_fields.append('_links')
        data_map = {}
        for row_dict in row_dicts:
            id = row_dict['id']
            extra_dict = {k: row_dict[k] for k in extra_fields}
            if id in data_map:
                data_map[id]['conveners'].append(extra_dict)
            else:
                data_map[id] = {k: row_dict[k] for k in base_fields}
                if extra_dict['convener_cms_id'] is not None:
                    data_map[id]['conveners'] = [extra_dict]
        data = [
            {'id': id, **info}
            for (id, info)
            in data_map.items()
        ]
        return data
    
    @classmethod
    def find(cls: Type['AbstractCRUDUnit'], needle: Optional[ResourceFilter] = None) -> List[Dict[str, Any]]:
        query = cls.expandQueryForConveners(super().get_query(resource_filter=needle))
        return cls.processConvenerResults(query)
    
    @classmethod
    def get(cls: Type['AbstractCRUDUnit'], needle: ResourceFilter) -> Dict[str, Any]:
        query = cls.expandQueryForConveners(super().get_query(resource_filter=needle))
        results = cls.processConvenerResults(query)
        assert len(results) == 1
        return results[0]
    
    @classmethod
    def post(cls) -> Dict[str, object]:
        payload = cls.get_payload()
        ia_dict = {
            f.column: payload.get(f.name)
            for f in cls.get_model_fields_list()
            if f.column is not None
        }
        mapper_class = cls.get_writable_mapper_classes().pop()
        mapper_object = mapper_class.from_ia_dict(ia_dict)
        cls.customize_mapper_before_persisting(mapper_object)
        cls.validate_mapper_before_persisting(mapper_object)
        session = cls.get_db_session()
        session.add(mapper_object)

        conveners = payload.get('conveners', [])
        for convener_dict in conveners:
            if not isinstance(convener_dict, dict) or 'convener_cms_id' not in convener_dict:
                raise IcmsException('Invalid convener data.')
            cms_id = convener_dict['convener_cms_id']
            if not isinstance(cms_id, int) or cms_id <= 0:
                raise IcmsException('Invalid convener CMS ID.')
            session.add(RoomRequestConvener(cms_id=cms_id, request=mapper_object))
        
        session.commit()        
        booking_data = cls.db_object_to_model_compliant_dict(mapper_object)
        cls.dispatch_notification(booking_data, 'created')
        return booking_data
    
    # TODO: Handle updating conveners in put_by_identity. Don't forget to dispatch notifications.

    @classmethod
    def put_by_identity(cls, *params):
        booking_data = super().put_by_identity(*params)
        cls.dispatch_notification(booking_data, 'updated')
        return booking_data


class BookingRequestStatusApiCall(AbstractCRUDUnit):

    FIELD_ID = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.id).id().readonly(True).build()
    FIELD_WEEK_ID = ModelFieldDefinition.builder(ModelFactory.Integer).column(
        RoomRequest.cms_week_id).readonly(True).build()
    FIELD_STATUS = ModelFieldDefinition.builder(ModelFactory.String).column(
        RoomRequest.status).enum(['done', 'pending', 'suspended', 'deleted']).build()

    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return [cls.FIELD_ID, cls.FIELD_WEEK_ID, cls.FIELD_STATUS]

    @classmethod
    def get_model_name(cls) -> str:
        return "Booking Request Status"

    @classmethod
    def put_by_identity(cls, *params):
        status_data = super().put_by_identity(*params)
        # to dispatch an email we need to refetch the info and pass the data through another unit's method
        # suboptimal but rarely executed and reuses existing code
        status = status_data.get(cls.FIELD_STATUS.name)
        status_action_map = {
            'done': 'approved',
            'deleted': 'canceled'
        }
        action = status_action_map.get(status, 'modified (status update)')
        request_data = BookingRequestApiCall.get_by_identity(
            status_data.get(cls.FIELD_ID.name))
        BookingRequestApiCall.dispatch_notification(request_data, action)
        return status_data


class BookingProjectApiCall(AbstractCRUDUnit, Listonly):
    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return [
            ModelFieldDefinition.builder(ModelFactory.Boolean).column(
                CmsProject.official).build(),
            ModelFieldDefinition.builder(ModelFactory.String).column(
                CmsProject.status).build(),
            ModelFieldDefinition.builder(
                ModelFactory.String).column(CmsProject.code).id().build(),
            ModelFieldDefinition.builder(
                ModelFactory.String).column(CmsProject.name).build(),
        ]

    @classmethod
    def get_model_name(cls) -> str:
        return "Booking Project"
