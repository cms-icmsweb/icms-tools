
from asyncio.log import logger
from werkzeug.exceptions import Conflict, NotFound, InternalServerError
from flask_login import current_user
import json

from blueprints.api_restplus.api_factory import handle_errors, SingletonApiFactory
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from blueprints.api_restplus.exceptions import BadRequestException
from blueprints.api_restplus.api_units.egroup_api_units import EGroup, _getMemberListFromPayload, defaultProperties

from util.EgroupHandler import EgroupHandler

import logging

# do not logger.info(very long) replies from the SOAP service
ztl = logging.getLogger("zeep.transports")
ztl.setLevel(logging.WARNING)

cadiArcProperties = {
            'mailProps': { 'MailPostingRestrictions' : { 
                                'PostingRestrictions' : 'OwnerAdminsAndOthers', # this allows Owners, admins, and the members of the eGroup to send mails
                                'OtherRecipientsAllowedToPost': [
                                        {
                                            'Type': 'StaticEgroup',
                                            'Name': 'service-cds-mail-robots',
                                        },
                                    ],
                            },
                            'SenderAuthenticationEnabled' : False,
                            'WhoReceivesDeliveryErrors'   :  'Sender',
                            'MaxMailSize'                 : 10,
                            'ArchiveProperties'           : 'DoesNotExist',
                        }, 
            'privacyProps': { 'Privacy' : 'Members',
                              'Selfsubscription' : 'Closed',
                            },
            'admin_egroup': 'cms-web-access-admins',
        }


class EgroupCADIApiCall(BaseApiUnit):
    _model = ModelFactory.make_model( 'EGroup CADI Info', {
        EGroup.group_name: ModelFactory.String(),
        EGroup.hr_ids: ModelFactory.String(),
        EGroup.egroups: ModelFactory.String(),
        EGroup.accounts: ModelFactory.String(),
        EGroup.emails: ModelFactory.String(),
        'admin_egroup': ModelFactory.String(),
        'privacy_info': ModelFactory.String(),
        'email_properties': ModelFactory.String(),
    } )
    _response_model = ModelFactory.make_model( 'EGroup CADI Response Info', {
        'status': ModelFactory.String(),
        'payload': ModelFactory.String(),
    } )

    # for the start, we try to simplify the work on the Java/JSP/CADI side by simply passing 
    # strings for the arguments and do the parsing here. In the longer term, there should also 
    # be to communicate properly via JSON (e.g. for other services and/or scripts)
    _args = (ParserBuilder()
                .add_argument('group_name', type=ParserBuilder.STRING, required=True, default=None)
                .add_argument('hr_ids', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('egroups', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('accounts', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('emails', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('admin_egroup', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('privacy_info', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('email_properties', type=ParserBuilder.STRING, required=False, default=None)
                .parser)

    _api = SingletonApiFactory.get_api()


    @classmethod
    def _createEgroupWithMembers(cls, payload):
        egName   = payload.get('group_name', None)
        logger.info( f'request to create eGroup {egName}:' )
        
        privPropInfo = payload.get( "privacy_info" )
        mailPropInfo = payload.get( "email_properties" )
        adminEgroupInfo = payload.get( "admin_egroup" )

        mailProps = defaultProperties['mailProps']
        privacyProps = defaultProperties['privacyProps']
        adminEgroup = defaultProperties['admin_egroup']

        if privPropInfo == 'cadiARC':
            privacyProps = cadiArcProperties['privacyProps']
        if mailPropInfo == 'cadiARC':
            mailProps = cadiArcProperties['mailProps']
        if adminEgroupInfo: 
            adminEgroup = adminEgroupInfo

        logger.info( f'\n mailProps: {mailProps}\n')

        hrIds, eGroups, accounts, emails = ([], [], [], [])
        try:
            egName_foo, hrIds, eGroups, accounts, emails = _getMemberListFromPayload(cls, payload)
        except BadRequestException as e:
            # ignore case that eGroup was created w/o members:
            if 'missing parameters: at least one of "hr_ids",' not in str(e):
                raise e
            egName_foo = egName # set default in case ...
        finally:
            if egName != egName_foo:
                msg = f'ERROR: found different eGroup name in payload: found {egName_foo} for {egName} '
                logger.error( f'post_egroup> {msg}' )
                raise InternalServerError(msg)

        logger.info( f'extracted list of requested members from payload: {len(hrIds)}, {len(eGroups)}, {len(accounts)}, {len(emails)} ... ' )

        try:
            status, msg = EgroupHandler().newGroup(egName, description='CADI created egroup via the iCMS API -- testing for the moment', mailProps=mailProps, privacy=privacyProps, adminEgroup=adminEgroup)
            logger.info( f'\n - result: {status} - {msg}')
        except Conflict as e:
            raise e
        except Exception as e:
            # logger.info( f'Ignoring the Exception I just caught: str(e)')
            raise e

        logger.info( f'adding requested members from payload: {len(hrIds)}, {len(eGroups)}, {len(accounts)}, {len(emails)}' )

        # check if there is any type of member requested in the call, if so, add them:
        if hrIds or eGroups or accounts or emails:
            status, msg = EgroupHandler().addMembersBulk(egName, { 'hr_ids'  : hrIds,
                                                               'egroups' : eGroups,
                                                               'accounts': accounts,
                                                               'emails'  : emails,
                                                            }                                                     )
            logger.info( f'\n++> bulk addition of members returned: {status} - {msg}')
        return egName

    @classmethod
    def post_cadi_egroup(cls):

        payload = cls.get_payload()        
        logger.warning( f'\nfound payload: {payload}')
        args = cls.parse_args()
        logger.warning( f'\nfound args   : {args}')
        if not payload:
            msg = f'ERROR> missing parameters: no payload found in request.'
            logger.error(msg)
            raise BadRequestException(msg)

        logger.info( f'\n==post_cadi_egroup==> payload: {payload} \n')

        egName = cls._createEgroupWithMembers(payload)
        res = EgroupHandler().getGroupMembers( egName )
        logger.debug( f'\ngot: {res}\n')

        # res[1] is the list of Persons w/o a primary account/ID/Email
        result = { 'status': 'OK', 'payload': ','.join( res[0]+res[2]+res[3]+res[4] ) }

        logger.debug( f'\nreturning: {result}\n')

        return result

