from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from icms_orm.common import MO, MoStatusValues as MoStatus, Person, FundingAgency as FA, Affiliation, PersonStatus, Assignment
from datetime import date
from flask_login import current_user
import sqlalchemy as sa
from icms_orm.cmspeople import Person as PeopleData, MoData


class MoListCall(BaseApiUnit):
    _cols_dict = {
        Person.cms_id: ModelFactory.Integer,
        Person.first_name: ModelFactory.String,
        Person.last_name: ModelFactory.String,
        Affiliation.inst_code: ModelFactory.String,
        MO.year: ModelFactory.Integer,
        MO.status: ModelFactory.String,
        MO.inst_code.label('mo_inst_code'): ModelFactory.String,
        FA.name.label('fa_name'): ModelFactory.String
    }
    _model = ModelFactory.make_model('MO List Entry', _cols_dict)
    _args = ParserBuilder().\
        add_argument(MO.year.key, ParserBuilder.INTEGER, True).\
        add_argument(MO.cms_id.key, ParserBuilder.INTEGER, False).\
        add_argument(MO.inst_code.key, ParserBuilder.STRING, False).\
        add_argument(MO.fa_id.key, ParserBuilder.INTEGER, False).\
        parser

    @classmethod
    def get(cls):
        _cols = list(cls._cols_dict.keys())
        q = MO.session().query(*_cols).\
            join(Person, MO.cms_id == Person.cms_id).\
            join(Affiliation, sa.and_(
                Person.cms_id == Affiliation.cms_id,
                Affiliation.is_primary == True,
                Affiliation.start_date <= date.today()
            ), isouter=True).\
            join(FA, MO.fa_id == FA.id, isouter=True).\
            join(PersonStatus, sa.and_(PersonStatus.cms_id == Person.cms_id, PersonStatus.start_date <= date.today()),
                 isouter=True). \
            order_by(MO.cms_id, sa.desc(MO.timestamp), sa.desc(Affiliation.start_date), sa.desc(PersonStatus.start_date)).\
            distinct(MO.cms_id)

        q = cls.attach_query_filters(q, cls.parse_args(), rules=[MO.year, MO.cms_id, MO.inst_code, MO.fa_id])
        return cls.rows_to_list_of_dicts(q.all(), _cols)


class MoStepsCall(BaseApiUnit):
    _cols_dict = {
        MO.id: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.READONLY: True
        }),
        MO.cms_id: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        MO.year: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        MO.status: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        MO.timestamp: ModelFactory.field_proxy(ModelFactory.DateTime, {
            ModelFactory.READONLY: True
        }),
        MO.set_by.label('writer_cms_id'): ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.READONLY: True
        }),
        Person.first_name.label('writer_first_name'): ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        Person.last_name.label('writer_last_name'): ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        })
    }

    _model = ModelFactory.make_model('MO Step', _cols_dict)

    _args = ParserBuilder().\
        add_argument(MO.cms_id.key, ParserBuilder.INTEGER, True).\
        add_argument(MO.year.key, ParserBuilder.INTEGER, True).\
        parser

    @classmethod
    def _do_get(cls, args_dict):
        """
        Centralising the retrieval from DB for the get and post methods
        """
        q = MO.session().query(*list(cls._cols_dict.keys())).join(Person, MO.set_by == Person.cms_id, isouter=True).\
            order_by(sa.desc(MO.timestamp))
        q = cls.attach_query_filters(q, args_dict, rules=[MO.id, MO.cms_id, MO.year])
        return q.all()

    @classmethod
    def get(cls):
        return cls.rows_to_list_of_dicts(cls._do_get(cls.parse_args()), list(cls._cols_dict.keys()))

    @classmethod
    def post(cls):
        """
        This will require quite a heavy validation in terms of:
            - who wants to perform the action
            - what action is about to be perfomed

        New status can be created only if there is no status yet, values are:
            - anything free
            - proposed

        Certain status entries require certain predecessors, like:
            - approved, rejected (req. proposed)

        Some others will need another endpoint, like moving transferring mo to another person
            - swapped out (req. approved)
            - swapped in (req. nothing)
        """

        payload = cls.get_payload()
        # let's create the destination DB row (without adding to the DB for now)
        mo_record = MO.from_ia_dict({MO.set_by: current_user.cms_id})
        cls.update_db_object(mo_record, payload, rules=cls._cols_dict.keys(), defer_commit=True)
        cls._validate_new_mo_record(mo_record)
        MO.session().add(mo_record)
        MO.session().commit()
        return cls.row_to_dict(cls._do_get({MO.id: mo_record.id})[0], list(cls._cols_dict.keys()))

    @classmethod
    def _validate_new_mo_record(cls, mo_record):
        # TODO: now the validation should happen - exceptions to be raised when in trouble.

        pass


class MoProjectListCall(BaseApiUnit):
    """
    Overview of PhDs to assist with M&O-B calculations.

    Uses the old DB to fetch the members with PHDMO for the given year alongside their
     project.
    """
    _cols_dict = {
        PeopleData.cmsId: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.READONLY: True
        }),
        PeopleData.firstName: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        PeopleData.lastName: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        PeopleData.instCode: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        PeopleData.project: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        'moYear': ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.READONLY: True
        }),
        'fundingAgency': ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
    }
    _model = ModelFactory.make_model('MO Project List', _cols_dict)

    _args = (
        ParserBuilder()
        .add_argument('mo_year', ParserBuilder.INTEGER, True)
        .parser
    )

    @classmethod
    def get(cls):
        args_dict = cls.parse_args()
        mo_year = args_dict['mo_year']
        phd_mo_col = MoData.get_phd_mo_column_for_year(mo_year)
        fa_col = MoData.get_phd_fa_column_for_year(mo_year)
        _cols_out = [
            PeopleData.cmsId,
            PeopleData.firstName,
            PeopleData.lastName,
            PeopleData.instCode,
            PeopleData.project,
            fa_col.label('fundingAgency'),
        ]
        q = (
            PeopleData.session()
            .query(*_cols_out)
            .join(MoData, MoData.cmsId == PeopleData.cmsId)
            .filter(phd_mo_col == 'YES')
            .order_by(PeopleData.cmsId)
        )
        item_list = cls.rows_to_list_of_dicts(q.all(), _cols_out)

        # now check the new table of project assignments to see who has multiple 
        # projects assigned and update the results for those
        qA = (Assignment.session()
              .query(Assignment.cms_id, Assignment.project_code, Assignment.fraction)
              .filter( Assignment.end_date.is_(None) )
              .filter( Assignment.fraction > 0. )
              .order_by(Assignment.cms_id, Assignment.fraction.desc())
        )
        multiProjList = {}   # [ {x[0]: f'{x[1]}: {x[2]}'} for x in qA.all() ]
        for cms_id, proj, frac in qA.all():
            if cms_id not in multiProjList.keys(): multiProjList[ cms_id ] = ''
            if multiProjList[ cms_id ] != '' : multiProjList[ cms_id ] += ' - ' 
            multiProjList[ cms_id ] += f'{proj}' ## for now only show project names, fractions are fixed at 50/50

        for item in item_list:
            item['moYear'] = mo_year
            if item['cmsId'] in multiProjList.keys():
                item['project'] = multiProjList[ item['cmsId'] ]
        return item_list
