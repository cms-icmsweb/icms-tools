import flask_login
from icms_orm.cmspeople import Person, Institute, MemberActivity
from util.trivial import current_db_ssn
from flask_login import current_user
from sqlalchemy import or_
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from blueprints.api_restplus.logic.teamleaders_logic import members_epr_change_suspension as change_suspension

class SuspensionsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model('Person Suspension Status', {
        Person.cmsId.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        Person.instCode.key: ModelFactory.String,
        Person.lastName.key: ModelFactory.String,
        Person.firstName.key: ModelFactory.String,
        MemberActivity.name.key: ModelFactory.String,
        Person.isAuthor.key: ModelFactory.Boolean,
        Person.isAuthorSuspended.key: ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.REQUIRED: True
        }),
    })

    @classmethod
    def do_get_people(cls, cmsId=None):
        ssn = current_db_ssn()
        cols =[Person.cmsId, Person.instCode, Person.lastName, Person.firstName, MemberActivity.name, Person.isAuthorSuspended, Person.isAuthor]
        q = ssn.query(*cols).join(MemberActivity, Person.activityId == MemberActivity.id)

        #Admins can see users of every institute
        if not current_user.is_admin():
            inst_codes = flask_login.current_user.get_represented_institutes_codes()
            q = q.filter(Person.instCode.in_(inst_codes))
        
        if cmsId is not None:
            q = q.filter(Person.cmsId == cmsId)
            return cls.row_to_dict (q.one(), cols)
        else:
            return cls.rows_to_list_of_dicts(q.all(), cols)

    @classmethod
    def get_people(cls):
        return cls.do_get_people()

class EditSuspensionStatusApiCall(SuspensionsApiCall):

    @classmethod
    def edit_status(cls):
        payload = cls.get_payload()
        change_suspension(current_db_ssn(), [payload['cmsId']], current_user.person.cmsId, payload['isAuthorSuspended'] == True)
        return cls.do_get_people(payload['cmsId'])
