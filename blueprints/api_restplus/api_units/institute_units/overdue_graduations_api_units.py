from blueprints.users.user_class import User
from datetime import date
from sqlalchemy import or_
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from models.icms_people import Person, MemberActivity, PersonHistory
from models.icms_toolkit import ActivityCheck
from flask_login import current_user
from util import constants
from blueprints.api_restplus.exceptions import AccessViolationException
import logging

_activity_check_model = ModelFactory.make_model('Student Activity Check Update', {
    'cmsId': ModelFactory.field_proxy(ModelFactory.Integer, {
        ModelFactory.REQUIRED: True
    }),
    'status': ModelFactory.field_proxy(ModelFactory.String, {
        ModelFactory.REQUIRED: True
    }),
    'checkerCmsId': ModelFactory.field_proxy(ModelFactory.Integer, {
        ModelFactory.REQUIRED: False
    }),
    'checkerName': ModelFactory.field_proxy(ModelFactory.String, {
        ModelFactory.REQUIRED: False
    })
})


class OverdueGraduationsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model('Long Lasting Student Info', {
        Person.cmsId.key: ModelFactory.Integer,
        Person.instCode.key: ModelFactory.String,
        Person.lastName.key: ModelFactory.String,
        Person.firstName.key: ModelFactory.String,
        Person.status.key: ModelFactory.String,
        Person.isAuthor.key: ModelFactory.Boolean,
        Person.dateCreation.key: ModelFactory.Date,
        'isPHD': ModelFactory.Boolean,
        'lastChangeDate': ModelFactory.Date,
        'years': ModelFactory.String,
        'canEdit': ModelFactory.Boolean,

        'activityCheck': ModelFactory.Nested(_activity_check_model)
    })

    @classmethod
    def get_overdue_graduations(cls):
        inst_code = None if cls.access_all_institutes() else current_user.person.instCode
        years = 3

        students = []
        ssn = Person.session
        q = ssn.query(Person).join(MemberActivity,
                                   Person.activityId == MemberActivity.id)
        q = q.filter(or_(
            (MemberActivity.name == 'Doctoral Student'),
            (MemberActivity.name == 'Non-Doctoral Student')
        )).filter(Person.status.like('CMS%')).join(PersonHistory).order_by(Person.instCode, Person.lastName)

        if inst_code is not None:
            q = q.filter(Person.instCode == inst_code)

        ppl = q.all()
        cms_ids = {p.cmsId for p in ppl}

        acs = ssn.query(ActivityCheck).filter(
            ActivityCheck.for_cms_id.in_(cms_ids)).all()
        acs = {ac.for_cms_id: ac for ac in acs}

        tuples = [(p, acs[p.cmsId] if p.cmsId in acs else None) for p in ppl]

        # ActivityCheck has no FGN keys to Person so selecting independently
        cms_ids = {r[0]
                   for r in ActivityCheck.session.query(ActivityCheck.by_cms_id).all()}
        check_authors = {x.cmsId: x for x in ActivityCheck.session.query(
            Person).filter(Person.cmsId.in_(cms_ids)).all()}

        for person, activity_check in tuples:
            student = cls.create_student_row(
                person, activity_check, years, check_authors)
            if student:
                students.append(student)

        return students

    @classmethod
    def access_all_institutes(cls):
        return current_user.is_admin() or \
            current_user.has_flag(constants.FLAG_SPOKESPERSON_AND_DEPUTIES) or \
            current_user.is_engagement_office_member()

    @classmethod
    def create_student_row(cls, person, activity_check, years, check_authors):
        activity_changes = person.history.get_activity_changes()
        last_change_date = activity_changes[0][1] if activity_changes else None
        days_as_phd = (
            date.today() - (last_change_date or person.dateCreation.date())).days
        if days_as_phd < years * 365:
            return None

        is_phd = True if person.activity.get(
            MemberActivity.name) == 'Doctoral Student' else False
        return {
            Person.cmsId.key: person.get(Person.cmsId),
            Person.instCode.key: person.get(Person.instCode),
            Person.lastName.key: person.get(Person.lastName),
            Person.firstName.key: person.get(Person.firstName),
            Person.status.key: person.get(Person.status),
            Person.isAuthor.key: person.get(Person.isAuthor),
            Person.dateCreation.key: person.get(Person.dateCreation),
            'isPHD': is_phd,
            'lastChangeDate': last_change_date,
            'years': '%.2f' % (days_as_phd / 365.0),
            'activityCheck': cls.format_activity_check(activity_check, check_authors),
            'canEdit': cls.user_has_editting_permission(person)
        }

    @classmethod
    def user_has_editting_permission(cls, person: Person):
        user: User
        user = current_user._get_current_object()
        return person.cmsId in user.team_subordinate_cms_ids or user.is_admin() or user.person.cmsId == person.cmsId

    @classmethod
    def format_activity_check(cls, activity_check, check_authors):
        if(not activity_check):
            return None

        return {
            'status': 'confirmed' if activity_check.confirmed else 'declined',
            'checkerCmsId': activity_check.by_cms_id,
            'checkerName': '{} {}'.format(
                check_authors[activity_check.by_cms_id].lastName,
                check_authors[activity_check.by_cms_id].firstName
            )
        }


class ConfirmStudentStatusApiCall(OverdueGraduationsApiCall):

    _model = _activity_check_model

    @classmethod
    def confirm_status(cls):
        payload = cls.get_payload()
        if not payload['status']:
            confirm = 0
        else:
            confirm = 1 if payload['status'] == 'confirmed' else -1

        session = ActivityCheck.session
        person = session.query(Person).filter(
            Person.cmsId == payload['cmsId']).one()
        check = session.query(ActivityCheck).filter(ActivityCheck.for_cms_id == person.cmsId). \
            order_by(ActivityCheck.timestamp.desc()).first()

        # if student tries to override someone else's input, forbid, warn and encourage to take contact with support
        if current_user.person.cmsId == person.cmsId and check is not None and check.by_cms_id != person.cmsId:
            logging.warn(
                'Student %d attempted to override ActivityCheck info set by %d. [Confirm is: %s, attempted to set: %s]' %
                (person.cmsId, check.by_cms_id, str(check.confirmed), str(form.confirm.data)))
            raise AccessViolationException("The input has already been provided by an institute representative and unfortunately you cannot override it. "
                                           + "Should there be a mismatch, please clarify with your institute representative or contact icms-support@cern.ch to report the problem.")

        if confirm == 0:
            if check:
                session.delete(check)
                session.commit()

                return {
                    'status': None,
                    'checkerCmsId': None,
                    'checkerName': None
                }
        else:
            if not check:
                ActivityCheck(confirmed=(confirm == 1), by_cms_id=current_user.get_id(),
                              for_cms_id=person.cmsId, for_activity=person.activityId)
            else:
                check.confirmed = (confirm == 1)
                check.by_cms_id = current_user.get_id()
                check.for_cms_id = person.cmsId
                check.for_activity = person.activityId

        return {
            'status': payload['status'],
            'checkerCmsId': current_user.person.cmsId,
            'checkerName': '{} {}'.format(
                current_user.person.lastName,
                current_user.person.firstName
            )
        }
