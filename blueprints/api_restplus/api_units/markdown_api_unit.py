from icms_orm.common import Tenure, Position, PositionName, OrgUnit, OrgUnitType, OrgUnitTypeName, Person, Affiliation
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from datetime import date, datetime
import sqlalchemy as sa
from icms_orm.common import ImageUpload, MarkdownNote


class MarkdownApiCall(BaseApiUnit):
    _model = ModelFactory.make_model('Markdown Note', {
        MarkdownNote.id.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False
        }),
        MarkdownNote.name.key: ModelFactory.field_proxy(ModelFactory.String,{
            ModelFactory.REQUIRED: True
        }),
        MarkdownNote.body.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        })
    })

    _args = ParserBuilder().\
        add_argument(MarkdownNote.id.key, required=False, type=ParserBuilder.INTEGER).\
        add_argument(MarkdownNote.name.key, required=False, type=ParserBuilder.STRING).\
        parser

    @classmethod
    def get_list(cls):
        params = cls.parse_args()
        _cols = [MarkdownNote.id, MarkdownNote.name]
        q = MarkdownNote.session().query(*_cols)
        q = cls.attach_query_filters(q, params, rules=[
            (MarkdownNote.id, None, None),
            (MarkdownNote.name, None, BaseApiUnit.ProcessorFunctions.wildcard_match)
        ])
        return cls.rows_to_list_of_dicts(q.all(), _cols)

    @classmethod
    def get_note(cls):
        params = cls.parse_args()
        q = MarkdownNote.session().query(MarkdownNote)
        q = cls.attach_query_filters(query=q, params_map=params, rules=[(MarkdownNote.id, None, None), (MarkdownNote.name, None, None)])
        return q.one().to_dict()

    @classmethod
    def post_note(cls):
        payload = cls.get_payload()
        _sent_id = payload.get(MarkdownNote.id.key)
        _ssn = MarkdownNote.session()
        _note = MarkdownNote.from_ia_dict({}) if _sent_id is None else _ssn.query(MarkdownNote).filter(
            MarkdownNote.id == _sent_id).one()
        cls.update_db_object(_note, payload=payload, rules=[(MarkdownNote.name, None, None), (MarkdownNote.body, None, None)])
        return _note.to_dict()

