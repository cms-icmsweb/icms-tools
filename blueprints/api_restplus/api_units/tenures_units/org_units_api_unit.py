from icms_orm.common import Tenure, Position, PositionName, OrgUnit, OrgUnitType, OrgUnitTypeName, Person, Affiliation
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from blueprints.api_restplus.exceptions import IcmsException
import sqlalchemy as sa


class OrgUnitsApiCall(BaseApiUnit):

    _args = ParserBuilder(). \
        add_argument(OrgUnit.id.key, type=ParserBuilder.INTEGER, required=False, default=None). \
        add_argument(OrgUnit.domain.key, type=ParserBuilder.STRING, required=False, default=None). \
        add_argument('type', type=ParserBuilder.STRING, required=False, default=None,
                     choices=[_v.lower() for _v in OrgUnitTypeName.values()]). \
        add_argument(OrgUnit.superior_unit_id.key, type=ParserBuilder.INTEGER, required=False, default=None). \
        add_argument(OrgUnit.is_active.key, type=ParserBuilder.BOOLEAN, required=False, default=None). \
        add_argument('min_unit_level', type=ParserBuilder.INTEGER, required=False, default=None). \
        add_argument('max_unit_level', type=ParserBuilder.INTEGER, required=False, default=None). \
        parser

    _model = ModelFactory.make_model('Org Unit Info', {
        OrgUnit.id.key: ModelFactory.Integer,
        OrgUnit.domain.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: 'ECAL',
            ModelFactory.DESCRIPTION: 'Examples can include sub detector names, names of committes, boards etc.'
        }),
        'type': ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.ENUM: [_v.lower() for _v in OrgUnitTypeName.values()]
        }),
        OrgUnitType.level.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.READONLY: True,
        }),
        OrgUnit.is_active.key: ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.REQUIRED: False,
            ModelFactory.DEFAULT: True
        }),
        OrgUnit.superior_unit_id.key: ModelFactory.NullableInteger,
        OrgUnit.enclosing_unit_id.key: ModelFactory.NullableInteger,
        OrgUnit.outermost_unit_id.key: ModelFactory.NullableInteger,
        'dependent_units': ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.ELEMENT_TYPE: ModelFactory.Integer,
            ModelFactory.DEFAULT: [],
            ModelFactory.EXAMPLE: [],
            ModelFactory.DESCRIPTION: 'List of dependent unit IDs',
            ModelFactory.READONLY: True
        })
    })

    @classmethod
    def _do_get_units(cls, kwargs, one_row_only=False):
        ssn = OrgUnit.session()
        sq = ssn.query(
            sa.func.array_agg(OrgUnit.id).label('dependent_units'),
            OrgUnit.superior_unit_id.label('unit_id')
        ).group_by(OrgUnit.superior_unit_id).subquery()

        _cols = [OrgUnit.id, OrgUnit.domain, OrgUnitType.name.label('type'), OrgUnit.superior_unit_id,
                 sq.c.dependent_units, OrgUnit.is_active, OrgUnitType.level, OrgUnit.outermost_unit_id, OrgUnit.enclosing_unit_id]

        q = ssn.query(*_cols).join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).join(sq, sq.c.unit_id == OrgUnit.id,
                                                                                        isouter=True)
        q = cls.attach_query_filters(q, kwargs, rules=[
            (OrgUnit.id, None, None),
            (OrgUnit.domain, None, None),
            (OrgUnit.superior_unit_id, None, None),
            (OrgUnitType.name, 'type', None),
            (OrgUnit.is_active, None, None)
        ])

        if kwargs.get('max_unit_level', None) is not None:
            q = q.filter(OrgUnitType.level <= kwargs.get('max_unit_level'))
        if kwargs.get('min_unit_level', None) is not None:
            q = q.filter(OrgUnitType.level >= kwargs.get('min_unit_level'))

        if one_row_only:
            return cls.row_to_dict(q.one(), cols_list=_cols)
        return cls.rows_to_list_of_dicts(q.all(), cols_list=_cols)

    @classmethod
    def get_units(cls):
        return cls._do_get_units(cls.parse_args())

    @classmethod
    def get_unit(cls):
        return cls._do_get_units(cls.parse_args(), one_row_only=True)

    @classmethod
    def post_unit(cls):
        payload = cls.get_payload()
        ssn = OrgUnit.session()
        _type_map = {_r[0].lower(): _r[1] for _r in ssn.query(OrgUnitType.name, OrgUnitType.id).all()}
        unit = ssn.query(OrgUnit).filter(OrgUnit.id == payload.get(OrgUnit.id.key)).one() if OrgUnit.id.key in payload \
            else OrgUnit.from_ia_dict({})
        cls.update_db_object(target=unit, payload=payload, rules=[
            (OrgUnit.superior_unit_id, None, None),
            (OrgUnit.type_id, 'type', lambda x: _type_map.get(x)),
            (OrgUnit.domain, None, None),
            (OrgUnit.enclosing_unit_id, None, None),
            (OrgUnit.outermost_unit_id, None, None),
            (OrgUnit.is_active, None, None)
        ])
        assert isinstance(unit, OrgUnit)
        if unit.superior_unit_id is not None and unit.superior_unit_id == unit.id:
            raise IcmsException('Unit cannot be its own superior unit!')
        return cls._do_get_units({OrgUnit.id.key: unit.get(OrgUnit.id)})[0]
