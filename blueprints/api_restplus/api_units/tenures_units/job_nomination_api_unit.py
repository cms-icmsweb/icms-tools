from typing import Any, List
from logging import Logger
import logging
from icms_orm.common import (
    Position,
    Person,
    Affiliation,
    Assignment,
    Project,
    OrgUnit,
    OrgUnitType,
    Institute,
    Country,
    Region,
)
from icms_orm.toolkit import (
    JobOpening,
    JobOpenPosition,
    JobNomination,
    JobNominationStatus,
    JobQuestionnaire,
    EmailMessage,
)
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.api_restplus.exceptions import (
    AccessViolationException,
    BadRequestException,
    NotFoundException,
)
from datetime import date, datetime, timedelta
import sqlalchemy as sa
from sqlalchemy import desc, or_
from sqlalchemy.sql.expression import literal_column
import flask
import sqlalchemy as sa
import flask_login
from flask_login import current_user
from flask import abort
from sqlalchemy.orm import aliased
from util import constants as const
from sqlalchemy import func, select

log: Logger = logging.getLogger(__name__)


class JobNominationApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "JobNomination Info",
        {
            JobNomination.nomination_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobOpenPosition.job_opening_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobOpenPosition.position_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobOpenPosition.job_unit_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            "position_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "unit_domain": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "unit_type": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "project_codes": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "job_id": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobOpening.title.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: "Spokesperson",
                },
            ),
            JobOpening.questionnaire_uri.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: "link to questionnaire",
                },
            ),
            JobOpening.questionnaire_id: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobQuestionnaire.questions: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobNomination.nominee_id: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Person.first_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Person.last_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Person.cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Person.email.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Affiliation.inst_code.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {ModelFactory.READONLY: True, ModelFactory.EXAMPLE: "CERN"},
            ),
            Institute.country_code.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {ModelFactory.READONLY: True, ModelFactory.EXAMPLE: "CH"},
            ),
            Country.region_code.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {ModelFactory.READONLY: True, ModelFactory.EXAMPLE: "CH"},
            ),
            "region_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {ModelFactory.READONLY: True, ModelFactory.EXAMPLE: "CERN"},
            ),
            Project.code.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.READONLY: True,
                    ModelFactory.EXAMPLE: ["DAQ", "BRIL", "HGCAL"],
                },
            ),
            JobNomination.questionnaire_answers.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobNominationStatus.actor_remarks.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            "statuses": ModelFactory.List(
                ModelFactory.Nested(
                    ModelFactory.make_model(
                        "Job nomination status Info",
                        {
                            JobNominationStatus.id.key: ModelFactory.field_proxy(
                                ModelFactory.Integer,
                                {
                                    ModelFactory.READONLY: True,
                                },
                            ),
                            JobNominationStatus.actor_id.key: ModelFactory.field_proxy(
                                ModelFactory.Integer,
                                {
                                    ModelFactory.READONLY: True,
                                },
                            ),
                            JobNominationStatus.nomination_id.key: ModelFactory.field_proxy(
                                ModelFactory.Integer,
                                {
                                    ModelFactory.READONLY: True,
                                },
                            ),
                            JobNominationStatus.actor_remarks.key: ModelFactory.field_proxy(
                                ModelFactory.String,
                                {
                                    ModelFactory.REQUIRED: False,
                                },
                            ),
                            JobNominationStatus.status.key: ModelFactory.field_proxy(
                                ModelFactory.String,
                                {
                                    ModelFactory.REQUIRED: False,
                                },
                            ),
                            JobNominationStatus.last_modified.key: ModelFactory.field_proxy(
                                ModelFactory.DateTime,
                                {
                                    ModelFactory.READONLY: False,
                                },
                            ),
                            Person.first_name.key: ModelFactory.field_proxy(
                                ModelFactory.String,
                                {
                                    ModelFactory.REQUIRED: False,
                                },
                            ),
                            Person.last_name.key: ModelFactory.field_proxy(
                                ModelFactory.String,
                                {
                                    ModelFactory.REQUIRED: False,
                                },
                            ),
                            Person.email.key: ModelFactory.field_proxy(
                                ModelFactory.String,
                                {
                                    ModelFactory.REQUIRED: False,
                                },
                            ),
                        },
                    )
                )
            ),
        },
    )

    _args = (
        ParserBuilder()
        .add_appendable_argument(
            "only_active", type=ParserBuilder.BOOLEAN, required=False, default=None
        )
        .add_argument(
            "for_nominee_selection",
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=False,
        )
        .parser
    )

    @classmethod
    def get_all_nominations(cls):
        args = cls.parse_args()
        q_cols = [
            JobNomination.nomination_id,
            JobNomination.open_position_id,
            JobOpening.id.label("job_id"),
            JobOpening.title,
            JobOpening.questionnaire_uri,
            JobOpenPosition.position_id,
            JobOpenPosition.job_unit_id,
            JobNomination.nominee_id,
            JobNomination.questionnaire_answers,
            Person.first_name,
            Person.last_name,
            Person.cms_id,
            Person.email,
            Affiliation.inst_code,
            Institute.country_code,
            Country.region_code,
            Region.name.label("region_name"),
            Position.name.label("position_name"),
            OrgUnit.domain.label("unit_domain"),
            OrgUnitType.name.label("unit_type"),
            Project.code.label("project_codes"),
        ]

        q = (
            JobNomination.session()
            .query(
                JobNomination.nomination_id.label(
                    "toolkit_job_nomination_nomination_id"
                ),
                JobNomination.open_position_id.label(
                    "toolkit_job_nomination_open_position_id"
                ),
                JobOpening.id.label("toolkit_job_opening_id"),
                JobOpening.title.label("toolkit_job_opening_title"),
                JobOpening.questionnaire_uri.label(
                    "toolkit_job_opening_questionnaire_uri"
                ),
                JobOpenPosition.position_id.label(
                    "toolkit_job_open_position_position_id"
                ),
                JobOpenPosition.job_unit_id.label(
                    "toolkit_job_open_position_job_unit_id"
                ),
                JobNomination.nominee_id.label("toolkit_job_nomination_nominee_id"),
                JobNomination.questionnaire_answers.label(
                    "toolkit_job_nomination_questionnaire_answers"
                ),
                Person.first_name.label("public_person_first_name"),
                Person.last_name.label("public_person_last_name"),
                Person.cms_id.label("public_person_cms_id"),
                Person.email.label("public_person_email"),
                Affiliation.inst_code.label("public_affiliation_inst_code"),
                Institute.country_code,
                Country.region_code,
                Region.name.label("region_name"),
                Position.name.label("position_name"),
                OrgUnit.domain.label("unit_domain"),
                OrgUnitType.name.label("unit_type"),
                func.string_agg(Project.code, ",").label("public_project_codes"),
            )
            .join(JobOpenPosition, JobNomination.open_position_id == JobOpenPosition.id)
            .join(JobOpening, JobOpenPosition.job_opening_id == JobOpening.id)
            .join(Person, JobNomination.nominee_id == Person.cms_id)
            .outerjoin(Affiliation, Person.cms_id == Affiliation.cms_id)
            .outerjoin(Institute, Institute.code == Affiliation.inst_code)
            .outerjoin(Country, Country.code == Institute.country_code)
            .outerjoin(Region, Region.code == Country.region_code)
            .outerjoin(Assignment, Affiliation.cms_id == Assignment.cms_id)
            .outerjoin(Project, Assignment.project_code == Project.code)
            .join(Position, Position.id == JobOpenPosition.position_id)
            .join(OrgUnit, JobOpenPosition.job_unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
            .join(
                JobNominationStatus,
                JobNominationStatus.nomination_id == JobNomination.nomination_id,
            )
            .filter(Affiliation.is_primary.is_(True))
            .filter(Affiliation.start_date <= date.today())
            .filter(
                sa.or_(
                    Affiliation.end_date.is_(None),
                    Affiliation.end_date > date.today(),
                )
            )
            .group_by(
                JobOpenPosition.position_id,
                OrgUnit.domain,
                OrgUnitType.name,
                JobOpenPosition.job_unit_id,
                JobNomination.nomination_id,
                JobOpening.id,
                JobOpening.title,
                JobOpening.questionnaire_uri,
                Person.first_name,
                Person.last_name,
                Position.name,
                Person.cms_id,
                Person.email,
                Affiliation.inst_code,
                Institute.country_code,
                Country.region_code,
                Region.name,
            )
        )
        if (
            not current_user.is_admin()
            and not current_user.is_cms_cbchair
            and not args.get("for_nominee_selection")
        ):
            q = q.filter(
                sa.or_(
                    JobNomination.nominee_id == current_user.cms_id,
                    sa.and_(
                        JobNominationStatus.actor_id == current_user.cms_id,
                        JobNominationStatus.status == "active",
                    ),
                )
            )
        actors_nomination_statuses = (
            JobNominationStatus.session().query(JobNominationStatus).all()
        )

        accepted_ids = []
        for status in actors_nomination_statuses:
            accepted_ids.append(status.nomination_id)
        q = q.filter(JobNomination.nomination_id.in_(accepted_ids))
        active = args.get("only_active", None)
        if active is not None:
            q = q.filter(JobNominationStatus.status == const.JOB_NOMINATED)
        nominations = cls.rows_to_list_of_dicts(q.all(), q_cols)
        for nomination in nominations:
            _status_cols = [
                JobNominationStatus.id,
                JobNominationStatus.actor_id,
                JobNominationStatus.nomination_id,
                JobNominationStatus.actor_remarks,
                JobNominationStatus.status,
                JobNominationStatus.last_modified,
                Person.first_name,
                Person.last_name,
                Person.email,
            ]
            statuses = (
                JobNominationStatus.session()
                .query(*_status_cols)
                .join(Person, Person.cms_id == JobNominationStatus.actor_id)
                .filter(
                    JobNominationStatus.nomination_id == nomination["nomination_id"]
                )
                .order_by(JobNominationStatus.last_modified)
                .all()
            )
            nomination["statuses"] = cls.rows_to_list_of_dicts(
                statuses, cols_list=_status_cols
            )
        return nominations

    @classmethod
    def get_nomination_obj(cls, nomination_id):
        args = cls.parse_args()
        q_cols = [
            JobNomination.nomination_id,
            JobOpenPosition.job_opening_id,
            JobNomination.nominee_id,
            JobNomination.questionnaire_answers,
            JobOpening.id,
            JobOpening.title,
            JobOpening.questionnaire_id,
            JobOpening.questionnaire_uri,
            Person.first_name,
            Person.last_name,
            Person.email,
            Affiliation.inst_code,
            Project.code,
        ]

        q = (
            JobNomination.session()
            .query(*q_cols)
            .join(JobOpenPosition, JobNomination.open_position_id == JobOpenPosition.id)
            .join(JobOpening, JobOpenPosition.job_opening_id == JobOpening.id)
            .join(Person, JobNomination.nominee_id == Person.cms_id)
            .join(
                Affiliation,
                Person.cms_id == Affiliation.cms_id,
            )
            .filter(JobNomination.nominee_id == current_user.cms_id)
            .filter(Affiliation.is_primary.is_(True))
            .filter(Affiliation.start_date <= date.today())
            .filter(
                sa.or_(
                    Affiliation.end_date.is_(None),
                    Affiliation.end_date > date.today(),
                )
            )
        )

        actors_nomination_statuses = (
            JobNominationStatus.session()
            .query(JobNominationStatus)
            .filter(JobNominationStatus.actor_id == current_user.cms_id)
            .all()
        )
        accepted_ids = []
        for status in actors_nomination_statuses:
            accepted_ids.append(status.nomination_id)
        q = q.filter(JobNomination.nomination_id.in_(accepted_ids))
        active = args.get("only_active", None)

        if active is not None:
            q = q.filter(JobNominationStatus.status == const.JOB_NOMINATED)
        nominations = cls.rows_to_list_of_dicts(
            q.filter(JobNomination.nomination_id == nomination_id), q_cols
        )
        for nomination in nominations:
            _status_cols = [
                JobNominationStatus.id,
                JobNominationStatus.actor_id,
                JobNominationStatus.nomination_id,
                JobNominationStatus.actor_remarks,
                JobNominationStatus.status,
                JobNominationStatus.last_modified,
                Person.first_name,
                Person.last_name,
                Person.email,
            ]
            statuses = (
                JobNominationStatus.session()
                .query(*_status_cols)
                .join(Person, Person.cms_id == JobNominationStatus.actor_id)
                .filter(
                    JobNominationStatus.nomination_id == nomination["nomination_id"]
                )
                .all()
            )
            nomination["statuses"] = cls.rows_to_list_of_dicts(
                statuses, cols_list=_status_cols
            )
        if nominations is None:
            raise NotFoundException("Job Nomination not found in DB")
        return nominations

    @classmethod
    def create_nomination(cls):
        payload = cls.get_payload()
        job_id = payload.get("job_id")
        position_id = payload.get("position_id")
        job_unit_id = payload.get("job_unit_id")
        nominee_id = payload.get("nominee_id")
        jop = (
            JobOpenPosition.session()
            .query(JobOpenPosition)
            .filter(JobOpenPosition.position_id == position_id)
            .filter(JobOpenPosition.job_unit_id == job_unit_id)
            .filter(JobOpenPosition.job_opening_id == job_id)
            .first()
        )
        if jop.status != const.JOB_STATUS_ACTIVE:
            raise BadRequestException("Nomination is close for  %s Job" % jop.position)

        if nominee_id is not None:
            nominee = (
                Person.session()
                .query(Person)
                .filter(Person.cms_id == nominee_id)
                .first()
            )
            if nominee is None:
                raise BadRequestException("Invalid Nominee")

        nomination = (
            JobNomination.session()
            .query(JobNomination)
            .join(
                JobNominationStatus,
                JobNomination.nomination_id == JobNominationStatus.nomination_id,
            )
            .filter(JobNomination.open_position_id == jop.id)
            .filter(JobNomination.nominee_id == nominee_id)
            .filter(
                sa.and_(
                    JobNominationStatus.actor_id == current_user.cms_id,
                    JobNominationStatus.status == "active",
                ),
            )
            .first()
        )

        if nomination is not None:
            raise BadRequestException(
                f"Nominee already proposed for this position by you"
            )

        # Check if nominated for the same position before save the current
        # nomination in order to decide later on if an email will be sent
        nominated_for_the_same_position = (
            JobNomination.session()
            .query(JobNomination)
            .join(
                JobNominationStatus,
                JobNomination.nomination_id == JobNominationStatus.nomination_id,
            )
            .filter(
                sa.and_(
                    JobNomination.open_position_id == jop.id,
                    JobNomination.nominee_id == nominee_id,
                )
            )
            .all()
        )
        existing_status = ""
        if nominated_for_the_same_position:
            existing_nomination = nominated_for_the_same_position[0]
            existing_statuses = (
                JobNominationStatus.session()
                .query(JobNominationStatus)
                .filter(
                    JobNominationStatus.nomination_id
                    == existing_nomination.nomination_id
                )
                .order_by(desc(JobNominationStatus.last_modified))
                .all()
            )
            existing_status = existing_statuses[0].status
            existing_actor = existing_statuses[0].actor_id
            existing_remarks = existing_statuses[0].actor_remarks

        job_nomination = JobNomination.from_ia_dict({})
        job_nomination.nominee_id = nominee.cms_id
        job_nomination.open_position_id = jop.id
        ssn = JobNomination.session()
        ssn.add(job_nomination)
        ssn.commit()

        status = JobNominationStatus.from_ia_dict({})
        status.nomination_id = job_nomination.nomination_id
        status.status = const.JOB_STATUS_ACTIVE
        status.actor_id = current_user.cms_id
        nominator = status.actor_id
        status.actor_remarks = payload.get("actor_remarks")
        ssn2 = JobNominationStatus.session()
        ssn2.add(status)
        ssn2.commit()
        if existing_status and existing_status != "active":
            if existing_status == "accepted" or existing_status == "rejected":
                status2 = JobNominationStatus.from_ia_dict({})
                status2.nomination_id = job_nomination.nomination_id
                status2.status = existing_status
                status2.actor_id = existing_actor
                status2.actor_remarks = (
                    f"Nomination already found with status: {existing_status} "
                    f"and remarks: {existing_remarks}"
                )
                ssn4 = JobNominationStatus.session()
                ssn4.add(status2)
                ssn4.commit()
            elif existing_status == "selected" or existing_status == "declined":
                status3a = JobNominationStatus.from_ia_dict({})
                status3a.nomination_id = job_nomination.nomination_id
                status3a.status = const.JOB_NOMINATION_ACCEPTED
                status3a.actor_id = nominee.cms_id
                status3a.actor_remarks = "Nominee already accepted."
                ssn5 = JobNominationStatus.session()
                ssn5.add(status3a)
                ssn5.commit()
                status3b = JobNominationStatus.from_ia_dict({})
                status3b.nomination_id = job_nomination.nomination_id
                status3b.status = existing_status
                status3b.actor_id = existing_actor
                status3b.actor_remarks = (
                    f"Nomination already found with status: {existing_status} "
                    f"and remarks: {existing_remarks}"
                )
                ssn6 = JobNominationStatus.session()
                ssn6.add(status3b)
                ssn6.commit()
        job_nomination_details = cls.get_nomination_obj(job_nomination.nomination_id)

        jssn = JobOpening.session()

        # These are the columns we will want to select
        _cols = [
            JobOpening.id,
            JobOpening.title,
            JobOpening.send_mail_to_nominee,
            JobOpening.nominations_deadline,
            JobOpening.days_for_nominee_response,
            JobOpening.questionnaire_uri,
            JobOpening.notify_egroups,
        ]
        _cols_for_jopssn = [
            JobOpenPosition.id,
            JobOpenPosition.position_id,
            JobOpenPosition.job_unit_id,
            JobOpenPosition.job_opening_id,
            JobOpenPosition.start_date,
            JobOpenPosition.end_date,
            JobOpenPosition.nominations_deadline,
            JobOpenPosition.status,
            OrgUnit.domain,
            OrgUnitType.name,
        ]
        jopssn = (
            JobOpenPosition.session()
            .query(*_cols_for_jopssn)
            .join(OrgUnit, JobOpenPosition.job_unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
            .filter(JobOpenPosition.id == job_nomination.open_position_id)
            .first()
        )

        unit = jopssn.domain + " " + jopssn.name

        jq = (jssn.query(*_cols).filter(JobOpening.id == jopssn.job_opening_id)).first()

        # Send email to the nominee only when is the first time he/she is being nominated for a position
        if jq.send_mail_to_nominee and not nominated_for_the_same_position:

            cb_chair_team_and_egroups = "cms-cbchairteam@cern.ch"
            # add egroups in cb_chair_team_and_egroups string
            list_of_egroups = jq.notify_egroups.split(",")
            if list_of_egroups:
                for egroup in list_of_egroups:
                    if egroup:
                        if "@" in egroup:
                            cb_chair_team_and_egroups += "," + egroup
                        else:
                            cb_chair_team_and_egroups += "," + egroup + "@cern.ch"

            title = jq.title
            nominee_name = nominee.first_name
            support = "icms-support@cern.ch"
            args = {
                "job_title": title,
                "unit": unit,
                "questionnaire_uri": jq.questionnaire_uri,
                "nominee_name": nominee_name,
                "nominator": nominator,
                "final_deadline": jq.nominations_deadline
                + timedelta(days=jq.days_for_nominee_response),
            }
            mail_body = flask.render_template(
                "emails/auto_reply_to_nominee.txt", **args
            )
            EmailMessage.compose_message(
                sender=support,
                bcc=cb_chair_team_and_egroups,
                reply_to=cb_chair_team_and_egroups,
                subject="[iCMS-jobOpenings] Your nomination for " + title,
                source_app="toolkit",
                to=nominee.email,
                body=mail_body,
                db_session=flask.current_app.db.session,
            )
        return job_nomination_details

    @classmethod
    def edit_nomination(cls, n_id):
        payload = cls.get_payload()
        status = payload.get("status")
        actor = payload.get("actor_id")
        nomination = (
            JobNomination.session()
            .query(JobNomination)
            .filter(JobNomination.nomination_id == n_id)
            .first()
        )
        if not nomination:
            raise NotFoundException("Job Nomination does not exists")
        answers = payload.get("questionnaire_answers")
        if answers:
            cls.update_db_object(
                nomination,
                {"questionnaire_answers": answers},
                rules=[
                    (JobNomination.questionnaire_answers, None, None),
                ],
            )

        # Find if other nominations exist for the same position and the same nominee
        # If one entry, is just the entry we try to update !
        existing_nominations = (
            JobNomination.session()
            .query(JobNomination)
            .filter(JobNomination.open_position_id == nomination.open_position_id)
            .filter(JobNomination.nominee_id == nomination.nominee_id)
            .all()
        )

        job_nomination_status = (
            JobNominationStatus.session()
            .query(JobNominationStatus)
            .filter(JobNominationStatus.nomination_id == n_id)
            .filter(JobNominationStatus.status == const.JOB_STATUS_ACTIVE)
            .first()
        )
        if job_nomination_status is None:
            raise BadRequestException("Job Nomination status is not active to update")
        for existing_nom in existing_nominations:
            j_n_status = JobNominationStatus.from_ia_dict({})
            j_n_status.nomination_id = existing_nom.nomination_id
            j_n_status.status = status
            j_n_status.actor_id = actor
            j_n_status.actor_remarks = payload.get("actor_remarks")
            ssn3 = JobNominationStatus.session()
            ssn3.add(j_n_status)
            ssn3.commit()
        get_job_nomination_details = cls.get_nomination_obj(nomination.nomination_id)

        # email notification is status is selected or declined
        nominee = (
            Person.session()
            .query(Person)
            .filter(Person.cms_id == nomination.nominee_id)
            .first()
        )
        if nominee is None:
            raise BadRequestException("invalid Nominee")

        jssn = JobOpening.session()

        _cols = [
            JobOpening.id,
            JobOpening.title,
            JobOpening.send_mail_to_nominee,
            JobOpening.nominations_deadline,
            JobOpening.days_for_nominee_response,
            JobOpening.questionnaire_uri,
            JobOpening.notify_egroups,
        ]
        _cols_for_jopssn = [
            JobOpenPosition.id,
            JobOpenPosition.position_id,
            JobOpenPosition.job_unit_id,
            JobOpenPosition.job_opening_id,
            JobOpenPosition.start_date,
            JobOpenPosition.end_date,
            JobOpenPosition.nominations_deadline,
            JobOpenPosition.status,
            OrgUnit.domain,
            OrgUnitType.name,
        ]
        jopssn = (
            JobOpenPosition.session()
            .query(*_cols_for_jopssn)
            .join(OrgUnit, JobOpenPosition.job_unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
            .filter(JobOpenPosition.id == nomination.open_position_id)
            .first()
        )

        unit = jopssn.domain + " " + jopssn.name

        jq = (jssn.query(*_cols).filter(JobOpening.id == jopssn.job_opening_id)).first()

        # preparation before the email cases
        cb_chair_team_and_egroups = "cms-cbchairteam@cern.ch"
        # add egroups in cb_chair_team_and_egroups string
        list_of_egroups = jq.notify_egroups.split(",")
        if list_of_egroups:
            for egroup in list_of_egroups:
                if egroup:
                    if "@" in egroup:
                        cb_chair_team_and_egroups += "," + egroup
                    else:
                        cb_chair_team_and_egroups += "," + egroup + "@cern.ch"
        if status == "selected":
            title = jq.title
            nominee_name = nominee.first_name
            support = "icms-support@cern.ch"
            args = {
                "job_title": title,
                "unit": unit,
                "nominee_name": nominee_name,
            }
            mail_body = flask.render_template("emails/nominee_selection.txt", **args)
            EmailMessage.compose_message(
                sender=support,
                bcc=cb_chair_team_and_egroups,
                reply_to=cb_chair_team_and_egroups,
                subject="[iCMS-jobOpenings] You are selected for " + title,
                source_app="toolkit",
                to=nominee.email,
                body=mail_body,
                db_session=flask.current_app.db.session,
            )
        elif status == "declined":
            title = jq.title
            nominee_name = nominee.first_name
            support = "icms-support@cern.ch"
            args = {
                "job_title": title,
                "unit": unit,
                "nominee_name": nominee_name,
            }
            mail_body = flask.render_template("emails/nominee_decline.txt", **args)
            EmailMessage.compose_message(
                sender=support,
                bcc=cb_chair_team_and_egroups,
                reply_to=cb_chair_team_and_egroups,
                subject="[iCMS-jobOpenings] You are declined for " + title,
                source_app="toolkit",
                to=nominee.email,
                body=mail_body,
                db_session=flask.current_app.db.session,
            )
        elif status == "rejected":
            title = jq.title
            nominee_name = nominee.first_name
            args = {
                "job_title": title,
                "nominee_name": nominee_name,
                "remarks": payload.get("actor_remarks"),
                "unit": unit,
            }
            mail_body = flask.render_template("emails/nominee_rejection.txt", **args)
            EmailMessage.compose_message(
                sender="icms-support@cern.ch",
                reply_to="cms-cbchairteam@cern.ch",
                subject="[iCMS-jobOpenings] You rejected your nomination for " + title,
                source_app="toolkit",
                to=nominee.email,
                body=mail_body,
                db_session=flask.current_app.db.session,
            )
        elif status == "accepted":
            title = jq.title
            nominee_name = nominee.first_name
            args = {
                "job_title": title,
                "nominee_name": nominee_name,
                "questionnaire_uri": jq.questionnaire_uri,
                "remarks": payload.get("actor_remarks"),
                "unit": unit,
            }
            mail_body = flask.render_template("emails/nominee_acceptance.txt", **args)
            EmailMessage.compose_message(
                sender="icms-support@cern.ch",
                reply_to="cms-cbchairteam@cern.ch",
                subject="[iCMS-jobOpenings] You accepted your nomination for " + title,
                source_app="toolkit",
                to=nominee.email,
                body=mail_body,
                db_session=flask.current_app.db.session,
            )

        return get_job_nomination_details


class JobNominationStatusApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Job NominationStatus Info",
        {
            JobNominationStatus.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobNominationStatus.actor_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobNominationStatus.nomination_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobNominationStatus.actor_remarks.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobNominationStatus.status.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobNominationStatus.last_modified.key: ModelFactory.field_proxy(
                ModelFactory.DateTime,
                {
                    ModelFactory.READONLY: False,
                },
            ),
            JobNomination.questionnaire_answers.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: False,
                },
            ),
        },
    )
