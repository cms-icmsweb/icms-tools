from icms_orm.common import (
    Tenure,
    Position,
    PositionName,
    OrgUnit,
    OrgUnitType,
    OrgUnitTypeName,
    Person,
    Affiliation,
)
from icms_orm.common import ExOfficioMandate as EOM
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from datetime import date, datetime
import sqlalchemy as sa


class TenuresApiCall(BaseApiUnit):

    _ARG_NAME_EXCLUDE_PAST = "exclude_past"
    _ARG_NAME_AS_OF = "as_of"
    _ARG_NAME_MAX_POSITION_LEVEL = "max_position_level"
    _ARG_NAME_MIN_POSITION_LEVEL = "min_position_level"
    _ARG_NAME_SORT_LIST = "sort_list"

    _model = ModelFactory.make_model(
        "Tenure Info",
        {
            Tenure.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Tenure.cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: 9981},
            ),
            Person.last_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Person.first_name.key: ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.READONLY: True}
            ),
            Person.email.key: ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.READONLY: True}
            ),
            Affiliation.inst_code.key: ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.READONLY: True}
            ),
            Tenure.position_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: 2,
                },
            ),
            "position": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            OrgUnit.domain.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                    ModelFactory.EXAMPLE: "ECAL",
                },
            ),
            Tenure.unit_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: 3},
            ),
            "unit_type": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                    ModelFactory.EXAMPLE: OrgUnitTypeName.BOARD,
                },
            ),
            Tenure.start_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: date(2015, 9, 1).isoformat(),
                },
            ),
            Tenure.end_date.key: ModelFactory.field_proxy(
                ModelFactory.Date, {ModelFactory.EXAMPLE: date(2017, 1, 1).isoformat()}
            ),
            "position_level": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
            "unit_level": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
            "src_position_level": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
            "src_unit_level": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
            "src_unit_type": ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.READONLY: True}
            ),
            "src_unit_id": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "ex_officio_rule_id": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
        },
    )

    _args = (
        ParserBuilder()
        .add_appendable_argument("cms_id", type=ParserBuilder.INTEGER, required=False)
        .add_argument(
            _ARG_NAME_EXCLUDE_PAST,
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=False,
        )
        .add_argument(
            _ARG_NAME_AS_OF, type=ParserBuilder.DATE, required=False, default=None
        )
        .add_appendable_argument(
            "domain", type=ParserBuilder.STRING, required=False, default=None
        )
        .add_appendable_argument(
            "unit_type",
            type=ParserBuilder.STRING,
            required=False,
            default=None,
            choices=[_v.lower() for _v in OrgUnitTypeName.values()],
        )
        .add_appendable_argument(
            "unit_id", type=ParserBuilder.INTEGER, required=False, default=None
        )
        .add_argument(
            _ARG_NAME_MIN_POSITION_LEVEL,
            type=ParserBuilder.INTEGER,
            required=False,
            default=None,
        )
        .add_argument(
            _ARG_NAME_MAX_POSITION_LEVEL,
            type=ParserBuilder.INTEGER,
            required=False,
            default=None,
        )
        .add_argument(
            _ARG_NAME_SORT_LIST,
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=True,
        )
        .parser
    )

    _payload_translation_rules = [
        (Tenure.cms_id, None, None),
        (Tenure.unit_id, None, None),
        (Tenure.position_id, None, None),
        (Tenure.start_date, None, None),
        (Tenure.end_date, None, None),
    ]

    @classmethod
    def _do_get_tenures(cls, kwargs, just_one_row=False):
        """
        Some things to keep in mind:
        - the same tenure id can show up in multiple units (ex-officio) - hence the distinct on tenure id and unit id
        - the same cms id can have multiple separate tenures with the same unit - so unit and cms id do not guarantee uniqueness
        - we pick the earliest affiliation NOT ENDING before the tenure's start. For some cases we have no affiliation info for the past, so that yields the earliest available
        """
        ssn = Tenure.session()

        # Two queries will be needed - one for "direct" tenures, the other for ex-officio inclusions
        q1 = (
            ssn.query(
                Tenure.id.label("id"),
                Tenure.unit_id.label("unit_id"),
                Position.name.label("position"),
                Position.level.label("position_level"),
                OrgUnitType.name.label("unit_type"),
                OrgUnitType.level.label("unit_level"),
                Position.level.label("src_position_level"),
                OrgUnitType.name.label("src_unit_type"),
                OrgUnitType.level.label("src_unit_level"),
                OrgUnit.id.label("src_unit_id"),
                sa.sql.expression.literal_column("null").label("ex_officio_rule_id"),
            )
            .join(Position, Position.id == Tenure.position_id)
            .join(OrgUnit, Tenure.unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
        )

        src_position = sa.orm.aliased(Position, name="SrcPosition")
        src_unit = sa.orm.aliased(OrgUnit, name="SrcOrgUnit")
        src_unit_type = sa.orm.aliased(OrgUnitType, name="SrcUnitType")
        q2 = (
            ssn.query(
                Tenure.id.label("id"),
                EOM.dst_unit_id.label("unit_id"),
                sa.func.coalesce(
                    EOM.dst_position_name,
                    sa.func.concat(
                        src_position.name,
                        " of ",
                        src_unit.domain,
                        " ",
                        src_unit_type.name,
                    ),
                ).label("position"),
                EOM.dst_position_level.label("position_level"),
                OrgUnitType.name.label("unit_type"),
                OrgUnitType.level.label("unit_level"),
                src_position.level.label("src_position_level"),
                src_unit_type.name.label("src_unit_type"),
                src_unit_type.level.label("src_unit_level"),
                src_unit.id.label("src_unit_id"),
                EOM.id.label("ex_officio_rule_id"),
            )
            .join(src_position, Tenure.position_id == src_position.id)
            .join(src_unit, src_unit.id == Tenure.unit_id)
            .join(src_unit_type, src_unit_type.id == src_unit.type_id)
            .join(
                EOM,
                sa.and_(
                    Tenure.unit_id == EOM.src_unit_id,
                    src_position.level == EOM.src_position_level,
                    sa.or_(
                        EOM.src_position_name == None,
                        EOM.src_position_name == src_position.name,
                    ),
                ),
            )
            .join(OrgUnit, EOM.dst_unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
        )

        # Then we create a unified subquery - however, if only one row was required, we ignore the ex-officio records
        sq = q1.subquery() if just_one_row else q1.union(q2).subquery()

        # These are the columns we will want to select
        _cols = [
            sq.c.id,
            sq.c.position,
            sq.c.unit_id,
            sq.c.position_level,
            sq.c.unit_type,
            sq.c.unit_level,
            sq.c.src_unit_level,
            sq.c.src_unit_type,
            sq.c.src_position_level,
            sq.c.src_unit_id,
            sq.c.ex_officio_rule_id,
            Tenure.cms_id,
            Tenure.start_date,
            Tenure.end_date,
            Tenure.position_id,
            Person.first_name,
            Person.last_name,
            Person.email,
            Affiliation.inst_code,
            OrgUnit.domain,
        ]

        # Now we can join the subquery against all the other information that we will want to show
        # Affiliation join is tricky - we want the first affiliation ending after tenure's start (or unterminated)
        q = (
            ssn.query(*_cols)
            .join(Tenure, sq.c.id == Tenure.id)
            .join(Person, Tenure.cms_id == Person.cms_id)
            .join(OrgUnit, sq.c.unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
            .join(
                Affiliation,
                sa.and_(
                    Person.cms_id == Affiliation.cms_id,
                    Affiliation.is_primary == True,
                    sa.or_(
                        Affiliation.end_date == None,
                        Affiliation.end_date > Tenure.start_date,
                    ),
                ),
            )
            .order_by(Tenure.id, sq.c.unit_id, Affiliation.start_date)
            .distinct(Tenure.id, sq.c.unit_id)
        )

        _query_filtering_rules = [
            Tenure.id,
            Tenure.cms_id,
            OrgUnit.domain,
            (sq.c.unit_id, "unit_id", None),
            (
                OrgUnitType.name,
                "unit_type",
                BaseApiUnit.ProcessorFunctions.wildcard_match,
            ),
        ]

        q = cls.attach_query_filters(
            query=q, params_map=kwargs, rules=_query_filtering_rules
        )

        _exclude_past = kwargs.get(cls._ARG_NAME_EXCLUDE_PAST)
        if _exclude_past:
            q = q.filter(
                sa.or_(Tenure.end_date == None, Tenure.end_date > date.today())
            )
        _as_of = kwargs.get(cls._ARG_NAME_AS_OF)
        if _as_of:
            q = q.filter(
                sa.and_(
                    Tenure.start_date <= _as_of,
                    sa.or_(Tenure.end_date == None, Tenure.end_date >= _as_of),
                )
            )
        _max_position_level = kwargs.get(cls._ARG_NAME_MAX_POSITION_LEVEL)
        if _max_position_level is not None:
            q = q.filter(sq.c.position_level <= _max_position_level)
        _min_position_level = kwargs.get(cls._ARG_NAME_MIN_POSITION_LEVEL)
        if _min_position_level is not None:
            q = q.filter(sq.c.position_level >= _min_position_level)
        _sort_list = kwargs.get(cls._ARG_NAME_SORT_LIST)
        if just_one_row:
            return cls.row_to_dict(q.one(), _cols)
        response = cls.rows_to_list_of_dicts(q.all(), _cols)
        if _sort_list:
            response = sorted(
                response,
                key=lambda d: (
                    f'{d["domain"]} {d["unit_type"]}',
                    d["position_level"],
                    d["position"],
                ),
            )
        return response

    @classmethod
    def get_tenures(cls):
        return cls._do_get_tenures(cls.parse_args())


class TenureApiCall(TenuresApiCall):

    _args = (
        ParserBuilder().add_argument(Tenure.id.key, ParserBuilder.INTEGER, True).parser
    )

    @classmethod
    def get_tenure(cls):
        return cls._do_get_tenures(cls.parse_args(), just_one_row=True)

    @classmethod
    def post_tenure(cls):
        payload = cls.get_payload()
        ssn = Tenure.session()
        _sent_id = payload.get(Tenure.id.key)
        tenure = (
            _sent_id
            and ssn.query(Tenure).filter(Tenure.id == _sent_id).one()
            or Tenure.from_ia_dict({})
        )
        cls.update_db_object(tenure, payload, rules=cls._payload_translation_rules)
        return cls._do_get_tenures(
            {Tenure.id.key: tenure.get(Tenure.id)}, just_one_row=True
        )

    @classmethod
    def delete_tenure(cls):
        q = Tenure.session().query(Tenure)
        q = cls.attach_query_filters(q, cls.parse_args(), [Tenure.id])
        q.delete()
        Tenure.session().commit()
