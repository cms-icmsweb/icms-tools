from icms_orm.common import ExOfficioMandate, OrgUnit, OrgUnitType
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from datetime import date, datetime
import sqlalchemy as sa


class ExOfficioMandatesApiCall(BaseApiUnit):
    _args = ParserBuilder().add_argument(ExOfficioMandate.id.key, ParserBuilder.INTEGER, False).parser

    _dst_unit_alias = sa.orm.aliased(OrgUnit)
    _dst_unit_type_alias = sa.orm.aliased(OrgUnitType)

    _model_col_tuples = [
        (ExOfficioMandate.id, ModelFactory.Integer),
        (ExOfficioMandate.last_modified, ModelFactory.field_proxy(
            ModelFactory.DateTime, {ModelFactory.REQUIRED: False}
        )),
        (ExOfficioMandate.start_date, ModelFactory.field_proxy(
            ModelFactory.Date, {
                ModelFactory.REQUIRED: False,
                ModelFactory.DEFAULT: '2000-01-01'
            }
        )),
        (ExOfficioMandate.end_date, ModelFactory.field_proxy(
            ModelFactory.NullableDate, {
                ModelFactory.REQUIRED: False,
            }
        )),
        (ExOfficioMandate.src_unit_id, ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        })),
        (ExOfficioMandate.dst_unit_id, ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        })),
        (ExOfficioMandate.src_position_level, ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        })),
        (ExOfficioMandate.dst_position_level, ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        })),
        (ExOfficioMandate.src_position_name, ModelFactory.NullableString),
        (ExOfficioMandate.dst_position_name, ModelFactory.NullableString),
        (OrgUnit.domain.label('src_unit_domain'), ModelFactory.field_proxy(
            ModelFactory.String, {
                ModelFactory.READONLY: True
            }
        )),
        (OrgUnitType.name.label('src_unit_type'), ModelFactory.field_proxy(
            ModelFactory.String, {
                ModelFactory.READONLY: True
            }
        )),
        (_dst_unit_alias.domain.label('dst_unit_domain'), ModelFactory.field_proxy(
            ModelFactory.String, {
                ModelFactory.READONLY: True
            }
        )),
        (_dst_unit_type_alias.name.label('dst_unit_type'), ModelFactory.field_proxy(
            ModelFactory.String, {
                ModelFactory.READONLY: True
            }
        ))
    ]

    _model = ModelFactory.make_model('Ex-Officio Mandate', {k: v for k, v in _model_col_tuples})

    @classmethod
    def post_mandate(cls):
        ssn = ExOfficioMandate.session()
        payload = cls.get_payload()
        _sent_id = payload.get(ExOfficioMandate.id.key)
        mandate = _sent_id and ssn.query(ExOfficioMandate).filter(ExOfficioMandate.id == _sent_id).one() \
                  or ExOfficioMandate.from_ia_dict({})
        cls.update_db_object(mandate, payload, rules=ExOfficioMandate.ia_list())
        return cls.get_mandates(only_for_id=mandate.id)

    @classmethod
    def get_mandates(cls, only_for_id=None):
        _cols = [t[0] for t in cls._model_col_tuples]
        q = ExOfficioMandate.session().query(*_cols).join(OrgUnit, OrgUnit.id == ExOfficioMandate.src_unit_id).\
            join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).\
            join(cls._dst_unit_alias, cls._dst_unit_alias.id == ExOfficioMandate.dst_unit_id).\
            join(cls._dst_unit_type_alias, cls._dst_unit_type_alias.id == cls._dst_unit_alias.type_id)
        if only_for_id is not None:
            return cls.row_to_dict(q.filter(ExOfficioMandate.id == only_for_id).one(), _cols)
        return cls.rows_to_list_of_dicts(q.all(), _cols)

    @classmethod
    def get_mandate(cls):
        return cls.get_mandates(only_for_id=cls.parse_args().get(ExOfficioMandate.id.key))
