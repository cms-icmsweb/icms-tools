from icms_orm.common import Tenure, Position, PositionName, OrgUnit, OrgUnitType, OrgUnitTypeName, Person, Affiliation
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
import sqlalchemy as sa


class OrgUnitsTreeApiCall(BaseApiUnit):
    _args = None

    @classmethod
    def recursive_unit_mapping(cls, iteration_number=4):
        unit_json_mapping = {
            OrgUnit.id.key: ModelFactory.Integer(),
            OrgUnit.domain.key: ModelFactory.String(),
            'type': ModelFactory.String(),
        }
        if iteration_number > 0:
            unit_json_mapping['dependent_units'] = ModelFactory.List(
                ModelFactory.Nested(cls.recursive_unit_mapping(iteration_number - 1)))
        return ModelFactory.make_model('Org Unit Info %d' % iteration_number, unit_json_mapping)

    @classmethod
    def get_model(cls):
        return cls.recursive_unit_mapping(4)

    @classmethod
    def get_units_tree(cls):
        ssn = OrgUnit.session()
        _cols = [OrgUnit.id, OrgUnit.domain, OrgUnitType.name.label('type'), OrgUnit.superior_unit_id]
        _units = ssn.query(*_cols).join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).all()
        _units = cls.rows_to_list_of_dicts(_units, _cols)
        _units_by_id = {_u.get('id'): _u for _u in _units}
        for _unit in _units[:]:
            _superior_id = _unit.get(OrgUnit.superior_unit_id.key)
            if _superior_id is not None:
                _superior = _units_by_id.get(_superior_id)
                _superior['dependent_units'] = _superior.get('dependent_units', [])
                _superior['dependent_units'].append(_unit)
                _units.remove(_unit)
        return _units
