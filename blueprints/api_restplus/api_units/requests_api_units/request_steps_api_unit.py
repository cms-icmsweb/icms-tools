from dataclasses import Field
from logging import Logger
import logging
from typing import Any, Dict, List
import flask_login
from icmsutils.datatypes import ActorData

from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import \
    ModelFieldDefinition as FieldDef
from blueprints.api_restplus.api_units.basics.crud import Deleteless, Postless
from blueprints.api_restplus.api_units.basics.model_field_types import (
    Boolean, Date, DateTime, Integer, Raw, String)
from icms_orm.common import (Request, RequestStatusValues, RequestStep,
                             RequestStepStatusValues)
from icmsutils.businesslogic.requests import RequestsService

log: Logger = logging.getLogger(__name__)


class RequestStepsApiUnit(AbstractCRUDUnit, Deleteless, Postless):
    '''
    Generic API unit for dealing with request steps. 
    All the actions are however performed through the RequestsService.

    It can be extended to handle POST requests - those would not contain the 
    corresponding step id (by model's definition) so it would only make sense 
    for requests having a single pending step (probably the majority actually).
    '''
    FIELD_ID = FieldDef.builder(Integer).column(
        RequestStep.id).readonly().id().build()
    FIELD_REQUEST_ID = FieldDef.builder(Integer).column(
        RequestStep.request_id).readonly().build()
    FIELD_CREATION_DATE = FieldDef.builder(DateTime).column(
        RequestStep.creation_date).readonly().build()
    FIELD_CREATOR_CMS_ID = FieldDef.builder(Integer).column(
        RequestStep.creator_cms_id).readonly().build()
    FIELD_LAST_MODIFIED = FieldDef.builder(DateTime).column(
        RequestStep.last_modified).readonly().build()
    FIELD_UPDATER_CMS_ID = FieldDef.builder(Integer).column(
        RequestStep.updater_cms_id).readonly().build()
    FIELD_STATUS = FieldDef.builder(String).column(RequestStep.status).enum(
        RequestStepStatusValues.values()).required().build()
    FIELD_DEADLINE = FieldDef.builder(DateTime).column(
        RequestStep.deadline).readonly().build()
    FIELD_TITLE = FieldDef.builder(String).column(
        RequestStep.title).readonly().build()
    FIELD_REMARKS = FieldDef.builder(
        String).column(RequestStep.remarks).build()
    FIELD_ACTIONABLE_UPON = FieldDef.builder(
        Boolean).name('is_actionable_upon').build()

    @classmethod
    def get_model_fields_list(cls) -> List[FieldDef]:
        return [
            cls.FIELD_ID,
            cls.FIELD_REQUEST_ID,
            cls.FIELD_CREATION_DATE,
            cls.FIELD_CREATOR_CMS_ID,
            cls.FIELD_LAST_MODIFIED,
            cls.FIELD_UPDATER_CMS_ID,
            cls.FIELD_STATUS,
            cls.FIELD_DEADLINE,
            cls.FIELD_TITLE,
            cls.FIELD_REMARKS,
            cls.FIELD_ACTIONABLE_UPON
        ]

    @classmethod
    def get_model_name(cls) -> str:
        return 'RequestStep'

    @classmethod
    def put_by_identity(cls, *params):
        """
        Translates received input into RequestsService's actions: approving or rejecting corresponding step.
        """        
        step_id: int = params[0]
        actor: ActorData = ActorData(flask_login.current_user.cms_id)
        payload = cls.get_payload()
        new_status = payload[cls.FIELD_STATUS.name]
        if new_status == RequestStepStatusValues.APPROVED:
            log.debug(f'Approving request step {step_id}')
            RequestsService.approve_step(
                request_id=None, actor_data=actor, step_id=step_id)
        elif new_status == RequestStepStatusValues.REJECTED:
            log.debug(f'Rejecting request step {step_id}')
            RequestsService.reject_step(
                request_id=None, actor_data=actor, step_id=step_id)
        else:
            log.warning(
                f'Received an unexpected step status update: {new_status} for step id {step_id}')
        return cls.get_by_identity(step_id)

    @classmethod
    def row_to_dict(cls, row, cols_list):
        """
        Adds information on whether the step can be acted upon.
        As of now this represents a potential performance bottleneck and will need to be addressed 
        as the number of pending requests grows.
        """
        a_dict = super().row_to_dict(row, cols_list)
        actor: ActorData = ActorData(flask_login.current_user.cms_id)
        a_dict[cls.FIELD_ACTIONABLE_UPON.name] = RequestsService.can_act(
            request_id=a_dict[cls.FIELD_REQUEST_ID.name], actor_data=actor, step_id=a_dict[cls.FIELD_ID.name])
        return a_dict
