from typing import List
import flask
from blueprints.api_restplus.api_units.basics import ModelFactory
from blueprints.api_restplus.api_units.basics.base_classes import BaseApiUnit
from blueprints.api_restplus.api_units.basics.crud import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics.crud import \
    ModelFieldDefinition as MFD
from blueprints.users.user_class import User
from flask_login import current_user, login_user, logout_user
from icms_orm.cmspeople.people import Person
from icmsutils.exceptions import IcmsException
from util import constants as const


class ImpersonateUserCall(AbstractCRUDUnit):

    FIELD_REAL_CMS_ID = MFD.builder(
        ModelFactory.Integer).name('real_cms_id').build()
    FIELD_ACTIVE_CMS_ID = MFD.builder(
        ModelFactory.Integer).name('active_cms_id').build()

    @classmethod
    def get_model_fields_list(cls) -> List[MFD]:
        return [cls.FIELD_REAL_CMS_ID, cls.FIELD_ACTIVE_CMS_ID]

    @classmethod
    def get_model_name(cls):
        return 'Impersonation Info'

    @classmethod
    def impersonate(cls, cms_id):
        real_id = cls.get_real_cms_id()
        if cms_id != real_id:
            person: Person = Person.query.get(cms_id)
            logout_user()
            user = User(person=person)
            login_user(user)
            flask.session[const.SESSION_KEY_REAL_CMS_ID] = real_id
        return cls.get_impersonation_info()

    @classmethod
    def stop_impersonating(cls):
        real_cms_id = cls.get_real_cms_id()

        if real_cms_id == cls.get_active_cms_id():
            raise IcmsException(
                "The user is not currently impersonating anyone.")

        logout_user()
        you = Person.query.get(real_cms_id)
        user = User(person=you)
        login_user(user)
        del flask.session[const.SESSION_KEY_REAL_CMS_ID]
        return cls.get_impersonation_info()

    @classmethod
    def get_impersonation_info(cls):
        return {
            cls.FIELD_REAL_CMS_ID.name: cls.get_real_cms_id(),
            cls.FIELD_ACTIVE_CMS_ID.name: cls.get_active_cms_id() or cls.get_real_cms_id(),
        }

    @classmethod
    def get_real_cms_id(cls):
        return flask.session.get(const.SESSION_KEY_REAL_CMS_ID, None) or cls.get_active_cms_id()

    @classmethod
    def get_active_cms_id(cls):
        return current_user.is_authenticated and current_user.person.cmsId or None
