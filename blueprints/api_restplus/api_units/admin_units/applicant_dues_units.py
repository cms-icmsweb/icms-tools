from datetime import datetime
from json import loads
from typing import Dict, List, Optional

from flask_restx.fields import Raw
from pydantic import BaseModel
from sqlalchemy import func, and_

from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from icms_orm.cmspeople import Person
from icms_orm.epr import TimeLineUser, EprUser, EprInstitute, Category
from icmsutils.businesslogic import servicework
from icmsutils.timelinesutils import year_fraction_left


class PersonWithDues(BaseModel, Raw):
    cms_id: int
    name: str
    institute: str
    category: str
    status: str
    never_susp_since: Optional[datetime] = None
    due_start_year: Optional[float] = None
    due_last_year: Optional[float] = None
    due_this_year: Optional[float] = None
    total_due: Optional[float] = None
    sum_due_correct: Optional[float] = None
    worked_in_advance: Optional[float] = None
    errors: Optional[str] = None

    @classmethod
    def from_lists(cls, row: List, cols: List):
        entry = {col: entry for col, entry in zip(cols, row)}
        return PersonWithDues(**entry)


class ApplicantDuesCall(BaseApiUnit):
    person_with_dues = ModelFactory.Nested(ModelFactory.make_model("Person with dues", {
        "cms_id": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        "name": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "institute": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "category": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "status": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "never_susp_since": ModelFactory.field_proxy(ModelFactory.DateTime, {
            ModelFactory.REQUIRED: False
        }),
        "due_start_year": ModelFactory.field_proxy(ModelFactory.Float, {
            ModelFactory.REQUIRED: False
        }),
        "due_last_year": ModelFactory.field_proxy(ModelFactory.Float, {
            ModelFactory.REQUIRED: False
        }),
        "due_this_year": ModelFactory.field_proxy(ModelFactory.Float, {
            ModelFactory.REQUIRED: False
        }),
        "total_due": ModelFactory.field_proxy(ModelFactory.Float, {
            ModelFactory.REQUIRED: False
        }),
        "sum_due_correct": ModelFactory.field_proxy(ModelFactory.Float, {
            ModelFactory.REQUIRED: False
        }),
        "worked_in_advance": ModelFactory.field_proxy(ModelFactory.Float, {
            ModelFactory.REQUIRED: False
        }),
        "errors": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        })
    }))

    _model = ModelFactory.make_model("People with dues", {
        "people": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_with_dues
        }),
    })

    @staticmethod
    def get_applicant_dues():
        """
        We want to find all the applicants who would be eligible for the 6-X reduction
        """
        ssn = TimeLineUser.session()
        ads = servicework.AppDueStats(db_session=ssn)
        missing_due_tolerance = 1
        year = datetime.now().year
        cols = ["cms_id", "name", "institute", "status", "category", "never_susp_since",
                "due_start_year", "due_last_year", "due_this_year", "total_due", "sum_due_correct", "errors"]

        data = (ssn.query(EprUser.cmsId, EprUser.name, EprInstitute.code, EprUser.status, Category.name)
            .join(EprInstitute, EprUser.mainInst == EprInstitute.id)
            .join(Category, EprUser.category == Category.id).all())

        dues_this = ApplicantDuesCall.count_due(ssn, year)
        dues_last = ApplicantDuesCall.count_due(ssn, year - 1)

        people_with_dues = [PersonWithDues.from_lists(row, cols) for row in data if row[0] in dues_this]

        exp_starts = {cms_id: y for cms_id, y in
                      ssn.query(Person.cmsId, func.coalesce(Person.dateAuthorUnsuspension, Person.dateCreation)).all()}

        people_with_dues_2 = []
        for person in people_with_dues:
            dues_start_year = servicework.AppDueStats.get_first_due_year(ads, person.cms_id)
            unsusp_since = exp_starts.get(person.cms_id)
            if not unsusp_since:
                continue
            unsusp_since = unsusp_since.date()
            app_target_due = servicework.get_annual_applicant_due(unsusp_since.year)

            person.never_susp_since = unsusp_since
            person.due_start_year = dues_start_year

            worked_in_advance = 0.00
            if person.never_susp_since.year < dues_start_year and servicework.get_annual_applicant_due(
                    person.never_susp_since.year) > 0:
                for fn in (servicework.get_person_worked, servicework.get_person_shifts_done):
                    for y in range(person.never_susp_since.year, dues_start_year):
                        worked_in_advance += fn(person.cmsId, y, ssn)

            person.worked_in_advance = worked_in_advance
            person.due_last_year = round(dues_last.get(person.cms_id, 0), 2)
            person.due_this_year = round(dues_this.get(person.cms_id, 0), 2)
            person.total_due = person.due_last_year + person.due_this_year
            person.sum_due_correct = max(app_target_due - worked_in_advance, 0)
            person.errors = ApplicantDuesCall.get_errors(person, missing_due_tolerance, year, app_target_due, ads)
            people_with_dues_2.append(loads(person.json()))

        return {"people": people_with_dues_2}

    @staticmethod
    def get_errors(person: PersonWithDues, missing_due_tolerance: float, year: int, app_target_due: float,
                   ads: object) -> Optional[str]:
        if person.total_due < (6.0 - missing_due_tolerance) and person.due_start_year < year:
            return "Too little due?"
        elif person.worked_in_advance + person.total_due > servicework.get_annual_applicant_due(
                person.never_susp_since.year) + 0.001:
            if person.worked_in_advance:
                return f"Too much due! Worked {person.worked_in_advance} in advance. ∑ due should be {app_target_due}"
            else:
                return f"Too much due! Should be {servicework.get_annual_applicant_due(person.never_susp_since.year)} for a person never suspended since {person.never_susp_since.year}."
        elif person.status.lower() == "exmember":
            return "Leftover due (ExMember)"
        elif person.category in ["non-doctoral", "admin"]:
            return f"Leftover due {person.category}"
        elif person.due_last_year == year:
            year_target = app_target_due * year_fraction_left(
                servicework.AppDueStats.get_first_due_date(ads, person.cmd_id))
            if abs(year_target - person.due_this_year) > year_target / 365.0:
                return f"Should have {year_target} in {year} (initial app year)"
        return None

    @staticmethod
    def count_due(ssn, year: int) -> Dict[int, float]:
        return {cms_id: dues for cms_id, dues in ssn.query(TimeLineUser.cmsId, func.sum(TimeLineUser.dueApplicant))
            .filter(and_(TimeLineUser.year == year, TimeLineUser.dueApplicant > 0))
            .group_by(TimeLineUser.cmsId).all()}
