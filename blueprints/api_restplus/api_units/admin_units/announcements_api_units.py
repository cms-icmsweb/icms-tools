from models.icms_common import Announcement
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from sqlalchemy import desc

class AnnouncementsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model('Announcements Info', {
        Announcement.id.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }), 
        Announcement.application.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }), 
        Announcement.type.key :  ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Announcement.subject.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Announcement.content.key :  ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Announcement.start_datetime.key : ModelFactory.field_proxy(ModelFactory.DateTime, {
            ModelFactory.REQUIRED: True,
        }), 
        Announcement.end_datetime.key : ModelFactory.field_proxy(ModelFactory.DateTime, {
            ModelFactory.REQUIRED: False,
        }),
    })

    _args = ParserBuilder().parser

    @classmethod
    def get_announcements(cls):
        ssn = Announcement.session()

        cols = [
            Announcement.id, 
            Announcement.application, 
            Announcement.type, 
            Announcement.subject,
            Announcement.content, 
            Announcement.start_datetime, 
            Announcement.end_datetime
        ]

        data = ssn.query(*cols).order_by(desc(Announcement.start_datetime)).all()
        return cls.rows_to_list_of_dicts(data, cols)

class AnnouncementApiCall(AnnouncementsApiCall):

    @classmethod
    def post_announcement(cls):
        payload = cls.get_payload()

        updatable_cols = [
            Announcement.application, 
            Announcement.type, 
            Announcement.subject,
            Announcement.content, 
            Announcement.start_datetime, 
            Announcement.end_datetime
        ]

        announcement = Announcement.from_ia_dict({})
        cls.update_db_object(announcement, payload, rules=updatable_cols)

        cols = [Announcement.id] + updatable_cols
        data = Announcement.session().query(*cols).filter(Announcement.id == announcement.get(Announcement.id)).one()
        return cls.row_to_dict(data, cols)