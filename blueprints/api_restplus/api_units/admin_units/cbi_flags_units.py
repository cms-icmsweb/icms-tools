from typing import List, Dict

from sqlalchemy import or_, func

from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from icms_orm.cmspeople import Person, Institute, PeopleFlagsAssociation as PFA
from icmsutils.businesslogic.flags import Flag
from util.structs import Link


def to_dicts(data: List, cols: List) -> List[Dict[str, str]]:
    entries = []
    for row in data:
        entry = {col: entry for col, entry in zip(cols, row)}
        entries.append(entry)
    return entries


class CBIFlagsCall(BaseApiUnit):
    multi_cbi_person_data = ModelFactory.Nested(ModelFactory.make_model("Multi CBI person data", {
        "cms_id": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        "last_name": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "first_name": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),

    }))

    person_data = ModelFactory.Nested(ModelFactory.make_model("CBI person data", {
        **multi_cbi_person_data.model,
        "inst_code": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        }),
    }))

    _model = ModelFactory.make_model("CBI flags", {
        "cbi_unflagged": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "cbi_unlinked": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "cbd_unflagged": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "cbd_unlinked": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "multicbis": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: multi_cbi_person_data
        })
    })

    @staticmethod
    def get_cbi_flags():
        ssn = PFA.session()
        sq_cbi_flagged = ssn.query(PFA.cmsId.label("cmsId")).filter(PFA.flagId == Flag.INST_REP).subquery()
        sq_cbd_flagged = ssn.query(PFA.cmsId.label("cmsId")).filter(PFA.flagId == Flag.INST_DEP).subquery()

        sq_cbi_linked = ssn.query(Institute.cbiCmsId.label("cmsId")).filter(Institute.cmsStatus.ilike("Yes")).filter(
            Institute.cbiCmsId != None).subquery()
        sq_cbd_linked = ssn.query(Institute.cbdCmsId.label("cmsId")).filter(Institute.cmsStatus.ilike("Yes")).filter(
            Institute.cbdCmsId != None).subquery()
        sq_cbd2_linked = ssn.query(Institute.cbd2CmsId.label("cmsId")).filter(Institute.cmsStatus.ilike("Yes")).filter(
            Institute.cbd2CmsId != None).subquery()

        p_cols = [Person.cmsId, Person.lastName, Person.firstName, Person.instCode]
        p_cols_keys = ["cms_id", "last_name", "first_name", "inst_code"]
        multicbi_person_cols = [Person.lastName, Person.firstName, Person.cmsId]
        multicbi_person_cols_keys = ["cms_id", "last_name", "first_name"]

        # find de facto CBIs without the flag
        cbi_unflagged = ssn.query(*p_cols).join(sq_cbi_linked, Person.cmsId == sq_cbi_linked.c.cmsId).filter(
            Person.cmsId.notin_(sq_cbi_flagged)).all()
        cbi_unflagged_td = to_dicts(cbi_unflagged, p_cols_keys)

        # find CBI flag holders without association
        cbi_unlinked = ssn.query(*p_cols).join(sq_cbi_flagged, Person.cmsId == sq_cbi_flagged.c.cmsId).filter(
            Person.cmsId.notin_(sq_cbi_linked)).all()
        cbi_unlinked_td = to_dicts(cbi_unlinked, p_cols_keys)

        # find de facto CBDs without the flag
        cbd_unflagged = ssn.query(*p_cols).filter(or_(
            Person.cmsId.in_(sq_cbd_linked), Person.cmsId.in_(sq_cbd2_linked)
        )).filter(Person.cmsId.notin_(sq_cbd_flagged)).all()
        cbd_unflagged_td = to_dicts(cbd_unflagged, p_cols_keys)

        # find CBD flag holders without the association
        cbd_unlinked = ssn.query(*p_cols).filter(Person.cmsId.notin_(sq_cbd_linked)).filter(
            Person.cmsId.notin_(sq_cbd2_linked)).filter(Person.cmsId.in_(sq_cbd_flagged)).all()
        cbd_unlinked_td = to_dicts(cbd_unlinked, p_cols_keys)

        # find people who are CBI/CBDs to multiple member institutes
        multicbis_query = ssn.query(func.count(Institute.code).label("N"), Person.lastName, Person.firstName,
                                    Person.cmsId).filter(
            Institute.cmsStatus.ilike("yes")).join(Person, or_(*[Person.cmsId == col for col in
                                                                 [Institute.cbiCmsId, Institute.cbdCmsId,
                                                                  Institute.cbd2CmsId]])). \
            group_by(*multicbi_person_cols).having(func.count(Institute.code) > 1).all()

        multicbis = to_dicts(multicbis_query, multicbi_person_cols_keys)

        return {
            "cbi_unflagged": cbi_unflagged_td,
            "cbi_unlinked": cbi_unlinked_td,
            "cbd_unflagged": cbd_unflagged_td,
            "cbd_unlinked": cbd_unlinked_td,
            "multicbis": multicbis
        }
