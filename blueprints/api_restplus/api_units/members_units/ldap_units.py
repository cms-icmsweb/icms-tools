from icmsutils.ldaputils import LdapProxy, LdapPerson
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException
from icms_orm.common import Person
from flask import current_app
from icmsutils.funcutils import Cache
from webapp.webapp import Webapp


class LdapPersonInfoCall(BaseApiUnit):
    _args = ParserBuilder(). \
        add_argument(Person.hr_id.key, ParserBuilder.INTEGER, required=True).\
        parser

    _model = ModelFactory.make_model('LDAP Person Info', {
        'hr_id': ModelFactory.Integer,
        'full_name': ModelFactory.String,
        'login': ModelFactory.String,
        'gid': ModelFactory.String,
        'account_status': ModelFactory.String,
        'department': ModelFactory.String,
        'phone': ModelFactory.String,
        'mobile': ModelFactory.String,
        'office': ModelFactory.String,
        'po_box': ModelFactory.String,
        'mail': ModelFactory.String,
        'cms_egroups': ModelFactory.List(ModelFactory.String),
        'in_zh': ModelFactory.Boolean,
    })

    @classmethod
    def ldap_person_to_dict(cls, ldap_person):
        return {
            'hr_id': ldap_person.hrId,
            'full_name': ldap_person.fullName,
            'login': ldap_person.login,
            'gid': ldap_person.gid,
            'account_status': ldap_person.accountStatus,
            'department': ldap_person.department,
            'phone': ldap_person.officePhone,
            'mobile': ldap_person.mobilePhone,
            'office': ldap_person.office,
            'po_box': ldap_person.postOfficeBox,
            'mail': ldap_person.mail,
            'cms_egroups': ldap_person.cmsEgroups,
            'in_zh': ldap_person.is_in_zh(),
        }

    @classmethod
    def get_ldap_proxy(cls) -> LdapProxy:
        return Webapp.current_app().get_ldap_proxy()

    @classmethod
    def get_info(cls):
        hr_id = cls.parse_args().get(Person.hr_id.key)
        proxy = cls.get_ldap_proxy()
        ldap_person = proxy.get_person_by_hrid(hr_id)
        if ldap_person is None:
            raise ArbitraryErrorCodeException(
                404, 'LDAP query yielded no results')
        assert isinstance(ldap_person, LdapPerson), 'ldap_person variable of type {0} rather than LdapPerson'.\
            format(type(ldap_person))
        return cls.ldap_person_to_dict(ldap_person)


class LdapPeopleLiteInfoCall(LdapPersonInfoCall):
    _args = ParserBuilder().add_argument('gid', ParserBuilder.INTEGER,
                                         required=False, default=1399).parser

    @classmethod
    @Cache.expiring(timeout=12 * 3600)
    def get_lite_info(cls):
        _gid = cls.parse_args().get('gid')
        proxy = cls.get_ldap_proxy()
        people_list = [cls.ldap_person_to_dict(_p) for _p in proxy.find_people_by_gid(
            _gid, attrlist=LdapProxy.ATTRLIST_LITE)]
        return people_list
