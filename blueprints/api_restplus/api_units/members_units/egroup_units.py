from util.CmsAccountChecker import AccountChecker
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException
from icms_orm.common import Person
from flask import current_app
from icmsutils.funcutils import Cache


class EgroupPersonInfoCall(BaseApiUnit):
    _args = ParserBuilder(). \
        add_argument(Person.hr_id.key, ParserBuilder.INTEGER, required=True).\
        parser

    _model = ModelFactory.make_model('EGroup Person Info', {
        'check_passed': ModelFactory.Boolean,
        'issues_found': ModelFactory.List(ModelFactory.String),
    })

    @classmethod
    def get_info(cls):
        hr_id = cls.parse_args().get(Person.hr_id.key)
    
        ac = AccountChecker(hr_id, verbose=True)
        results = ac.check()
        
        issues = []
        if not results['accountExists']:
            issues.append( "Something seems to have gone very wrong when you created your CERN account. Please contact icms-support@cern.ch to further clarify.")

        if not results['accountEnabled']:
            issues.append( "At present the account is disabled. Please contact the CERN Service desk (service-desk@cern.ch, tel.Cern-77777) to get it activated.")
            
        if not results['zhPrimaryOK']:
            issues.append( "Primary is not in ZH or ZJ")
            
        if not results['zhSecondaryOK']:
            issues.append( "You have already one (or more) secondary account(s) registered in the CMS computing group ('ZH' or 'ZJ'), adding your primary account will go against the CMS policy to only have one account per user in the 'zh' group to ensure fair use of the resources. Please contact icms-support@cern.ch, explaining why you need the account in 'zh', to further clarify.")
            
        if not results['otherExptOK']:
            issues.append( "Your account is presently registered with another LHC experiment. By common policy agreed between the major LHC experiments, everybody can only have an account in one of the experiments. Please contact icms-support@cern.ch to get added to the CMS computing group and removed from the other experiment one(s).")

        return {
            'check_passed': len(issues) == 0,
            'issues_found': issues 
        }

