from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from icms_orm.common import Person, CareerEvent, CareerEventTypeValues as CarrerEventType
from blueprints.api_restplus.exceptions import AccessViolationException
from datetime import date
import sqlalchemy as sa
from flask_login import current_user


class CarrerEventApiCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_argument(CareerEvent.id.key, ParserBuilder.INTEGER, required=False).\
        add_argument(Person.cms_id.key, ParserBuilder.INTEGER, required=False).\
        add_argument(CareerEvent.event_type.key, ParserBuilder.STRING, required=False,
                     choices=list(map(str.lower, CarrerEventType.values()))
                     ).\
        parser
    _model = ModelFactory.make_model('Career Event Info', {
        CareerEvent.id: ModelFactory.Integer,
        CareerEvent.cms_id: ModelFactory.Integer,
        CareerEvent.date: ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.EXAMPLE: date(2015, 9, 1).isoformat()
        }),
        CareerEvent.event_type: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.ENUM: list(map(str.lower, CarrerEventType.values()))
        })
    })

    @classmethod
    def _get_query_and_cols(cls):
        q_cols = [CareerEvent.id, CareerEvent.cms_id, CareerEvent.date, CareerEvent.event_type]
        args = cls.parse_args()
        q = CareerEvent.session().query(*q_cols)
        q = cls.attach_query_filters(q, params_map=args, rules=[CareerEvent.id, CareerEvent.cms_id, CareerEvent.event_type])
        return q, q_cols

    @classmethod
    def get_one(cls):
        q, cols = cls._get_query_and_cols()
        return cls.row_to_dict(q.one(), cols)

    @classmethod
    def get_many(cls):
        q, cols = cls._get_query_and_cols()
        return cls.rows_to_list_of_dicts(q.all(), cols)

    @classmethod
    def post(cls):
        args = cls.get_payload()
        if not current_user.is_admin() and not current_user.cms_id == args.get(CareerEvent.cms_id.key):
            raise AccessViolationException('Only the users themselves can modify this information.')
        # payload, not args, as we expect the model as input!
        entry = CareerEvent.from_ia_dict({})
        if args.get(CareerEvent.id.key) is not None:
            entry = CareerEvent.session().query(CareerEvent).filter(CareerEvent.id == args.get(CareerEvent.id.key)).one()
        cls.update_db_object(entry, args, rules=CareerEvent.ia_list())
        return entry.to_dict()


    @classmethod
    def delete(cls):
        args = cls.parse_args()
        if not current_user.is_admin() and not current_user.cms_id == args.get(CareerEvent.cms_id.key):
            raise AccessViolationException('Only the users themselves can modify this information.')
        q, _ = cls._get_query_and_cols()
        y = q.delete()
        CareerEvent.session().commit()
        return y

