from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory as MF, ParserBuilder as PB
from icms_orm.common import ApplicationAsset
from enum import Enum


class Field(Enum):
    CODE = 'code'
    ENTITY_TYPE = 'type'
    TITLE = 'title'
    FIRST_NAME = 'firstname'
    LAST_NAME = 'lastname'
    EMAIL = 'email'


class EntityType(Enum):
    INSTITUTE = 'inst'
    FUNDING_AGENCY = 'fa'


class ExternalOutreachContactsCall(BaseApiUnit):
    _args = PB().add_argument(Field.CODE.value, PB.STRING, required=True).parser
    _model = MF.make_model('External Outreach Contact', {
        Field.CODE.value: MF.field_proxy(MF.String, {MF.REQUIRED: True}),
        Field.ENTITY_TYPE.value: MF.field_proxy(MF.String, {MF.DEFAULT: EntityType.INSTITUTE.value, MF.REQUIRED: False, MF.ENUM: [_x.value for _x in list(EntityType)]}),
        Field.TITLE.value: MF.String,
        Field.FIRST_NAME.value: MF.field_proxy(MF.String, {MF.DEFAULT: None, MF.REQUIRED: True}),
        Field.LAST_NAME.value:  MF.field_proxy(MF.String, {MF.DEFAULT: None, MF.REQUIRED: True}),
        Field.EMAIL.value:  MF.field_proxy(MF.String, {MF.DEFAULT: None, MF.REQUIRED: True}),
    })
    _asset_name = 'external_outreach_contacts'

    @classmethod
    def get(cls, args_override=None):
        args = args_override or cls.parse_args()
        existing = ApplicationAsset.retrieve(cls._asset_name) or []
        for e in existing:
            if e.get(Field.CODE.value) == args.get(Field.CODE.value):
                return e
        return None

    @classmethod
    def list(cls):
        return ApplicationAsset.retrieve(cls._asset_name) or []

    @classmethod
    def delete(cls):
        args = cls.parse_args()
        existing = ApplicationAsset.retrieve(cls._asset_name) or []
        existing = [e for e in existing if e.get(Field.CODE.value) != args.get(Field.CODE.value)]
        ApplicationAsset.store(cls._asset_name, existing)
        return ApplicationAsset.retrieve(cls._asset_name) or []

    @classmethod
    def save(cls):
        new_one = cls.get_payload()
        existing = ApplicationAsset.retrieve(cls._asset_name) or []
        existing = [e for e in existing if e.get(Field.CODE.value) != new_one.get(Field.CODE.value)]
        existing.append(new_one)
        ApplicationAsset.store(cls._asset_name, existing)
        return cls.get(args_override=new_one)

    @classmethod
    def convert_old_entries(cls):
        """
        Just a helper one-off method to convert old-style formatted entries to a richer representation
        """
        existing = ApplicationAsset.retrieve(cls._asset_name) or []
        new_entries = []
        for entry in existing:
            if Field.CODE.value in entry.keys() and Field.ENTITY_TYPE.value in entry.keys():
                new_entries.append(entry)
            else:
                new_entry = dict()
                new_entry[Field.CODE.value] = entry.get('inst_code') or entry.get('fa_code')
                new_entry[Field.ENTITY_TYPE.value] = EntityType.INSTITUTE.value if entry.get('inst_code') else EntityType.FUNDING_AGENCY.value
                new_entry[Field.TITLE.value] = entry.get('title')
                new_entry[Field.FIRST_NAME.value] = entry.get('contact_name').split(' ')[0]
                new_entry[Field.LAST_NAME.value] = ' '.join(entry.get('contact_name').split(' ')[1:])
                new_entry[Field.EMAIL.value] = entry.get('contact_email')
                new_entries.append(new_entry)              
        ApplicationAsset.store(cls._asset_name, new_entries)
        new_entries = ApplicationAsset.retrieve(cls._asset_name) or []
        return {'before': existing, 'after': new_entries}

