from blueprints.api_restplus.api_units.basics import BaseApiUnit, ParserBuilder, ModelFactory
from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
from icms_orm.common import Person, PersonStatusView as PSV
import sqlalchemy as sa


class ApplicationCheckApiCall(BaseApiUnit):

    _ARG_NAME_MRPP = 'most_recent_per_person'
    _ARG_NAME_LRO = 'last_run_only'

    _args = ParserBuilder().\
        add_argument(AAC.cmsId.key, type=ParserBuilder.INTEGER, required=False).\
        add_argument(AAC.runNumber.key, type=ParserBuilder.INTEGER, required=False).\
        add_argument(AAC.instCode.key, type=ParserBuilder.STRING, required=False).\
        add_argument(_ARG_NAME_MRPP, type=ParserBuilder.BOOLEAN, required=False, default=True). \
        add_argument(_ARG_NAME_LRO, type=ParserBuilder.BOOLEAN, required=False, default=False). \
        parser

    _model_cols_dict = {
        AAC.id: ModelFactory.Integer,
        AAC.cmsId: ModelFactory.Integer,
        Person.first_name: ModelFactory.String,
        Person.last_name: ModelFactory.String,
        PSV.activity: ModelFactory.String,
        AAC.runNumber: ModelFactory.Integer,
        AAC.datetime: ModelFactory.DateTime,
        AAC.instCode: ModelFactory.String,
        AAC.moStatus: ModelFactory.Boolean,
        AAC.daysCount: ModelFactory.Integer,
        AAC.workedInst: ModelFactory.Float,
        AAC.workedSelf: ModelFactory.Float,
        AAC.neededInst: ModelFactory.Float,
        AAC.passed: ModelFactory.Boolean,
        AAC.failed: ModelFactory.Boolean,
        AAC.reason: ModelFactory.String
    }

    _model = ModelFactory.make_model('Authorship Application Check', _model_cols_dict)

    @classmethod
    def list_most_recent_checks(cls):
        filter_args = cls.parse_args()
        columns = list(cls._model_cols_dict.keys())
        q = AAC.session().query(*columns).join(Person, Person.cms_id == AAC.cmsId).join(PSV, Person.cms_id == PSV.cms_id)
        q = cls.attach_query_filters(q, filter_args, columns)
        if filter_args.get(cls._ARG_NAME_LRO) is True:
            # if no filtering is provided, we default to showing the records for the most recent run only
            sq = AAC.session().query(sa.func.max(AAC.runNumber).label('number')).subquery()
            q = q.join(sq, AAC.runNumber == sq.c.number)
        elif filter_args.get(cls._ARG_NAME_MRPP) is True:
            q = q.order_by(AAC.cmsId, sa.desc(AAC.datetime)).distinct(AAC.cmsId)
        return cls.rows_to_list_of_dicts(q.all(), columns)

