from datetime import datetime
from typing import Any, Dict, List, Optional, Type

from flask_login import current_user
from icms_orm import PseudoEnum
from icms_orm.cmspeople import Person as ProtoPerson
from icms_orm.common import Person, UserAuthorData
from icmsutils.prehistory.prehistory_writer import PersonAndPrehistoryWriter
from sqlalchemy.orm.query import Query

from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.api_restplus.api_units.basics.crud import (
    AbstractCRUDUnit,
    Deleteless,
    ModelFieldDefinition,
)
from blueprints.api_restplus.api_units.basics.model_field_types import (
    DateTime,
    Integer,
    String,
)
from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException


class AuthordataApiUnit(AbstractCRUDUnit, Deleteless):
    class Field(PseudoEnum):
        CMS_ID = (
            ModelFieldDefinition.builder(Integer)
            .column(UserAuthorData.cmsid)
            .name("cms_id")
            .id()
            .build()
        )
        HR_ID = (
            ModelFieldDefinition.builder(Integer)
            .column(Person.hr_id)
            .name("hr_id")
            .readonly()
            .build()
        )
        INSPIRE_ID = (
            ModelFieldDefinition.builder(String)
            .column(UserAuthorData.inspireid)
            .name("inspire_id")
            .build()
        )
        ORCID_ID = (
            ModelFieldDefinition.builder(String)
            .column(UserAuthorData.orcid)
            .name("orc_id")
            .build()
        )
        LAST_UPDATE = (
            ModelFieldDefinition.builder(DateTime)
            .column(UserAuthorData.lastupdate)
            .name("last_update")
            .readonly()
            .build()
        )
        FORENAME = (
            ModelFieldDefinition.builder(String)
            .column(Person.first_name)
            .name("forename")
            .readonly()
            .build()
        )
        SURNAME = (
            ModelFieldDefinition.builder(String)
            .column(Person.last_name)
            .name("surname")
            .readonly()
            .build()
        )

    @classmethod
    def get_model_fields_list(
        cls: Type["AuthordataApiUnit"],
    ) -> List[ModelFieldDefinition]:
        return list(cls.Field.values())

    @classmethod
    def get_model_name(cls: Type["AuthordataApiUnit"]) -> str:
        return "Authordata"

    @classmethod
    def get_query(
        cls: Type["AuthordataApiUnit"], resource_filter: Optional[ResourceFilter]
    ) -> Query:
        q: Query = super().get_query(resource_filter=resource_filter)
        return q.join(Person, Person.cms_id == UserAuthorData.cmsid)

    # overriding default impelmentation as it leads to permission-related queries for each object
    @classmethod
    def _add_hypermedia_to_row_dict(cls, input_dict):
        if current_user.is_admin() or current_user.cms_id == input_dict["cms_id"]:
            input_dict["_links"] = {"update": "update-link-placeholder"}
        return input_dict

    @classmethod
    def customize_mapper_before_persisting(
        cls: Type["AuthordataApiUnit"], target: UserAuthorData
    ):
        # The DB wants both CMS ID and HR ID but we can spare FE the hassle of providing both
        p: Person = Person.query.filter(Person.cms_id == target.cmsid).one()
        target.hrid = p.hr_id
        target.lastupdate = datetime.utcnow()

    @classmethod
    def db_object_to_model_compliant_dict(
        cls: Type["AuthordataApiUnit"], target: UserAuthorData
    ):
        return cls.get_by_identity(target.cmsid)

    @classmethod
    def _propagate_to_old_db(cls: Type["AuthordataApiUnit"], target: Dict[str, Any]):
        """Technically it could be plugged directly in db_object_to_model...dict but it would smell"""
        person: ProtoPerson = ProtoPerson.query.filter(
            ProtoPerson.cmsId == target[cls.Field.CMS_ID.name]
        ).one()
        writer: PersonAndPrehistoryWriter = PersonAndPrehistoryWriter(
            ProtoPerson.session(), person, current_user.person
        )
        writer.set_new_value(ProtoPerson.authorId, target[cls.Field.INSPIRE_ID.name])
        writer.apply_changes(do_commit=True)

    @classmethod
    def put_by_identity(cls: Type["AuthordataApiUnit"], *params) -> Dict[str, Any]:
        """Updates person\'s author data, including propagation of INSPIRE ID to the old DB"""
        if params[0] != cls.get_payload().get(cls.Field.CMS_ID.name):
            raise ArbitraryErrorCodeException(
                400, "Conflicting CMS IDs (payload vs path variable)"
            )
        val = super().put_by_identity(*params)
        cls._propagate_to_old_db(val)
        return val

    @classmethod
    def post(cls: Type["AuthordataApiUnit"]) -> Dict[str, object]:
        """Inserts a new author data record, propagating the INSPIRE ID to the old DB"""
        val = super().post()
        cls._propagate_to_old_db(val)
        return val
