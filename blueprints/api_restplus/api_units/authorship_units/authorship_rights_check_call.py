from os import read
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import List
from blueprints.api_restplus.api_units.basics.builders import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics.crud import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import ModelFactory
from icmsutils.businesslogic.authorship import AuthorRightsChecker as Checker


class AuthorshipRightsCheckUnit(AbstractCRUDUnit):
    FIELD_CMS_ID = ModelFieldDefinition.builder(
        ModelFactory.Integer).name("cms_id").id().build()
    FIELD_REMARKS = ModelFieldDefinition.builder(
        ModelFactory.String).name("remarks").readonly(True).build()
    FIELD_IS_AUTHOR = ModelFieldDefinition.builder(
        ModelFactory.Boolean).name("is_author").readonly(True).build()
    FIELD_TO_LOSE_AUTHORSHIP = ModelFieldDefinition.builder(
        ModelFactory.Boolean).name("to_lose_authorship").readonly(True).build()
    FIELD_TO_GAIN_AUTHORSHIP = ModelFieldDefinition.builder(
        ModelFactory.Boolean).name("to_gain_authorship").readonly(True).build()

    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return [cls.FIELD_CMS_ID, cls.FIELD_IS_AUTHOR, cls.FIELD_TO_GAIN_AUTHORSHIP, cls.FIELD_TO_LOSE_AUTHORSHIP, cls.FIELD_REMARKS]

    @classmethod
    def get_model_name(cls) -> str:
        return "Authorship Rights Check"

    @classmethod
    def get(cls, needle: ResourceFilter):
        return cls.run(read_only=True, cms_id=needle.value)

    @classmethod
    def put(cls, needle: ResourceFilter):
        return cls.run(read_only=False, cms_id=needle.value)

    @classmethod
    def run(cls, read_only=True, cms_id=None):
        results = []
        cms_ids = []
        if cms_id:
            cms_ids.append(cms_id)
        checker: Checker = Checker(cms_ids)
        checker.run(only_create_model=read_only, force_dry_run=read_only)
        for cms_id in checker.get_cms_ids():
            results.append(dict(
                cms_id=cms_id,
                remarks=checker.get_remarks(cms_id),
                is_author=checker.is_current_author(cms_id),
                is_active_member=checker.is_active_member(cms_id),
                to_gain_authorship=cms_id in checker.get_authors_to_add(),
                to_lose_authorship=cms_id in checker.get_authors_to_drop()
            ))
        return len(results) == 1 and results[0] or results

    @classmethod
    def is_model_field_eligible_for_parser(cls, field: ModelFieldDefinition):
        return field.name == cls.FIELD_CMS_ID.name
