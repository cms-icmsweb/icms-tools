import typing

from blueprints.api_restplus.api_factory import SingletonApiFactory
from blueprints.api_restplus.api_units.basics.builders import \
    ModelFieldDefinition

from blueprints.api_restplus.api_units.basics.model_field_types import Integer, NullableInteger
from blueprints.api_restplus.api_units.basics.model_field_types import Boolean
from blueprints.api_restplus.api_units.basics.model_field_types import String, NullableString
from blueprints.api_restplus.api_units.basics.model_field_types import Raw
from blueprints.api_restplus.api_units.basics.model_field_types import Date, NullableDate
from blueprints.api_restplus.api_units.basics.model_field_types import DateTime
from blueprints.api_restplus.api_units.basics.model_field_types import Nested
from blueprints.api_restplus.api_units.basics.model_field_types import List
from blueprints.api_restplus.api_units.basics.model_field_types import Float


class ModelFactory(object):
    # TODO: refactor away the references, remove this proxy
    Integer = Integer
    Boolean = Boolean
    String = String
    Raw = Raw
    Date = Date
    DateTime = DateTime
    Nested = Nested
    List = List
    Float = Float
    NullableDate = NullableDate
    NullableString = NullableString
    NullableInteger = NullableInteger

    # Param names that can be passed off to model's argument constructor (we use our proxy for that)
    REQUIRED = 'required'
    ENUM = 'enum'
    DEFAULT = 'default'
    DESCRIPTION = 'description'
    EXAMPLE = 'example'
    ELEMENT_TYPE = 'cls_or_instance'
    READONLY = 'readonly'
    # that's the field name from source object that will be translated into destination object's field during marshaling
    ATTRIBUTE = 'attribute'

    # a safeguard against name collisions
    _names_registry = set()

    @classmethod
    def _register_name(cls, name: str):
        if name in cls._names_registry:
            raise ValueError(
                'API model class name {0} already in use!'.format(name))
        cls._names_registry.add(name)

    @classmethod
    def make_model(cls, name: str, fields_map):
        # this ugly line checks if map keys happen to have a key attribute (like InstrumentedAttribute instances)
        cls._register_name(name)
        fields_map = {
            hasattr(_k, 'key') and _k.key or
            str(_k): _v
            for _k, _v in fields_map.items()
        }
        return SingletonApiFactory.get_api().model(name, fields_map)

    @classmethod
    def make_model_from_fields_list(cls, name: str, fields: typing.List[ModelFieldDefinition]):
        cls._register_name(name)
        fields_map = dict()
        field: ModelFieldDefinition
        for field in fields:
            field_options = field.as_pruned_dict()
            fields_map[field.name] = cls.field_proxy(field.type, field_options)
        return SingletonApiFactory.get_api().model(name, fields_map)

    @classmethod
    def make_hypermedia_model(cls, name: str, fields_map: dict):
        fields_and_links = dict()
        fields_and_links.update(fields_map)
        _key = '_links'
        while _key in fields_and_links.keys():
            _key = '_' + _key
        fields_and_links[_key] = ModelFactory.field_proxy(
            ModelFactory.Raw, {ModelFactory.READONLY: True})
        return cls.make_model(name, fields_and_links)

    @classmethod
    def field_proxy(cls, field_type, field_options):
        """
        :param field_type: one of the types exposed through ModelFactory, like ModelFactory.Raw
        :param field_options: ideally, a dict
        :return:
        """
        if isinstance(field_options, dict):
            return field_type(**field_options)
        elif isinstance(field_options, list):
            return field_type(*field_options)
        raise Exception(
            'Cannot produce the API Model Field proxy from this field_options object. ')
