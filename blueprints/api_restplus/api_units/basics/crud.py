import abc

from blueprints.api_restplus.api_units.basics.model_field_types import AnyFieldType, Boolean, Date, DateTime, Integer, String
import logging
from collections import OrderedDict
from dataclasses import dataclass
from typing import Any, Dict, List, Optional, Set, Type

import flask
from sqlalchemy.orm.query import Query
from blueprints.api_restplus.api_units.basics import BaseApiUnit
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.api_restplus.api_units.basics.builders import (
    ModelFieldDefinition, ParserBuilder)
from blueprints.api_restplus.api_units.basics.factories import ModelFactory
from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException
from icms_orm import IcmsModelBase
from icms_orm.orm_interface.icms_alchemy_session import IcmsAlchemySession

log: logging.Logger = logging.getLogger(__name__)


class AbstractCRUDUnit(BaseApiUnit, metaclass=abc.ABCMeta):

    @classmethod
    def get_payload(cls: Type['AbstractCRUDUnit']):
        """
        Overriding the superclass' method to convert dates, 
        datetimes and whatever else we discover down the road not to do what it should
        """
        payload = super().get_payload()
        field: ModelFieldDefinition
        for field in cls.get_model_fields_list():
            value = payload.get(field.name, None)
            if value is None:
                if field.default is not None:
                    value = payload[field.name] = field.default
                else:
                    continue
            if field.type == DateTime:
                payload[field.name] = DateTime().parse(value)
            elif field.type == Date:
                payload[field.name] = Date().parse(value)
        return payload

    @classmethod
    @abc.abstractclassmethod
    def get_model_fields_list(cls: Type['AbstractCRUDUnit']) -> List[ModelFieldDefinition]:
        return []

    @classmethod
    @abc.abstractclassmethod
    def get_model_name(cls: Type['AbstractCRUDUnit']) -> str:
        return ''

    @classmethod
    def get_id_fields(cls: Type['AbstractCRUDUnit']) -> List[ModelFieldDefinition]:
        """
        Provides the list of fields consituing the entry's identity.
        For compatibility reasons that might fall back to returning the first field defined.
        """
        f: ModelFieldDefinition
        result = [f for f in cls.get_model_fields_list() if f.id]
        if not result:
            log.warning(
                f'API model {cls.get_model_name()} defines no ID fields - the first field will be picked')
            return cls.get_model_fields_list()[0:1]
        return result

    @classmethod
    def _add_hypermedia_to_row_dict(cls: Type['AbstractCRUDUnit'], input_dict, path_variables=None):
        field: ModelFieldDefinition
        path_vars = OrderedDict()
        for field in cls.get_id_fields():
            path_vars[field.name] = input_dict.get(field.name, None)
        return super()._add_hypermedia_to_row_dict(input_dict, path_variables=path_vars)

    @classmethod
    def get_model_metadata_fields_list(cls: Type['AbstractCRUDUnit']) -> List[ModelFieldDefinition]:
        """
        Only returns the metadata fields (as opposed to regular fields, defined in the subclass)
        """
        return [
            ModelFieldDefinition.builder(ModelFactory.Raw).name(
                '_links').readonly(True).default([]).build(),
        ]

    @classmethod
    def get_db_session(cls: Type['AbstractCRUDUnit']) -> IcmsAlchemySession:
        return flask.current_app.db.session()

    @classmethod
    def get_model(cls: Type['AbstractCRUDUnit']):
        if cls._model is None:
            cls._model = ModelFactory.make_model_from_fields_list(
                cls.get_model_name(), cls.get_model_fields_list() + cls.get_model_metadata_fields_list())
        return cls._model

    @classmethod
    def get_db_columns(cls: Type['AbstractCRUDUnit']):
        # TODO: test that only DB-backed fields are returned here
        return [f.column.label(f.name) for f in cls.get_model_fields_list() if f.column is not None]

    @classmethod
    def get_writable_mapper_classes(cls: Type['AbstractCRUDUnit']) -> Set[IcmsModelBase]:
        result = set()
        field: ModelFieldDefinition
        for field in cls.get_model_fields_list():
            if not field.readonly and field.column is not None:
                klass = field.column.parent._identity_class
                result.add(klass)
        return result

    @classmethod
    def get_query(cls: Type['AbstractCRUDUnit'], resource_filter: Optional[ResourceFilter] = None) -> Query:
        """
        Translates the list of model fields into a set of corresponding columns to be queried.
        Override this method to perform refinements (eg. joins)
        """
        cols = cls.get_db_columns()
        session = cls.get_db_session()
        q = session.query(*cols)
        while resource_filter is not None:
            if resource_filter.column is not None:
                q = q.filter(resource_filter.column == resource_filter.value)
            resource_filter = resource_filter.next
        return q

    @classmethod
    def validate_mapper_before_persisting(cls: Type['AbstractCRUDUnit'], mapper: IcmsModelBase):
        """
        Everything's fine by default, subclasses can perform their checks by overriding this method.
        """
        pass

    @classmethod
    def customize_mapper_before_persisting(cls: Type['AbstractCRUDUnit'], target: IcmsModelBase):
        """
        Allows last-minute modification of persisted-to-be object, before it's written to DB
        """

    @classmethod
    def db_object_to_model_compliant_dict(cls: Type['AbstractCRUDUnit'], db_object: IcmsModelBase) -> Dict[str, Any]:
        """
        Converts a DB object into a dictionary that can be picked up by restplus' model creator.
        Typically this would come handy after a call to post/put/patch when we have an instance 
        of a mapped entity (as opposed to a list of column values).
        """
        ia_dict = db_object.to_ia_dict()
        result = dict()
        field: ModelFieldDefinition
        for field in cls.get_model_fields_list():
            if field.column is not None and field.column in ia_dict.keys():
                result[field.name] = ia_dict[field.column]
        return result

    @classmethod
    def find(cls: Type['AbstractCRUDUnit'], needle: Optional[ResourceFilter] = None) -> List[Dict[str, Any]]:
        '''Generic get (find all) method retrieving the records / instances matching provided parameter(s).'''
        cols = cls.get_db_columns()
        # here we can inject the criteria coming from a parser
        data = cls.get_query(needle).all()
        return cls.rows_to_list_of_dicts(data, cols)

    @classmethod
    def get(cls: Type['AbstractCRUDUnit'], needle: ResourceFilter) -> Dict[str, Any]:
        '''Generic DB search, running a query parametrized with the provided needle'''
        return cls.row_to_dict(cls.get_query(needle).one(), cls.get_db_columns())

    @classmethod
    def get_by_identity(cls: Type['AbstractCRUDUnit'], *params) -> Dict[str, Any]:
        '''Generic get (find one) method retrieving the record / instance identified by provided parameter(s).'''
        return cls.get(cls.make_id_filter(params))

    @classmethod
    def put(cls: Type['AbstractCRUDUnit'], needle: ResourceFilter, patch_mode: bool = False) -> Dict[str, Any]:
        """
        :param bool patch_mode: if True, only the fields sent over will be overwritten
        """
        query = cls.get_db_session().query(cls.get_writable_mapper_classes().pop())
        current_needle: Optional[ResourceFilter] = needle
        while current_needle is not None:
            query = query.filter(current_needle.column == current_needle.value)
            current_needle = current_needle.next
        target: IcmsModelBase = query.one()
        payload = cls.get_payload()
        field: ModelFieldDefinition
        for field in cls.get_model_fields_list():
            value = payload.get(field.name)
            if field.readonly or (value is None and patch_mode is True) or field.column is None:
                continue
            target.set(field.column, value)
        cls.customize_mapper_before_persisting(target)
        cls.validate_mapper_before_persisting(target)
        cls.get_db_session().add(target)
        cls.get_db_session().commit()
        return cls.db_object_to_model_compliant_dict(target)

    @classmethod
    def put_by_identity(cls: Type['AbstractCRUDUnit'], *params) -> Dict[str, Any]:
        '''Generic put method updating the record / instance identified by provided parameter(s).'''
        return cls.put(cls.make_id_filter(params))

    @classmethod
    def patch_by_identity(cls: Type['AbstractCRUDUnit'], *params) -> Dict[str, Any]:
        '''Generic patch method updating the record / instance identified by provided parameter(s).'''
        return cls.put(cls.make_id_filter(params), patch_mode=True)

    @classmethod
    def delete_by_identity(cls: Type['AbstractCRUDUnit'], *params):
        '''Generic delete method removing the record / instance identified by provided parameter(s).'''
        return cls.delete(cls.make_id_filter(params))

    @classmethod
    def post(cls: Type['AbstractCRUDUnit']) -> Dict[str, object]:
        '''Generic post method creating a record / instance from the payload.'''
        payload = cls.get_payload()
        ia_dict = {f.column: payload.get(
            f.name) for f in cls.get_model_fields_list() if f.column is not None}
        mapper_class = cls.get_writable_mapper_classes().pop()
        mapper_object = mapper_class.from_ia_dict(ia_dict)
        cls.customize_mapper_before_persisting(mapper_object)
        cls.validate_mapper_before_persisting(mapper_object)
        session = cls.get_db_session()
        session.add(mapper_object)
        session.commit()
        return cls.db_object_to_model_compliant_dict(mapper_object)

    @classmethod
    def patch(cls: Type['AbstractCRUDUnit'], needle: ResourceFilter) -> Dict[str, Any]:
        return cls.put(needle, patch_mode=True)

    @classmethod
    def delete(cls: Type['AbstractCRUDUnit'], needle: ResourceFilter):
        session = cls.get_db_session()
        if 1 != cls.get_query(needle).delete():
            raise ArbitraryErrorCodeException(
                404, 'Found no matching rows to be removed')
        session.commit()

    @classmethod
    def make_id_filter(cls: Type['AbstractCRUDUnit'], *values_in_cols_order) -> ResourceFilter:
        '''
        Creates a ResourceFilter allowing to identify a specific instance / record. 
        Matches incoming values against fields defined as id, assuming the orders to match.
        '''
        if len(values_in_cols_order) != len(cls.get_id_fields()):
            raise ValueError(
                f'Cannot create ID filter using {len(cls.get_id_fields())} ID fields and {len(cls.get_id_fields())} provided values!')
        field: ModelFieldDefinition
        # building a chain like id_field1.next.next.next
        previous_filter: Optional[ResourceFilter] = None
        for field, value in reversed(list(zip(cls.get_id_fields(), values_in_cols_order))):
            _f = ResourceFilter(field.column, value, next=previous_filter)
            previous_filter = _f
        if previous_filter is None:
            raise ValueError(
                f'Failed to generate an id filter for {cls.__name__}')
        return previous_filter

    @classmethod
    def _get_parser_type_for_model_type(cls, model_type):
        """
        :return: a matching argument type (or None)
        Provides model-to-parser type translations for auto-generated parsers.
        """
        if issubclass(model_type, Boolean):
            return ParserBuilder.BOOLEAN
        if issubclass(model_type, Integer):
            return ParserBuilder.INTEGER
        elif issubclass(model_type, String):
            return ParserBuilder.STRING
        elif issubclass(model_type, Date):
            return ParserBuilder.DATE
        return None

    @classmethod
    def get_args(cls: Type['AbstractCRUDUnit']):
        """
        Provides a simplistic (yet usually sufficient) implementation of query string argument parser.
        The resulting arguments set will be used to perform a query by example withn (:func:`AsbtractCRUDUnit.find` method).

        In order for a model field to be included in such a parser, 
        a mapping between the model's field type and parser's field type needs to exist in :func:`AsbtractCRUDUnit._get_parser_type_for_model_type`.
        It also needs to meet the criteria imposed by :func:`AsbtractCRUDUnit.is_model_field_eligible_for_parser`.
        Any of the methods mentioned above can be overridden to customise the default behavior.
        """
        builder: ParserBuilder = ParserBuilder()

        field: ModelFieldDefinition
        for field in cls.get_model_fields_list():
            if not cls.is_model_field_eligible_for_parser(field):
                continue
            target_type = cls._get_parser_type_for_model_type(field.type)
            if target_type is not None:
                builder.add_argument(
                    name=field.name, type=target_type, required=False)
        return builder.parser

    @classmethod
    def is_model_field_eligible_for_parser(cls: Type['AbstractCRUDUnit'], field: ModelFieldDefinition) -> bool:
        """
        When creating a default argument parser, this method is consulted on field's eligibility.
        """
        return field.column is not None


class Postless():
    """
    A mixin indicating that the API unit's POST method is not to be exposed
    """
    pass


class Deleteless():
    """
    A mixin indicating that the API unit's DELETE method is not to be exposed
    """
    pass


class Putless():
    """
    A mixin indicating that the API unit's PUT method is not to be exposed
    """
    pass


class Getless():
    """
    A mixin indicating that the API unit's GET (by id) method is not to be exposed
    """
    pass


class Listless():
    """
    A mixin indicating that the unit's GET (collection) method is not to be exposed
    """
    pass


class Listonly(Deleteless, Postless, Putless, Getless):
    """
    A convenience mixin masking out all methods but the GET (collection)
    """
    pass
