from typing import Dict
from blueprints.api_restplus.api_factory import SingletonApiFactory
import logging


class HyperlinkWrapper(object):
    def __init__(self, label: str, resource: type, method: str, **url_params):
        self._label = label
        self._resource = resource
        self._method = method
        self._url_params = url_params
        logging.debug(
            'Created a HyperLink wrapper labelled {0}'.format(self._label))

    @classmethod
    def get_for_resource(cls, label: str, resource, method: str, **params):
        return HyperlinkWrapper(label, resource, method, **params)

    @property
    def url(self):
        return SingletonApiFactory.get_api().url_for(self._resource, **self._url_params)

    def get_url(self, path_variables: Dict = None):
        '''Checks with the API instance for a URL matching the relevant resource and provided parameters'''
        return SingletonApiFactory.get_api().url_for(self._resource, **(path_variables or self._url_params or {}))

    @property
    def method(self):
        return self._method

    @property
    def label(self):
        return self._label
