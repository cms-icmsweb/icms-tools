from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from flask_login import current_user
from icms_orm.common import ImageUpload
from blueprints.api_restplus.exceptions import AccessViolationException
import flask, io


class ImageApiCall(BaseApiUnit):
    _model = None

    _args = ParserBuilder(). \
        add_argument(ImageUpload.id.key, ParserBuilder.INTEGER, required=False). \
        add_argument(ImageUpload.name.key, ParserBuilder.STRING, required=False). \
        parser

    @classmethod
    def get_images_list(cls):
        ssn = ImageUpload.session()
        args = cls.parse_args()
        _cols = [ImageUpload.id, ImageUpload.name, ImageUpload.url]
        q = ssn.query(*_cols)
        q = cls.attach_query_filters(q, args, [ImageUpload.name, ImageUpload.id])
        return cls.rows_to_list_of_dicts(q.all(), _cols)

    @classmethod
    def get_image(cls):
        ssn = ImageUpload.session()
        args = cls.parse_args()
        # For now returning only the data straight from the DB - later on we'll add options to fetch from url
        q = ssn.query(ImageUpload.data)
        q = cls.attach_query_filters(q, args, [ImageUpload.name, ImageUpload.id])
        binary = q.one()[0]

        if 'wsgi.file_wrapper' in flask.request.environ:
            # Falling back to a default that has been proven to work across environments.
            # Otherwise there has been blood after deployment (ERROR:root:io.UnsupportedOperation: fileno).
            del flask.request.environ['wsgi.file_wrapper']
        return flask.send_file(io.BytesIO(binary), mimetype='image/png', attachment_filename='image.png')


class ImageUploadApiCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_argument(ImageUpload.id.key, ParserBuilder.INTEGER, required=False).\
        add_argument(ImageUpload.name.key, ParserBuilder.STRING, required=True).\
        add_argument(ImageUpload.data.key, ParserBuilder.FILE, required=True).\
        parser

    _model = ModelFactory.make_model('Image Upload Info', {
        ImageUpload.id.key: ModelFactory.Integer,
        ImageUpload.name.key: ModelFactory.String,
        ImageUpload.url.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        })
    })

    @classmethod
    def upload_image(cls):
        if not current_user.is_admin():
            raise AccessViolationException('Only admins can upload images, sorry!')
        args = cls.parse_args()
        _sent_id = args.get(ImageUpload.id.key)
        _sent_name = args.get(ImageUpload.name.key)
        _sent_data = args.get(ImageUpload.data.key)
        _ssn = ImageUpload.session()
        iu = _ssn.query(ImageUpload).filter(ImageUpload.id == _sent_id).one() if _sent_id else ImageUpload.from_ia_dict({})
        cls.update_db_object(iu, payload=args, rules=[(ImageUpload.name, None, None), (ImageUpload.data, None, lambda f: f.read(-1))])
        _ssn.add(iu)
        _ssn.commit()
        return iu.to_dict()
