from datetime import datetime
import validators
from datetime import date
from urllib.parse import urlparse

from flask_login import current_user
import sqlalchemy as sa
from sqlalchemy import case, func

from icms_orm.toolkit import PhdInfo, CmsProject
from icms_orm.cmsanalysis import CadiAnalysis, AWG
from icms_orm.old_notes import Note
from models.icms_common import Affiliation as NewAffiliation
from icms_orm.common import Person
from icms_orm.epr import TimeLineUser

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.api_restplus.api_units.basics.model_field_types import DateTime, List
from blueprints.api_restplus.exceptions import (
    BadRequestException,
    AccessViolationException,
)

ENSURE_THESIS_LINK_IN_CDS = True


class PhdInfoApiCall(BaseApiUnit):

    student_model = ModelFactory.make_model(
        "Student",
        {
            PhdInfo.cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: 1},
            ),
            Person.first_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Person.last_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )

    updater_model = ModelFactory.make_model(
        "Updater",
        {
            PhdInfo.last_updated_by.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Person.first_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Person.last_name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )

    phd_info_model = ModelFactory.make_model(
        "PhD Info",
        {
            PhdInfo.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            PhdInfo.thesis_title.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: "ThesisTitle",
                },
            ),
            PhdInfo.thesis_link.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: "https://cds.cern.ch/record/1",
                },
            ),
            "student": ModelFactory.Nested(student_model),
            PhdInfo.student_institute.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: "CERN",
                },
            ),
            PhdInfo.enrollment_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            PhdInfo.dissertation_defense_date.key: ModelFactory.field_proxy(
                ModelFactory.NullableDate,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            PhdInfo.physics_groups.key: ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: ["BPH", "B2G", "BTV"],
                },
            ),
            PhdInfo.cms_projects.key: ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: ["DAQ", "BRIL", "COM"],
                },
            ),
            PhdInfo.cadi_analyses.key: ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: ["SUS-09-002", "TOP-09-014", "FTR-21-001"],
                },
            ),
            PhdInfo.cms_notes.key: ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: [
                        "CMS IN-2005/026",
                        "CMS AN-2007/053",
                        "CMS DN-2012/010",
                    ],
                },
            ),
            PhdInfo.remarks.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: " ",
                },
            ),
            PhdInfo.is_active.key: ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: True},
            ),
            PhdInfo.last_updated_at.key: ModelFactory.field_proxy(
                ModelFactory.DateTime,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "start_date": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "end_date": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "updater": ModelFactory.Nested(updater_model),
            "_allowed_actions": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.EXAMPLE: ["edit", "delete"],
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                    ModelFactory.DEFAULT: ["edit", "delete"],
                },
            ),
        },
    )
    _model = ModelFactory.make_model(
        "PhD Info List",
        {
            "phd_info": ModelFactory.List(ModelFactory.Nested(phd_info_model)),
            "_allowed_collection_actions": ModelFactory.Nested(
                ModelFactory.make_model(
                    "Collection Actions",
                    {
                        "actions": ModelFactory.field_proxy(
                            ModelFactory.String,
                            {
                                ModelFactory.REQUIRED: False,
                                ModelFactory.READONLY: True,
                            },
                        ),
                        "institutes": ModelFactory.field_proxy(
                            ModelFactory.List,
                            {
                                ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                                ModelFactory.REQUIRED: False,
                                ModelFactory.READONLY: True,
                            },
                        ),
                    },
                )
            ),
        },
    )
    _args = (
        ParserBuilder()
        .add_argument(PhdInfo.id.key, type=ParserBuilder.INTEGER, required=False)
        .add_argument(PhdInfo.cms_id.key, type=ParserBuilder.INTEGER, required=False)
        .add_argument(
            PhdInfo.last_updated_by.key, type=ParserBuilder.INTEGER, required=False
        )
        .add_argument(
            PhdInfo.enrollment_date.key, type=ParserBuilder.DATE, required=False
        )
        .add_argument(
            PhdInfo.thesis_title.key, type=ParserBuilder.STRING, required=False
        )
        .add_argument(
            PhdInfo.thesis_link.key, type=ParserBuilder.STRING, required=False
        )
        .add_argument(
            PhdInfo.dissertation_defense_date.key,
            type=ParserBuilder.DATE,
            required=False,
        )
        # appendable arguments
        .add_appendable_argument(
            PhdInfo.physics_groups.key, type=ParserBuilder.STRING, required=False
        )
        .add_appendable_argument(
            PhdInfo.cms_projects.key, type=ParserBuilder.STRING, required=False
        )
        .add_appendable_argument(
            PhdInfo.cadi_analyses.key, type=ParserBuilder.STRING, required=False
        )
        .add_appendable_argument(
            PhdInfo.cms_notes.key, type=ParserBuilder.STRING, required=False
        )
        .add_argument(
            "include_deleted",
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=False,
        )
        .parser
    )
    _args_mapping = [
        PhdInfo.id,
        PhdInfo.cms_id,
        PhdInfo.last_updated_by,
        PhdInfo.enrollment_date,
        PhdInfo.thesis_title,
        PhdInfo.thesis_link,
        PhdInfo.dissertation_defense_date,
    ]
    _cols_in = [
        PhdInfo.cms_id,
        PhdInfo.student_institute,
        PhdInfo.last_updated_by,
        PhdInfo.enrollment_date,
        PhdInfo.thesis_title,
        PhdInfo.thesis_link,
        PhdInfo.dissertation_defense_date,
        PhdInfo.physics_groups,
        PhdInfo.cms_projects,
        PhdInfo.cadi_analyses,
        PhdInfo.cms_notes,
        PhdInfo.remarks,
        PhdInfo.is_active,
    ]

    @classmethod
    def basic_validation(cls, payload, request_type):
        # CMS id control
        if payload["student"]["cms_id"] <= 0:
            msg = f"Invalid CMS id: {payload['student']['cms_id']}. CMS id must be an integer greater than 0"
            raise BadRequestException(msg)
        # Is active control
        if not payload["is_active"]:
            msg = f"You cannot {request_type} an inactive entry."
            raise BadRequestException(msg)

    @classmethod
    def common_validate_entries(cls, payload):
        # Dependancies from other tables contol
        # Awg
        physics_groups = (
            AWG.session()
            .query(AWG.name)
            .filter(AWG.name.in_(payload["physics_groups"]))
            .all()
        )
        matched_physics_groups = set(
            physics_group[0] for physics_group in physics_groups
        )
        if not (matched_physics_groups == set(payload["physics_groups"])):
            raise BadRequestException("Invalid physics group")
        # Cms project
        cms_projects = (
            CmsProject.session()
            .query(CmsProject.code)
            .filter(CmsProject.code.in_(payload["cms_projects"]))
            .all()
        )
        matched_cms_projects = set(cms_project[0] for cms_project in cms_projects)
        if not (matched_cms_projects == set(payload["cms_projects"])):
            raise BadRequestException("Invalid CMS project")
        # Analysis
        # Some analysis codes start with a "d" in the Analysis table.
        # It is not possible for them to start both with and without the "d".
        if payload["cadi_analyses"]:
            submitted_analyses = payload["cadi_analyses"]
            target_analyses = submitted_analyses + [
                f"d{analysis}" for analysis in submitted_analyses
            ]
            cadi_analyses = (
                CadiAnalysis.session()
                .query(CadiAnalysis.code)
                .filter(CadiAnalysis.code.in_(target_analyses))
                .all()
            )
            matched_cadi_analyses = set(
                cadi_analysis[0].strip("d") for cadi_analysis in cadi_analyses
            )
        else:
            matched_cadi_analyses = set()
        if not (matched_cadi_analyses == set(payload["cadi_analyses"])):
            raise BadRequestException("Invalid analysis code")
        # Note
        cms_notes = (
            Note.session()
            .query(Note.cmsNoteId)
            .filter(Note.cmsNoteId.in_(payload["cms_notes"]))
            .all()
        )
        matched_cms_notes = set(cms_note[0] for cms_note in cms_notes)
        if not (matched_cms_notes == set(payload["cms_notes"])):
            raise BadRequestException("Invalid CMS note")
        # Dates control
        if (
            payload["dissertation_defense_date"]
            and payload["enrollment_date"] > payload["dissertation_defense_date"]
        ):
            raise BadRequestException(
                f"Enrollment date ({payload['enrollment_date']}) cannot be after"
                f" dissertation defense date ({payload['dissertation_defense_date']})"
            )
        # Thesis link control
        thesis_link = payload["thesis_link"]
        if thesis_link:
            if not validators.url(thesis_link):
                raise BadRequestException(f"Invalid link provided: {thesis_link}")
            if ENSURE_THESIS_LINK_IN_CDS:
                parsed = urlparse(payload["thesis_link"])
                domain = parsed.netloc
                path = parsed.path
                if domain == "cds.cern.ch" and path.startswith("/record/"):
                    path_integer = path.split("/record/")[1]
                    if not path_integer.isdigit():
                        raise BadRequestException(
                            f"Invalid record in link provided: {path_integer}"
                        )
                else:
                    raise BadRequestException(
                        f"Expected a link to a CDS record; instead got: {thesis_link}"
                    )

    @classmethod
    def validate_new_entry(cls, payload):
        cls.basic_validation(payload, "create")
        existing_entry = (
            PhdInfo.session()
            .query(PhdInfo.id)
            .filter(PhdInfo.cms_id == payload["student"]["cms_id"])
            .filter(PhdInfo.is_active == True)
            .one_or_none()
        )
        if existing_entry:
            msg = (
                f"An entry for CMS id: {payload['student']['cms_id']} exists already. "
                f"Please try again with an other CMS id or edit the correct entry."
            )
            raise BadRequestException(msg)
        cls.common_validate_entries(payload)

    @classmethod
    def validate_updated_entry(cls, payload, info_id):
        cls.basic_validation(payload, "edit")
        existing_cms_id = (
            PhdInfo.session()
            .query(PhdInfo.cms_id)
            .filter(PhdInfo.id == info_id)
            .filter(PhdInfo.is_active == True)
            .one_or_none()
        )
        if existing_cms_id is None:
            raise BadRequestException(
                f"No active info found for the given CMS id: {payload['student']['cms_id']}"
            )
        elif existing_cms_id[0] != payload["student"]["cms_id"]:
            msg = f"You cannot edit the CMS id ({payload['student']['cms_id']}) of a student"
            raise BadRequestException(msg)
        cls.common_validate_entries(payload)

    @classmethod
    def get_student_inst_codes_query(cls, cms_id):
        return (
            TimeLineUser.session()
            .query(sa.distinct(TimeLineUser.instCode))
            .filter(TimeLineUser.cmsId == cms_id)
            .filter(TimeLineUser.status == "CMS")
            .filter(TimeLineUser.category == 1)
        )

    @classmethod
    def check_if_student_in_institute(cls, payload):
        # Institutes
        institutes_query = (
            cls.get_student_inst_codes_query(payload["student"]["cms_id"])
            .filter(TimeLineUser.instCode == payload["student_institute"])
            .all()
        )
        if not institutes_query:
            student_cms_id = payload["student"]["cms_id"]
            institute = payload["student_institute"]
            raise AccessViolationException(
                f"No PhD student found in {institute} with CMS id: {student_cms_id}"
            )

    @classmethod
    def canCreate(cls, cms_id):
        return (
            current_user.is_admin()
            or current_user.is_leader_of_member(cms_id)
            or current_user.cms_id == cms_id
        )

    @classmethod
    def get_create_permissions(cls):
        """Return an (action, institutes) tuple that indicates for
        which institutes the current user can create entries.
        """

        if current_user.is_admin():
            return "create-all", []
        represented_insts = current_user.get_represented_institutes_codes()
        if represented_insts:
            return "create-team", list(represented_insts.keys())
        student_insts = cls.get_student_inst_codes_query(current_user.cms_id).all()
        if student_insts:
            return "create-self", [i[0] for i in student_insts]
        return "", []

    @classmethod
    def canEdit(cls, cms_id):
        return (
            current_user.is_admin()
            or current_user.is_leader_of_member(cms_id)
            or current_user.cms_id == cms_id
        )

    @classmethod
    def canDelete(cls, cms_id):
        return current_user.is_admin() or current_user.is_leader_of_member(cms_id)

    @classmethod
    def get_allowed_actions(cls, cms_id) -> list:
        item_permissions = []
        if cls.canEdit(cms_id):
            item_permissions.append("edit")
        if cls.canDelete(cms_id):
            item_permissions.append("delete")
        return item_permissions

    @classmethod
    def get_allowed_actions_dict(cls, phd_info):
        actions = {}
        institute_codes = list(current_user.get_represented_institutes_codes().keys())
        for entry in phd_info:
            item_permissions = []
            if (
                current_user.is_admin()
                or (entry.student_institute in institute_codes)
                or current_user.cms_id == entry.cms_id
            ):
                item_permissions.append("edit")
            if current_user.is_admin() or (entry.student_institute in institute_codes):
                item_permissions.append("delete")
            actions[entry.cms_id] = item_permissions
        return actions

    @classmethod
    def string_to_list(cls, phd_info):
        for col in ["physics_groups", "cms_projects", "cadi_analyses", "cms_notes"]:
            phd_info_col = getattr(phd_info, col)
            if phd_info_col:
                setattr(phd_info, col, phd_info_col.split(","))
            else:
                setattr(phd_info, col, [])
        return phd_info

    @classmethod
    def list_to_string(cls, payload):
        for col in ["physics_groups", "cms_projects", "cadi_analyses", "cms_notes"]:
            payload[col] = ",".join(payload[col])
        return payload

    @classmethod
    def get_start_end_date(cls, cms_id=None):
        ssn = PhdInfo.session()
        # CTE = common table expression
        cte = (
            ssn.query(
                PhdInfo.thesis_title.label("thesis_title"),
                TimeLineUser.cmsId.label("user_cms_id"),
                TimeLineUser.status.label("status"),
                TimeLineUser.category.label("category"),
                TimeLineUser.timestamp.label("start_date"),
                func.lead(TimeLineUser.timestamp)
                .over(
                    partition_by=[TimeLineUser.cmsId], order_by=[TimeLineUser.timestamp]
                )
                .label("end_date"),
            )
            .select_from(TimeLineUser)
            .join(PhdInfo, PhdInfo.cms_id == TimeLineUser.cmsId)
            .filter(PhdInfo.is_active == True)
            .cte("cte")
        )
        min_date = datetime.strptime("1900-01-01", "%Y-%m-%d")
        max_date = datetime.strptime("2099-12-31", "%Y-%m-%d")
        q = (
            ssn.query(
                cte.c.thesis_title,
                cte.c.user_cms_id,
                case(
                    [
                        (
                            func.min(func.coalesce(cte.c.start_date, min_date))
                            == min_date,
                            None,
                        )
                    ],
                    else_=func.min(cte.c.start_date),
                ).label("phd_start_date"),
                case(
                    [
                        (
                            func.max(func.coalesce(cte.c.end_date, max_date))
                            == max_date,
                            None,
                        )
                    ],
                    else_=func.max(cte.c.end_date),
                ).label("phd_end_date"),
            )
            .filter(cte.c.category == 1)
            .filter(cte.c.status.startswith("CMS"))
            .group_by(cte.c.user_cms_id, cte.c.thesis_title)
            .order_by(cte.c.user_cms_id)
        )
        dict_from_query = {}
        if cms_id is not None:
            result = q.filter(cte.c.user_cms_id == cms_id).one()
            dict_from_query[result[1]] = [result[0], result[2], result[3]]
        else:
            result = q.all()
            for row in result:
                dict_from_query[row[1]] = [row[0], row[2], row[3]]
        return dict_from_query

    @classmethod
    def get_phd_info_list(cls):
        args = cls.parse_args()
        q = PhdInfo.session().query(PhdInfo)
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        if not args.get("include_deleted"):
            q = q.filter(PhdInfo.is_active == True)
        res = q.all()
        PhdInfo.session().expunge_all()
        response = {}
        response["phd_info"] = res
        actions, institutes = cls.get_create_permissions()
        response["_allowed_collection_actions"] = {}
        response["_allowed_collection_actions"]["actions"] = actions
        response["_allowed_collection_actions"]["institutes"] = institutes
        start_end_date_info = cls.get_start_end_date()
        allowed_actions_dict = cls.get_allowed_actions_dict(response["phd_info"])
        for phd_info_entry in response["phd_info"]:
            cls.string_to_list(phd_info_entry)
            _, phd_info_entry.start_date, phd_info_entry.end_date = start_end_date_info[
                phd_info_entry.cms_id
            ]
            phd_info_entry.updater.last_updated_by = phd_info_entry.last_updated_by
            phd_info_entry._allowed_actions = allowed_actions_dict[
                phd_info_entry.cms_id
            ]
        return response

    @classmethod
    def get_phd_info_entry(cls, info_id):
        res = (
            PhdInfo.session().query(PhdInfo).filter(PhdInfo.id == info_id).one_or_none()
        )
        PhdInfo.session().expunge_all()
        if res is None:
            raise BadRequestException("No info found for the given id.")
        item = res
        cls.string_to_list(item)
        start_end_date_info = cls.get_start_end_date(item.cms_id)
        _, item.start_date, item.end_date = start_end_date_info[item.cms_id]
        item.updater.last_updated_by = item.last_updated_by
        item._allowed_actions = cls.get_allowed_actions(item.cms_id)
        return item

    @classmethod
    def create_phd_info(cls):
        payload = cls.get_payload()
        cls.validate_new_entry(payload)
        if not cls.canCreate(payload["student"]["cms_id"]):
            raise AccessViolationException(
                "You are not allowed to add info for this CMS id."
            )
        cls.check_if_student_in_institute(payload)
        new_phd_info = PhdInfo.from_ia_dict({})
        cls.list_to_string(payload)
        cls.update_db_object(
            new_phd_info,
            {
                **payload,
                "cms_id": payload["student"]["cms_id"],
                "last_updated_by": current_user.cms_id,
            },
            cls._cols_in,
        )
        return cls.get_phd_info_entry(new_phd_info.id)

    @classmethod
    def edit_phd_info(cls, info_id):
        payload = cls.get_payload()
        cls.validate_updated_entry(payload, info_id)
        if not cls.canEdit(payload["student"]["cms_id"]):
            raise AccessViolationException(
                "You are not allowed to edit the info of this CMS id."
            )
        cls.check_if_student_in_institute(payload)
        info_to_update = PhdInfo.session().query(PhdInfo).get(info_id)
        cls.list_to_string(payload)
        cls.update_db_object(
            info_to_update,
            {
                **payload,
                "cms_id": payload["student"]["cms_id"],
                "last_updated_by": current_user.cms_id,
            },
            cls._cols_in,
        )
        return cls.get_phd_info_entry(info_to_update.id)

    @classmethod
    def delete_phd_info(cls, info_id):
        info_to_delete = PhdInfo.session().query(PhdInfo).get(info_id)
        if not info_to_delete:
            raise BadRequestException("No info found for the given id.")
        if not cls.canDelete(info_to_delete.cms_id):
            raise AccessViolationException(
                "You are not allowed to delete the info of this CMS id."
            )
        cls.update_db_object(
            info_to_delete,
            {"is_active": False, "last_updated_by": current_user.cms_id},
            [PhdInfo.is_active, PhdInfo.last_updated_by],
        )
        return {}
