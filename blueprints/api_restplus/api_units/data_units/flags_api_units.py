import flask
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from models.icms_people import Flag, PeopleFlagsAssociation
from icms_orm.cmspeople import Institute, Person, CountryRegionMapping


class FlagsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model(
        "Flag Info",
        {
            Person.cmsId.key: ModelFactory.Integer,
            Person.lastName.key: ModelFactory.String,
            Person.firstName.key: ModelFactory.String,
            Person.instCode.key: ModelFactory.String,
            Flag.id.key: ModelFactory.String,
            Flag.desc.key: ModelFactory.String,
            Institute.country.key: ModelFactory.String,
            CountryRegionMapping.region_code.key: ModelFactory.String,
        },
    )

    @classmethod
    def get_flags(cls):
        PFA = PeopleFlagsAssociation
        ssn = PFA.session

        cols = [
            Person.cmsId,
            Person.lastName,
            Person.firstName,
            Person.instCode,
            Flag.id,
            Flag.desc,
            Institute.country,
            CountryRegionMapping.region_code,
        ]
        q = (
            ssn.query(*cols)
            .join(PFA, Person.cmsId == PFA.cmsId)
            .join(Flag, Flag.id == PFA.flagId)
            .join(Institute, Institute.code == Person.instCode)
            .join(CountryRegionMapping, CountryRegionMapping.name == Institute.country)
        )
        return cls.rows_to_list_of_dicts(q.all(), cols)
