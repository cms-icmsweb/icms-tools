from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)


class AwardProjectsApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Awards Project",
        {
            "projects": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.READONLY: True,
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                },
            ),
        },
    )
    _args = ParserBuilder().parser

    _project_choices = (
        "Administration",
        "BRIL",
        "DAQ",
        "Diversity, Equity, Inclusion",
        "ECAL",
        "HCAL",
        "HGCAL",
        "L1 Trigger",
        "MTD",
        "Muons",
        "Offline and Computing",
        "Other",
        "Outreach and Communication",
        "PPD",
        "PPS",
        "Run Coordination",
        "Technical Coordination",
        "Tracker",
        "Trigger Coordination",
        "Upgrade Coordination",
    )

    @classmethod
    def list_projects(cls):
        return {"projects": cls._project_choices}

    @classmethod
    def get_projects(cls):
        return cls._project_choices
