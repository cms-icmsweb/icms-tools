from datetime import date

from flask_login import current_user
from icms_orm.common import Country, Institute, Person, PersonStatus
from icms_orm.toolkit import Award, AwardNomination, AwardType
from sqlalchemy import and_, func, literal_column, or_
from sqlalchemy.orm import aliased

from blueprints.api_restplus.api_units.award_units.project_unit import (
    AwardProjectsApiCall,
)
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)


def json_agg(table):
    return func.json_agg(literal_column(f'"{table.__tablename__}"'))


class NomineesApiCall(BaseApiUnit):

    _model = ModelFactory.make_model(
        "Award_Nominee",
        {
            "nominations": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.Nested(
                        ModelFactory.make_model(
                            "Award Nomination Brief",
                            {
                                "nomination_id": ModelFactory.field_proxy(
                                    ModelFactory.Integer, {ModelFactory.REQUIRED: True}
                                ),
                                "nominator_cms_id": ModelFactory.field_proxy(
                                    ModelFactory.Integer, {ModelFactory.REQUIRED: True}
                                ),
                                "working_relationship": ModelFactory.field_proxy(
                                    ModelFactory.String, {ModelFactory.REQUIRED: True}
                                ),
                                "justification": ModelFactory.field_proxy(
                                    ModelFactory.String, {ModelFactory.REQUIRED: True}
                                ),
                                "proposed_citation": ModelFactory.field_proxy(
                                    ModelFactory.String, {ModelFactory.REQUIRED: True}
                                ),
                                "remarks": ModelFactory.field_proxy(
                                    ModelFactory.String, {ModelFactory.REQUIRED: False}
                                ),
                                "is_active": ModelFactory.field_proxy(
                                    ModelFactory.Boolean, {ModelFactory.REQUIRED: True}
                                ),
                                "last_updated_at": ModelFactory.field_proxy(
                                    ModelFactory.DateTime, {ModelFactory.REQUIRED: True}
                                ),
                                "last_updated_by": ModelFactory.field_proxy(
                                    ModelFactory.Integer, {ModelFactory.REQUIRED: True}
                                ),
                            },
                        )
                    ),
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            # Extra info for nominee
            "nominee_first_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_last_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_cms_status": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_activity.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nomination_projects.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            AwardNomination.nominee_institute.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_phd_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )

    _project_choices = AwardProjectsApiCall.get_projects()

    _args = (
        ParserBuilder()
        .add_argument("include_deleted", type=ParserBuilder.BOOLEAN, required=False)
        .add_appendable_argument(
            AwardNomination.nominee_cms_id.key,
            type=ParserBuilder.INTEGER,
            required=False,
        )
        .add_appendable_argument(
            AwardNomination.nomination_projects.key,
            type=ParserBuilder.STRING,
            choices=_project_choices,
            required=False,
        )
        .parser
    )
    _args_mapping = [
        (AwardNomination.award_id, None, None),
        (AwardNomination.nominee_cms_id, None, None),
        (AwardNomination.nomination_projects, None, None),
    ]

    Nominee = aliased(Person, name="nominee")
    NomineeStatus: PersonStatus = aliased(PersonStatus, name="nominee_status")
    NomineeCountry = aliased(Country, name="nominee_country")
    _cols_out = [
        json_agg(AwardNomination).label("nominations"),
        AwardNomination.nominee_cms_id,
        Nominee.first_name.label("nominee_first_name"),
        Nominee.last_name.label("nominee_last_name"),
        NomineeStatus.status.label("nominee_cms_status"),
        AwardNomination.nominee_activity,
        AwardNomination.nomination_projects,
        AwardNomination.nominee_institute,
        AwardNomination.nominee_phd_date,
    ]
    _cols_in = None

    @staticmethod
    def get_generic_permissions() -> list:
        if current_user.is_award_admin:
            return ["view-all-nominations"]
        return ["view-own-nominations"]

    @classmethod
    def _base_get_query(cls):
        return (
            AwardNomination.session()
            .query(*cls._cols_out)
            .join(Award, Award.id == AwardNomination.award_id)
            .join(AwardType, Award.award_type_id == AwardType.id)
            .join(cls.Nominee, cls.Nominee.cms_id == AwardNomination.nominee_cms_id)
            .join(
                cls.NomineeStatus,
                and_(
                    cls.NomineeStatus.cms_id == AwardNomination.nominee_cms_id,
                    cls.NomineeStatus.start_date <= date.today(),
                    or_(
                        cls.NomineeStatus.end_date.is_(None),
                        cls.NomineeStatus.end_date >= date.today(),
                    ),
                ),
            )
            .join(Institute, Institute.code == AwardNomination.nominee_institute)
        )

    @classmethod
    def list_nominees(cls, award_id=None):
        args = cls.parse_args()
        q = cls._base_get_query().group_by(
            AwardNomination.nominee_cms_id,
            cls.Nominee.first_name,
            cls.Nominee.last_name,
            cls.NomineeStatus.status,
            AwardNomination.nominee_activity,
            AwardNomination.nomination_projects,
            AwardNomination.nominee_institute,
            AwardNomination.nominee_phd_date,
        )
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        if award_id:
            q = cls.attach_query_filters(q, {"award_id": award_id}, cls._args_mapping)
        if "view-all-nominations" not in cls.get_generic_permissions():
            q = q.filter(AwardNomination.nominator_cms_id == current_user.cms_id)
        if args.get("include_deleted") is not True:
            q = q.filter(AwardNomination.is_active.is_(True))
        res = q.all()
        item_list = cls.rows_to_list_of_dicts(res, cls._cols_out)
        return item_list
