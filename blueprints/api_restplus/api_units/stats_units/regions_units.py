from blueprints.api_restplus.api_units.basics import BaseApiUnit
from util.regions import regions
from icms_orm.common import Region

class RegionsApiCall(BaseApiUnit):

    @classmethod
    def get(cls):
        ssn = Region.session()
        q_cols = [Region.code, Region.name]
        q = ssn.query(*q_cols)
        results = q.all()
        results = [{k.key: v for (k, v) in zip(q_cols, row)} for row in results]
        return results