from datetime import date
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ParserBuilder
from icms_orm.common import Institute, InstituteStatus, Country, Person, PersonStatus
from sqlalchemy import func, extract, or_, and_, literal_column


class MemberOverviewApiCall(BaseApiUnit):
    _args = (
        ParserBuilder()
        .add_argument(name="year", type=ParserBuilder.INTEGER, required=False)
        .parser
    )

    @classmethod
    def get_people_stats(cls, year: int):
        session = Person.session()
        count = func.count(Person.cms_id)
        q = (
            session.query(PersonStatus.activity, Person.gender, count)
            .join(Person, Person.cms_id == PersonStatus.cms_id, isouter=True)
            .filter(PersonStatus.status.in_( ['CMS','CMSEXTENDED', 'CMSEMERITUS','CMSAFFILIATE'] ) )
            .filter( PersonStatus.start_date <= date(year,12,31) , 
                     or_(
                        date(year,12,31) <= PersonStatus.end_date,
                        PersonStatus.end_date.is_(None)
                     )
            )
            .group_by(PersonStatus.activity, Person.gender)
            .order_by(PersonStatus.activity, Person.gender)
        )
        return cls.rows_to_list_of_dicts(
            q.all(),
            cols_list=[PersonStatus.activity, Person.gender, literal_column("count")],
        )

    @classmethod
    def get_country_stats(cls, year: int):
        session = Country.session()
        count = func.count(Institute.code.distinct())
        q = (
            session.query(Country.name, Institute.country_code, count)
            .join(Country, Country.code == Institute.country_code)
            .join( InstituteStatus, InstituteStatus.code == Institute.code )
            .filter( InstituteStatus.start_date <= date(year,12,31) )
            .filter(
                     or_ (
                        date(year,12,31) <= InstituteStatus.end_date,
                        InstituteStatus.end_date.is_(None)
                    )
            )
            .filter( InstituteStatus.status.in_(["Yes", "Associated", "Cooperating"]) )
            .group_by(Institute.country_code, Country.name)
        )
        return cls.rows_to_list_of_dicts(
            q.all(),
            cols_list=[
                literal_column("country"),
                Country.code,
                literal_column("count"),
            ],
        )

    @classmethod
    def get_institute_stats(cls, year: int):
        session = InstituteStatus.session()
        count = func.count(InstituteStatus.code)
        q = (
            session.query(count)
            .filter( InstituteStatus.start_date <= date(year,12,31),
                     or_(
                        date(year,12,31) <= InstituteStatus.end_date,
                        InstituteStatus.end_date.is_(None)
                    )
            )
            .filter(InstituteStatus.status.in_(["Yes", "Associated", "Cooperating"]))
        )
        return cls.rows_to_list_of_dicts(q.all(), cols_list=[literal_column("count")])

    @classmethod
    def get_stats(cls):
        args = cls.parse_args()
        year = args.get("year") or date.today().year
        return {
            "instCountryStats": cls.get_country_stats(year),
            "pplStats": cls.get_people_stats(year),
            "nInst": cls.get_institute_stats(year),
            "year": year,
        }
