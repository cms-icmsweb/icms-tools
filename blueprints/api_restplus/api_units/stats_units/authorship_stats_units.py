from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from icms_orm.epr import TimeLineUser as TLU
import sqlalchemy as sa


class AuthorshipFractionsCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_argument('year', ParserBuilder.INTEGER, False).\
        add_argument('cms_id', ParserBuilder.INTEGER, False).\
        parser

    _model = ModelFactory.make_model('Authorship Fractions', {
        'year': ModelFactory.Integer,
        'cms_id': ModelFactory.Integer,
        'fraction': ModelFactory.Float
    })

    @classmethod
    def get(cls):
        args = cls.parse_args()
        _cols = [
            TLU.year,
            TLU.cmsId.label('cms_id'),
            sa.func.sum(TLU.yearFraction * sa.case([(TLU.isAuthor==True, 1)], else_=0)).label('fraction')
        ]
        q = TLU.session().query(*_cols)
        q = cls.attach_query_filters(q, args, rules=[(TLU.year, None, None), (TLU.cmsId, 'cms_id', None)])
        q = q.group_by(TLU.year, TLU.cmsId)
        return cls.rows_to_list_of_dicts(rows=q.all(), cols_list=_cols)
