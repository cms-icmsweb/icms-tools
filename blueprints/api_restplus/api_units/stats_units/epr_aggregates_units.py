from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from icms_orm.common import EprAggregate as Agg, EprAggregateUnitValues as AggUnit
import sqlalchemy as sa
import logging


class EprAggregatesCall(BaseApiUnit):

    _grouping_columns = [Agg.year, Agg.cms_id, Agg.inst_code, Agg.project_code]

    _args = ParserBuilder().\
        add_argument(Agg.year.key, ParserBuilder.INTEGER, False).\
        add_argument(Agg.cms_id.key, ParserBuilder.INTEGER, False).\
        add_argument(Agg.inst_code.key, ParserBuilder.STRING, False).\
        add_argument(Agg.project_code.key, ParserBuilder.STRING, False). \
        add_appendable_argument(Agg.aggregate_unit.key, ParserBuilder.STRING, required=False,
                                choices=list(map(str.lower, AggUnit.values())),
                                default=list(map(str.lower,
                                                 [AggUnit.PLEDGES_DONE, AggUnit.SHIFTS_DONE, AggUnit.INST_PLEDGES_DONE])),
                                help='Allowed values are: {0}. Check the docs for defaults.'.format(
                                    ', '.join(map(str.lower, AggUnit.values())))). \
        add_appendable_argument('grouping_columns', ParserBuilder.STRING, required=False,
                                choices=[_c.key for _c in _grouping_columns],
                                default=[_c.key for _c in _grouping_columns],
                                help='Allowed values are: {0}. All are on by default.'.format(
                                    ', '.join([_c.key for _c in _grouping_columns]))). \
        parser

    _model = ModelFactory.make_model('EPR Aggregate', {
        Agg.year.key: ModelFactory.Integer,
        Agg.cms_id.key: ModelFactory.Integer,
        Agg.inst_code.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.DESCRIPTION: 'Institute code that the work was delivered for.'
        }),
        Agg.project_code.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.DESCRIPTION: 'Project that the work was delivered for.'
        }),
        Agg.aggregate_value.key: ModelFactory.field_proxy(ModelFactory.Float, {})
    })

    @classmethod
    def get(cls):
        args = cls.parse_args()
        # picking only those grouping columns that have been asked for
        _grouping_cols = [_c for _c in cls._grouping_columns if _c.key in args['grouping_columns']]
        logging.debug('Grouping columns will be: {0}'.format(', '.join([_c.key for _c in _grouping_cols])))
        _cols = _grouping_cols + [sa.func.sum(Agg.aggregate_value).label(Agg.aggregate_value.key)]
        q = Agg.session().query(*_cols)
        # the list of aggregate units seems to be correctly translated into an "IN" clause
        q = cls.attach_query_filters(q, args, rules=[Agg.year, Agg.inst_code, Agg.cms_id, (Agg.aggregate_unit, None, str.upper), Agg.project_code])
        q = q.group_by(*_grouping_cols)
        return cls.rows_to_list_of_dicts(rows=q.all(), cols_list=_cols)
