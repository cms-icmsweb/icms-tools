import sqlalchemy as sa
from icms_orm.toolkit import Voting, VotingMerger, VotingMergerMember
from datetime import datetime as dt
from icms_orm.cmspeople import Institute
from icmsutils.exceptions import IcmsException


def get_merger(ssn, id):
    # VotingMerger has a magic "members" property
    return ssn.query(VotingMerger).filter(VotingMerger.id == id).one()


def get_mergers(ssn, active_as_of_date, page, per_page, desc):
    q = ssn.query(VotingMerger)
    if active_as_of_date:
        q = q.filter(sa.or_(VotingMerger.valid_from == None, VotingMerger.valid_from <= active_as_of_date)).filter(
            sa.or_(VotingMerger.valid_till == None, VotingMerger.valid_till >= active_as_of_date))
    q = q.order_by((desc and sa.desc or sa.asc)(VotingMerger.id))
    return q.paginate(page, per_page, error_out=False).items


def create_voting_merger(ssn, payload):
    kwargs = {}
    for ia in [VotingMerger.valid_till, VotingMerger.valid_from, VotingMerger.representative_cms_id]:
        if ia.key in payload and ia.key != 'members':
            kwargs[ia.key] = payload[ia.key]
    new_merger = VotingMerger(**kwargs)

    countries = set()
    if VotingMerger.country.key in payload:
        countries.add(payload[VotingMerger.country.key])

    inst_codes = {x.get(VotingMergerMember.inst_code.key) for x in payload.get('members', [])}

    insts = ssn.query(Institute.code, Institute.country).filter(Institute.code.in_(inst_codes)).all()

    if len(inst_codes) != len(insts):
        raise IcmsException('Some of the specified institutes don\'t exist.')

    for code, country in insts:
        countries.add(country)
        new_merger.members.append(VotingMergerMember(code))

    if not new_merger.valid_from:
        new_merger.valid_from = dt.today()

    new_merger.country = '/'.join(countries)
    ssn.add(new_merger)
    ssn.commit()
    return new_merger


def remove_merger(ssn, merger_id):
    merger = ssn.query(VotingMerger).filter(VotingMerger.id == merger_id).one();
    for member in merger.members:
        ssn.delete(member)
        ssn.delete(merger)
    ssn.commit()
