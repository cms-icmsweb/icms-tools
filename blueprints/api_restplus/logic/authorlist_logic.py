import sqlalchemy as sa
from icms_orm.cmsanalysis import CDS
from icms_orm.cmsanalysis import CadiAnalysis as A
from icms_orm.cmsanalysis import CDSStatusValues, CDSStepValues
from icms_orm.cmsanalysis import PaperAuthor as PA
from icms_orm.cmsanalysis import PaperAuthorHistory as PAH
from icms_orm.cmsanalysis import PaperAuthorHistoryActions as PAHAction
from icms_orm.cmsanalysis import PaperAuthorStatusValues as AuthorStatus

from .api_logic_utils import QueryTools


class Arg(object):
    cms_id = "cms_id"
    code = "code"
    al_status = "al_status"


def get_al_info(*_, **kwargs):
    """
    AL code, status (open/closed/None), date opened and corresponding analysis status
    """
    code_filter = kwargs.get(Arg.code)
    open_close_entries_query = (
        PAH.session()
        .query(PAH.code, PAH.action, PAH.date)
        .filter(PAH.action.in_((PAHAction.LIST_CLOSE, PAHAction.LIST_GEN)))
        .order_by(PAH.historyID)
    )
    if code_filter:
        open_close_entries_query = open_close_entries_query.filter(
            PAH.code.like(f"%{code_filter}%")
        )
    open_close_entries = open_close_entries_query.all()
    al_latest_action = {}
    al_first_date = {}
    for code, action, date_ in open_close_entries:
        code = code.strip()
        al_latest_action[code] = action
        if code not in al_first_date:
            al_first_date[code] = date_
    status_by_latest_action = {
        PAHAction.LIST_GEN: "open",
        PAHAction.LIST_CLOSE: "close",
        None: None,
    }
    al_statuses = {
        code: status_by_latest_action[latest_action]
        for code, latest_action in al_latest_action.items()
    }
    status_filter = kwargs.get(Arg.al_status)
    filtered_al_map = {
        code: (status, al_first_date[code])
        for code, status in al_statuses.items()
        if status_filter is None or status == status_filter
    }
    cadi_status_query = A.session().query(A.code, A.status)
    if code_filter:
        cadi_status_query = cadi_status_query.filter(A.code.like(f"%{code_filter}%"))
    cadi_status_results = cadi_status_query.all()
    cadi_statuses = {
        code.strip().lstrip("d"): status for code, status in cadi_status_results
    }
    al_info = []
    for code in sorted(filtered_al_map.keys()):
        al_status, first_date = filtered_al_map[code]
        cadi_status = cadi_statuses.get(code)
        al_info.append(
            {
                "code": code,
                "al_status": al_status,
                "opened_on": first_date,
                "status": cadi_status,
            }
        )
    return al_info


def get_al_mentions(*args, **kwargs):
    q_cols = [
        sa.distinct(
            sa.case(
                [(A.code.like("d%"), sa.func.substring(A.code, 2, 10))], else_=A.code
            )
        ).label("code"),
        A.status,
        CDS.title,
        A.publi,
        sa.func.str_to_date(CDS.stepDate, "%d/%m/%Y").label("cds_upload_date"),
    ]
    q = (
        PA.session()
        .query(*q_cols)
        .join(
            PA,
            sa.case(
                [(A.code.like("d%"), sa.func.substring(A.code, 2, 10))], else_=A.code
            )
            == PA.code,
        )
        .join(CDS, CDS.code == PA.code)
    )
    q = q.filter(
        sa.and_(CDS.step == CDSStepValues.SUB, CDS.status == CDSStatusValues.DONE)
    )
    q = q.filter(PA.status.in_({AuthorStatus.IN, AuthorStatus.IN_NEW}))
    q = QueryTools.apply_filter_funcs(
        q,
        {Arg.cms_id: lambda _q: _q.filter(PA.cmsid == kwargs.get(Arg.cms_id))},
        kwargs,
    )
    return QueryTools.result_as_list_of_maps(q, q_cols)
