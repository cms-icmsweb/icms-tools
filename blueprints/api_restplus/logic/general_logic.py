import json
from icms_orm.cmspeople import Person
from icms_orm.common import ApplicationAsset

import sqlalchemy as sa, re, logging, os
from util.trivial import current_db_ssn
import app_profiles as config
from subprocess import Popen, PIPE
from flask import send_file
import tempfile


class Attr(object):
    name = 'name'
    application = 'application'
    data = 'data'


def get_app_asset_info(*args, **kwargs):

    name = kwargs.get(Attr.name)
    ssn = current_db_ssn()
    appAsset = ssn.query(ApplicationAsset).filter(ApplicationAsset.name==name).one_or_none()

    return { 'name': appAsset.name, 'application': appAsset.application, 'data': appAsset.data }


def post_app_asset_info(*args, **kwargs):
    name = kwargs.get(Attr.name)
    app = kwargs.get(Attr.application)
    data = kwargs.get(Attr.data)

    logging.debug( '==> got: ', name, app, data )

    ApplicationAsset.store( name=name, application=app, value=data )

    return { 'OK' }
