from icms_orm.cmspeople import PersonHistory as History
from icmsutils.prehistory import PrehistoryParser


def update_person_history(cms_id, payload):
    hist = History.query.filter(History.cmsId == cms_id).one()
    hist.set(History.history, PrehistoryParser.merge_lines_into_raw_history(payload[History.history.key]))
    History.session().add(hist)
    History.session().commit()
    return hist
