from icms_orm.common import ApplicationAsset


def get_application_assets_by_name(name):
    return ApplicationAsset.session().query(ApplicationAsset).filter(
        ApplicationAsset.name.like('%{name}%'.format(name=name))).all()
