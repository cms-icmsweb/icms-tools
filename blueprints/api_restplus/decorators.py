from flask_login import current_user
from flask_login import login_required
from flask import current_app
from functools import wraps
from blueprints.api_restplus.param_stores import ParamStoreProvider

from icmsutils.permissions import AuthorizationContext
from blueprints.api_restplus.exceptions import AccessViolationException
from webapp.permissions import PermissionsManager
from icms_orm.toolkit import RestrictedActionTypeValues as ActionType
import logging
import flask


class AuthorizationContextFactory():
    @classmethod
    def create_context(cls):
        context = AuthorizationContext()
        param_store_getter: ParamStoreProvider
        for param_store_getter in ParamStoreProvider.get_all():
            context.register_parameter_store(
                param_store_getter.prefix, param_store_getter.getter())
        return context


def pre_authenticated_only(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if current_user.is_authenticated:
            return func(*args, **kwargs)
        else:
            raise AccessViolationException('Authenticated users only!')
    return decorated_function


def check_permissions(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated:
            return current_app.login_manager.unauthorized()

        if getattr(func.view_class, 'skip_auto_permission_check', False):
            logging.debug(
                'Automatic permission checks skipped for: %s',
                func.view_class.__name__
            )
            return func(*args, **kwargs)

        context = AuthorizationContextFactory.create_context()
        action = ActionType.get_for_http_method_name(
            flask.request.method.lower())
        PermissionsManager.set_caches_ttl(
            current_app.config['AUTHORIZATIONS_CACHE_TTL'])
        if PermissionsManager.can_perform(action=action, resource_key=flask.request.url_rule.rule, context=context):
            return func(*args, **kwargs)
        else:
            message: str = f'Permissions check failed for user {current_user.username}, method {flask.request.method} and endpoint {flask.request.url_rule}'
            logging.warning(message)
            raise AccessViolationException(message)
    return decorated_function


def admin_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if current_user.is_authenticated and current_user.is_admin():
            return func(*args, **kwargs)
        else:
            raise AccessViolationException('Admins only!')
    return decorated_function


def engagement_office_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if current_user.is_authenticated and (current_user.is_admin() or current_user.is_engagement_office_member()):
            return func(*args, **kwargs)
        else:
            raise AccessViolationException(
                'Only Engagement Office members (and admins) can access this resource!')

    return decorated_function


def login_usually_required(func):
    """
    That is NOT how one should code but how to override API-wide decorators for a specific endpoint only?
    """
    if func.__module__  == 'blueprints.api_restplus.endpoints.relay_endpoints':
        return func
    return login_required(func)
