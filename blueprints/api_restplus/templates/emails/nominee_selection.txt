Dear {{ nominee_name }},

Congratulations.

You have been selected for the position with title: {{ job_title }}. 

Job Unit: {{ unit }}.

Contact cms-cbchairteam@cern.ch by replying to this email for next steps.

Best regards,
iCMS mailing system.