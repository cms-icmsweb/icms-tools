Dear {{ nominee_name }},

You have just rejected a nomination for the position with title: {{ job_title }}.
Job Unit: {{ unit }}.
Remarks: {{ remarks }}

We kindly invite you to voluntarily complete a brief survey to help us improve the nomination and selection procedures. Your feedback is greatly appreciated.
Link to survey: https://forms.gle/96NtfvHAPJtD9KjZ7

Best regards,
iCMS mailing system.