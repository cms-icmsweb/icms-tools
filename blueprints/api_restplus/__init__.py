from flask_restx.namespace import Namespace
from blueprints.api_restplus.api_factory import SingletonApiFactory
from blueprints.api_restplus.blueprint import restplus_blueprint, api
from blueprints.api_restplus.endpoints.admin_endpoints import ns as admin_ns
from blueprints.api_restplus.endpoints.permissions_endpoints import (
    ns as admin_permissions_ns,
)
from blueprints.api_restplus.endpoints.authorlist_endpoints import ns as author_list_ns
from blueprints.api_restplus.endpoints.authorship_endpoints import authorship_ns
from blueprints.api_restplus.endpoints.booking_endpoints import booking_ns
from blueprints.api_restplus.endpoints.cadi_endpoints import cadi_ns
from blueprints.api_restplus.endpoints.diagnostic_endpoints import (
    diagnostics_ns as diagnostic,
)
from blueprints.api_restplus.endpoints.servicework_endpoints import epr_ns
from blueprints.api_restplus.endpoints.general_endpoints import ns as general_ns
from blueprints.api_restplus.endpoints.icms_public_endpoints import ns as icms_public_ns
from blueprints.api_restplus.endpoints.institute_endpoints import ns as institute_ns
from blueprints.api_restplus.endpoints.janitorial_endpoints import ns as janitorial
from blueprints.api_restplus.endpoints.members_endpoints import (
    ns as institutes_namespace,
)
from blueprints.api_restplus.endpoints.mo_endpoints import ns as mo_ns
from blueprints.api_restplus.endpoints.old_notes_endpoints import ns as old_notes_ns
from blueprints.api_restplus.endpoints.tenures_endpoints import ns as tenures_ns
from blueprints.api_restplus.endpoints.requests_endpoints import ns as requests_ns
from blueprints.api_restplus.endpoints.portal_news_endpoints import ns as portal_news_ns
from blueprints.api_restplus.endpoints.relay_endpoints import ns as relay_ns
from blueprints.api_restplus.endpoints.statistics_endpoints import stats_ns as stats_ns
from blueprints.api_restplus.endpoints.statistics_endpoints import (
    stats_epr_ns as stats_epr_ns,
)
from blueprints.api_restplus.endpoints.voting_endpoints import ns as voting_ns
from blueprints.api_restplus.endpoints.awards_endpoints import ns as award_ns
from blueprints.api_restplus.endpoints.egroup_endpoints import ns as egroups_ns
from blueprints.api_restplus.endpoints.cadi_egroup_endpoints import (
    ns as cadi_egroups_ns,
)
from blueprints.api_restplus.endpoints.weekly_meetings_endpoints import (
    ns as weekly_meetings_ns,
)
from blueprints.api_restplus.endpoints.phd_info_endpoints import ns as phd_info_ns


namespace: Namespace
namespaces = [
    icms_public_ns,
    admin_permissions_ns,
    institutes_namespace,
    cadi_ns,
    epr_ns,
    authorship_ns,
    voting_ns,
    mo_ns,
    stats_ns,
    stats_epr_ns,
    author_list_ns,
    diagnostic,
    janitorial,
    old_notes_ns,
    general_ns,
    tenures_ns,
    portal_news_ns,
    relay_ns,
    admin_ns,
    institute_ns,
    booking_ns,
    requests_ns,
    award_ns,
    egroups_ns,
    weekly_meetings_ns,
    cadi_egroups_ns,
    phd_info_ns,
]

for namespace in sorted(namespaces, key=lambda n: n.name):
    SingletonApiFactory.get_api().add_namespace(namespace)
