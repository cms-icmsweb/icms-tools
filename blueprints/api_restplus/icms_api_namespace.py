import logging
from typing import Any, Dict, Optional, Type

from flask_restx import Api
from flask_restx.namespace import Namespace
from flask_restx.resource import Resource

from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics.builders import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics.crud import (
    Deleteless,
    Getless,
    Listless,
    Postless,
    Putless,
)
from blueprints.api_restplus.api_units.basics.model_field_types import Integer
from blueprints.api_restplus.api_units.basics.wrappers import HyperlinkWrapper

log: logging.Logger = logging.getLogger(__name__)


class IcmsApiNamespace(Namespace):
    """
    Initial take on centralizing the boilerplate and passing the resource path into handler object
    Work needed:
    - customise the /<int:id> generation (infer from handler.make_filter()?)

    There might be a problem ahread in case of composite IDs (like /X/Y/Z) as the order they are passed in
    as *args and **kwargs might not be the one intended.
    """

    def register_crud_handler_class(
        self, api: Api, handler_class: Type[AbstractCRUDUnit], path: str
    ):
        handler = handler_class
        collection_class = None
        item_class = None

        collection_superclasses = []
        if not issubclass(handler_class, Postless):
            collection_superclasses.append(
                self._create_resource_post_superclass(api, handler, path)
            )
        if not issubclass(handler_class, Listless):
            collection_superclasses.append(
                self._create_resource_list_superclass(api, handler, path)
            )

        item_superclasses = []
        if not issubclass(handler_class, Getless):
            item_superclasses.append(
                self._create_resource_get_superclass(api, handler, path)
            )
        if not issubclass(handler_class, Putless):
            item_superclasses.append(
                self._create_resurce_put_superclass(api, handler, path)
            )
        if not issubclass(handler_class, Deleteless):
            item_superclasses.append(
                self._create_resource_delete_superclass(api, handler, path)
            )

        if collection_superclasses:
            collection_class = type(
                "CollectionResource", tuple(collection_superclasses), {}
            )
            self.add_resource(collection_class, path)
        if item_superclasses:
            item_class = type("ItemResource", tuple(item_superclasses), {})
            if not issubclass(handler_class, Putless):
                handler_class.register_hyperlink_wrapper(
                    HyperlinkWrapper.get_for_resource(
                        "update", item_class, method="PUT"
                    )
                )
            if not issubclass(handler_class, Deleteless):
                handler_class.register_hyperlink_wrapper(
                    HyperlinkWrapper.get_for_resource(
                        "delete", item_class, method="DELETE"
                    )
                )
            self.add_resource(
                item_class, f"{path}{self._generate_id_path(handler_class)}"
            )

    def _generate_id_path(self, handler_class: Type[AbstractCRUDUnit]):
        """
        Generates the part of the URL that "points" to a specific insance of the resource, usually that would be
        "/<int:id>" but might as well be "/<string:code>", "/<int:cms_id>" or something else that uniquely identifies
        a resource's instance.
        """
        id_fields = handler_class.get_id_fields()
        type_str = "int" if id_fields[0].type == Integer else "string"
        return f"/<{type_str}:{id_fields[0].name}>"

    def _create_resource_list_superclass(
        self, api: Api, handler: Type[AbstractCRUDUnit], path: str
    ):
        class CrudResource(Resource):
            @api.marshal_list_with(handler.get_model())
            @api.expect(handler.get_args(), validate=True)
            @api.doc(id=f"list_{handler.__name__}")
            def get(self):
                mdf: ModelFieldDefinition
                fields_by_name: Dict[str, ModelFieldDefinition] = {
                    mdf.name: mdf for mdf in handler.get_model_fields_list()
                }
                args: Dict[str, Any] = handler.get_args().parse_args()
                current_filter: Optional[ResourceFilter] = None
                for key, value in args.items():
                    if value is None:
                        continue
                    if key not in fields_by_name.keys():
                        log.warning(
                            f"CRUD handler class {handler.__class__} cannot handle query string parameter {key}"
                        )
                        continue
                    model_field: ModelFieldDefinition = fields_by_name[key]
                    current_filter = ResourceFilter(
                        model_field.column,
                        value,
                        current_filter,
                        fallback_key=key if model_field.column is None else None,
                    )
                return handler.find(current_filter)

        CrudResource.get.__doc__ = handler.find.__doc__
        return CrudResource

    def _create_resource_get_superclass(
        self, api: Api, handler: Type[AbstractCRUDUnit], path: str
    ):
        class CrudResource(Resource):
            @api.marshal_with(handler.get_model())
            @api.doc(id=f"get_single_{handler.__name__}")
            def get(self, *identity_args, **kwargs):
                return handler.get_by_identity(
                    *(list(identity_args) + list(kwargs.values()))
                )

        CrudResource.get.__doc__ = handler.get_by_identity.__doc__
        return CrudResource

    def _create_resource_post_superclass(
        self, api: Api, handler: Type[AbstractCRUDUnit], path: str
    ):
        class CrudResource(Resource):
            @api.marshal_with(handler.get_model())
            @api.expect(handler.get_model(), validate=True)
            @api.doc(id=f"post_{handler.__name__}")
            def post(self):
                return handler.post()

        CrudResource.post.__doc__ = handler.post.__doc__
        return CrudResource

    def _create_resurce_put_superclass(
        self, api: Api, handler: Type[AbstractCRUDUnit], path: str
    ):
        class CrudResource(Resource):
            @api.marshal_with(handler.get_model())
            @api.expect(handler.get_model(), validate=True)
            @api.doc(id=f"put_{handler.__name__}")
            def put(self, *args, **kwargs):
                return handler.put_by_identity(*(list(args) + list(kwargs.values())))

        CrudResource.put.__doc__ = handler.put_by_identity.__doc__
        return CrudResource

    def _create_resource_delete_superclass(
        self, api: Api, handler: Type[AbstractCRUDUnit], path: str
    ):
        class CrudResource(Resource):
            @api.doc(id=f"delete_{handler.__name__}")
            def delete(self, *args, **kwargs):
                return handler.delete_by_identity(*(list(args) + list(kwargs.values())))

        CrudResource.delete.__doc__ = handler.delete_by_identity.__doc__
        return CrudResource
