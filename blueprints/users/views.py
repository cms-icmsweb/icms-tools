import datetime
import logging
import re
import time
import sys
import traceback
from typing import Optional
import flask
from flask import flash, current_app
from flask import redirect, url_for, render_template
from flask_login import current_user, login_user, login_required, logout_user
from .forms import LoginPasswordForm, LoginIgnorePasswordForm
from models.icms_people import Person, PersonHistory
from util import constants as const
from blueprints.users.blueprint import users_blueprint, User, Credentials
from blueprints.users.blueprint import is_bypass_enabled, obtain_credentials, get_user_for_credentials
from util import glossary, decorators
from util.EgroupHandler import EgroupHandler
from util.structs import PreformatedText
from icmsutils.ldaputils import LdapProxy
from icms_orm.common import EmailMessage, EmailLog
from util.accountCheck import AccountChecker, analyse, helpInfo
from webapp import Webapp


@users_blueprint.route('/signout', methods=['GET', 'POST'])
def route_sign_out():
    if not current_user.is_anonymous:
        logging.debug('Logging out %s' % current_user.person.niceLogin)
        # checking if impersonation mode is active
        real_cms_id = flask.session.get(const.SESSION_KEY_REAL_CMS_ID, None)
        logout_user()
        flash(glossary.MSG_LOGOUT_SUCCESSFUL, const.FLASK_FLASH_INFO)

        if real_cms_id:
            you = Webapp.db_session().query(
                Person).filter(Person.cmsId == real_cms_id).one()
            user = User(person=you)
            login_user(user)
            flash('You were logged back in as yourself, an admin!',
                  const.FLASK_FLASH_INFO)
            del flask.session[const.SESSION_KEY_REAL_CMS_ID]
    return flask.redirect(flask.url_for('route_vue'))


def _do_login():
    form = LoginIgnorePasswordForm() if is_bypass_enabled() else LoginPasswordForm()
    credentials: Optional[Credentials] = obtain_credentials(
        flask.request.headers, form)
    if credentials is not None:
        try:
            user = get_user_for_credentials(credentials=credentials)
            login_user(user)
            return redirect(flask.request.args.get('next') or '/')
        except Exception as e:
            flask.flash('Authentication error occurred',
                        const.FLASK_FLASH_WARNING)
            traceback.print_exc()
    return render_template('signin.html', form=form), 403


@users_blueprint.route('/signin', methods=['GET', 'POST'])
def route_sign_in():
    attempts = 0
    attempts_limit = 2
    while attempts < attempts_limit:
        attempts += 1
        try:
            return _do_login()
        except Exception as e:
            logging.exception('Caught exception when trying to login. Have %d attemps remaining...' % (
                attempts_limit - attempts))
            time.sleep(5)
    sys.exit(1)


@users_blueprint.route('/modal_history/<int:cms_id>')
@login_required
def route_modal_history(cms_id):
    last_name, first_name, history = Person.session.query(Person.lastName, Person.firstName, PersonHistory.history)\
        .join(PersonHistory, Person.cmsId == PersonHistory.cmsId).filter(PersonHistory.cmsId == cms_id).one_or_none()
    details = PreformatedText((history or '').strip())
    return flask.render_template('dialog_details.html', details=details, title='History of %s, %s' % (last_name, first_name))


@users_blueprint.route('/check_new_users')
@login_required
@decorators.roles_required(const.ROLE_ADMIN)
def route_check_new_users():

    start_date = datetime.datetime.utcnow() - datetime.timedelta(days=2)
    current_app.logger.debug(
        "checking accounts registered since: %s " % start_date)

    cms_people = Person.query.filter(Person.hrId > 0) \
        .filter(Person.dateCreation > start_date) \
        .order_by(Person.dateCreation.desc()).all()

    logging.info('found %s people ' % len(cms_people))

    ldap = LdapProxy.get_instance()

    new_users = []
    for p in cms_people:

        full_name = p.lastName + ', ' + p.firstName

        ldap_user = ldap.get_person_by_hrid(p.hrId)
        login = ldap_user.login.strip()

        if login == '':
            logging.info(
                f'no account set yet for cmsId: {p.cmsId} user \'{full_name}\' -- skipping check ')
            continue

        ac = AccountChecker(infoOK=True, verbose=False)
        ac.check(p.hrId)

        check_result = analyse(ac.issues)

        add_to_zh_admin, add_to_zh_user, disabled_button, mail_reg_btn, remove_buttons, help_info = extractBtnAndHelpInfo(
            ac, ldap_user)

        if disabled_button:
            current_app.logger.debug('+++> ', disabled_button)

        err_info = {"addToZHAdmin": add_to_zh_admin,
                    "addToZHUser": add_to_zh_user,
                    "disabledBtn": disabled_button,
                    "mailRegBtn": mail_reg_btn,
                    "removeButtons": remove_buttons,
                    "helpInfo": help_info,
                    }

        new_users.append({'person': p,
                          'result': check_result,
                          'issues': ac.issues,
                          'account': login,
                          'errInfo': err_info,
                          })

        # if checkResult :
        #     # logging.info( " " )
        #     logging.info( "checking cmsId: %s user '%s' account '%s' -- OK" % (p.cmsId, fullName, login) )
        #     # pass
        # else :
        #     # logging.info( " " )
        #     logging.info( "checking cmsId: %s user '%s' account '%s' (%s) -- ISSUES found:" % (
        #     p.cmsId, fullName, login, p.dateRegistration) )
        #     if ac.issues[ 'error' ] :
        #         for item in ac.issues[ 'error' ] :
        #             if 'account disabled' in item[ 'check' ] :
        #                 print ' ' * 25, " --- account '%s' is disabled " % login
        #                 # print "found account disabled, mail is:\n", accountCheck.msgInButDisabled % (fullName, login)
        #             elif 'primary account in zh' in item[ 'check' ] :
        #                 print ' ' * 25, " --- primary account %s not in ZH " % login
        #             elif 'secondary account in another experiment' in item[ 'check' ] :
        #                 print ' ' * 25, " --- %s " % item[ 'msg' ]
        #             else :
        #                 print item

        # sendMailInButDisabled(account=person.login,
        #                       email=person.mail,
        #                       userFirstName=person.fullName)
        # print "Mail sent to %s ... " % person.mail

    return flask.render_template('check_new_users.html', newUsers=new_users, startDate=str(start_date))


@users_blueprint.route('/account_status/<int:hr_id>')
@login_required
@decorators.roles_required(const.ROLE_ADMIN)  # , const.ROLE_SECRETARIAT)
def route_account_status(hr_id):

    ldap_person = LdapProxy.get_instance().get_person_by_hrid(hr_id)

    if not ldap_person:
        msg = "No person found in LDAP for hrID: %s " % hr_id
        flash(msg, const.FLASK_FLASH_DANGER)
        logging.error(msg)
        # print "Referrer: ", flask.request.referrer
        return redirect(flask.request.referrer)

    ac = AccountChecker(verbose=False)
    ac.check(hr_id)

    logging.info("------- analyzing result: %s" % analyse(ac.issues))

# # -- fake a few errors for testing
#     for grp,ex in [ ('zp', 'Atlas'), ('z5', 'LHCb') ]:
#         msg = 'primary account "%s" for hrId %s is also in group %s (%s)' % (ldap_person.login, hr_id, grp, ex)
#         ac.issues['error'].append( { 'check' : 'primary account in other expt',
#                                  'status':'error',
#                                  'msg' : msg } )
#
#     msg = 'primary account "%s" for hrId %s NOT in zh' % (ldap_person.login, hr_id)
#     ac.issues['error'].append( { 'check' : 'primary account in zh',
#                                  'status':'error',
#                                  'msg' : msg } )
#
#     msg = 'account %s for hrId %s exists, but is presently disabled' % (ldap_person.login, hr_id)
#     logging.warning( msg )
#     ac.issues[ 'error' ].append( { 'check'  : 'account disabled',
#                                    'status' : 'error',
#                                    'msg'    : msg } )
#
#     # msg = 'user with hrID "%s" not found in CMS DB' % hr_id
#     # logging.debug( msg )
#     # ac.issues[ 'error' ].append( { 'check'  : 'user in CMS DB',
#     #                                  'status' : 'error',
#     #                                  'msg'    : msg } )
#
# # -- end fake errors for testing

    addToZHAdmin, addToZHUser, disabledButton, mailRegBtn, removeButtons, helpInfo = extractBtnAndHelpInfo(
        ac, ldap_person)

    logging.info('found removeButtons: %s' % str(removeButtons))
    logging.info('found disabledButton: %s' % str(disabledButton))
    logging.info('found mailRegBtn: %s' % str(mailRegBtn))
    logging.info('found addToZH: User: %s, Admin: %s' %
                 (str(addToZHUser), str(addToZHAdmin)))

    return render_template('account_status.html',
                           person=ldap_person,
                           issues=ac.issues,
                           addToZHUser=addToZHUser,
                           addToZHAdmin=addToZHAdmin,
                           removeButtons=removeButtons,
                           disabledBtn=disabledButton,
                           mailRegBtn=mailRegBtn,
                           helpInfo=helpInfo)


def extractBtnAndHelpInfo(ac, ldap_person):

    addToZHUser = (len(ac.issues['error']) == 1) and (
        ac.issues['error'][0]['check'].strip() == 'primary account in zh/zj')

    # in case there are more issues (e.g. membership also in another experiment), only allow admins to add to 'zh'
    addToZHOther = False
    for item in ac.issues['error']:
        if item[ 'check' ].strip() == 'primary account in zh/zj': addToZHOther = True
    addToZHAdmin = (current_user.is_admin()) and (addToZHUser or addToZHOther)

    removeButtons = checkRequestRemoval(ac.issues['error'])

    disabledButton = None
    if User(cms_id=current_user.person.cmsId).person.hrId != ldap_person.hrId:
        disabledButton = checkAccountDisabled(ac.issues['error'])

    mailRegBtn = checkRegistrationNeeded(ac.issues['error'])

    return addToZHAdmin, addToZHUser, disabledButton, mailRegBtn, removeButtons, helpInfo


def checkAccountDisabled(errList):

    if errList:
        reLine = re.compile(
            r'account (\w+) for hrId \d+ exists, but is presently disabled')
        for item in errList:
            if item['check'] == 'account disabled':
                # print '++> msg: "%s:' % item['msg']
                reLineMatch = reLine.match(item['msg'])
                if reLineMatch:
                    return {item['msg']: reLineMatch.group(1)}

    return None


def checkRegistrationNeeded(errList):

    if errList:
        for item in errList:
            # print '++> msg: "%s:' % item['msg']
            if (item['check'] == 'user NOT in CMS DB'):
                # print '***> returning msg: "%s:' % item['msg']
                return {item['msg']: item['msg']}

    return None


def checkRequestRemoval(errList):

    removeButtons = []
    if errList:
        reLine = re.compile(
            r'primary account "\w+" for hrId \d+ is also in (LDAP group|eGroup) (\w\w) \((\w+)\)')
        for item in errList:
            if 'primary account in other expt' in item['check']:
                # print '++> msg: "%s:' % item['msg']
                reLineMatch = reLine.match(item['msg'])
                if reLineMatch:
                    removeButtons.append({item['msg']: reLineMatch.group(2)})
                else:
                    msg = 'ERROR no match found in regex for "%s"' % item['msg']
                    logging.error(msg)
                    flash(msg, const.FLASK_FLASH_DANGER)

    return removeButtons


@users_blueprint.route('/sendMailRegNeeded/<int:hr_id>')
@login_required
@decorators.roles_required(const.ROLE_ADMIN)
def route_send_mail_reg_needed(hr_id):

    ldap_person = LdapProxy.get_instance().get_person_by_hrid(hr_id)

    logging.info("+++++++ request to send mail for registration needed for hrId %s (%s) to zh" %
                 (hr_id, ldap_person.login))

    args = {'name': ldap_person.fullName}
    mail_body = flask.render_template('emails/registrationNeeded.txt', **args)

    email, log = EmailMessage.compose_message(sender=flask.current_app.config['EMAIL_FROM'],
                                              bcc=flask.current_app.config['EMAIL_BCC'],
                                              reply_to=flask.current_app.config['EMAIL_REPLY_TO'],
                                              subject='Not registered with CMS (%s) - action needed' % ldap_person.login,
                                              source_app='toolkit',
                                              to=ldap_person.mail,
                                              body=mail_body,
                                              # do not yet send, we'll need to check if it's already done ...
                                              db_session=None,
                                              )

    res = EmailMessage.query.filter_by(hash=email.hash).all()
    msg = "found %s existing mail(s) with hash %s: %s " % (
        len(res), email.hash, '\n'.join([str(x) for x in res]))
    if res:
        oldLog = EmailLog.query.filter_by(email_id=res[0].id).first()
        logging.warning(msg)
        logging.warning('A mail was already sent for this user (%s) on %s' % (
            ldap_person.login, oldLog.timestamp))
        flash('A mail was already sent for this user (%s) on %s' %
              (ldap_person.login, oldLog.timestamp), const.FLASK_FLASH_WARNING)
        return redirect(url_for('users.route_account_status', hr_id=hr_id))

    try:
        Person.session.add(email)
        Person.session.add(log)
        Person.session.commit()
    except Exception as e:
        msg = "Exception caught when trying to add email to DB: %s" % str(e)
        logging.error(msg)
        flash(msg, const.FLASK_FLASH_DANGER)

    msg = "Mail for registration needed sent to User %s (hrId %s, account '%s') " % (
        ldap_person.fullName, hr_id, ldap_person.login)
    logging.info(msg)
    flash(msg, const.FLASK_FLASH_SUCCESS)

    return redirect(url_for('users.route_account_status', hr_id=hr_id))


@users_blueprint.route('/sendMailAccountDisabled/<int:hr_id>')
@login_required
@decorators.roles_required(const.ROLE_ADMIN)
def route_send_mail_disabled(hr_id):

    ldap_person = LdapProxy.get_instance().get_person_by_hrid(hr_id)

    logging.info("+++++++ request to send mail for disabled account for hrId %s (%s) to zh" %
                 (hr_id, ldap_person.login))

    args = {'name': ldap_person.fullName, 'login': ldap_person.login}
    mail_body = flask.render_template('emails/accountDisabled.txt', **args)

    email, log = EmailMessage.compose_message(sender=flask.current_app.config['EMAIL_FROM'],
                                              bcc=flask.current_app.config['EMAIL_BCC'],
                                              reply_to=flask.current_app.config['EMAIL_REPLY_TO'],
                                              subject='Your account %s is disabled - action needed' % ldap_person.login,
                                              source_app='toolkit',
                                              to=ldap_person.mail,
                                              body=mail_body,
                                              # do not yet send, we'll need to check if it's already done ...
                                              db_session=None,
                                              )

    res = EmailMessage.query.filter_by(hash=email.hash).all()
    msg = "found %s existing mail(s) with hash %s: %s " % (
        len(res), email.hash, '\n'.join([str(x) for x in res]))
    if res:
        oldLog = EmailLog.query.filter_by(email_id=res[0].id).first()
        logging.warning(msg)
        logging.warning('A mail was already sent for this user (%s) on %s' % (
            ldap_person.login, oldLog.timestamp))
        flash('A mail was already sent for this user (%s) on %s' %
              (ldap_person.login, oldLog.timestamp), const.FLASK_FLASH_WARNING)
        return redirect(url_for('users.route_account_status', hr_id=hr_id))

    try:
        Person.session.add(email)
        Person.session.add(log)
        Person.session.commit()
    except Exception as e:
        msg = "Exception caught when trying to add email to DB: %s" % str(e)
        logging.error(msg)
        flash(msg, const.FLASK_FLASH_DANGER)

    msg = "Mail for disabled account sent to User %s (hrId %s, account '%s') " % (
        ldap_person.fullName, hr_id, ldap_person.login)
    logging.info(msg)
    flash(msg, const.FLASK_FLASH_SUCCESS)

    return redirect(url_for('users.route_account_status', hr_id=hr_id))


@users_blueprint.route('/sendMailAccountAdded/<int:hr_id>')
@login_required
@decorators.roles_required(const.ROLE_ADMIN)
def route_send_mail_account_added(hr_id):

    ldap_person = LdapProxy.get_instance().get_person_by_hrid(hr_id)

    logging.info("+++++++ request to send mail for account added to zh for hrId %s (%s) to zh" %
                 (hr_id, ldap_person.login))

    args = {'name': ldap_person.fullName,
            'login': ldap_person.login, 'removedFromOther': ''}
    mail_body = flask.render_template('emails/accountAdded.txt', **args)

    email, log = EmailMessage.compose_message(sender=flask.current_app.config['EMAIL_FROM'],
                                              bcc=flask.current_app.config['EMAIL_BCC'],
                                              reply_to=flask.current_app.config['EMAIL_REPLY_TO'],
                                              subject='Your account %s is now added to "zh"' % ldap_person.login,
                                              source_app='toolkit',
                                              to=ldap_person.mail,
                                              body=mail_body,
                                              # do not yet send, we'll need to check if it's already done ...
                                              db_session=None,
                                              )

    res = EmailMessage.query.filter_by(hash=email.hash).all()
    msg = "found %s existing mail(s) with hash %s: %s " % (
        len(res), email.hash, '\n'.join([str(x) for x in res]))
    if res:
        oldLog = EmailLog.query.filter_by(email_id=res[0].id).first()
        logging.warning(msg)
        logging.warning('A mail was already sent for this user (%s) on %s' % (
            ldap_person.login, oldLog.timestamp))
        flash('A mail was already sent for this user (%s) on %s' %
              (ldap_person.login, oldLog.timestamp), const.FLASK_FLASH_WARNING)
        return redirect(url_for('users.route_account_status', hr_id=hr_id))

    try:
        Person.session.add(email)
        Person.session.add(log)
        Person.session.commit()
    except Exception as e:
        msg = "Exception caught when trying to add email to DB: %s" % str(e)
        logging.error(msg)
        flash(msg, const.FLASK_FLASH_DANGER)

    msg = "Mail for added account sent to User %s (hrId %s, account '%s') " % (
        ldap_person.fullName, hr_id, ldap_person.login)
    logging.info(msg)
    flash(msg, const.FLASK_FLASH_SUCCESS)

    return redirect(url_for('users.route_account_status', hr_id=hr_id))


@users_blueprint.route('/addToZH/<int:hr_id>')
@login_required
@decorators.roles_required(const.ROLE_ADMIN)
def route_add_to_zh(hr_id):

    ldap_person = LdapProxy.get_instance().get_person_by_hrid(hr_id)

    logging.info("+++++++ request to add hrId %s (%s) to zh" %
                 (hr_id, ldap_person.login))

    egh = EgroupHandler()
    egh.addMemberPerson('zh', hr_id)

    msg = "User %s (hrId %s, account '%s') was added to zh" % (
        ldap_person.fullName, hr_id, ldap_person.login)
    logging.info(msg)
    flash(msg, const.FLASK_FLASH_SUCCESS)

    return redirect(url_for('users.route_account_status', hr_id=hr_id))


@users_blueprint.route('/request_removal_other/<string:grp>/<int:hr_id>', methods=['GET'])
@login_required
@decorators.roles_required(const.ROLE_ADMIN)
def route_request_removal_other(grp, hr_id):

    if grp not in [u'z5', u'zp', u'z2']:
        msg = "illegal group '%s' to request removal from." % grp
        logging.error('request_removal_other> %s' % msg)
        flash(msg, const.FLASK_FLASH_DANGER)
        return redirect(url_for('users.route_account_status', hr_id=hr_id))

    logging.info(
        'request_removal_other> request to remove %s from %s' % (hr_id, grp))

    ldap_person = LdapProxy.get_instance().get_person_by_hrid(hr_id)

    args = {'grp': grp, 'hrId': hr_id,
            'name': ldap_person.fullName, 'login': ldap_person.login}
    mail_body = flask.render_template(
        'emails/requestRemoveFromOtherExpt.txt', **args)

    email, log = EmailMessage.compose_message(sender=flask.current_app.config['EMAIL_FROM'],
                                              bcc=flask.current_app.config['EMAIL_BCC'],
                                              reply_to=flask.current_app.config['EMAIL_REPLY_TO'],
                                              subject='Please remove %s from %s' % (
                                                  ldap_person.fullName, grp),
                                              source_app='toolkit',
                                              to='%s-admins@cern.ch' % grp,
                                              body=mail_body,
                                              # do not yet send, we'll need to check if it's already done ...
                                              db_session=None,
                                              )

    res = EmailMessage.query.filter_by(hash=email.hash).all()
    msg = "Found %s existing mail(s) with hash %s: %s " % (
        len(res), email.hash, '\n'.join([str(x) for x in res]))
    if res:
        oldLog = EmailLog.query.filter_by(email_id=res[0].id).first()
        logging.warning('request_removal_other> %s' % msg)
        msg = 'A mail was already sent to %s admins on %s' % (
            grp, oldLog.timestamp)
        logging.warning('request_removal_other> %s' % msg)
        flash(msg, const.FLASK_FLASH_WARNING)
        return redirect(url_for('users.route_account_status', hr_id=hr_id))

    try:
        EmailMessage.session().add(email)
        EmailMessage.session().add(log)
        EmailMessage.session().commit()
    except Exception as e:
        msg = "Exception caught when trying to add email to DB: %s" % str(e)
        logging.error('request_removal_other> %s' % msg)
        flash(msg, const.FLASK_FLASH_DANGER)

    msg = 'sent request to get %s (account "%s") removed from %s' % (
        ldap_person.fullName, ldap_person.login, grp)
    logging.info('request_removal_other> %s' % msg)
    flash(msg, const.FLASK_FLASH_SUCCESS)

    return redirect(url_for('users.route_account_status', hr_id=hr_id))
