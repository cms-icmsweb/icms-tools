import json
import time
import os

import flask
from flask import g, request, current_app, jsonify, make_response, abort
from flask_login import current_user

from icms_orm.common import UserAuthorData
from icms_orm.cmspeople import Person as PeopleData, Institute as InstData

from .blueprint import api_blueprint
from sqlalchemy import or_, and_

from icmsutils.funcutils import Cache
import webapp


# @Cache.expiring(timeout=60)
@api_blueprint.route('/authorData', methods=['POST'])
def route_authorData():

    start = time.time()
    authorData = g.db.session.query(UserAuthorData)\
                     .filter(UserAuthorData.cmsid > 0)\
                     .filter(UserAuthorData.hrid > 0)\
                     .all()
    peopleWithInspireIDs = (g.db.session.query(PeopleData.hrId, PeopleData.firstName, PeopleData.lastName)
                            .filter(PeopleData.authorId.like('INSPIRE%'))
                            .all()
                            )
    delta = time.time()-start

    names = {}
    for hrid, fName, lName in peopleWithInspireIDs:
        names[hrid] = '%s, %s' % (lName, fName)

    result = []
    for ad in authorData:
        item = json.loads(ad.to_json())
        item.update({'name': names[item['hrId']]})
        result.append(item)

    current_app.logger.info('found %d (%d) entries for UserAuthorData in %s sec.' % (
        len(result), len(peopleWithInspireIDs), delta))

    resReq = jsonify({'data': result})

    return resReq

# the query for the InspireID at http://cms.cern.ch/iCMS/user/inspire?action=list is:
# select PeopleData.NameCMS,PeopleData.NamfCMS,PeopleData.HRpersonId,PeopleData.InstCode as x1InstCode,PeopleData.StatusCMS,PeopleData.Author,PeopleData.AuthorTK,
# InstData.Domain as x2Domain,InstData.SpiresICN as x3SpiresICN,PeopleData.authorId as ZauthorId
# from PeopleData
# left join InstData on InstData.InstCode = PeopleData.InstCode
# where
#   (
#     ( PeopleData.StatusCMS='CMS' or (PeopleData.StatusCMS='EXMEMBER' and PeopleData.Author=1) ) and
#     (InstData.CMSstatus='yes') and
#     (PeopleData.Activity="2" or PeopleData.Activity="17" or PeopleData.Activity="18" or PeopleData.Activity="3" or PeopleData.Activity="6" or PeopleData.Activity="9") and
#     (PeopleData.authorId is NULL or PeopleData.authorId="")
#   ) or (
#     PeopleData.AuthorTK=1 and (PeopleData.authorId is NULL or PeopleData.authorId="")
#   )
# order by PeopleData.NameCMS

# @Cache.expiring(timeout=60)


@api_blueprint.route('/authorsNoOrcID', methods=['POST'])
def route_authorsNoOrcID():

    selectedActivities = [2, 17, 18, 3, 6, 9]
    # get the potentials, ignoring AuthorTK for now ...
    potentials = (g.db.session.query(PeopleData)
                  .join(InstData, InstData.code == PeopleData.instCode)
                  .filter(PeopleData.instCode == InstData.code)
                  .filter(InstData.cmsStatus.ilike('yes'))
                  .filter(or_(PeopleData.status == 'CMS',
                              and_(PeopleData.status == 'EXMEMBER',
                                   PeopleData.isAuthor == 1)
                              ))
                  .filter(PeopleData.activityId.in_(selectedActivities))
                  .filter(PeopleData.authorId.like('INSPIRE%'))
                  .filter(PeopleData.cmsId > 0)
                  .filter(PeopleData.hrId > 0)
                  .all())
    current_app.logger.info('found %d potentials from DB' % len(potentials))

    # get the ones who already have OrcIDs:
    withOrcId = [x[0] for x in (g.db.session.query(UserAuthorData.cmsid)
                                 .filter(UserAuthorData.orcid != None)
                                .all())]
    current_app.logger.info(
        'found %d people with OrcIDs from DB' % len(withOrcId))

    canChangeAll = False
    if current_user.is_admin() or current_user.is_in_egroup('icms-guests'):
        canChangeAll = True

    selectedActivities = [2, 17, 18, 3, 6, 9]
    result = []
    skipAct = 0
    skipOrcId = 0
    for person in potentials:

        if person.activityId not in selectedActivities:
            skipAct += 1
            continue
        if person.cmsId in withOrcId:
            skipOrcId += 1
            continue

        # allow the current user to add his/her own OrcID, or allow an "orcid-admin" to do it for anyone
        canChangeId = (
            person.cmsId == current_user.person.cmsId) or canChangeAll

        # if person.cmsId in [ 7093, 3040 ]:
        #     current_app.logger.info( 'current_user: %s is admin: %s in eGroup %s idCheck: %s canChangeAll: %s - canId: %s' %
        #                              (current_user.person.cmsId, current_user.is_admin(),
        #                               current_user.is_in_egroup( 'icms-guests' ),
        #                               person.cmsId == current_user.person.cmsId,
        #                               canChangeAll,
        #                               canChangeId ) )

        # here we now have the people w/o OrcID (but with an InspireID)
        result.append({'person': {"name": '%s, %s' % (person.lastName, person.firstName),
                                  "InstCode": person.instCode,
                                  "cmsId": person.cmsId,
                                  "hrId": person.hrId,
                                  "authorId": person.authorId,
                                  "canChangeId": canChangeId,
                                  }
                       })

    current_app.logger.info(
        'found %d (potential) authors without OrcID set (skipped %d/%d [activity/orcId])' % (len(result), skipAct, skipOrcId))

    resReq = jsonify({'data': result})

    return resReq


@api_blueprint.route('/alFile', methods=['POST'])
def route_alFile():

    fName = None
    if 'fName' in request.form.keys():
        fName = request.form.get('fName')
    else:
        fieldName = list(request.form.keys())[0]
        fName = json.loads(fieldName)['fName']

    webapp.Webapp.remount_eos_if_needed(flask.current_app)

    # from https://stackoverflow.com/questions/27628053/uploading-and-downloading-files-with-flask#27638470
    current_app.logger.info('request to download %s' % fName)

    content = None
    eosTopDir = flask.current_app.config.get('AL_FILES_DIR')
    fPath = os.path.join(eosTopDir, fName)
    if not os.path.exists(fPath):
        current_app.logger.error("file not found at path: %s " % fPath)
        return abort(404)

    with open(fPath, mode='r', encoding='utf-8') as inFile:
        try:
            content = inFile.read()
        except:
            current_app.logger.error("error reading file at path: %s " % fPath)
            return abort(404)

    if content:
        current_app.logger.info("sending file content from path: %s " % fPath)

        response = make_response(content)
        response.headers["Content-Disposition"] = "attachment; filename=%s" % fName
        response.headers["Cache-Control"] = "must-revalidate"
        response.headers["Pragma"] = "must-revalidate"
        if 'tex' in fName.split('.')[-1]:
            response.headers["Content-type"] = "application/x-tex"
        elif 'xml' in fName.split('.')[-1]:
            response.headers["Content-type"] = "application/xml"
            response.headers["Content-encoding"] = "utf-8"
        return response

    current_app.logger.error("Error reading file at %s " % fPath)
    return abort(404)
