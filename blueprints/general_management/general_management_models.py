import logging
import re
from datetime import datetime, timedelta

import requests
from icms_orm.toolkit import CmsWeek, Room, RoomWeekRel

from util import external

log: logging.Logger = logging.getLogger(__name__)


class IndicoBooking(object):
    def __init__(self, room, time_start, time_end):
        self.room = room
        self.time_start = time_start
        self.time_end = time_end

    @staticmethod
    def add_new_room_to_toolkit_db(indico_id, room_str, db):
        log.debug(f"Adding a room from string: {room_str}")
        pattern = r"(\w+)[-/](\w+)-(\w+)(?: - (.*))*"
        m = re.search(pattern, room_str)
        if m:
            custom_name = m.group(4).strip() if m.group(4) else None
            room = Room(
                indico_id=indico_id,
                building=m.group(1),
                floor=m.group(2),
                room_nr=m.group(3),
                name=custom_name if custom_name else None,
            )
            db.session.add(room)
            db.session.commit()
            log.info(f"Auto-imported room from indico: {str(room)}")
            return room
        else:
            log.warning(
                f"Failed to import room #{indico_id} to toolkit DB! Indico room string: {room_str}"
            )
        return None

    @staticmethod
    # @funcutils.Cache.expiring(timeout=300)
    def get_virtual_bookings(db, cms_week_id):
        """
        Returns virtual bookings for (external) CMS Weeks. Rather than pulled from Indico, these are just made up here.
        :param db: db to connect to
        :param cms_week_id: id of cms
        :return:
        """

        rooms = (
            db.session.query(Room)
            .join(RoomWeekRel, RoomWeekRel.room_id == Room.id)
            .filter(RoomWeekRel.week_id == cms_week_id)
            .all()
        )

        bookings = []

        if not rooms:
            return bookings

        date_start, date_end = (
            db.session.query(CmsWeek.date, CmsWeek.date_end)
            .filter(CmsWeek.id == cms_week_id)
            .one()
        )

        for day in [
            date_start + timedelta(days=x)
            for x in range(0, (date_end - date_start).days)
        ]:
            for room in rooms:
                bookings.append(
                    IndicoBooking(
                        room=room,
                        time_start=datetime(day.year, day.month, day.day, 8),
                        time_end=datetime(day.year, day.month, day.day, 20),
                    )
                )
        return bookings

    @staticmethod
    # @funcutils.Cache.expiring(timeout=300)
    def get_bookings_between(
        db,
        indico_token,
        indico_secret,
        date_start,
        date_end,
        reasons=["cms week", "cms physics week"],
        booked_for="*cms*secretariat*",
    ):
        bookings = []
        rooms = (
            db.session.query(Room)
            .order_by(Room.building, Room.floor, Room.room_nr)
            .all()
        )
        rooms_by_indico_id = {room.indico_id: room for room in rooms}
        params = {
            "detail": "reservations",
            "from": date_start.strftime("%Y-%m-%d"),
            "to": date_end.strftime("%Y-%m-%d"),
            "pretty": "yes",
            "bookedfor": booked_for,
            "cancelled": "no",
            "rejected": "no",
            "confirmed": "yes",
            "occurrences": "yes",
        }

        req_full_path = "%s%s" % (
            "https://indico.cern.ch",
            external.build_indico_request(
                path="/export/reservation/CERN.json",
                params=params,
                api_key=indico_token,
                secret_key=indico_secret,
            ),
        )

        # switch to token auth:
        headers = {"Authorization": "Bearer %s" % indico_token}
        output = requests.get(req_full_path, headers=headers).json()

        for result in output["results"]:
            if any([reason in result["reason"].lower() for reason in reasons]):
                for occurrence in result["occurrences"]:
                    if not occurrence["is_cancelled"]:
                        datetime_from = datetime.strptime(
                            "%s %s"
                            % (
                                occurrence["startDT"]["date"],
                                result["startDT"]["time"],
                            ),
                            "%Y-%m-%d %H:%M:%S",
                        )
                        datetime_till = datetime.strptime(
                            "%s %s"
                            % (occurrence["endDT"]["date"], result["endDT"]["time"]),
                            "%Y-%m-%d %H:%M:%S",
                        )
                        room_str = result["room"]["fullName"]
                        room_id = int(result["room"]["id"])
                        room = None
                        if room_id in rooms_by_indico_id:
                            room = rooms_by_indico_id[room_id]
                        else:
                            logging.warning(
                                "Found indico booking for a room indico_id=#%d that is yet unknown to the toolkit!"
                                % room_id
                            )
                            room = IndicoBooking.add_new_room_to_toolkit_db(
                                indico_id=room_id, room_str=room_str, db=db
                            )
                            if room:
                                rooms_by_indico_id[room.indico_id] = room
                            else:
                                logging.warn(
                                    "Room indico_id=%d was neither found in toolkit DB, nor imported automatically.Skipping a booking!"
                                    % room_id
                                )
                                continue
                        bookings.append(
                            IndicoBooking(
                                room=room,
                                time_start=datetime_from,
                                time_end=datetime_till,
                            )
                        )

        return bookings

    def __repr__(self):
        return "IndicoBooking for room %s on %s" % (
            str(self.room),
            str(self.time_start),
        )
