from functools import wraps
from util.flask_extensions import redirect_unauthorized
import flask
import util.constants
import flask_login


def admin_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if flask_login.current_user.is_admin():
            return func(*args, **kwargs)
        else:
            flask.flash('Insufficient privileges: you are not an admin.',
                        util.constants.FLASK_FLASH_DANGER)
            return redirect_unauthorized()
    return decorated_function


def cbi_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if flask_login.current_user.is_cbi():
            return func(*args, **kwargs)
        else:
            flask.flash('Insufficient privileges: only CBIs can visit the page requested.',
                        util.constants.FLASK_FLASH_DANGER)
            return redirect_unauthorized()
    return decorated_function


def roles_required(roles):
    def decorator(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            if flask_login.current_user.get_roles_mask() & roles:
                return func(*args, **kwargs)
            else:
                flask.flash('Your privileges were insufficient to see the requested page. '
                            'You have been redirected here instead.', util.constants.FLASK_FLASH_DANGER)
                return redirect_unauthorized()
        return decorated_function
    return decorator


def any_flag_required(flags):
    def decorator(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            for flag in flags:
                if flask_login.current_user.has_flag(flag):
                    return func(*args, **kwargs)
            flask.flash(
                'You cannot access the requested page because you are not a holder of any of the following flags: %s '
                % (', '.join(flags)), util.constants.FLASK_FLASH_DANGER)
            return redirect_unauthorized()
        return decorated_function
    return decorator


def weak_login_required(func):
    """
    Basically a copy of the standard login_required decorator except that here we check for anonymous user rather than
    not authenticated
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if flask.current_app.login_manager._login_disabled:
            return func(*args, **kwargs)
        elif flask_login.current_user.is_anonymous:
            return flask.current_app.login_manager.unauthorized()
        return func(*args, **kwargs)
    return decorated_view


def clearance_required(clearance, params_remapping={}):
    def decorator(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            cls = clearance.__class__
            if clearance(cls, **{k: kwargs.get(v, None) for k, v in params_remapping.items()}):
                return func(*args, **kwargs)
            else:
                flask.flash('Your privileges were insufficient to see the requested page. '
                            'If you think this is an error, please contact icms-support@cern.ch', util.constants.FLASK_FLASH_DANGER)
                return redirect_unauthorized()
        return decorated_view
    return decorator
