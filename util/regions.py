regions = [
            {
                'name': 'CERN',
                'countries': ['CERN'] 
            },
            {
                'name': 'France',
                'countries': ['France'] 
            },
            {
                'name': 'Germany',
                'countries': ['Germany'] 
            },
            {
                'name': 'Italy',
                'countries': ['Italy'] 
            },
            {
                'name': 'Russia and Dubna Member States (RDMS)',
                'countries': [
                    'Russia',
                    'Armenia',
                    'Belarus',
                    'Georgia',
                    'Ukraine',
                    'Uzbekistan'
                ] 
            },
            {
                'name': 'Switzerland',
                'countries': ['Switzerland'] 
            },
            {
                'name': 'United Kingdom',
                'countries': ['United Kingdom'] 
            },
            {
                'name': 'USA',
                'countries': ['USA'] 
            },
            {
                'name': 'Other CERN Member States (OCMS)',
                'countries': [
                    'Austria',
                    'Belgium',
                    'Bulgaria',
                    'Finland',
                    'Greece',
                    'Hungary',
                    'Poland',
                    'Portugal',
                    'Serbia',
                    'Spain'
                ]
            },
            {
                'name': 'Other States A (OSA)',
                'countries': [
                    'China',
                    'India',
                    'Iran',
                    'Korea',
                    'Malaysia',
                    'New Zealand',
                    'Pakistan',
                    'Sri Lanka',
                    'Taiwan',
                    'Thailand'
                ]
            },
            {
                'name': 'Other States B (OSB)',
                'countries': [
                    'Bahrain',
                    'Brazil',
                    'Colombia',
                    'Croatia',
                    'Cyprus',
                    'Ecuador',
                    'Egypt',
                    'Estonia',
                    'Ireland',
                    'Kuwait',
                    'Lebanon',
                    'Latvia',
                    'Lithuania',
                    'Mexico',
                    'Montenegro',
                    'Oman',
                    'Qatar',
                    'Saudi Arabia',
                    'Turkey',
                    'Ukraine'
                ]
            },
        ]