"""
A file containing methods that we're likely to cache so that we keep em all here
"""
from icms_orm.cmspeople import Person as LegacyPerson, Institute as LegacyInstitute, PeopleFlagsAssociation as PFA
from icms_orm.cmspeople import User as LegacyUser, MoData as LegacyMoData
from icms_orm.common import Person, Institute, InstituteLeader
from icmsutils.funcutils import Cache
from util.trivial import current_db_ssn


@Cache.expiring(timeout=300)
def get_flags_by_cms_id():
    result = dict()
    for cms_id, flag_id in current_db_ssn().query(PFA.cmsId, PFA.flagId):
        result[cms_id] = result.get(cms_id, set())
        result[cms_id].add(flag_id)
    return result


@Cache.expiring(timeout=300)
def get_leader_id_by_inst_code():
    return {r[0]: r[1] for r in current_db_ssn().query(InstituteLeader.inst_code, InstituteLeader.cms_id).
        filter(InstituteLeader.end_date == None).filter(InstituteLeader.is_primary == True).all()}


@Cache.expiring(timeout=300)
def get_leader_and_deputies_ids_by_inst_code():
    result = dict()
    for inst_code, cms_id in current_db_ssn().query(InstituteLeader.inst_code, InstituteLeader.cms_id).\
            filter(InstituteLeader.end_date == None).all():
        result[inst_code] = result.get(inst_code, set())
        result[inst_code].add(cms_id)
    return result


