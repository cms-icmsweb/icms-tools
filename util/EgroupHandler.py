#
#  Updated version to access the CERN eGroup SOAP interface using Zeep:
#     Zeep: http://docs.python-zeep.org/en/master/index.html
#
#     eGroups docu: https://aisdocs.web.cern.ch/display/ED/
#     SOAP interface: https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsServicesSchema.xsd
#
# last update: AP, 2017-08-11
#

import logging
import netrc
import os
import sys
from http.client import CONFLICT
from logging.handlers import RotatingFileHandler

import zeep
from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from werkzeug.exceptions import Conflict, InternalServerError, NotFound
from zeep.plugins import HistoryPlugin
from zeep.transports import Transport

# create logger
logger = logging.getLogger("eGroup_handler_logger")
logger.setLevel(logging.INFO)

ztl = logging.getLogger("zeep.transports")
ztl.setLevel(logging.INFO)

# create console handler and set level to debug
if not os.path.exists("logs"):
    os.makedirs("logs")
ch = RotatingFileHandler(
    "logs/eGroup_handler_logger.log", maxBytes=1000000, backupCount=1
)
ch.setLevel(logging.INFO)

# create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)


class EgroupHandler(object):

    _instance = None

    def __new__(cls):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        netrcFileName = "/data/secrets/.netrc"
        if not os.path.exists(netrcFileName):
            netrcFileName = os.path.join(os.getenv("HOME"), ".netrc")

        (login, account, pwd) = netrc.netrc(netrcFileName).authenticators(
            "egroups.cern.ch"
        )
        self.eGlogin = login
        self.eGpwd = pwd

        url = "https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl"

        session = Session()
        session.auth = HTTPBasicAuth(login, pwd)

        history = HistoryPlugin()
        self.client = zeep.Client(
            url, transport=Transport(session=session), plugins=[history]
        )

    def checkOK(self, replyIn, ignoreWarn=False):
        reply = str(replyIn)

        if "error" in reply and replyIn["error"] is not None:
            msg = f"\n... error found in reply: {replyIn['error']}"
            logger.error(msg)
            return False

        if not ignoreWarn and "warnings" in reply and replyIn["warnings"] != []:
            logger.info("... warnings found in reply: ")
            for warn in replyIn["warnings"]:
                logger.info('\t -- "%s"' % warn["Message"])
            return False

        return True

    def findGroup(self, groupName=None):

        if not groupName:
            logger.warning("no groupname given ... ")
            return False, "no group-name given"

        # logger.debug( 'client.service.FindEgroupByName getting group %s' % groupName )

        result = self.client.service.FindEgroupByName(groupName)

        try:
            logger.error(result.error.Message)
            raise NotFound(result.error.Message)
        except AttributeError:
            return True, result.result

    def deleteGroup(self, groupName=None):

        if not groupName:
            logger.warning("no groupname given ... ")
            return False, None

        # logger.debug( 'deleting eGroup %s' % groupName )

        result = self.client.service.DeleteEgroup(groupName)

        if not self.checkOK(result):
            msg = f"removeMember> ERROR: Could not delete group {groupName} - reason: {str(result)} "
            logging.error(msg)
            raise InternalServerError(msg)

        return True

    def getGroupMembers(self, groupName=None):

        memberList = []
        memberNoAccount = []
        egMemberList = []
        accountList = []
        emailList = []

        if not groupName:
            logger.warning("getGroupMembers> no groupname given ... ")
            return memberList, memberNoAccount, egMemberList, accountList, emailList

        status, groupInfo = self.findGroup(groupName)
        if not status:
            msg = 'error from findGroup for "%s" : "%s" ' % (groupName, str(groupInfo))
            logger.error(msg)
            raise NotFound(msg)

        for member in groupInfo.Members:
            if member.Type == "StaticEgroup":
                egMemberList.append(member.Name)
            elif member.Type == "DynamicEgroup":
                egMemberList.append(member.Name)
            elif member.Type == "External":
                emailList.append(member.Email)
            elif member.Type == "Account":
                accountList.append(member.Name)
            elif member.Type == "Person":
                # if member.Type != 'Person' : logger.debug( 'new type; ',member
                try:
                    memInfo = str(member.PrimaryAccount)
                    memInfo += ";" + str(member.ID)
                    if member.Email:
                        memInfo += ";" + str(member.Email)
                    memberList.append(memInfo)
                except AttributeError:
                    memberNoAccount.append(member)
                    logger.error(f"getGroupMembers> No account ??? for {member} ")
                    continue
            else:
                msg = f"Unhandled member type {member.Type} found -- ignoring member {member} !"
                logger.error(msg)

        return memberList, memberNoAccount, egMemberList, accountList, emailList

    def accountInEgroup(self, account, groupName):

        if not groupName:
            logger.warning("no groupname given ... ")
            return False, None

        status, groupInfo = self.findGroup(groupName)
        if not status:
            msg = 'error from findGroup for "%s" : "%s" ' % (groupName, str(groupInfo))
            logger.error(msg)
            raise NotFound(msg)

        if len(groupInfo.Members) == 0:
            msg = 'found NO members in group "%s"' % groupName
            logger.error(msg)
            return False, None
            # raise InternalServerError( msg )

        target_username = account.lower()
        for member in groupInfo.Members:
            if member.Type == "StaticEgroup":
                continue  # ignore eGroups as members
            username = (
                member.PrimaryAccount.lower()
                if member.PrimaryAccount
                else member.Name.lower()
            )
            if username == target_username:
                return True, member

        logger.warning(
            "default exit - primary account %s not found in %s (%s members checked)"
            % (account, groupName, len(groupInfo.Members))
        )
        return False, None

    def addMembersBulk(self, eGroupName, newMembers):

        nTotal = 0
        for k, v in newMembers.items():

            if k in ["hr_ids", "Person"]:
                nTotal += len(v)
                for id in v:
                    logger.info(f"going to add {k} {id} to eGroup {eGroupName} ")
                    self.addMemberPerson(eGroupName, id)

            elif k in ["accounts", "Account"]:
                nTotal += len(v)
                for id in v:
                    if id.strip() == "":
                        continue
                    logger.info(f"going to add {k} {id} to eGroup {eGroupName} ")
                    self.addMemberAccount(eGroupName, id)

            elif k in ["emails", "External"]:
                nTotal += len(v)
                for id in v:
                    if id.strip() == "":
                        continue
                    logger.info(f"going to add {k} {id} to eGroup {eGroupName} ")
                    self.addMemberEmail(eGroupName, id)

            elif k in ["egroups", "StaticEgroup"]:
                nTotal += len(v)
                for id in v:
                    if id.strip() == "":
                        continue
                    logger.info(f"going to add {k} {id} to eGroup {eGroupName} ")
                    self.addMemberEGroup(eGroupName, id)

            elif k in ["DynamicEgroup"]:
                nTotal += len(v)
                for id in v:
                    if id.strip() == "":
                        continue
                    logging.info(
                        "adding dynamic eGroup %s to eGroup %s" % (v, eGroupName)
                    )
                    member = {"Name": id, "Type": "DynamicEgroup"}
                    return self.addMember(eGroupName, member)

            else:
                logger.warning(
                    f"unknown type {k} found when trying to add members in bulk to eGroup {eGroupName} "
                )

        return True, f"added a total of {nTotal} new members to {eGroupName} "

    def addMemberEmail(self, eGroupName, eMailAddress):
        logger.debug("adding email %s to eGroup %s" % (eMailAddress, eGroupName))
        if "cern.ch" in eMailAddress:
            return (
                False,
                "CERN emails (%s) need to be added as accounts." % eMailAddress,
            )
        member = {"Email": eMailAddress, "Type": "External"}
        return self.addMember(eGroupName, member)

    def addMemberAccount(self, eGroupName, account):
        logger.debug("adding account %s to eGroup %s" % (account, eGroupName))
        member = {"Name": account, "Type": "Account"}
        return self.addMember(eGroupName, member)

    def addMemberPerson(self, eGroupName, hrId):
        logger.debug("adding person with CERN ID %s to eGroup %s" % (hrId, eGroupName))
        member = {"ID": hrId, "Type": "Person"}
        return self.addMember(eGroupName, member)

    def addMemberEGroup(self, eGroupName, eGrpToAdd):
        logging.debug("adding eGroup %s to eGroup %s" % (eGrpToAdd, eGroupName))
        member = {"Name": eGrpToAdd, "Type": "StaticEgroup"}
        return self.addMember(eGroupName, member)

    def addMember(self, eGroupName, member):

        status, group = self.findGroup(eGroupName)
        if not status:
            logger.error("error from findGroup for %s : %s " % (eGroupName, str(group)))
            return False, str(group)

        # in case the eGroup does not have members, accessing the "members" list will give an error:
        # AttributeError: EgroupType instance has no attribute 'Members'
        # if this is the case, create a new members list from scratch and add the new member to it.
        # Kudos to Joel.Closier@cern.ch for finding and sharing this

        overWriteMembers = False  # or True, if you want to reset the list
        ret = self.client.service.AddEgroupMembers(
            eGroupName, overWriteMembers, [member]
        )

        if not self.checkOK(ret):
            if ret["error"]:
                logger.error(
                    "Could not add user % s to group %s - reason: %s "
                    % (str(member), eGroupName, str(ret))
                )
                return False, f'ERROR: {ret["error"]}'

            if ret["warnings"]:
                logger.warning(
                    "Could not add user % s to group %s - reason: %s "
                    % (str(member), eGroupName, str(ret))
                )
                return True, f'WARNING: {ret["warnings"]}'

        logger.info("adding %s to %s returned: %s" % (str(member), eGroupName, ret))
        return True, "OK"

    def removeMemberPerson(self, eGroupName, hrId):
        logging.debug(
            "removing person with CERN ID %s from eGroup %s" % (hrId, eGroupName)
        )
        member = {"ID": hrId, "Type": "Person"}
        return self.removeMember(eGroupName, member)

    def removeMemberAccount(self, eGroupName, account):
        logging.debug(
            "removing person with CERN ID %s from eGroup %s" % (account, eGroupName)
        )
        member = {"Name": account, "Type": "Account"}
        return self.removeMember(eGroupName, member)

    def removeMemberEGroup(self, eGroupName, eGrpToRemove):
        logging.debug(
            "removing person with CERN ID %s from eGroup %s"
            % (eGrpToRemove, eGroupName)
        )
        member = {"Name": eGrpToRemove, "Type": "StaticEgroup"}
        return self.removeMember(eGroupName, member)

    def removeMemberEmail(self, eGroupName, eMailToRemove):
        logging.debug("removing email %s from eGroup %s" % (eMailToRemove, eGroupName))
        member = {"Email": eMailToRemove, "Type": "External"}
        return self.removeMember(eGroupName, member)

    def removeMember(self, eGroupName, member):

        status, group = self.findGroup(eGroupName)
        if not status:
            msg = f"removeMember> ERROR from findGroup for {eGroupName}: {str(group)} "
            logging.error(msg)
            raise NotFound(msg)

        # in case the eGroup does not have members, accessing the "members" list will give an error:
        # AttributeError: EgroupType instance has no attribute 'Members'
        # if this is the case, create a new members list from scratch and add the new member to it.
        # Kudos to Joel.Closier@cern.ch for finding and sharing this

        ret = self.client.service.RemoveEgroupMembers(eGroupName, [member])
        logging.info("removing %s to %s returned: %s" % (str(member), eGroupName, ret))

        if not self.checkOK(
            ret, ignoreWarn=True
        ):  # ignore warnings about non-existing members
            msg = f"removeMember> ERROR: Could not remove user {str(member)} from group {eGroupName} - reason: {str(ret)} "
            logging.error(msg)
            raise InternalServerError(msg)

        return True, "OK"

    def newGroup(
        self,
        groupName=None,
        description=None,
        mailProps=None,
        privacy=None,
        adminEgroup=None,
    ):

        logger.debug("creating new eGroup %s" % groupName)

        try:
            logger.debug("++++++++> first, attempt to find new group ... ")
            groupChk = self.client.service.FindEgroupByName(groupName)
        except Exception as e:
            raise e

        # print( f'\n\n-->> got: {str(groupChk)}\n\n')

        # explicitly check the returned object to make sure an eGroup with that name does NOT already exist
        # and the service returns the correct error.
        expectedMsg = f"The name {groupName} does not correspond to any valid egroup."
        expectedCode = "NOT_FOUND"
        # print( f'\n\n-->> expected: Code: {expectedCode} - msg: {expectedMsg}\n\n')
        if (
            "error" in groupChk
            and groupChk["error"] is not None
            and groupChk["error"]["Code"] == expectedCode
            and groupChk["error"]["Message"] == expectedMsg
        ):
            msg = f"++++++++>  ... {groupName} does not yet exist, going to create it "
            logger.debug(msg)
        else:
            msg = f"ERROR: eGroup '{groupName}' already exists! Nothing done."
            logger.error(msg)
            raise Conflict(msg)

        msg = ""

        # create a new group and set it up:
        owner = {"PersonId": 422405}

        defaultMailProps = {
            "MailPostingRestrictions": {"PostingRestrictions": "Everyone"},
            "SenderAuthenticationEnabled": False,
            "WhoReceivesDeliveryErrors": "GroupOwner",
            "MaxMailSize": 10,
            "ArchiveProperties": "DoesNotExist",
        }
        if not mailProps:
            mailProps = defaultMailProps

        defaultPrivacy = {
            "Privacy": "Users",
            "Selfsubscription": "Closed",
        }

        if not privacy:
            privacy = defaultPrivacy

        newGroup = {
            "ID": None,  # ID is now optional
            "Name": groupName,
            "Type": "StaticEgroup",
            "Description": description if description else "iCMS testing group",
            "Usage": "SecurityMailing",  # 'EgroupsOnly' or 'SecurityMailing'
            "BlockingReason": "Manual",
            "Owner": owner,
            "AdministratorEgroup": adminEgroup
            if adminEgroup
            else "cms-web-access-admins",
            "EmailProperties": mailProps,
        }

        logger.debug(f"\ngoing to call update with: {newGroup}\n")
        newGroup.update(privacy)

        # newProp = newGroup.EmailProperties
        # newProp.MailPostingRestrictions.PostingRestrictions = "Everyone"

        logger.debug("++++++++> going to call sync ... ")
        logger.debug(f"\n==> newGroup is: {newGroup} ")

        # now create the new e-group (as it's ID is 0 it should be created)
        ret = self.client.service.SynchronizeEgroup(newGroup)
        logger.debug(f"\n====>> SynchronizeEgroup returned: {ret}")

        if not self.checkOK(ret, ignoreWarn=True):
            logger.info(
                "\n++++++++>  error, could not create the eGroup. Aborting ! \n"
            )
            return False, str(ret)  # raise InternalServerError( str(msg) )

        return True, "OK"


def test():

    egCkr = EgroupHandler()
    tstGrpName = "ap-tst-foo1"

    logger.debug("findGroup returned: ", egCkr.findGroup(tstGrpName))

    # logger.debug( "deleting returned : ", egCkr.deleteGroup(tstGrpName)
    # logger.debug( "findGroup returned: ", egCkr.findGroup(tstGrpName)

    ret = egCkr.newGroup(tstGrpName)
    logger.debug("newGroup  returned: ", ret)
    logger.debug(
        "addMemberAccount :", egCkr.addMemberAccount(tstGrpName, "andreasp")
    )  # still check if that correctly gives an error ...
    if not ret:
        return  # no need to continue if error when creating new group.

    logger.debug(
        "addMemberEmail   :", egCkr.addMemberEmail(tstGrpName, "apfeiffer1@gmail.com")
    )

    logger.debug("addMemberAccount :", egCkr.addMemberPerson(tstGrpName, 422405))
    logger.debug("addMemberAccount :", egCkr.addMemberPerson(tstGrpName, 422405))

    mem, noAcct = egCkr.getGroupMembers(tstGrpName)
    logger.debug(
        "found %s members in egroup %s, plus %s w/o account "
        % (len(mem), tstGrpName, len(noAcct))
    )
    logger.debug(mem)
    logger.debug(noAcct)


def cleanup():
    egCkr = EgroupHandler()
    egCkr.deleteGroup("ap-tst-foo1")
    # egCkr.deleteGroup('ap-tst-foo2')


def check():
    egCkr = EgroupHandler()

    groupName = "hn-cms-admin"  #''cms-web-access' # 'zh'

    # zhGroup = egCkr.findGroup( groupName )
    # logger.debug( zhGroup

    zhMems, zhNoAcct, egMemberList, accountList, emailList = egCkr.getGroupMembers(
        groupName
    )
    logger.debug(
        "found %s members in egroup %s, plus %s w/o account :"
        % (len(zhMems), groupName, len(zhNoAcct))
    )
    logger.debug(zhMems)
    logger.debug(zhNoAcct)
    logger.debug(egMemberList)
    logger.debug(accountList)
    logger.debug(emailList)


def main():
    # check()
    test()
    cleanup()


if __name__ == "__main__":
    logFmt = "%(asctime)s %(levelname)s : '%(message)s'"
    logger.basicConfig(format=logFmt, level=logger.WARNING)
    main()
