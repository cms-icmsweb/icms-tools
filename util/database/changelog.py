from enum import Enum
import icms_orm
import logging
import traceback
import flask_login
from datetime import datetime, date


class DbChangesListener(object):

    class Operation(Enum):
        INSERT = 'INSERT'
        DELETE = 'DELETE'
        UPDATE = 'UPDATE'

    class ChangeDescriptor(object):
        def __init__(self, operation=None, schema_name: str=None, table_name: str=None, pkey_values: tuple=None, previous_values: dict=None, new_values: dict=None, user_cms_id: int=None):
            self.operation = operation
            self.schema_name = schema_name
            self.table_name = table_name
            self.pkey_values = pkey_values
            self.previous_values = previous_values
            self.new_values = new_values
            self.user_cms_id = user_cms_id
    
        def __str__(self):
            return ''.format()

        def log_the_change(self):
            RowChange = icms_orm.common.RowChange
            ROT = icms_orm.common.RowOperationTypes
            _ia_dict = {
                RowChange.schema_name: self.schema_name,
                RowChange.table_name: self.table_name,
                RowChange.former_values: self.previous_values,
                RowChange.new_values: self.new_values,
                RowChange.operation: {DbChangesListener.Operation.INSERT: ROT.INSERT, DbChangesListener.Operation.UPDATE: ROT.UPDATE, DbChangesListener.Operation.DELETE: ROT.DELETE}.get(self.operation),
                RowChange.timestamp: datetime.utcnow(),
                RowChange.cms_id: self.user_cms_id
            }
            if self.pkey_values is not None:
                if len(self.pkey_values) == 1 and isinstance(self.pkey_values[0], int):
                    _ia_dict[RowChange.primary_key] = self.pkey_values[0]
                else:
                    _ia_dict[RowChange.composite_key] = list(self.pkey_values)

            RowChange.session().add(RowChange.from_ia_dict(_ia_dict))


    @staticmethod
    def event_listener(session, flush_context):
        """
        Objects that are about to added, edited, deleted are still in the session. They all have their instance_state descriptors too.
        The latter can be navigated like so:
            state.unmodified - a set of unchanged attrs
            state.committed_state - a dict of changed attr names with their values from DB
            state.object - object that was flushed
            state.identity - tuple of PK values, like (9981,)
            state.identity_key - some weird tuple, first comes the ORM type, than the tuple of PK values, last one was None in the test case
            state.dict - a dict (string: column value) reresenting the flushed object
        """
        logging.debug('Database hook called, about to log some changes!')
        
        try:
            _objects = []
            logging.debug('Session contains {0} objects: {1} new, {2} dirty and {3} deleted'.\
                format(len(session.identity_map.keys()), len(session.new), len(session.dirty), len(session.deleted)))
            for _bucket in (flush_context.session.dirty, flush_context.session.deleted, flush_context.session.new):
                for _e in _bucket:
                    _objects.append(_e)
            for _object in _objects:
                try:
                    _desc = DbChangesListener.ChangeDescriptor()
                    _desc.schema_name = _object.__table__.schema
                    _desc.table_name = _object.__table__.name
                    _desc.user_cms_id = None
                    try:
                        user = flask_login.current_user
                        if user.is_authenticated:
                            # Beware, user is never None (virtue of being a LocalProxy's instance but it swaps one in in the absence of a valid user!)
                            _desc.user_cms_id = user.cms_id
                    except AttributeError as e:
                        logging.debug('Failed to grab the cms_id attribute of flask_login.current_user: {0}'.format(e))
                    state = _object._sa_instance_state
                    logging.debug('Registering DB change for {schema}.{table_name}. Persistent: {persistent}, modified: {modified}, is deleted: {deleted}, was deleted: {wasdeleted}'.\
                        format(schema=_desc.schema_name, table_name=_desc.table_name, persistent=state.persistent, deleted=state.deleted, modified=state.modified, wasdeleted=state.was_deleted))
                    logging.debug('State object\'s class: {theclass}'.format(theclass=state.__class__))
                    logging.debug('State object\'s str: {state}'.format(state=state))
                    if state.persistent:
                        _desc.pkey_values = state.identity
                        if state.modified:
                            _desc.operation = DbChangesListener.Operation.UPDATE
                            for _key, _value in state.committed_state.items():
                                _desc.previous_values = state.committed_state
                                _desc.new_values = {_k: state.dict[_k] for _k in state.committed_state.keys()}
                        if state.deleted or state.object in session.deleted:
                            logging.debug('Is deleted: {0}, was deleted: {1}, is among the deleted: {2}'.format(state.deleted, state.was_deleted, state.object in session.deleted))
                            _desc.previous_values = {k.key: v for k, v in state.object.to_ia_dict().items()}
                            _desc.operation = DbChangesListener.Operation.DELETE
                    elif state.pending or state.transient:
                        _desc.operation = DbChangesListener.Operation.INSERT
                        _desc.new_values = {k.key: v for k, v in state.object.to_ia_dict().items()}
                    _desc.log_the_change()
                except Exception as e:
                    logging.warning('Failed to extract the info necessary for logging a DB change.')
                    logging.warning(traceback.format_exc())
        except Exception as e:
            logging.warning('Tried to log DB changes but something went very wrong.')
            logging.warning(traceback.format_exc())
