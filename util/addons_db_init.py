from xml.etree import ElementTree
from util import trivial, constants as const
import os, re
from datetime import date, datetime, timedelta
from sqlalchemy import distinct
import logging


def add_past_cms_weeks(toolkit_db):
    """
    Adds hardcoded CMS weeks to the DB unless the CmsWeeks table is not empty
    :param toolkit_db:
    :return:
    """
    from models.icms_toolkit import CmsWeek
    assert toolkit_db.session.query(CmsWeek).count() == 0
    past_weeks = (
        ('CMSWEEK', date(2012, 6, 25)),
        ('CMSWEEK', date(2012, 12, 10)),
        ('PHYSICS WEEK', date(2013, 2, 18)),
        ('CMSWEEK', date(2013, 4, 8)),
        ('CMSWEEK', date(2013, 7, 8)),
        ('CMSWEEK', date(2013, 12, 9)),
        ('DASWEEK', date(2014, 1, 13)),
        ('PHYSICSWEEK', date(2014, 2, 24)),
        ('CMSWEEK', date(2014, 6, 23)),
        ('UPGRADEWEEK', date(2014, 9, 1)),
        ('PHYSICSWEEK', date(2014, 10, 13)),
        ('PHYSICSUPGRADEWEEK', date(2015, 2, 9)),
        ('CMSWEEK', date(2015, 5, 4)),
        ('PHYSICSWEEK', date(2015, 6, 22)),
        ('CMSWEEK', date(2015, 10, 19)),
        ('PHYSICSWEEK', date(2015, 12, 7)),
        ('PHYSICSWEEK', date(2016, 2, 8)),
        ('CMSWEEK', date(2016, 4, 18)),
        ('CMSWEEK', date(2016, 6, 20)),
        ('CMSWEEK', date(2016, 9, 12)),
        ('CMSWEEK', date(2017, 1, 30)),
        ('CMSWEEK', date(2017, 4, 3))
    )

    for past_week in past_weeks:
        week = CmsWeek(*past_week)
        toolkit_db.session.add(week)
    toolkit_db.session.commit()


def add_past_votings(db):
    """

    :param db:
    :return:
    """
    from models.icms_toolkit import Voting
    assert db.session.query(Voting).count() == 0

    root = ElementTree.parse(os.path.join(trivial.get_project_root(), const.DIR_NAME_RSC, 'votings.xml'))
    KEY_DELTA_TO_START = 'DELEGATION_DELTA_TO_START'
    KEY_DURATION_MINS = 'DELEGATION_DURATION_MINS'
    for e in root.findall('DELEGATION'):
        del_dict = {}
        for key in ['POINT', 'VOTE', 'TYPE', 'YEAR', 'DATE', 'TIME', 'DATEBEFORE']:
            child = e.find('DELEGATION_%s' % key)
            if child is not None:
                value = child.get('value')
                if 'DATE' in key:
                    value = value.split()
                    value[1] = re.sub('[a-z]', '', value[1])
                    value = datetime.strptime(' '.join(value), '%A %d %B %Y')
                elif key == 'TIME':
                    m = re.search('([0-9]+)[.: ]([0-9]+)[.: -]+([0-9]+)[.: ]([0-9]+)', value)
                    if m is not None:
                        tokens = [int(x) for x in m.groups()]
                        del_dict[KEY_DELTA_TO_START] = timedelta(hours=tokens[0], minutes=tokens[1])
                        del_dict[KEY_DURATION_MINS] = 60 * (tokens[2]-tokens[0]) + tokens[3] - tokens[1]
                del_dict[key] = value
        logging.debug(del_dict)

        # create a voting object:
        start_time = del_dict['DATE']
        if KEY_DELTA_TO_START in del_dict.keys():
            start_time += del_dict[KEY_DELTA_TO_START]

        end_time = None
        if KEY_DURATION_MINS in del_dict.keys():
            end_time = start_time + timedelta(minutes=del_dict[KEY_DURATION_MINS])

        voting = Voting(code=del_dict['VOTE'], title=del_dict['TYPE'], start_time=start_time,
                        end_time=end_time, delegation_deadline=del_dict['DATEBEFORE'],
                        type=('POINT' in del_dict.keys() and del_dict['POINT'] or 'vote').upper())

        db.session.add(voting)
    db.session.commit()


def add_cern_countries():
    from models.icms_toolkit import CernCountry, CernCountryStatus
    ssn = CernCountry.session()
    assert ssn.query(CernCountry).count() == 0

    statuses = {'MEMBER': None, 'ASSOCIATE': None, 'CO-OPERATING': None, 'CONTACT': None, 'OBSERVER': None}

    for status_str in ['MEMBER', 'ASSOCIATE', 'CO-OPERATING', 'CONTACT', 'OBSERVER']:
        status = CernCountryStatus(name=status_str)
        statuses[status_str] = status
        ssn.add(status)
    ssn.commit()


    member_names = 'Belgium|Denmark|France|Germany|Greece|Italy|Netherlands|Norway|Sweden|Switzerland|United Kingdom'.split('|')
    member_names += 'Austria|Spain|Portugal|Finland|Poland|Hungary|Bulgaria|Israel|Czech Republic|Slovak Republic'.split('|')
    member_names = sorted(member_names)
    associate_names = 'Serbia|Cyprus|Turkey|Pakistan'.split('|')

    from models.icms_people import Institute

    for result in ssn.query(distinct(Institute.country)).order_by(Institute.country).all():
        cname = result[0]
        status = None
        if cname in member_names:
            status = statuses['MEMBER']
        elif cname in associate_names:
            status = statuses['ASSOCIATE']
        if status is not None:
            country = CernCountry(name=cname, status=status)
            ssn.add(country)
    ssn.commit()