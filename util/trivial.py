"""
This module gathers simple yet frequently used functions, not specific to the actual project.
"""


import os
import datetime
import socket
import traceback
from collections import OrderedDict
import time
import re
import flask
import logging
import subprocess

from util import constants


def get_project_root():
    """
    Finds the root project directory as the one containing the 'requirements.txt'
    :return:
    """
    current_dir = os.path.dirname(__file__)
    while 'requirements.txt' not in os.listdir(current_dir) and 'requirements' not in os.listdir(current_dir) and current_dir != '/':
        current_dir = os.path.dirname(current_dir)
    return current_dir


def time_to_string(some_time):
    """
    checks if some_time is a date or datetime and delegates the job accordingly
    """
    if isinstance(some_time, datetime.datetime):
        return datetime_to_string(some_time)
    return date_to_string(some_time)


def date_to_string(some_date):
    if some_date is not None:
        return some_date.strftime('%Y/%m/%d')
    else:
        return '1970/01/01'


def datetime_to_string(some_datetime):
    if some_datetime is not None:
        return some_datetime.strftime('%Y/%m/%d %H:%M')
    else:
        return '1970/01/01 00:00'


def extract_mysql_schema_name(connection_string):
    return extract_schema_name(connection_string)


def extract_schema_name(connection_string):
    schema = None
    if 'mysql://' in connection_string:
        schema = connection_string.split('/')[-1]
        if '?' in schema:
            schema = schema.split('?')[0]
    elif 'oracle://' in connection_string:
        p = r'oracle://(\w+):(?:[^@]+)@(?:\w+)'
        match = re.match(p, connection_string)
        if match:
            schema = match.group(1)
    return schema


def get_time():
    return time.time(), time.clock()


def get_exec_time_string(start_time, label):
    stop_time = get_time()
    return '%s executed in %.2f seconds (wall), %.2f seconds (CPU)' % (
        label, stop_time[0] - start_time[0], stop_time[1] - start_time[1])


def get_deployment_info(commands_override=None):
    cmds = commands_override or ('hostname', 'git describe --tags', 'git rev-parse head', 'git log -n 1', 'pwd', 'uname -a', 'python manage.py db show')
    results = OrderedDict()
    for cmd in cmds:
        for prefix in ('', '/bin/', '/usr/bin/'):
            result = subprocess.run(prefix + cmd, check=False, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            status = result.returncode
            output = result.stdout.decode('utf-8')
            if status == 0:
                results[cmd] = output
                break
    try:
        results['app.is_running_on_production()'] = flask.current_app.is_running_on_production()
        # Also let's have a look at what we receive with a typical request
        results['flask.request.path'] = flask.request.path
        results['flask.request.script_root'] = flask.request.script_root
        results['flask.request.host'] = flask.request.host
        results['flask.request.host_url'] = flask.request.host_url
    except Exception as e:
        logging.warning('Failed to obtain some details of current http request')
        logging.warning(traceback.format_exc())
    for config_key in ['AL_FILES_DIR', 'AL_DIFFS_DIR', 'AL_REF_DIR']:
        try:
            results['app.config["{0}"]'.format(config_key)] = flask.current_app.config.get(config_key)
        except Exception as e:
            logging.warning('Failed to obtain app config value for: %s', config_key)


    return results


def get_sso_login(request):
    return request.headers.get('User', 'anonymous')


def current_db_ssn():
    return flask.current_app.db.session


def add_field_aliases(target, alias_map):
    """
    Creates alias fields in existing objects so that some code can run seamlessly on either the new or the old version
    of the entity, like the Person objects from the old and new iCMS tools
    :param target: the object to be rigged
    :param alias_map: map of fields, like {old_field1: new_field1, ...}
    :return:
    """
    for existing, proxy in alias_map.items():
        setattr(target, proxy, getattr(target, existing))
    return target


def safely_call(fn, error_msg=None):
    """
    :param fn: function to invoke within a try-except brace
    :param error_msg: nothing for exception's message or a fixed text
    :return:
    """
    result = None
    try:
        result = fn()
    except BaseException as e:
        logging.exception('Trapped an exception')
        result = error_msg or str(e)
    finally:
        return result

