from util import constants as const
from util import trivial
import logging
import re
from flask import current_app


class Profile(object):
    CORS_ALLOWED_ORIGINS_REGEX = r"^http(s?)://[A-Za-z-.]+.cern.ch((:\d+)?)((/.*)?)$"
    CORS_ALLOWED_HEADERS = "content-type"
    CORS_ALLOWED_METHODS = "GET, POST, PUT, PATCH, DELETE, OPTIONS"
    SQLALCHEMY_POOL_SIZE = 20
    SQLALCHEMY_POOL_RECYCLE = 3600

    WTF_CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    DEBUG = False
    TESTING = False

    # Disabling this one so that 404 from API requests for nonexistent resources don't come back with "did you mean x?"
    ERROR_404_HELP = False
    LOGIN_IGNORE_PASSWORD = False
    LOGGER_HANDLER_POLICY = "never"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOGGING_LEVEL = logging.DEBUG
    ENVIRONMENT_VARIABLES = {"NLS_LANG": "ENGLISH_SWITZERLAND.WE8ISO8859P1"}
    CONFIG_OVERRIDE_FILENAME = "config.py"
    PRODUCTION_HOSTNAMES = ("vocms0190.cern.ch",)
    EMAIL_FROM = "icms-support@cern.ch"
    EMAIL_REPLY_TO = "icms-support@cern.ch"
    EMAIL_BCC = "icms-support@cern.ch"
    INPUT_FORMAT_PERSON = "{last_name}, {first_name} (CMSid {cms_id})"
    OLD_ICMS_HOST = "icmscc7"
    LDAP_URI = "ldaps://xldap.cern.ch"
    # default access rules: everyone can read, only admins can write
    DEFAULT_PERMISSION_RULES = {
        "read": {"u.is_cms": True},
        "write": {"u.is_admin": True},
        "delete": {"u.is_admin": True},
    }
    ICMS_PIGGYBACK_URL = "http://cmsicms:9898"
    AL_FILES_DIR = "/tmp/al-files"
    AL_REF_DIR = "/tmp/al-refs"
    AL_DIFFS_DIR = "/tmp/al-diffs"
    # can be used in tests for dumping response contents
    TEST_RESPONSE_DUMP_DIR = None
    AUTH_HEADER_NAME = "User"
    COMPRESS_MIMETYPES = [
        "text/html",
        "text/css",
        "text/xml",
        "application/json",
        "application/javascript",
        "application/prs.hal-forms+json",
    ]
    AUTHORIZATIONS_CACHE_TTL = -1
    IDENTITY_PROVIDER_TTL = 120
    IDENTITY_PROVIDER_ENFORCE_HTTPS = False
    IDENTITY_PROVIDER_TOKEN_DELIVERY_ENDPOINT = "/iCMS/analysisadmin/loginsso"
    DB_CHANGELOG_ENABLED = False
    STATS_AGE_BUCKET_WIDTH = 7
    STATS_AGE_BUCKETING_CAP = 99
    STATS_PRIVACY_THRESHOLD = 0
    # This was used to forward unmatched routes to old Vue app.
    WILDCARD_ROUTE_TEMPLATE = "_base_of_base.html"
    # The TL4\MO1 model proposed for SP2020 might be here to stay, might go or might only be needed every so often.
    ELECTIONS_WITH_PROPORTIONAL_MODEL = ["SP2020"]
    # Lists deprecated paths with their replacements (False: no replacement at all, None: use swaptokens on URL)
    DEPRECATED_ROUTES = {
        "/cadi/updatedCadiLines": None,
        "/cadi/approvedCadiLines": None,
        "/api/cadi/cadiAnalyses": "/restplus/relay/piggyback/cadi/analyses",
        "/api/cadi/viewCadiLines": "/restplus/relay/piggyback/cadi/analyses",
        "/api/approvedCadiLines": "/restplus/relay/piggyback/cadi/history/stateChanges?targetStates=PHYS-APP",
        "/api/updatedCadiLines": "/restplus/relay/piggyback/cadi/history/updates",
    }
    # For cases where the replacement is provided by another app (e.g. SPA) a few simple text replacements should suffice to translate one URL into another
    DEPRECATION_FALLBACK_SWAPTOKENS = {"/tools-api": "/tools"}
    LDAP_MOCKING_ENABLED = False
    LDAP_MOCK_ENTRIES = []
    CMSRESULTS_CDSPAPERDATA_JSON = "http://cms-results.web.cern.ch/cms-results/public-results/publications-vs-time/data/papers.json"


class ProfileProd(Profile):
    """
    instance/config.py can be used to add/override parameters defined here
    """

    CORS_ALLOWED_HEADERS = f"{Profile.CORS_ALLOWED_HEADERS}, X-Auth-Username, User, X-Sub, Access-Control-Allow-Origin"
    ENV = const.ENV_NAME_PROD
    OLD_ICMS_HOST = "cmsicms"
    AL_FILES_DIR = (
        "/eos/project-c/cmsweb/www/eoswebtest/secr/authorlistParser/authorListCMS-new"
    )
    AL_REF_DIR = "/afs/cern.ch/cms/cmssecr/www/Documents/authorListCMS"
    AL_DIFFS_DIR = "/afs/cern.ch/cms/cmssecr/www/Documents/authorListCMS_diff"
    IDENTITY_PROVIDER_ENFORCE_HTTPS = True
    LDAP_MOCKING_ENABLED = False
    LOGGING_LEVEL = logging.INFO

class ProfilePreProd(ProfileProd):
    """
    Prod-alike profile for test deployments
    """

    ENV = const.ENV_NAME_PROD
    OLD_ICMS_HOST = "icmscc7"
    ICMS_PIGGYBACK_URL = "http://icmscc7.cern.ch:9898"
    AL_FILES_DIR = ProfileProd.AL_FILES_DIR + "Dev"
    AL_DIFFS_DIR = ProfileProd.AL_DIFFS_DIR + "Dev"
    LDAP_MOCKING_ENABLED = False


class ProfileTest(Profile):
    # Uses a different filename for config overrides
    CONFIG_OVERRIDE_FILENAME = "config_test.py"

    TESTING = True
    DEBUG = False

    ENV = const.ENV_NAME_TEST
    LOGGING_LEVEL = logging.INFO
    SECRET_KEY = "NOBODY_KNOWS"
    WTF_CSRF_ENABLED = False
    LOGIN_IGNORE_PASSWORD = True
    HOSTNAME = "localhost"
    PORT = 6543
    AUTHORIZATIONS_CACHE_TTL = -1
    AL_FILES_DIR = "/tmp/test-al-files"
    AL_REF_DIR = "/tmp/test-al-refs"
    AL_DIFFS_DIR = "/tmp/test-al-diffs"
    INDICO_TOKEN = "FOO_TOOKEN"
    INDICO_SECRET = "BAR_SECRET"
    LDAP_MOCKING_ENABLED = True

    @classmethod
    def _get_dbuser(cls, connection_string):
        return re.search(r"://([_\w]+)[@:]", connection_string).group(1)

    @classmethod
    def _get_dbname(cls, connection_string):
        return re.search(r"/([_\w]+)(?:\?[\w]*)?$", connection_string).group(1)

    @classmethod
    def epr_db_name(cls):
        return cls._get_dbname(cls.SQLALCHEMY_BINDS[const.OPT_NAME_BIND_EPR])

    @classmethod
    def epr_db_user(cls):
        return cls._get_dbuser(cls.SQLALCHEMY_BINDS[const.OPT_NAME_BIND_EPR])

    @classmethod
    def toolkit_db_name(cls):
        return cls._get_dbname(cls.SQLALCHEMY_BINDS[const.OPT_NAME_BIND_TOOLKIT])

    @classmethod
    def toolkit_db_user(cls):
        return cls._get_dbuser(cls.SQLALCHEMY_BINDS[const.OPT_NAME_BIND_TOOLKIT])

    @classmethod
    def legacy_db_user(cls):
        return cls._get_dbuser(cls.SQLALCHEMY_BINDS[const.OPT_NAME_BIND_LEGACY])


class ProfileDev(Profile):
    """
    instance/config.py can be used to add/override parameters defined here
    """

    # CORS_ALLOWED_ORIGINS_REGEX = r'^http(s?)://localhost((:\d+)?)((/.*)?)$'
    CORS_ALLOWED_ORIGINS_REGEX = r"^http://localhost(:\d+)?(/.*)?$"
    CORS_ALLOWED_HEADERS = f"{Profile.CORS_ALLOWED_HEADERS}, X-Auth-Username, User, X-Sub, Access-Control-Allow-Origin"
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    ENV = const.ENV_NAME_DEVEL
    LOGGING_LEVEL = logging.WARNING
    REMEMBER_COOKIE_NAME = "tkt_remember_token"
    ICMS_PIGGYBACK_URL = "http://localhost:7070"
    LDAP_URI = "ldaps://localhost:9999"
    # In a dev environment, the SPA endpoint of identical path can be reached like so:
    DEPRECATION_FALLBACK_SWAPTOKENS = {"localhost:5000": "localhost:8080"}
    # Setting
    LDAP_MOCKING_ENABLED = True
    LDAP_MOCK_ENTRIES = ProfileTest.LDAP_MOCK_ENTRIES
    # Extra entries that should be recognized by our LDAP's mock instance
    LDAP_MOCK_ENTRIES = [
        {
            "cmsEgroups": ['cms-award-admins'],
            "accountStatus": "Active",
            "cernEgroups": "foo",
            "hrId": 901246,
            "familyName": "External",
            "firstName": "Pierre",
            "login": "pexter",
            "mail": "pexter@cern.ch",
            "fullName": "Pierre External",
            "institute": 'CERN',
            "department": "IT",
            "gid": 42,
            "office": "500",
        }
    ]


def get_toolkit_schema_name():
    """
    A convenience method to get the toolkit (icms-addons) schema name from the config of a stored app's instance
    """
    return trivial.extract_mysql_schema_name(
        current_app.config[const.OPT_NAME_BINDS][const.OPT_NAME_BIND_TOOLKIT]
    )


def get_old_icms_host():
    return current_app.config.get("OLD_ICMS_HOST", "icmsbox")
