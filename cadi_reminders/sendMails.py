

'''Common code for sending emails from/to CERN via its SMTP servers for all new iCMS services.
'''

__author__ = 'Miguel Ojeda'
__copyright__ = 'Copyright 2017, CERN CMS'
__credits__ = ['Miguel Ojeda', 'Andreas Pfeiffer', 'Tomasz Adrian Bawej']
__license__ = 'Unknown'
__maintainer__ = 'Andreas Pfeiffer'
__email__ = 'andreas.pfeiffer@cern.ch'

import logging
import smtplib
import json

from email.mime.text import MIMEText
import email.header

from threading import Thread

class Message:
    """
    A helper class to collect all the info for an eMail message
    """
    def __init__(self):
        # set some defaults:
        self.fromAddress  = None
        self.toAddresses  = None
        self.ccAddresses  = None
        self.bccAddresses = None
        self.subject = ''
        self.body = ''

    def isValid(self):
        # define valid message to have at least a "to", "from", and "body" ... also a "subject" ?
        return (self.toAddresses is not None and
                self.fromAddress is not None and
                self.body is not None and self.body.strip() is not '' )

    def __repr__(self):
        # to print the content of the message
        # return self.toXML()
        return json.dumps( json.loads(self.toJson()), indent=4 ).replace('\\n', '\n')

    def toXML(self):

        retVal = '<message>'
        content = json.loads( self.toJson() )
        for item in ['from', 'to', 'cc', 'bcc', 'subject', 'body']:
            if item in content and content[item]:
                retVal += '\n   <%s> %s </%s>' % (item, content[ item ], item)
            else:
                if item in ['subject', 'body']:
                    retVal += '\n   <%s> [no %s] </%s>' % (item, item, item)
        retVal += "\n</message>"
        return retVal

    def toJson(self):
        # provide info in json format
        content = { 'from' : self.fromAddress,
                    'to' : self.toAddresses,
                    'cc' : self.ccAddresses,
                    'bcc' : self.bccAddresses,
                    'subject'  : self.subject,
                    'body' : self.body
                    }
        return json.dumps(content)

    def fromJson(self, jsonString):
        # re-set self from json string

        content = json.loads(jsonString)
        self.fromAddress = content['from']
        self.toAddresses = content['to']
        self.ccAddresses = content['cc']
        self.bccAddresses = content['bcc']
        self.subject = content['subject']
        self.body = content['body']


def test():
    msg = Message()
    msg.fromAddress = 'andreas.pfeiffer@cern.ch'
    msg.toAddresses = ['andreas.pfeiffer@cern.ch', 'apfeiffer1@gmail.com']
    print( msg )

    msg.ccAddresses = ['foo@bar.com']
    msg.bccAddresses = ['bar@foo.com']
    msg.subject = 'json-test - what else '
    msg.body = 'json test body .... '

    j0 = msg.toJson()

    msg1=Message()
    msg1.fromJson( j0 )

    assert (msg.fromAddress == msg1.fromAddress)
    assert (msg.toAddresses == msg1.toAddresses)
    assert (msg.ccAddresses== msg1.ccAddresses)
    assert (msg.bccAddresses== msg1.bccAddresses)
    assert (msg.subject == msg1.subject)
    assert (msg.body == msg1.body)

    print( msg )
    print( msg1 )


if __name__ == '__main__':
    test()
