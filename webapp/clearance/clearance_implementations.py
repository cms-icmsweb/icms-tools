from flask_login import current_user
from util import cache
from .clearance_core import SingleFlagClearanceBase
from icmsutils.businesslogic.flags import Flag


class IcmsRoot(SingleFlagClearanceBase):
    @classmethod
    def _required_flag(cls):
        return Flag.ICMS_ROOT


class SystemAdmin(IcmsRoot):
    @classmethod
    def _required_flag(cls):
        return Flag.ICMS_ADMIN


class ManagementBoardMember(SystemAdmin):
    @classmethod
    def _required_flag(cls):
        return Flag.MANAGEMENT_BOARD


class TeamLeaderOrDeputy(SystemAdmin):
    code_param_name = 'inst_code'

    @classmethod
    def impl_can_read(cls, *args, **kwargs):
        return True

    @classmethod
    def impl_can_edit(cls, *args, **kwargs):
        cms_id = current_user.get_id()
        inst_code = kwargs.get(cls.code_param_name, None)
        return cms_id in cache.get_leader_and_deputies_ids_by_inst_code().get(inst_code, []) \
               or cms_id in cache.get_flags_by_cms_id().get(Flag.ICMS_ADMIN, [])


class AuthorListGenerator(SystemAdmin):

    @classmethod
    def impl_can_edit(cls, *args, **kwargs):
        cms_id = current_user.get_id()
        return ( cms_id == 750 ) # George Alverson for now ...


class DiversityOffice(SystemAdmin):

    @classmethod
    def impl_can_edit(cls, *args, **kwargs):
        cms_id = current_user.get_id()
        return ( cms_id == 5870 or cms_id == 5324 ) #  Meenakshi Narain (5870) and Jo Cole (5324)
