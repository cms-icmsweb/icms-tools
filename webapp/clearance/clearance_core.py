from util import cache
from icmsutils.businesslogic.flags import Flag
from flask_login import current_user


class ClearanceBase(object):

    @classmethod
    def can_read(cls, *args, **kwargs):
        for base_cls in cls.__bases__:
            if issubclass(base_cls, ClearanceBase):
                if base_cls.impl_can_read(*args, **kwargs):
                    return True
        return cls.impl_can_read(*args, **kwargs)

    @classmethod
    def can_edit(cls, *args, **kwargs):
        for base_cls in cls.__bases__:
            if issubclass(base_cls, ClearanceBase):
                if base_cls.impl_can_edit(*args, **kwargs):
                    return True
        return cls.impl_can_edit(*args, **kwargs)

    @classmethod
    def impl_can_read(cls, *args, **kwargs):
        return False

    @classmethod
    def impl_can_edit(cls, *args, **kwargs):
        return False


    @classmethod
    def get_instance(cls, params=None):
        instance = cls()
        if params:
            for key, value in params.items():
                setattr(instance, key, value)
        return instance


class SingleFlagClearanceBase(ClearanceBase):
    @classmethod
    def _required_flag(cls):
        """
        Just override this method to get a clearance class depending on any flag necessary
        """
        return Flag.ICMS_ROOT

    @classmethod
    def __flag_checker(cls):
        return cls._required_flag() in cache.get_flags_by_cms_id().get(current_user.get_id(), [])

    @classmethod
    def impl_can_edit(cls, *args, **kwargs):
        return cls.__flag_checker()

    @classmethod
    def impl_can_read(cls, *args, **kwargs):
        return cls.__flag_checker()