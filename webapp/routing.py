from flask import url_for


class VueRoutes(object):

    @staticmethod
    def __url_for_vue_path(vue_path):
        """
        Matches the name of route defined in webapp.py
        TODO: remove once the transition to Vue is complete
        """
        return url_for('route_vue', vue_path=vue_path)

    @staticmethod
    def overdue_graduations():
        return VueRoutes.__url_for_vue_path(vue_path='/inst/overdueGraduations')

    @staticmethod
    def open_author_lists():
        return VueRoutes.__url_for_vue_path(vue_path='al/openLists')

    @staticmethod
    def al_mentions(cms_id=None):
        return VueRoutes.__url_for_vue_path(vue_path='users/signedPapers/{0}'.format(cms_id is not None and cms_id or ''))

    @staticmethod
    def phd_mo_list(mode, year=None):
        vue_path = '/gm/mo/{0}/'.format(mode)
        if year is not None:
            vue_path += str(year)
        return VueRoutes.__url_for_vue_path(vue_path=vue_path)

    @staticmethod
    def inst_members(inst_code):
        return VueRoutes.__url_for_vue_path(vue_path='/inst/{0}'.format(inst_code))

    @staticmethod
    def epr_due_corrector():
        return VueRoutes.__url_for_vue_path(vue_path='/epr/correctDue')

    @staticmethod
    def epr_due_corrections():
        return VueRoutes.__url_for_vue_path(vue_path='/epr/dueCorrections')

    @staticmethod
    def cms_people():
        return VueRoutes.__url_for_vue_path(vue_path='/people')

    @staticmethod
    def cms_tenures():
        return VueRoutes.__url_for_vue_path(vue_path='/cms/tenures')

    @staticmethod
    def cms_comms_overview():
        return VueRoutes.__url_for_vue_path(vue_path='/cms/comms')

    @staticmethod
    def markdown_editor():
        return VueRoutes.__url_for_vue_path(vue_path='/admin/markdown')

    @staticmethod
    def user_profile(cms_id=None):
        return VueRoutes.__url_for_vue_path(vue_path='/users/profile{0}'.format('/{0}'.format(cms_id) if cms_id else ''))

    @staticmethod
    def suspensions():
        return VueRoutes.__url_for_vue_path(vue_path='/inst/suspensions')

    @staticmethod
    def inst_profile(inst_code):
        return VueRoutes.__url_for_vue_path(vue_path='/inst{0}'.format('/{0}'.format(inst_code) if inst_code else ''))

    @staticmethod
    def org_units():
        return VueRoutes.__url_for_vue_path(vue_path='/cms/units')

    @staticmethod
    def cms_institutes():
        return VueRoutes.__url_for_vue_path(vue_path='/cms/institutes')

    @staticmethod
    def pending_notes():
        return VueRoutes.__url_for_vue_path(vue_path='/notes/pending')

    @staticmethod
    def mo_admin():
        return VueRoutes.__url_for_vue_path(vue_path='/secr/mo')

    @staticmethod
    def voting_list(code=None):
        vue_path = ''
        if code:
            vue_path = '/voting/votingList/{0}'.format(code)
        else:
            vue_path = '/voting/list'
        return VueRoutes.__url_for_vue_path(vue_path)

    @staticmethod
    def cms_stats():
        return VueRoutes.__url_for_vue_path('/cms/stats')

    @staticmethod
    def author_applications():
        return VueRoutes.__url_for_vue_path('/applicants')

    @staticmethod
    def notes():
        return VueRoutes.__url_for_vue_path('/notes')

    @staticmethod
    def list_emails():
        return VueRoutes.__url_for_vue_path(vue_path='/admin/emails')

    @staticmethod
    def send_email():
        return VueRoutes.__url_for_vue_path(vue_path='/admin/send_email')

    @staticmethod
    def list_announcements():
        return VueRoutes.__url_for_vue_path(vue_path='/admin/announcements')

    @staticmethod
    def member_authors():
        return VueRoutes.__url_for_vue_path(vue_path='/al/memberAuthors')

    @staticmethod
    def pending_authors():
        return VueRoutes.__url_for_vue_path(vue_path='/al/pendingAuthors')

    @staticmethod
    def flags():
        return VueRoutes.__url_for_vue_path(vue_path='/flags')

    @staticmethod
    def permissions_management():
        return VueRoutes.__url_for_vue_path(vue_path='/admin/permissions')

    @staticmethod
    def cms_emeriti():
        return VueRoutes.__url_for_vue_path(vue_path='/cms/emeriti')

    @staticmethod
    def gm_cms_week_bookings(week_id: int):
        return VueRoutes.__url_for_vue_path(vue_path='/gm/cms_week_bookings{0}'.format(f'/{week_id}' if week_id else ''))

    @staticmethod
    def gm_cms_week_rooms():
        return VueRoutes.__url_for_vue_path(vue_path='/gm/cms_week_rooms')

    @staticmethod
    def gm_cms_weeks():
        return VueRoutes.__url_for_vue_path(vue_path='/gm/cms_weeks')

    @staticmethod
    def arc_members():
        return VueRoutes.__url_for_vue_path(vue_path='/cadi/arcs/members')

    @staticmethod
    def cadi_lines():
        return VueRoutes.__url_for_vue_path(vue_path='/cadi/lines')

    @staticmethod
    def awgs():
        return VueRoutes.__url_for_vue_path(vue_path='/cadi/awgs')
