import socket

from icmsutils.businesslogic.mo_new import MoModel

from blueprints.cadi_viewer import views as cadi_views
from blueprints.users import views as user_views
from blueprints.admin import admin_views
from util.structs import Endpoint
from util.constants import BP_NAME_ADMIN, BP_NAME_CADI_VIEWER
from util.constants import BP_NAME_USERS
from util.constants import OPT_NAME_PRODUCTION_HOSTNAMES
from flask_login import current_user
from flask import current_app as app

from webapp.routing import VueRoutes


class MenuElement(object):

    def __init__(self, label, endpoint=None, guard_fn=None):
        self.children = []
        self._endpoint = endpoint
        self._label = label
        self.__guard_fn = guard_fn if callable(guard_fn) else lambda: True
        if not callable(self.__guard_fn):
            raise ValueError('Guard function should be a callable.')

    @classmethod
    def simple(cls, label, blueprint_name, view_fn, guard_fn=None):
        return cls(label=label, endpoint=Endpoint(blueprint=blueprint_name, view_method=view_fn).url(), guard_fn=guard_fn)

    def add(self, child):
        self.children.append(child)

    def to_be_shown(self):
        return self.__guard_fn()

    def label(self):
        return callable(self._label) and self._label() or self._label

    def endpoint(self):
        return callable(self._endpoint) and self._endpoint() or self._endpoint

    def __call__(self):
        return self.__guard_fn()

    @staticmethod
    def guard_is_admin():
        return current_user.is_authenticated and current_user.is_admin()

    @staticmethod
    def guard_is_authenticated():
        return current_user.is_authenticated

    @staticmethod
    def guard_is_identified():
        return not current_user.is_anonymous

    @staticmethod
    def guard_is_welcome_guest():
        return not (current_user.is_authenticated or current_user.is_anonymous)

    @staticmethod
    def guard_is_not_prod():
        return socket.gethostname() not in app.config[OPT_NAME_PRODUCTION_HOSTNAMES]

    @staticmethod
    def guard_is_cbi():
        return current_user.is_authenticated and (current_user.is_cbi() or current_user.is_admin())

    @staticmethod
    def guard_is_secr():
        return current_user.is_authenticated and current_user.is_secretariat_member()

    @staticmethod
    def guard_is_engagement_office():
        return current_user.is_engagement_office_member() or current_user.is_admin()

    @staticmethod
    def guard_is_mgt_board():
        return current_user.is_mgt_board_member()

    @staticmethod
    def guard_has_clearance(clearance_method):
        return current_user.is_authenticated and clearance_method()

    @staticmethod
    def guard_is_management_level():
        return any(['COM_' in _f.id for _f in current_user.get_flags()])

    @staticmethod
    def guard_block():
        return False


class TopMenuFactory(object):
    @staticmethod
    def produce_top_menus():
        """
        The code of this method used to float on the module level and execute only once, upon import.
        Hence all the guard_fns and such - these could now be safely replaces with something less complex as the menus will be assembled upon each page reload
            which in turn will not be that much of a problem once the switch to SPA is complete.
        """
        # MENU USER
        menu_user = MenuElement(label=lambda: 'User %s' % (
            not current_user.is_anonymous and current_user.person.niceLogin or '',))
        menu_user.add(MenuElement.simple('Login', BP_NAME_USERS, user_views.route_sign_in,
                                         guard_fn=lambda: not MenuElement.guard_is_identified()))
        menu_user.add(MenuElement.simple('Logout', BP_NAME_USERS,
                                         user_views.route_sign_out, guard_fn=MenuElement.guard_is_identified))
        menu_user.add(MenuElement('Profile', endpoint=VueRoutes.user_profile(
        ), guard_fn=MenuElement.guard_is_authenticated))
        menu_user.add(MenuElement(label='AL mentions', endpoint=VueRoutes.al_mentions(
        ), guard_fn=MenuElement.guard_is_authenticated))

        # MENU COLLABORATION
        menu_collaboration = MenuElement(
            label='Collaboration', guard_fn=MenuElement.guard_is_authenticated)
        menu_collaboration.add(MenuElement('Units', endpoint=VueRoutes.org_units(
        ), guard_fn=MenuElement.guard_is_authenticated))
        menu_collaboration.add(MenuElement(label='Institutes', endpoint=VueRoutes.cms_institutes(
        ), guard_fn=MenuElement.guard_is_authenticated))
        menu_collaboration.add(MenuElement(label='People', endpoint=VueRoutes.cms_people(
        ), guard_fn=MenuElement.guard_is_authenticated))

        sub_menu_cms_weeks = MenuElement(label='CMS Weeks')
        sub_menu_cms_weeks.add(MenuElement(
            'Room Booking', VueRoutes.gm_cms_week_bookings(None)))
        sub_menu_cms_weeks.add(MenuElement(
            'CMS Weks Management', VueRoutes.gm_cms_weeks(), MenuElement.guard_is_admin))
        sub_menu_cms_weeks.add(MenuElement(
            'Rooms', VueRoutes.gm_cms_week_rooms(), MenuElement.guard_is_admin))
        menu_collaboration.add(sub_menu_cms_weeks)

        sub_menu_mo = MenuElement(
            label='MO Lists', guard_fn=MenuElement.guard_is_authenticated)
        for args in [
            ('Official PHD MO Lists', VueRoutes.phd_mo_list(
                mode=MoModel.Mode.OFFICIAL), MenuElement.guard_is_authenticated),
            ('Pre-approval MO view', VueRoutes.phd_mo_list(mode=MoModel.Mode.PRE_APPROVAL),
             MenuElement.guard_is_authenticated),
            ('Current MO overview', VueRoutes.phd_mo_list(
                mode=MoModel.Mode.POST_APPROVAL), MenuElement.guard_is_authenticated),
        ]:
            sub_menu_mo.add(MenuElement(*args))

        menu_collaboration.add(sub_menu_mo)

        menu_collaboration.add(MenuElement(
            'Authorship Applications', VueRoutes.author_applications(), MenuElement.guard_is_authenticated))
        menu_collaboration.add(MenuElement(
            'Emeritus Nominations', VueRoutes.cms_emeriti(), MenuElement.guard_is_authenticated))
        menu_collaboration.add(MenuElement(
            'CMS Statistics', VueRoutes.cms_stats(), MenuElement.guard_is_authenticated))

        menu_collaboration.add(MenuElement(
            'iCMS Flags', endpoint=VueRoutes.flags()))
        menu_collaboration.add(MenuElement(
            'Overdue Graduations', endpoint=VueRoutes.overdue_graduations()))
        menu_collaboration.add(MenuElement('Tenures', endpoint=VueRoutes.cms_tenures(
        ), guard_fn=MenuElement.guard_is_authenticated))
        menu_collaboration.add(MenuElement('Communications', endpoint=VueRoutes.cms_comms_overview(
        ), guard_fn=MenuElement.guard_is_authenticated))

        # MENU INSTITUTE
        menu_inst = MenuElement(
            label='Institute', guard_fn=MenuElement.guard_is_authenticated)

        if hasattr(current_user, 'inst_code'):
            menu_inst.add(MenuElement('Institute Profile', VueRoutes.inst_members(
                current_user.inst_code), MenuElement.guard_is_authenticated))

        menu_inst.add(MenuElement('Pending Authors',
                                  endpoint=VueRoutes.pending_authors()))
        menu_inst.add(MenuElement('Member Authors',
                                  endpoint=VueRoutes.member_authors()))

        menu_inst.add(MenuElement('EPR Suspensions',
                                  endpoint=VueRoutes.suspensions()))
        menu_inst.add(MenuElement('Overdue Graduations',
                                  endpoint=VueRoutes.overdue_graduations()))

        # MENU WORKS
        menu_works = MenuElement(
            label='Works', guard_fn=MenuElement.guard_is_authenticated)
        sub_menu_cadi = MenuElement(
            label='CADI Views', guard_fn=MenuElement.guard_is_authenticated)
        sub_menu_cadi.add(MenuElement(label='CADI Lines', endpoint=VueRoutes.cadi_lines(),
                                            guard_fn=MenuElement.guard_is_authenticated))
        sub_menu_cadi.add(MenuElement(label='ARC Members', endpoint=VueRoutes.arc_members(),
                                            guard_fn=MenuElement.guard_is_authenticated)),
        sub_menu_cadi.add(MenuElement(label='AWGs', endpoint=VueRoutes.awgs(),
                                      guard_fn=MenuElement.guard_is_authenticated))

        for args in [
            ('PubComm Dashboard', BP_NAME_CADI_VIEWER, cadi_views.route_dashboard),
            ('CADI Line Latex Build Checker',
             BP_NAME_CADI_VIEWER, cadi_views.checkLatexBuild),
        ]:
            sub_menu_cadi.add(MenuElement.simple(*args))

        sub_menu_author_lists = MenuElement(label='CMS Author Lists')
        sub_menu_author_lists.add(MenuElement(label='Show open Author Lists', endpoint=VueRoutes.open_author_lists(),
                                              guard_fn=MenuElement.guard_is_authenticated))

        sub_menu_notes = MenuElement(label='CMS Notes')
        sub_menu_notes.add(MenuElement('Notes', endpoint=VueRoutes.notes(
        ), guard_fn=MenuElement.guard_is_authenticated))
        sub_menu_notes.add(MenuElement('Workflows', endpoint=VueRoutes.pending_notes(
        ), guard_fn=MenuElement.guard_is_authenticated))

        menu_works.add(sub_menu_cadi)
        menu_works.add(sub_menu_author_lists)
        menu_works.add(sub_menu_notes)

        menu_works.add(MenuElement.simple('Check DateEndSign', BP_NAME_ADMIN,
                                          admin_views.route_check_date_end_sign, MenuElement.guard_is_management_level)),

        return [menu_collaboration, menu_inst, menu_works, menu_user]
