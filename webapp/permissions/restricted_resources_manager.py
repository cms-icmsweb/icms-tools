from typing import Iterable, List
from icms_orm.toolkit import RestrictedResourceTypeValues as ResourceType
from icms_orm.toolkit import RestrictedResource as RestrictedResourceMapper, Permission
from icmsutils.funcutils import Cache
from icmsutils.permissions.authorization_context import AuthorizationContext
from webapp.permissions.restricted_resource import ToolkitRestrictedResource
from icmsutils.permissions import AbstractRestrictedResourcesManager
from icmsutils.permissions import RuleSetChecker


class RestrictedResourcesManager(AbstractRestrictedResourcesManager):

    class Fetcher():
        def fetch(self):
            result = dict()
            rows = Permission.session().query(RestrictedResourceMapper).filter(
                RestrictedResourceMapper.type == ResourceType.ENDPOINT).all()
            row: RestrictedResourceMapper
            for row in rows:
                result[row.key] = result.get(row.key, list())
                result[row.key].append(
                    RestrictedResourceMapper.from_ia_dict(row.to_ia_dict()))

            return result

    cache = Cache(lambda: RestrictedResourcesManager.Fetcher().fetch(), -1)

    @classmethod
    def set_cache_ttl(cls, seconds_to_live: int):
        cls.cache.timeout = seconds_to_live

    @classmethod
    def get_resource_by_key_and_context(cls, key: str, context: AuthorizationContext) -> ToolkitRestrictedResource:
        matches = cls.refine_matches(
            cls.cache().get(key, list()), context)
        if len(matches) > 1:
            raise ValueError('Multiple matching resources found for key {key}')
        return ToolkitRestrictedResource(db_resource_id=(matches[0].id if matches else None))

    @classmethod
    def refine_matches(cls, matches: Iterable[RestrictedResourceMapper], context: AuthorizationContext) -> List[RestrictedResourceMapper]:
        """
        The filters describe spcific conditions, for eg:
        {'p.id': 19} would require the path variable id=19, so that a (url) rule like:
        /resource/<int:id> will match a request like /resource/19 but not a request for /resource/20

        Another possibility, especially when using broad access classes, like:
        [{'p.cmsId': 'u.subordinate_cms_ids'}, {'r.cmsId': 'u.subordinate_cms_ids'}]
        would be to use the filters in order to narrow the lookup paramstores pool down to a specific one.
        Otherwise, for a path like `/something/<int:cmsId>` the check could be beaten by providing another cmsId eg. in the json data.

        In such cases, a filter can help ensuring that a specific paramter store contains the desired value 
        (or the resource will not be matched, no access rules will be fetched and the defaults will be applied).
        Eg, a filter like:
        {'p.cmsId': 'u.subordinate_cms_ids'}
        will require that the path variable (aka view arg) cmsId is among the subordinates' CMS IDs.
        """
        m: RestrictedResourceMapper
        return [m for m in matches if RuleSetChecker(context).check_rule_set(m.filters)]
