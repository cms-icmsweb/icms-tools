from typing import List

from icmsutils.funcutils import Cache
from webapp.permissions.restricted_resource import AbstractRestrictedResource
from icms_orm.toolkit import RestrictedActionTypeValues, Permission, AccessClass
from flask import current_app
import logging
from icmsutils.permissions import AbstractAccessRulesProvider
from icmsutils.permissions import AccessRuleWrapper


class ToolkitAccessRulesProvider(AbstractAccessRulesProvider):

    class Fetcher():
        def fetch(self):
            result = dict()
            ssn = Permission.session()
            stuff = ssn.query(AccessClass.rules, Permission.resource_id, Permission.action).join(
                Permission, Permission.access_class_id == AccessClass.id).all()
            for (rules, rsc_id, action) in stuff:
                _key = (rsc_id, action)
                result[_key] = result.get(_key, list())
                result[_key].append(rules)
            return result

    @classmethod
    def set_cache_ttl(cls, seconds_to_live: int):
        cls.cache.timeout = seconds_to_live

    cache = Cache(lambda: ToolkitAccessRulesProvider.Fetcher().fetch(), -1)

    @classmethod
    def get_rules_for_resource_and_action(cls, resource: AbstractRestrictedResource, action: str) -> List[AccessRuleWrapper]:
        assert action in RestrictedActionTypeValues.values(
        ), 'Unknown action: {0}'.format(action)
        assert isinstance(resource, AbstractRestrictedResource)
        _rules = cls.cache().get((resource.id, action), list())
        if len(_rules) > 0:
            return [AccessRuleWrapper(_r) for _r in _rules]
        else:
            logging.debug(
                f'No permission rules found for {resource.id}, falling back to application defaults.')
            return [AccessRuleWrapper(current_app.config['DEFAULT_PERMISSION_RULES'][action])]
