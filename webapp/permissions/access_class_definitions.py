from typing import Dict
from icms_orm.toolkit import AccessClass
from enum import Enum

import logging


class Attribute():
    user_flags = 'u.flags'
    user_cms_id = 'u.cms_id'
    user_subordinates = 'u.team_subordinate_cms_ids'
    json_cms_id = 'r.cms_id'
    path_cms_id = 'p.cms_id'


class AccessClassDefinition(Enum):
    root = {'ICMS_root': Attribute.user_flags}
    # care needs to be taken not to accept simultaneously cms_id and cmsId.
    # TODO: perform case-convertion and subsequent checks within the permission checking pipeline
    # TODO: array of expressions does not evaluate correctly
    # a catch-all self-access version
    self = [{Attribute.user_cms_id: Attribute.json_cms_id},
            {Attribute.user_cms_id: Attribute.path_cms_id}]
    # more refined variants: depending on where to look for the variable
    self_json = [{Attribute.user_cms_id: Attribute.json_cms_id}]
    self_path = [{Attribute.user_cms_id: Attribute.path_cms_id}]
    secretariat = {'ICMS_rootsecr': Attribute.user_flags}
    engagement_office = {'ENGT_OFFICE': Attribute.user_flags}
    anyone = [{'u.is_cms': True}]
    cern_member = [{}]
    inst_supervisor = [{Attribute.json_cms_id: Attribute.user_subordinates}, {
        Attribute.path_cms_id: Attribute.user_subordinates}]
    booking_owner = [{'p.id': 'u.owned_booking_ids'}]

    @classmethod
    def insert_missing_classes(cls):
        """
        Checks the database for existing access classes and inserts missing ones (name-wise comparison).
        """
        session = AccessClass.session()
        existing: Dict[str:AccessClass]
        existing = {x.name: x for x in session.query(AccessClass).all()}
        for ac_def in cls:
            if ac_def not in existing:
                logging.info(
                    f'Registering a new access class «{ac_def.name}»: f{ac_def.value}')
                ac = AccessClass.from_ia_dict(
                    {AccessClass.name: ac_def.name, AccessClass.rules: ac_def.value})
                session.add(ac)
        session.commit()
