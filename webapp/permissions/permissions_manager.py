from typing import Type

from icmsutils.permissions.abstract_classes import AbstractAccessRulesProvider, AbstractRestrictedResourcesManager
from webapp.permissions.restricted_resources_manager import RestrictedResourcesManager
from webapp.permissions.access_rules import ToolkitAccessRulesProvider
from icmsutils.permissions import AbstractPermissionsManager


class PermissionsManager(AbstractPermissionsManager):

    @classmethod
    def set_caches_ttl(cls, ttl: int):
        cls.get_resources_manager_class().set_cache_ttl(ttl)
        cls.get_rules_provider_class().set_cache_ttl(ttl)

    @classmethod
    def get_rules_provider_class(cls) -> Type[ToolkitAccessRulesProvider]:
        # This could be set at runtime but it wouldn't be of much use
        return ToolkitAccessRulesProvider

    @classmethod
    def get_resources_manager_class(cls) -> Type[RestrictedResourcesManager]:
        return RestrictedResourcesManager
