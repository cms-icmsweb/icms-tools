import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import Weeks from '../../../src/views/Weeks.vue';

describe('Weeks.vue', () => {
    const response = [
        {
            id: 1,
            title: "Week 1",
            date: '2020-01-01',
            date_end: "2020-01-07",
            is_external: false,
        },
        {
            id: 2,
            title: "Week 2",
            date: '2020-01-01',
            date_end: "2020-01-07",
            is_external: false,
        }
    ];

    const { initialize } = mockApiCall(AjaxCalls.BookingWeeksCall, { response });
    beforeEach(() => {
        initialize();
    });


    it('Loads data from the api call',() => {
        const wrapper = mount(Weeks);

        expect(AjaxCalls.BookingWeeksCall).toHaveBeenCalled();
        expect(wrapper.vm.data).toBe(response);
    });

    it('Opens a dialog upon clicking the create button', async () => {
        const wrapper = mount(Weeks);
        
        expect(wrapper.vm.showFormDialog).toBe(false);
        
        await wrapper.findComponent({name: 'v-btn'}).trigger('click');
        expect(wrapper.vm.showFormDialog).toBe(true);
    });

    it('Adds the new week to the list when it is created', async () => {
        const wrapper = mount(Weeks);

        const newWeek = {
            id: 3,
            title: "Week 3",
            date: '2020-01-01',
            date_end: "2020-01-07",
            is_external: false,
        };
        
        await wrapper.findComponent({ name: 'WeeksFormDialog'}).vm.$emit('itemcreated', newWeek);
        const { data } = wrapper.vm;
        expect(data[data.length - 1]).toBe(newWeek);
    });

    it('Changes the week in the list when it is updated', async () => {
        const wrapper = mount(Weeks);

        const updatedWeek = {
            id: 1,
            title: "Week 1 Updated",
            date: '2020-01-01',
            date_end: "2020-01-07",
            is_external: false,
        };
        
        await wrapper.findComponent({ name: 'WeeksFormDialog'}).vm.$emit('itemupdated', updatedWeek);
        const { data } = wrapper.vm;
        expect(data[0]).toBe(updatedWeek);
    });
})