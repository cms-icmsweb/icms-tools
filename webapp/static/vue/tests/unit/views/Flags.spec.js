import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import Flags from '../../../src/views/Flags.vue';

describe('Flags.vue', () => {
    const response = [
        {
            cmsId: 1,
            firstName: 'Ralph',
            lastName: 'Moore',
            instCode: 'CERN',
            id: 'PUB_MoU98',
            desc: 'MoU98: Signature List for Memo of Understanding 1998'
        },
        {
            cmsId: 1,
            firstName: 'Ralph',
            lastName: 'Moore',
            instCode: 'CERN',
            id: 'NEWS_comp',
            desc: 'MoU98: Signature List for Memo of Understanding 1998'
        },
        {
            cmsId: 2,
            firstName: 'Brian',
            lastName: 'Sanford',
            instCode: 'UCLA',
            id: 'PUB_TDRhcal',
            desc: 'MoU98: Signature List for Memo of Understanding 1998'
        },
        {
            cmsId: 3,
            firstName: 'Greta',
            lastName: 'Kris',
            instCode: 'CERN',
            id: 'COM_cfc',
            desc: 'MoU98: Signature List for Memo of Understanding 1998'
        },
    ];
    const { initialize } = mockApiCall(AjaxCalls.FlagsCall, { response });
    beforeEach(() => {
        initialize();
    });

    it('Loads results from the api', async () => {
        const wrapper = shallowMount(Flags);
        expect(AjaxCalls.FlagsCall).toHaveBeenCalled();

        await wrapper.vm.$nextTick()
        expect(wrapper.vm.data).toBe(response);
    });

    it('Displays the data on the table', async () => {
        const wrapper = mount(Flags);
        
        await wrapper.vm.$nextTick();
        const table = wrapper.findComponent({name: 'VuetifyTable'});
        
        expect(table.props('items')).toEqual(response);
        expect(table.html()).toMatchSnapshot();
    });

    it('Displays the correct options for filtering by type', async () => {
        const wrapper = mount(Flags);
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.typeOptions).toEqual([
            { text: "COM Flags", value: "COM" }, 
            { text: "NEWS Flags", value: "NEWS" },
            { text: "PUB Flags", value: "PUB"}
        ]);
    });
    
    it('Displays the correct options for filtering by person', async () => {
        const wrapper = mount(Flags);
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.peopleOptions).toEqual([
            { text: "Kris Greta", value: 3 }, 
            { text: "Moore Ralph", value: 1 },
            { text: "Sanford Brian", value: 2}
        ]);
    });

    it('Filters by type', async () => {
        const wrapper = mount(Flags);
        wrapper.setData({type: 'PUB'});
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([
            { cmsId: 1, id: 'PUB_MoU98' },
            { cmsId: 2, id: 'PUB_TDRhcal' }
        ]);
    });

    it('Filters by person', async () => {
        const wrapper = mount(Flags);
        wrapper.setData({person: 1});
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([
            { cmsId: 1, id: 'PUB_MoU98' },
            { cmsId: 1, id: 'NEWS_comp' }
        ]);
    });
});