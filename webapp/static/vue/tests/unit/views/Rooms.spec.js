import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import Rooms from '../../../src/views/Rooms.vue';

describe('Weeks.vue', () => {
    const response = [
        {
            id: 1,
            indico_id: 1,
            building: 1,
            floor: 1,
            room_nr: 1,
            custom_name: "Room 1",
            at_cern: false,
        },
        {
            id: 2,
            indico_id: 2,
            building: 2,
            floor: 2,
            room_nr: 2,
            custom_name: "Room 2",
            at_cern: false,
        }
    ];

    const { initialize } = mockApiCall(AjaxCalls.BookingRoomsCall, { response });
    beforeEach(() => {
        initialize();
    });


    it('Loads data from the api call',() => {
        const wrapper = mount(Rooms);

        expect(AjaxCalls.BookingRoomsCall).toHaveBeenCalled();
        expect(wrapper.vm.data).toBe(response);
    });

    it('Opens a dialog upon clicking the create button', async () => {
        const wrapper = mount(Rooms);
        
        expect(wrapper.vm.showFormDialog).toBe(false);
        
        await wrapper.findComponent({name: 'v-btn'}).trigger('click');
        expect(wrapper.vm.showFormDialog).toBe(true);
    });

    it('Adds the new room to the list when it is created', async () => {
        const wrapper = mount(Rooms);

        const newWeek = {
            id: 3,
            indico_id: 3,
            building: 3,
            floor: 3,
            room_nr: 3,
            custom_name: "Room 3",
            at_cern: false,
        }
        
        await wrapper.findComponent({ name: 'RoomFormDialog'}).vm.$emit('itemcreated', newWeek);
        const { data } = wrapper.vm;
        expect(data[data.length - 1]).toBe(newWeek);
    });

    it('Changes the room in the list when it is updated', async () => {
        const wrapper = mount(Rooms);

        const updatedWeek = {
            id: 1,
            indico_id: 1,
            building: 1,
            floor: 1,
            room_nr: 1,
            custom_name: "Room 1 Updated",
            at_cern: false,
        };
        
        await wrapper.findComponent({ name: 'RoomFormDialog'}).vm.$emit('itemupdated', updatedWeek);
        const { data } = wrapper.vm;
        expect(data[0]).toBe(updatedWeek);
    });
})