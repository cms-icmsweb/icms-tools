import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import Announcements from '../../../src/views/Announcements.vue';
import VuetifyTable from '../../../src/components/VuetifyTable.vue';

describe('Announcements.vue',() => {
    const response = [
        {
            application: "common",
            content: 'Due to upgrades of routers in the CERN IT department, ...',
            end_datetime: "2018-03-22T08:00:00",
            id: 1,
            start_datetime: "2018-03-20T17:09:35",
            subject: "Service Disruption",
            type: "WARNING",
        },
        {
            application: "common",
            content: "To apply fixes for the recent L1TF vulnerability ...",
            end_datetime: "2018-09-18T08:00:00",
            id: 2,
            start_datetime: "2018-09-12T08:00:00",
            subject: "Service Disruption",
            type: "WARNING",
        },{
            application: "epr",
            content: "It is now possible to set up eGroups as ...",
            end_datetime: "2018-10-19T00:00:00",
            id: 3,
            start_datetime: "2018-10-17T09:00:00",
            subject: "Managers can now be eGroups.",
            type: "INFO",
        }
    ];
    const { initialize } = mockApiCall(AjaxCalls.AnnouncementsCall, { response });
    beforeEach(() => {
        initialize();
    });

    it('Loads data from the api call',() => {
        const wrapper = shallowMount(Announcements);

        expect(AjaxCalls.AnnouncementsCall).toHaveBeenCalled();
        expect(wrapper.vm.announcements).toBe(response);
    });

    it('Displays the data on the table', async () => {
        const wrapper = mount(Announcements);
        
        await wrapper.vm.$nextTick();
        const table = wrapper.findComponent(VuetifyTable);
        
        expect(table.props('items')).toEqual(response);
        expect(table.html()).toMatchSnapshot();
    });

    it('Opens a dialog upon clicking the create button', () => {
        const wrapper = mount(Announcements);
        
        expect(wrapper.vm.isDialogOpen).toBe(false);
        wrapper.findComponent({name: 'v-btn'}).trigger('click').then(() => {
            expect(wrapper.vm.isDialogOpen).toBe(true);
        })
    });

    it('Adds the new announcement to the list when it is created', async () => {
        const wrapper = mount(Announcements);

        const newAnnouncement = {
            id: 4,
            application: "common",
            content: 'This is new',
            end_datetime: "2018-03-22T08:00:00", 
            start_datetime: "2018-03-20T17:09:35",
            subject: "Service Disruption",
            type: "WARNING",
        };
        
        await wrapper.findComponent({ name: 'CreateAnnouncementDialog'}).vm.$emit('created', newAnnouncement);
        const { announcements } = wrapper.vm;
        expect(announcements[announcements.length - 1]).toBe(newAnnouncement);
    });
});