import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import MemberAuthors from '../../../src/views/MemberAuthors.vue';

describe('MemberAuthors.vue', () => {
    const response = [
        {
            cmsId: 1,
            firstName: 'Ralph',
            lastName: 'Moore',
            instCode: 'CERN',
            moCurrentYear: true,
            freeMoCurrentYear: true,
            name: 'Physist',
            status: 'CMS',
            isAuthor: false,
            flagId: null,
        },
        {
            cmsId: 2,
            firstName: 'Brian',
            lastName: 'Sanford',
            instCode: 'UCLA',
            moCurrentYear: 'NO',
            freeMoCurrentYear: true,
            name: 'Mountain Climber',
            status: 'CMS',
            isAuthor: true,
            flagId: 'MISC_authorno',
        },
        {
            cmsId: 3,
            firstName: 'Greta',
            lastName: 'Kris',
            instCode: 'CERN',
            moCurrentYear: 'NO',
            freeMoCurrentYear: 'NO',
            name: 'Juggler',
            status: 'CMS',
            isAuthor: false,
            flagId: null,
        },
    ];
    const { initialize } = mockApiCall(AjaxCalls.MemberAuthorsCall, { response });
    beforeEach(() => {
        initialize();
    });

    it('Loads results from the api call', async () => {
        const wrapper = shallowMount(MemberAuthors);
        expect(AjaxCalls.MemberAuthorsCall).toHaveBeenCalled();

        await wrapper.vm.$nextTick();
        expect(wrapper.vm.data).toBe(response);
    });

    it('transforms the data correctly', async () => {
        const wrapper = shallowMount(MemberAuthors);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([
            {
                cmsId: 1,
                moCurrentYear: 'YES',
                freeMoCurrentYear: 'YES',
                isAuthor: 'NO',
                flagId: '---',
            },
            {
                cmsId: 2,
                moCurrentYear: 'NO',
                freeMoCurrentYear: 'YES',
                isAuthor: 'YES',
                flagId: 'MISC_authorno',
            },
            {
                cmsId: 3,
                moCurrentYear: 'NO',
                freeMoCurrentYear: 'NO',
                isAuthor: 'NO',
                flagId: '---',
            }
        ]);
    });

    it('Displays the data on the table', async () => {
        const wrapper = mount(MemberAuthors);
        
        await wrapper.vm.$nextTick();
        const table = wrapper.findComponent({ name: "VuetifyTable"});
        
        expect(table.html()).toMatchSnapshot();
    });

    it('Filters by institute', async () => {
        const wrapper = mount(MemberAuthors);
        wrapper.setData({institute: 'CERN'});
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([{ cmsId: 1 },{ cmsId: 3 }]);
    });

    it('Opens an Informational Dialog when prompted', async () => {
        const wrapper = mount(MemberAuthors);
        
        expect(wrapper.vm.isInfoDialogOpen).toBe(false);

        await wrapper.find('.v-icon--link').trigger('click');
        expect(wrapper.vm.isInfoDialogOpen).toBe(true);
    });

    it('Summons a warning modal when an action is selected', async () => {
        const wrapper = mount(MemberAuthors);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.selectedPerson).toBe(null);
        expect(wrapper.vm.isConfirmationDialogOpen).toBe(false);
        
        // Click the FREEZE button
        await wrapper.findAll('button').at(2).trigger('click');
        expect(wrapper.vm.selectedPerson).toBe(response[0]);
        expect(wrapper.vm.isConfirmationDialogOpen).toBe(true);
    });

    it('Displays the new flag if the action is confirmed', async () => {
        const wrapper = mount(MemberAuthors);
        
        await wrapper.vm.$nextTick();
        //Select the first person (Unfrozzen)
        wrapper.setData({ selectedPerson: wrapper.vm.data[0] })
        
        //Perform action
        await wrapper.findComponent({name: 'MemberAuthorConfirmationDialog'}).vm.$emit('suspensionupdate', 'MISC_authorno');
        //Flag should change to MISC_authorno  
        expect(wrapper.vm.data[0].flagId).toEqual('MISC_authorno');
    });
});