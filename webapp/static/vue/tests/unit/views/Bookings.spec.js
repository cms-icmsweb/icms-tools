import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import Bookings from '../../../src/views/Bookings.vue';
import Scheduler from '../../../src/components/scheduler/Scheduler.vue';

describe('Booking', () => {
    const weeks = [
        {
            id: 1,
            title: 'CMS Week 1',
            date: '2020-02-01',
            date_end: '2020-02-07',
            is_external: false
        },
        {
            id: 2,
            title: 'CMS Week 2',
            date: '2020-01-01',
            date_end: '2020-01-07',
            is_external: true
        },
    ];
    const weeksCall = mockApiCall(AjaxCalls.BookingWeeksCall, { response : weeks });
    
    const rooms = [
        {
            id: 1,
            building : 1,
            room_nr: 1,
            floor: 1,
            custom_name: 'Room 1',
            at_cern: true
        },
        {
            id: 2,
            building : 2,
            room_nr: 2,
            floor: 2,
            custom_name: 'Room 2',
            at_cern: true
        },
        {
            id: 3,
            building : 3,
            room_nr: 3,
            floor: 3,
            custom_name: 'Room 2',
            at_cern: false
        },
    ];
    const roomsCall = mockApiCall(AjaxCalls.BookingRoomsCall, { 
        requestParams: {
            weekId: 1
        }, 
        response : rooms 
    });

    const slots = [
        {
            roomId: 1,
            startTime: '2020-01-01 00:00:00',
            endTime: '2020-01-01 01:00:00',
        }
    ]
    const slotsCall = mockApiCall(AjaxCalls.BookingSlotsCall, { 
        requestParams: {
            weekId: 1
        }, 
        response : slots 
    });
    const requests = [
        {
            id: 2,
            roomId: 1,
            from: '2020-01-01 00:00:00',
            duration: 60,
            title: 'Request 1',
            status: 'deleted'
        },
        {
            id: 3,
            roomId: 2,
            from: '2020-01-01 00:00:00',
            duration: 60,
            title: 'Request 2',
            status: 'pending'
        },
        {
            id: 4,
            roomId: 1,
            from: '2020-01-01 00:00:00',
            duration: 60,
            title: 'Request 3',
            status: 'done'
        }
    ]
    const requestCall = mockApiCall(AjaxCalls.BookingRequestsCall, { 
        requestParams: {
            weekId: 1
        }, 
        response : requests 
    });
    const listProjectsCall = mockApiCall(AjaxCalls.BookingProjectsCall, { 
        requestParams: {
            weekId: 1
        }, 
        response : [] 
    });
    beforeEach(() => {
        weeksCall.initialize();
        roomsCall.initialize();
        slotsCall.initialize();
        requestCall.initialize();
        listProjectsCall.initialize();
    });

    it('Auto-selects the first week', async () => {
        const wrapper = mount(Bookings);
        expect(wrapper.vm.selectedWeekId).toBe(null);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.selectedWeekId).toBe(1);
    });
    it('Only shows the scheduler if a week is selected', async () => {
        const wrapper = mount(Bookings);
        expect(wrapper.findComponent(Scheduler).exists()).toBe(false);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.findComponent(Scheduler).exists()).toBe(true);
    });
    it('Filters out deleted booking requests', async () => {
        const wrapper = mount(Bookings);
        await wrapper.vm.$nextTick();

        expect(wrapper.findComponent(Scheduler).vm.bookings).toMatchObject([
            { title: "Reserved by CMS secretariat" },
            { title:  "Request 2" },
            { title:  "Request 3" },
        ]);
    });
    it('Only makes pending requests clickable', async () => {
        const wrapper = mount(Bookings);
        await wrapper.vm.$nextTick();

        expect(wrapper.findComponent(Scheduler).vm.bookings).toMatchObject([
            { clickable: false },
            { clickable: true },
            { clickable: true },
        ]);
    });
    it('Only shows relevant rooms', async () => {
        const wrapper = mount(Bookings);
        await wrapper.vm.$nextTick();

        expect(wrapper.findComponent(Scheduler).vm.rooms).toMatchObject([
            { value: 1 },
            { value: 2 },
        ]);
    });
    it('Displays every one of the cms weeks days', async () => {
        const wrapper = mount(Bookings);
        await wrapper.vm.$nextTick();

        expect(wrapper.findComponent(Scheduler).vm.days).toMatchObject([
            '2020-02-01',
            '2020-02-02',
            '2020-02-03',
            '2020-02-04',
            '2020-02-05',
            '2020-02-06',
            '2020-02-07',
        ]);
    });
});

