import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import RoomFormDialog from '../../../src/components/RoomFormDialog.vue'

describe('RoomFormDialog.vue', () => {
    it('Shows create room form if no id is provided', async () => {
        const wrapper = mount(RoomFormDialog, {
            propsData: {
                value: true,
                item: null
            }
        });
        await wrapper.vm.$nextTick();
        
        expect(wrapper.find('.v-card__title').text()).toEqual('Create Room');
    });

    it('Shows edit room form if no id is provided', async () => {
        const wrapper = mount(RoomFormDialog, {
            propsData: {
                value: true,
                item: { id : 1 }
            }
        });
        await wrapper.vm.$nextTick();
        
        expect(wrapper.find('.v-card__title').text()).toEqual('Edit Room');
    });

    it('Closes when the close button is clicked', async () => {
        const wrapper = mount(RoomFormDialog, {
            propsData: {
                value: true,
                item: null
            }
        });
        await wrapper.vm.$nextTick();

        //Click close
        await wrapper.findAllComponents({name: 'v-btn'}).at(0).trigger('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('Makes an api call to create a room on submit', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingRoomsCall, {
            requestParams: {
                indico_id: 1,
                building: 1,
                floor: 1,
                room_nr: 1,
                custom_name: null,
                at_cern: false
            }
        });
        createBookingCall.initialize();

        const wrapper = mount(RoomFormDialog,{
            propsData: {
                value: true,
                item: null
            }
        });  
        await wrapper.vm.$nextTick();

        wrapper.setData({
            formState: {
                indico_id: 1,
                building: 1,
                floor: 1,
                room_nr: 1,
                custom_name: null,
                at_cern: false
            }
        });
        await wrapper.vm.$nextTick();

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        
        expect(wrapper.emitted().itemcreated[0]).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('Makes an api call to edit a room on submit', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingRoomsCall, {
            requestParams: {
                id: 1,
                indico_id: 1,
                building: 1,
                floor: 1,
                room_nr: 1,
                custom_name: null,
                at_cern: false
            }
        });
        createBookingCall.initialize();

        const wrapper = mount(RoomFormDialog,{
            propsData: {
                value: true,
                item: { id: 1 }
            }
        });  
        await wrapper.vm.$nextTick();

        wrapper.setData({
            formState: {
                indico_id: 1,
                building: 1,
                floor: 1,
                room_nr: 1,
                custom_name: null,
                at_cern: false
            }
        });
        await wrapper.vm.$nextTick();

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        
        expect(wrapper.emitted().itemupdated[0]).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });
});