import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import SuspensionConfirmationDialog from '../../../src/components/SuspensionConfirmationDialog.vue'


describe('SuspensionConfirmationDialog.vue',() => {
    const person = {
        cmsId: 1,
        firstName: 'Ralph',
        lastName: 'Moore',
        instCode: 'CERN',
        name: 'Physist',
        isAuthor: false,
        isAuthorSuspended: true,
    };

    const { initialize, assertionMap } = mockApiCall(AjaxCalls.SuspensionsCall, { 
        requestParams : {
            cmsId: person.cmsId, 
            isAuthorSuspended : !person.isAuthorSuspended
        }
    });
    beforeEach(() => {
        initialize();
    });

    it('Displays a warning message', () => {
        const wrapper = mount(SuspensionConfirmationDialog,{
            propsData: {
                value: true,
                person
            }
        });
        
        // You are about to unsuspend the activity of 
        expect(wrapper.find('.v-card__text').element).toMatchSnapshot();
    })

    const testButtonPress = async button => {
        const wrapper = shallowMount(SuspensionConfirmationDialog,{
            propsData: {
                value: true,
                person
            }
        });
        
        // Click Button (Close : 0 , Continue: 1)
        await wrapper
            .findAllComponents({name: 'v-btn'})
            .at(button == 'close' ? 0 : 1)
            .vm.$emit('click');

        expect(wrapper.emitted().input[0]).toEqual([false]);
    }
    it('Closes the dialog on Close', () => testButtonPress('close'))
    it('Closes the dialog on Continue', () => testButtonPress('continue'))

    it('Makes the api call to change status on Continue', async () => {
        const wrapper = shallowMount(SuspensionConfirmationDialog,{
            propsData: {
                value: true,
                person
            }
        });

        // Click Continue
        await wrapper
            .findAllComponents({name: 'v-btn'})
            .at(1)
            .vm.$emit('click');

        assertionMap.forEach(({mock, value}) => {
            expect(mock).toHaveBeenCalledWith(value);
        });
        expect(wrapper.emitted().suspensionupdate[0])
            .toEqual([!person.isAuthorSuspended]);
    });

})