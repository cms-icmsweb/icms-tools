import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import BookingFormDialog from '../../../src/components/BookingFormDialog.vue'

describe('BookingFormDialog.vue', () => {
    const listProjectsCall = mockApiCall(AjaxCalls.BookingProjectsCall, { response : [] });
    beforeEach(() => {
        listProjectsCall.initialize();
    });

    it('Shows Create booking form if no id is provided', () => {
        const wrapper = mount(BookingFormDialog, {
            propsData: {
                value: true,
                booking: null
            }
        });

        expect(wrapper.find('.v-card__title').text()).toEqual('Create Booking');
    });

    it('Shows Edit booking form if id is provided', () => {
        const wrapper = mount(BookingFormDialog, {
            propsData: {
                value: true,
                booking: {id: 1}
            }
        });

        expect(wrapper.find('.v-card__title').text()).toEqual('Edit Booking');
    });

    it('Closes when the close button is clicked', async () => {
        const wrapper = mount(BookingFormDialog,{
            propsData: {
                value: true
            }
        });

        //Click close
        await wrapper.findAllComponents({name: 'v-btn'}).at(0).trigger('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it("Doesn't allow submitting when errors exist", async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingRequestsCall, {});
        createBookingCall.initialize();
        const wrapper = mount(BookingFormDialog,{
            propsData: {
                value: true
            }
        });
        
        //Click Submit
        wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        await wrapper.vm.$nextTick()
        //Create announcement was never called
        expect(AjaxCalls.BookingRequestsCall).toHaveBeenCalledTimes(0);
        //Dialog is still open
        expect(wrapper.emitted().input).toBeFalsy();
    });    

    it('Validates that starting time is before ending time', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingRequestsCall, {});
        createBookingCall.initialize();

        const wrapper = mount(BookingFormDialog,{
            propsData: {
                value: true
            },
            data(){
                return {
                    formState: {
                        title: 'Booking',
                        timeStart: '10:00',
                        timeEnd: '08:00'
                    }
                }
            }
        });  
        
        wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        await wrapper.vm.$nextTick()
        
        expect(wrapper.find('.v-messages__message').text()).toEqual('The starting time must be before the ending time.');
    });

    it('Makes an api call to create a booking on submit', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingRequestsCall, {
            requestParams: {
                title: 'Booking',
                time_start: '2020-01-01 10:00:00',
                duration: 60,
                week_id: 1,
                cms_room_id: 1,
                preferred_room_id: 1,
                cms_id_for: null,
                project: 'a',
                status: 'pending',
                webcast: false,
                official: true,
                capacity: null,
                remarks: null,
                reason: null,
                agenda_url: null,
                password: null,
            }
        });
        createBookingCall.initialize();

        const wrapper = mount(BookingFormDialog,{
            propsData: {
                value: true,
                booking: null,
                weekId: 1
            },
            data(){
                return {

                    formState: {
                        title: 'Booking',
                        date: '2020-01-01',
                        timeStart: '10:00',
                        timeEnd: '11:00',
                        weekId: 1,
                        room_id: 1,
                        project: 'a',
                        cmsIdFor: null
                    }
                }
            }
        });  

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        expect(wrapper.emitted().bookingcreated[0]).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('Makes an api call to edit a booking on submit', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingRequestsCall, {
            requestParams: {
                id: 1,
                title: 'Booking',
                time_start: '2020-01-01 10:00:00',
                duration: 60,
                week_id: 1,
                cms_room_id: 1,
                preferred_room_id: 1,
                cms_id_for: null,
                project: 'a',
                status: 'pending',
                webcast: false,
                official: true,
                capacity: null,
                remarks: null,
                reason: null,
                agenda_url: null,
                password: null,
            }
        });
        createBookingCall.initialize();

        const wrapper = mount(BookingFormDialog,{
            propsData: {
                value: true,
                booking: {id: 1},
                weekId: 1
            },
            data(){
                return {

                    formState: {
                        title: 'Booking',
                        date: '2020-01-01',
                        timeStart: '10:00',
                        timeEnd: '11:00',
                        weekId: 1,
                        room_id: 1,
                        project: 'a',
                        cmsIdFor: null
                    }
                }
            }
        });  

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        expect(wrapper.emitted().bookingupdated[0]).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

});
