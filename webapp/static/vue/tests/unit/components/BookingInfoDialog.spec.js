import mockApiCall from '../../utils/mockApiCall';
import { shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import BookingInfoDialog from '../../../src/components/BookingInfoDialog.vue'

describe('BookingInfoDialog.vue', () => {
    const booking = {
        id: 1,
        title: 'Booking',
        project: 'a',
        capacity: 50,
        official: true,
        webcast: true,
        password: '123456789',
        reason: 'reason',
        remarks: 'remarks',
        agenda_url: 'agenda_url',
        time_start: '2020-01-01 00:00:00',
        duration: 270,
        status: 'pending',
        _links: { update: {}}
    };

    const statusCall = mockApiCall(AjaxCalls.BookingStatusesCall, { 
        requestParams: {
            id: 1
        },
        response : {
            status: "pending",
            _links: { update: {} }
        } 
    });
    beforeEach(() => {
        statusCall.initialize();
    });

    it('Renders booking info correctly', () => {
        const wrapper = shallowMount(BookingInfoDialog, {
            propsData: {
                booking
            }
        });

        expect(wrapper.html()).toMatchSnapshot();
    });


    it("Doesn't allow any actions if the booking is not pending", () => {
        const wrapper = shallowMount(BookingInfoDialog, {
            propsData: {
                booking: {
                    ...booking,
                    status: 'done'
                }
            }
        });

        expect(wrapper.vm.canEdit).toBe(false);
        expect(wrapper.vm.canChangeStatus).toBe(false);
    });

    it("Doesn't display edit button if the user has no permission", () => {
        const wrapper = shallowMount(BookingInfoDialog, {
            propsData: {
                booking: {
                    ...booking,
                    _links: {}
                }
            }
        });

        expect(wrapper.vm.canEdit).toBe(false);
    });

    it("Doesn't display approve/remove buttons if the user has no permission", () => {
        const statusCallNoPermissions = mockApiCall(AjaxCalls.BookingStatusesCall, { 
            requestParams: {
                id: 1
            },
            response : {
                status: "pending",
                _links: { }
            } 
        });
        statusCallNoPermissions.initialize();
        
        const wrapper = shallowMount(BookingInfoDialog, {
            propsData: {
                booking
            }
        });

        expect(wrapper.vm.canChangeStatus).toBe(false);
    });
});
