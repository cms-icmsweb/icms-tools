import { shallowMount } from '../../utils/testCreators';
import Scheduler from '../../../src/components/scheduler/Scheduler.vue'
import DayPicker from '../../../src/components/scheduler/DayPicker.vue'
import Timeline from '../../../src/components/scheduler/Timeline.vue'

describe('Scheduler.vue', () => {
    it('Display an empty message when no bookings are provided', () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [],
            }
        });
        
        expect(wrapper.find('.empty-message .mb-2').text()).toEqual('No Bookings exist for this date');
    });

    it('Calculates the dates based on the existing bookings', () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [
                    {
                        from: '2020-01-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1'
                    },
                    {
                        from: '2020-02-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1'
                    },
                    {
                        from: '2020-03-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1'
                    },
                ],
            }
        });

        expect(wrapper.findComponent(DayPicker).props().days).toEqual([
            '2020-01-01',
            '2020-02-01',
            '2020-03-01',
        ]);
    })

    it('Calculates the rooms based on the existing bookings', async () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [
                    {
                        from: '2020-01-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1'
                    },
                    {
                        from: '2020-02-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_2',
                        title: 'booking_1'
                    },
                    {
                        from: '2020-03-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_3',
                        title: 'booking_1'
                    },
                ],
            }
        });
        
        await wrapper.vm.$nextTick();
        
        const rooms = wrapper.findAll('.room');
        expect(rooms.at(0).text()).toEqual('room_1');
        expect(rooms.at(1).text()).toEqual('room_2');
        expect(rooms.at(2).text()).toEqual('room_3');
    });

    it('Calculates the booking types based on the existing bookings', async () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [
                    {
                        from: '2020-01-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1',
                        type: 'type_1'
                    },
                    {
                        from: '2020-01-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1',
                        type: 'type_2'
                    },
                    {
                        from: '2020-01-01 00:00:00',
                        to: '2020-01-01 00:03:00',
                        room: 'room_1',
                        title: 'booking_1',
                        type: 'type_1'
                    },
                ],
            }
        });

        await wrapper.vm.$nextTick();
        
        expect(wrapper.findComponent(Timeline).props().types).toMatchObject([{name: 'type_1'}, {name: 'type_2'}]);
    });

    it('Displays all the hours for the given range', () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [],
                dayStartTime: '08:00:00',
                dayEndTime: '12:00:00',
            }
        });

        const hours = wrapper.findAll('.hour-container');
        expect(hours.at(0).text()).toBe('08:00');
        expect(hours.at(1).text()).toBe('09:00');
        expect(hours.at(2).text()).toBe('10:00');
        expect(hours.at(3).text()).toBe('11:00');

        expect(wrapper.findAll('.tickmark').length).toEqual(4);
    })

    it('Selects the correct bookings for a given day and room', async () => {
        const booking1 = {
            from: '2020-01-01 00:00:00',
            to: '2020-01-01 00:03:00',
            room: 'room_1',
            title: 'booking_1'
        };
        const booking2 = {
            from: '2020-01-01 00:00:00',
            to: '2020-01-01 00:03:00',
            room: 'room_2',
            title: 'booking_2'
        };
        const booking3 = {
            from: '2020-01-01 00:00:00',
            to: '2020-01-01 00:03:00',
            room: 'room_1',
            title: 'booking_3'
        };
        const booking4 = {
            from: '2020-02-01 00:00:00',
            to: '2020-02-01 00:03:00',
            room: 'room_1',
            title: 'booking_4'
        };
        const booking5 = {
            from: '2020-03-01 00:00:00',
            to: '2020-03-01 00:03:00',
            room: 'room_2',
            title: 'booking_5'
        };
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [booking1, booking2, booking3, booking4, booking5],
            }
        });

        await wrapper.vm.$nextTick();
        
        const timelines = wrapper.findAllComponents(Timeline)
        expect(timelines.at(0).props().bookings).toEqual([booking1, booking3]);
        expect(timelines.at(1).props().bookings).toEqual([booking2]);

        wrapper.setData({selectedDay : '2020-02-01'});
        await wrapper.vm.$nextTick();
        
        const timelines2 = wrapper.findAllComponents(Timeline)
        expect(timelines2.at(0).props().bookings).toEqual([booking4]);
        expect(timelines2.at(1).props().bookings).toEqual([]);

        wrapper.setData({selectedDay : '2020-03-01'});
        await wrapper.vm.$nextTick();
        
        const timelines3 = wrapper.findAllComponents(Timeline)
        expect(timelines3.at(0).props().bookings).toEqual([]);
        expect(timelines3.at(1).props().bookings).toEqual([booking5]);
    });

    it("Doesn't display buttons for creating bookings if no listener is attached", () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [],
            }
        });

        // Click create new booking button under empty message
        expect(wrapper.findComponent({name: 'v-btn'}).exists()).toBe(false);
        
    })

    it.only('Emits Booking Creation Event when clicking create booking on the empty timeline', async () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [],
            },
            listeners: {
                bookingadd: () => null
            }
        });

        // Click create new booking button under empty message
        await wrapper.findComponent({name: 'v-btn'}).vm.$emit('click');
        expect(wrapper.emitted().bookingadd[0]).toEqual([{ room: null, day: null}]);
    });

    it.only('Emits Booking Creation Event when clicking add on a room', async () => {
        const wrapper = shallowMount(Scheduler, {
            propsData : {
                bookings: [{
                    from: '2020-01-01 00:00:00',
                    to: '2020-01-01 00:03:00',
                    room: 'room_1',
                    title: 'booking_1'
                }],
            },
            listeners: {
                bookingadd: () => null
            }
        });

        await wrapper.vm.$nextTick();
        // Click '+' button on the first room
        await wrapper.findComponent({name: 'v-btn'}).vm.$emit('click');
        expect(wrapper.emitted().bookingadd[0]).toEqual([{ room: 'room_1', day: '2020-01-01'}]);
        

    });

});