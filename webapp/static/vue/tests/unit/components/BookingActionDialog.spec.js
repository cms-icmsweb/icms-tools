import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import BookingActionDialog from '../../../src/components/BookingActionDialog.vue'

describe('BookingActionDialog.vue', () => {
    it('On approve makes an api call to change status to done', async () => {
        const approveCall = mockApiCall(AjaxCalls.BookingStatusesCall, { 
            requestParams : {
                id: 1, 
                status: 'done'
            }
        });
        approveCall.initialize();
    
        const wrapper = shallowMount(BookingActionDialog,{
            propsData: {
                value: true,
                booking: {
                    id: 1,
                    status: 'pending'
                 },
                action: 'approve'
            }
        });
        
        await wrapper.findAllComponents({name: 'v-btn'}).at(1).vm.$emit('click');
        approveCall.assertionMap.forEach(({mock, value}) => {
            expect(mock).toHaveBeenCalledWith(value);
        });
        expect(wrapper.emitted().bookingstatuschange[0])
            .toEqual(['done']);
    })

    it('On remove makes an api call to change status to deleted', async () => {
        const removalCall = mockApiCall(AjaxCalls.BookingStatusesCall, { 
            requestParams : {
                id: 1, 
                status: 'deleted'
            }
        });
        removalCall.initialize();
    
        const wrapper = shallowMount(BookingActionDialog,{
            propsData: {
                value: true,
                booking: {
                    id: 1,
                    status: 'pending'
                 },
                action: 'remove'
            }
        });
        
        await wrapper.findAllComponents({name: 'v-btn'}).at(1).vm.$emit('click');
        removalCall.assertionMap.forEach(({mock, value}) => {
            expect(mock).toHaveBeenCalledWith(value);
        });
        expect(wrapper.emitted().bookingstatuschange[0])
            .toEqual(['deleted']);
    });

    it('On close, closes the dialog', async () => {
        const wrapper = shallowMount(BookingActionDialog,{
            propsData: {
                value: true,
                booking: {
                    id: 1,
                    status: 'pending'
                 },
                action: 'remove'
            }
        });
        await wrapper.findAllComponents({name: 'v-btn'}).at(0).vm.$emit('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('On continue, closes the dialog', async () => {
        const approveCall = mockApiCall(AjaxCalls.BookingStatusesCall, { 
            requestParams : {
                id: 1, 
                status: 'done'
            }
        });
        approveCall.initialize();
    
        const wrapper = shallowMount(BookingActionDialog,{
            propsData: {
                value: true,
                booking: {
                    id: 1,
                    status: 'pending'
                 },
                action: 'approve'
            }
        });

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).vm.$emit('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });
});
