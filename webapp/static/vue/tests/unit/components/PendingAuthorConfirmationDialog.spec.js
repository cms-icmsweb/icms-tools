import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import PendingAuthorConfirmationDialog from '../../../src/components/PendingAuthorConfirmationDialog.vue'

describe('PendingAuthorConfirmationDialog.vue', () => {
    const { initialize, assertionMap } = mockApiCall(AjaxCalls.PendingAuthorsCall, { 
        requestParams : {
            cmsId: 1, 
            action : 'admit'
        }
    });
    beforeEach(() => {
        initialize();
    });

    it('Displays a warning message', () => {
        const wrapper = mount(PendingAuthorConfirmationDialog,{
            propsData: {
                value: true,
                person: { cmsId: 1 },
                action: 'admit'
            }
        });
        
        // You are about to unsuspend the activity of 
        expect(wrapper.find('.v-card__text').element).toMatchSnapshot();
    });

    it('Closes the dialog on when clicking close', async () => {
        const wrapper = shallowMount(PendingAuthorConfirmationDialog,{
            propsData: {
                value: true,
                person: { cmsId: 1 },
                action: 'admit'
            }
        });
        
        // Click Button (Close : 0 , Continue: 1)
        await wrapper.findAllComponents({name: 'v-btn'}).at(0).vm.$emit('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('Makes the api call when clicking continue', async () => {
        const wrapper = shallowMount(PendingAuthorConfirmationDialog,{
            propsData: {
                value: true,
                person: { cmsId: 1 },
                action: 'admit'
            }
        });

        // Click Continue
        await wrapper.findAllComponents({name: 'v-btn'}).at(1).vm.$emit('click');

        assertionMap.forEach(({mock, value}) => {
            expect(mock).toHaveBeenCalledWith(value);
        });
        expect(wrapper.emitted().actioncompleted).toBeTruthy();
    });
});
