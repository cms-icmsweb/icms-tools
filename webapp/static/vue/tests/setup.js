import Vue from 'vue'
import Vuetify from 'vuetify'

jest.mock('../src/utils/AjaxCalls');
Vue.use(Vuetify)
Vue.config.productionTip = false;

global.$SCRIPT_ROOT = '';