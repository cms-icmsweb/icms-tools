import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import { createLocalVue, mount as testUtilsMount, shallowMount as testUtilsShallowMount } from '@vue/test-utils';

import { createStore } from '../../src/store';
import { createRouter } from '../../src/router';

// Creates a localVue instance with a store and router
export const createTestVue = () => {
    const localVue = createLocalVue()
    localVue.use(VueRouter)
    localVue.use(Vuex)

    const store = createStore()
    const router = createRouter()
    let vuetify = new Vuetify();
    return { store, router, localVue, vuetify }
}

// A wrapper function on mount and shallow mount
// used for mounting a component with a store and a router 
const createWrapper = (component, options, doShallowMount = false) => {
  const { localVue, store, router, vuetify } = createTestVue()

  const mountFunc = doShallowMount ? testUtilsShallowMount : testUtilsMount;
  return mountFunc(component, Object.assign({},{
      store,
      router,
      localVue,
      vuetify
  }, options
  ));
}

export const mount = (component, options) => createWrapper(component, options, false);
export const shallowMount = (component, options) => createWrapper(component, options, true);