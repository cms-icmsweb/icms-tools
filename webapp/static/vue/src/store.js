import Vue from 'vue'
import Vuex from 'vuex'
import Ajax from './utils/Ajax.js'

Vue.use(Vuex)

const storeProperties = {
    state: {
        // fine-grained details on our user - to be expanded with new parameters necessary for components' operation
        user: {cmsId: null, login: null, instCode: null, displayName: null},
        // "raw" response of API endpoint returning user's details - probably it should not be relied upon too much due to coupling with API's structure
        userInfo: {},
        loadingLocks: {user: false, tenures: false},
        errorMessage: null,
        infoMessage: null,
        // this field will be use to signal that store's setup is complete - necessary before transition to SPA as store will need some setup time upon each page load
        ready: false,
        tenures: []
    },

    mutations: {
        cmsId (state, cmsId){
            console.debug('Mutating CMS ID to be ' + cmsId);
            state.user.cmsId = cmsId;
        },
        userName (state, login){
            console.debug("Mutating login to be " + login);
            state.user.login = login;
        },
        instCode (state, code){
            console.debug("Mutating inst code to be " + code);
            state.user.inst = code;
        },
        displayName (state, displayName){
            console.debug("Mutating user's display name to be " + displayName);
            state.user.displayName = displayName;
        },
        userInfo (state, newInfo){
            // coarse first
            state.userInfo = newInfo;
            // then all the fine-grained details
            state.user.cmsId = newInfo['cms_id'];
            state.user.userName = newInfo['login']
            state.user.displayName = newInfo['first_name'] + ' ' + newInfo['last_name'];
            state.user.instCode = newInfo['inst_code'];
        },
        tenures (state, newData){
            state.tenures = newData;
        },
        errorMessage (state, errorMessage){
          console.debug("Mutating error message to be: " + errorMessage);  
          state.errorMessage = errorMessage;
        },
        infoMessage (state, infoMessage){
            console.debug("Mutating info message to read: " + infoMessage);
            state.infoMessage = infoMessage;
        },
        ready (state, newStatus){
            console.debug("Mutating store's ready parameter to " + newStatus);
            state.ready = newStatus;
        }
    },

    actions: {
        bootstrap(context){
            context.dispatch('loadUserInfoAsync');
        },
        bootstrapPartTwo(context){
            // this will be called once the user data is in
            if (context.state.loadingLocks.tenures == false){
                context.state.loadingLocks.tenures = true;
                Ajax.getTenures(function(data){
                    console.debug('Fetched user\'s tenures info.');
                    console.debug(data);
                    context.commit('tenures', data)
                    context.state.loadingLocks.tenures = false; 
                }, [context.state.user.cmsId], new Date().toISOString().substring(0, 10));
            }
        },
        loadUserInfoAsync(context){
            if (context.state.loadingLocks.user == false){
                context.state.loadingLocks.user = true;
                Ajax.getCurrentUserInfo(function(data){
                    console.debug('Grabbed current user info!');
                    console.debug(data);
                    context.commit('userInfo', data);
                    context.commit('ready', true);
                    context.state.loadingLocks.user = false;
                    context.dispatch('bootstrapPartTwo');
                });
            } else {
                console.debug('User info request already requested and has not returned yet. Dropping this request.');
            }
        },
    },

    getters: {
        userInfo: function(state){
            return state.userInfo;
        },

        userCmsId: function(state){
            return state.user.cmsId;
        },

        userName: function(state){
            return state.user.login;
        },
        
        userInstCode: function(state){
            return state.user.instCode;
        },

        userDisplayName: function(state){
            return state.user.displayName;
        },

        errorMessage: function(state){
            return state.errorMessage;
        },

        infoMessage: function(state){
            return state.infoMessage;
        },

        userDataAvailable: function(state){
            return state.user.cmsId != null && state.user.login != null && state.user.inst != null; 
        },

        ready: function(state){
            return state.ready;
        },

        isAdmin: (state) => {
            const { flags } = state.userInfo;
            return flags != null && flags != 'undefined' && flags.includes('ICMS_admin');
        }
    }
};


export class CustomStore extends Vuex.Store{
    constructor(properties){
        super(properties)
    }
    info(message){
        this.commit("infoMessage", message);
    }
    error(message){
        this.commit("errorMessage", message);
    }
}


export default new CustomStore(storeProperties);
export const createStore = () => new CustomStore(storeProperties);