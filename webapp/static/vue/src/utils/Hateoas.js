export default {
    ResponseProcessor: class {
        constructor(response) {
            this.response = response;
            this.entityCollectionHandler = function (label, arr) { console.debug("Collection labelled " + label + " of length " + arr.length); };
            this.linkHandler = function (label, link) { console.debug("Received a link labelled " + label + ": " + JSON.stringify(link)); }
            this.templateHandler = function (label, template) { console.debug("Received a template labelled " + label + ": " + JSON.stringify(template)); }
        }
        setResponse(response){
            this.response = response;
            return this;
        }
        setEntityCollectionHandler(callable) {
            this.entityCollectionHandler = callable;
            return this;
        }
        setLinkHandler(callable) {
            this.linkHandler = callable;
            return this;
        }
        setTemplateHandler(callable){
            this.templateHandler = callable;
            return this;
        }
        process(response = null) {
            response = response || this.response;
            var instance = this;
            Object.keys(response).forEach(e => {
                if (e == '_embedded') {
                    Object.keys(response[e]).forEach((typeName) => {
                        instance.entityCollectionHandler(typeName, response[e][typeName]);
                    });
                } else if (e == '_links') {
                    Object.keys(response[e]).forEach(link => {
                        instance.linkHandler(link, response[e][link]);
                    });
                } else if (e == '_templates') {
                    Object.keys(response[e]).forEach(templateName => {
                        instance.templateHandler(templateName, response[e][templateName]);
                    });
                } else if (e == 'page') {
                    console.debug('Page info from response:');
                    console.debug(response[e]);
                } else {
                    console.debug("Unhandled response field: " + e);
                }
            });
        }
    },
    ItemProcessor: class {
        constructor(item) {
            this.item = item;
            
        }
    }
}