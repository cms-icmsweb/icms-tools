import { IcmsPiggybackCall, IcmsPiggybackHalCall } from './PiggybackCalls'

export class LegacyNotesCall extends IcmsPiggybackHalCall{
    constructor() {
        super('/notes');
    }
    setYear(value) { return this.set('year', value); }
    setType(value) { return this.set('type', value); }
    setCmsId(value) { return this.set('cmsId', value); }
    stubs(){this.endpoint = this.endpoint.substring(0, this.endpoint.indexOf('/notes')) + '/notes/stubs'; return this;}
    notes(){this.endpoint = this.endpoint.substring(0, this.endpoint.indexOf('/notes')) + '/notes/public'; return this;}
}

export class LegacyNoteStatusesCall extends IcmsPiggybackCall {
    constructor() {
        super('/notes/statuses')
    }
    setTerm(value) { return this.set('term', value); }
}