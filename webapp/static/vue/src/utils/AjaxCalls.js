import {BaseCall, RestplusCall} from './BaseCalls'


export default {
    Call: BaseCall,

    InstitutesCall: class extends RestplusCall{
        constructor(){
            super('/members/institutes');
            this.set('code', []);
            this.set('status', []);
        }
        addCode(value){this.data['code'].push(value); return this;}
        addStatus(value){this.data['status'].push(value); return this;}
    },

    ApplicationsInfoCall: class extends RestplusCall {
        constructor() {
            super('/authorship/app_checks');
        }
        setLastRunOnly(value) { this.set('last_run_only', value); return this; }
    },

    PeopleCall: class extends RestplusCall {
        constructor() {
            super('/members/people');
        }
        setInstCode(value) { return this.set('inst_code', value); };
        setProject(value) { return this.set('project', value); };
        setStatus(value) { return this.set('ind_status', value); };
        setIsAuthor(value) { return this.set('is_author', value); };
    },

    LegacyNotesSetSubmitter: class extends RestplusCall {
        constructor() {
            super('/old_notes/submitter')
        }
        setCmsNoteId(value) { return this.set('cms_note_id', value); }
        setCmsId(value) { return this.set('cms_id', value); }
    },

    ExternalOutreachContactsCall: class extends RestplusCall {
        constructor() {
            super('/members/external_outreach_contacts');
        }
        setCode(value) { return this.set('code', value); }
        setType(value) { return this.set('type', value); }
        setTitle(value) { return this.set('title', value); }
        setFirstName(value) { return this.set('firstname', value); }
        setLastName(value) { return this.set('lastname', value); }
        setEmail(value) { return this.set('email', value); }
    },

    UsersInfoCall: class extends RestplusCall {
        constructor() {
            super('/members/users_info');
            console.debug('Users info call constructed');
        }
        setCmsId(value) { return this.set('cms_id', value); }
        setInstCode(value) { return this.set('inst_code', value); }
        setStatus(value) { return this.set('status', value); }
    },

    AssignmentsCall: class extends RestplusCall {
        constructor() {
            super('/members/assignments');
        }
        setCmsId(value) { return this.set('cms_id', value); }
        setId(value) { return this.set('id', value); }
        setProjectCode(value) { return this.set('project_code', value); }
        setFraction(value) { return this.set('fraction', value); }
        setActiveOnly(value) { return this.set('active_only', value); }
    },

    ProjectsCall: class extends RestplusCall {
        constructor() {
            super('/epr/projects')
        }
        setCode(value) { return this.set('code', value); }
    },

    TenuresCall: class extends RestplusCall {
        constructor() {
            super('/org_chart/tenures')
        }
        setCmsId(value) { return this.set('cms_id', value); }
        setExcludePast(value) { return this.set('exclude_past', value); }
        setAsOf(value) { return this.set('as_of', value); }
        setDomain(value) { return this.set('domain', value); }
        setUnitType(value) { return this.set('unit_type', value); }
        setUnitId(value) { return this.set('unit_id', value); }
        setMinPositionLevel(value) { return this.set('min_position_level', value); }
        setMaxPositionLevel(value) { return this.set('max_position_level', value); }
        
    },

    EmailsCall: class extends RestplusCall {
        constructor() {
            super('/admin/emails')
        }
    },

    EmailCall: class extends RestplusCall {
        constructor() {
            super('/admin/email')
        }
        setId(value){ return this.set('id', value); }
        setSender(value){ return this.set('sender', value); }
        setTo(value){ return this.set('to', value); }
        setBcc(value){ return this.set('bcc', value); }
        setCc(value){ return this.set('cc', value); }
        setSubject(value){ return this.set('subject', value); }
        setBody(value){ return this.set('body', value); }
    },

    AnnouncementsCall: class extends RestplusCall {
        constructor() {
            super('/admin/announcements')
        }
    },

    AnnouncementCall: class extends RestplusCall {
        constructor() {
            super('/admin/announcement')
        }
        setType(value){ return this.set('type', value); }
        setSubject(value){ return this.set('subject', value); }
        setContent(value){ return this.set('content', value); }
        setStartDatetime(value){ return this.set('start_datetime', value); }
        setEndDatetime(value){ return this.set('end_datetime', value); }
        setApplication(value){ return this.set('application', value); }
    },
    
    OverdueGraduationsCall: class extends RestplusCall {
        constructor(){
            super('/inst/overdue-graduations')
        }
    },

    ConfirmStudentStatusCall: class extends RestplusCall {
        constructor(){
            super('/inst/overdue-graduations/status')
        }
        setCmsId(value){ return this.set('cmsId', value); }
        setStatus(value){ return this.set('status', value); }
    },

    SuspensionsCall: class extends RestplusCall {
        constructor(){
            super('/inst/suspensions')
        }
        setCmsId(value){ return this.set('cmsId', value); }
        setIsAuthorSuspended(value){ return this.set('isAuthorSuspended', value); }
    },

    MemberAuthorsCall: class extends RestplusCall {
        constructor(){
            super('/authorlists/memberAuthors')
        }
        setCmsId(value){ return this.set('cmsId', value); }
        setFlagId(value){ return this.set('flagId', value); }
    },    
    
    PendingAuthorsCall: class extends RestplusCall {
        constructor(){
            super('/authorlists/pendingAuthors')
        }
        setCmsId(value){ return this.set('cmsId', value); }
        setAction(value){ return this.set('action', value); }
    },

    FlagsCall: class extends RestplusCall {
        constructor(){
            super('/members/flags');
        }
    },

    AuthorizationsCall: class extends RestplusCall {
        constructor(){
            super('/admin/permissions/rights');
        }
        setResourceId(value){return this.set('resource_id', value);}
        setAccessClassId(value){return this.set('access_class_id', value);}
        setAction(value){return this.set('action', value);}
    },

    ResourcesCall: class extends RestplusCall {
        constructor(){
            super('/admin/permissions/resources');
        }
        setFilters(value){return this.set('filters', value);}
        setKey(value){return this.set('key', value);}
        setType(value){return this.set('type', value);}
    },

    AccessClassesCall: class extends RestplusCall {
        constructor(){
            super('/admin/permissions/classes');
        }
        setName(value){return this.set('name', value);}
        setRules(value){return this.set('rules', value);}
    },

    RoutesCall: class extends RestplusCall {
        constructor(){
            super('/admin/routes');
        }
    },

    ParamStoresCall: class extends RestplusCall {
        constructor(){
            super('/admin/permissions/stores');
        }
    },

    MembersCountCall: class extends RestplusCall {
        constructor(){
            super('/statistics/members_count');
            this.set('status', []);
        }
        activeMembers(){this.data['status'].push('CMS'); return this;}
        groupByInst(){return this.set('grouping_column', 'inst_code');}
        onlyAuthors(){return this.set('is_author', true);}
    },

    ImpersonateUserCall: class extends RestplusCall {
        constructor(){
            super('/admin/impersonate');
        }
    },

    BookingWeeksCall: class extends RestplusCall {
        constructor(){
            super('/booking/weeks');
        }
        setTitle(value){return this.set('title', value)}
        setDate(value){return this.set('date', value)}
        setDateEnd(value){return this.set('date_end', value)}
        setIsExternal(value){return this.set('is_external', value)}
    },

    BookingRoomsCall: class extends RestplusCall {
        constructor(){
            super('/booking/rooms');
        }
        setWeekId(value){return this.set('weekId', value)}

        setIndicoId(value){return this.set('indico_id', value)}
        setBuilding(value){return this.set('building', value)}
        setFloor(value){return this.set('floor', value)}
        setRoomNr(value){return this.set('room_nr', value)}
        setCustomName(value){return this.set('custom_name', value)}
        setAtCern(value){return this.set('at_cern', value)}
    },

    BookingSlotsCall: class extends RestplusCall {
        constructor(){
            super('/booking/slots');
        }
        setWeekId(value){return this.set('weekId', value)}
    },

    BookingRequestsCall: class extends RestplusCall {
        constructor(){
            super('/booking/requests');
        }
        setWeekId(value){return this.set('cms_week_id', value)}
        
        setCmsWeekId(value){return this.set('cms_week_id', value)}
        setPreferredRoomId(value){return this.set('room_id_preferred', value)}
        setCmsRoomId(value){return this.set('room_id', value)}
        setStatus(value){return this.set('status', value)}
        setCmsIdFor(value){return this.set('cms_id_for', value)}
        setTitle(value){return this.set('title', value)}
        setProject(value){return this.set('project', value)}
        setWebcast(value){return this.set('webcast', value)}
        setOfficial(value){return this.set('official', value)}
        setTimeStart(value){return this.set('time_start', value)}
        setDuration(value){return this.set('duration', value)}
        setCapacity(value){return this.set('capacity', value)}
        setPassword(value){return this.set('password', value)}
        setAgendaUrl(value){return this.set('agenda_url', value)}
        setRemarks(value){return this.set('remarks', value)}
        setReason(value){return this.set('reason', value)}
    },

    BookingStatusesCall: class extends RestplusCall {
        constructor(){
            super('/booking/requests/statuses');
        }
        setStatus(value){return this.set('status', value)}
    },

    BookingProjectsCall: class extends RestplusCall {
        constructor(){
            super('/booking/projects');
        }   
    }
}