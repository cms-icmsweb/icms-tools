import {RestplusCall} from './BaseCalls';
import Hateoas from './Hateoas'


export class IcmsPiggybackCall extends RestplusCall {
    constructor(endpoint) {
        super(RestplusCall.isValidUrl(endpoint) ? endpoint : '/relay/piggyback' + endpoint);
    }
    set(key, value){
        super.set(key, value);
        // without a systematic approach to handle templated URLs, that will need to do the trick...
        this.endpoint = this.endpoint.replace('{?' + key + '}', '');
        return this;
    }
    setData(data){
        this.data = data;
        return this;
    }
}

/**
 * Convenience class for calls that expect a hypermedia response.
 * The call object comes with a built-in ResponseProcessor to cut down the boilerplate code.
 * The plain IcmsPiggybackCall can still be a better choice if some nitty-gritty aspects of 
 * response processing need to be defined in the client code.
 */
export class IcmsPiggybackHalCall extends IcmsPiggybackCall{
    constructor(endpoint){
        super(endpoint);
        var owner = this;
        this.processor = new Hateoas.ResponseProcessor(null);
        this.processor.setLinkHandler((label, link)=>{
            if (label === 'next'){
                // by default for paged resources: update the owner object's endpoint address and query on
                owner.endpoint = link.href;
                owner.get();
            }
        });
        super.setSuccessCallback((data, status, request)=>{
            // status and request will be ignored for the time being
            owner.processor.process(data)
        });
    }

    setProcessor(processor){
        this.processor = processor;
        return this;
    }

    getProcessor(){
        return this.processor;
    }

    /**
     * In simple cases, the success callback will be applied to '_embedded' collection(s)
     * extracted from the response. External interface will remain identical wrt BaseCall.
     * 
     * For more elaborate behavior don't set the success callback but customise the 
     * ResponseProcessor instance (this.processor) by providing the necessary callback or subclass
     * this class and create a dedicated ResponseProcessor class for it.
     * @param {Function} callable 
     */
    setSuccessCallback(callable){
        this.processor.setEntityCollectionHandler((label, collection)=>{
            callable(collection);
        });
        return this;
    }
}


export class PeopleCall extends IcmsPiggybackHalCall{
    constructor(){
        super('/people');
    }
    setCmsIds(values){
        return this.set('cmsIds', values.join(','));
    }
}

export class ArcCountsCall extends IcmsPiggybackHalCall {
    constructor() {
        super('/cadi/history/arcsPerYear');
    }
    setInstCode(value) { return this.set('instCode', value); };
    setYear(value) { return this.set('year', value); }
}

export class InstAddressesCall extends IcmsPiggybackCall {
    constructor(instCode) {
        var path = '/institutes/addresses';
        if (instCode !== null && instCode != undefined) {
            path += '/' + instCode;
        }
        super(path);
    }
}

export class EmeritusApprovalsCall extends IcmsPiggybackCall {
    constructor() {
        super('/people/statuses/requests');
        this.set('concern', 'CMSEMERITUS');
    }
    setYear(value) { return this.set('year', value); };
}

export class TempFileUploadCall extends IcmsPiggybackCall{
    constructor(){
        super('/tempFiles');
        this.data = new FormData();
        this.processData = false;
        this.contentType = false;
        this.dataType = null;
    }
    set(key, value) {this.data.append(key, value); return this;}
    setFile(value) {return this.set('file', value);}

    initAjaxCall(type, headers = null, data = null){
        // Overloading the entire method as there's something in the details here that doesn't add up otherwise
        return $.ajax({
            url: this.endpoint,
            data: this.data,
            contentType: this.contentType,
            processData: this.processData,
            dataType: this.dataType,
            type: 'POST',
            global: false,
        });
    }
}