import store from '../store.js';


export class BaseCall {
    constructor(endpoint, successCallback = null, failureCallback = null) {
        if (BaseCall.isValidUrl(endpoint)){
            this.endpoint = endpoint;
        } else {
            this.endpoint = $SCRIPT_ROOT + endpoint;
        }
        this.successCallback = successCallback || BaseCall.defaultSuccessHandler;
        this.failureCallback = failureCallback || BaseCall.defaultFailureHandler;
        this.data = {};
        this.processData = true;
        this.contentType = false;
        this.dataType = 'json';
    }

    static isValidUrl(endpoint){
        return endpoint.startsWith('http://') || endpoint.startsWith('https://');
    }

    getEndpoint() {
        return this.endpoint;
    }

    setId(id){
        /**
         * For scenarios where resource is accessed like /xyz/resource/id,
         * appends id at the end of the path.
         */
        if (id !== null){
            this.endpoint = this.endpoint + '/' + id;
        }
         return this;
    }

    set(paramName, paramValue) {
        this.data[paramName] = paramValue;
        return this;
    }

    setSuccessCallback(callable) {
        this.successCallback = callable;
        return this;
    }

    setFailureCallback(callable) {
        this.failureCallback = callable;
        return this;
    }

    get() {
        this.ajaxCall('GET', {});
    }

    post() {
        this.ajaxCall('POST');
    }

    put() {
        this.ajaxCall('PUT');
    }

    delete() {
        this.ajaxCall('DELETE', {});
    }

    initAjaxCall(type, headers = null, data = null) {
        console.debug('About to dispatch an AJAX call to «' + this.endpoint + '». Headers override: ' + JSON.stringify(headers));
        // jQuery will auto-convert the data into query string for GETs but OTOH we need to stringify it ourselves before submitting a POST
        data = data || this.data;
        if (type !== 'GET' && type !== 'DELETE') {
            data = JSON.stringify(data);
        }
        return $.ajax({
            headers: headers || { 'Content-Type': 'application/json' },
            processData: this.processData,
            contentType: this.contentType,
            data: data,
            type: type,
            url: this.endpoint,
            dataType: this.dataType,
            global: false,
        });
    }

    ajaxCall(type, headers = null, data = null) {
        return this.initAjaxCall(type, headers, data).done(this.successCallback).fail(this.failureCallback);
    }

    static defaultFailureHandler(request, textStatus, errorThrown) {
        var message = 'Problem contacting server: «' + textStatus + "»";
        if (typeof (request.responseJSON) != 'undefined') {
            message = 'AJAX error ' + request.status + ': ' + request.responseJSON.message;
        }
        store.commit('errorMessage', message);
        console.error(message);
    }

    static defaultSuccessHandler(data, testStatus, request) {
        console.debug('Data fetched well!');
        console.debug(data);
    }
}

export class RestplusCall extends BaseCall {
    constructor(endpoint) {
        super(BaseCall.isValidUrl(endpoint) ? endpoint : '/restplus' + endpoint);
    }
}