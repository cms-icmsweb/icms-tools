/* eslint-disable no-undef */
import Vue from 'vue'
import Router from 'vue-router'
import ALMentions from './views/ALMentions.vue'
// import MoManagement from './views/MoManagement.vue'
import MoAdmin from './views/MoAdmin.vue'
import PhdMoList from './views/PhdMoList.vue'
import OpenAuthorLists from './views/OpenAuthorLists.vue'
import NotFoundComponent from './views/NotFound.vue'
import VuetifyPersonPicker from './components/VuetifyPersonPicker.vue'
import DueCorrector from './views/DueCorrector.vue'
import DueCorrections from './views/DueCorrections.vue'
import People from './views/People.vue'
import Tenures from './views/Tenures.vue'
import Dashboard from './views/Dashboard.vue'
import UserProfile from './views/UserProfile.vue'
import InstProfile from './views/InstProfile.vue'
import CommsOverview from './views/CommsOverview.vue'
import MarkdownEditor from './views/MarkdownEditor.vue'
import OrgChart from './views/OrgChart.vue'
import PendingNotes from './views/PendingNotes.vue'
import ExOfficioMandates from './views/ExOfficioMandates.vue'
import OrgPositions from './views/OrgPositions.vue'
import VotingList from './views/VotingList.vue'
import VoteDelegations from './views/VoteDelegations.vue'
import MemberStats from './views/MemberStats.vue'
import AuthorApplicants from './views/AuthorApplicants.vue'
import NotesAdmin from './views/NotesAdmin.vue'
import NotesSecretariat from './views/notes/NotesSecretariat.vue'
import TenuresByUnit from './views/TenuresByUnit.vue'
import ListEmails from './views/ListEmails.vue'
import ComposeEmail from './views/ComposeEmail.vue'
import Announcements from './views/Announcements.vue'
import OverdueGraduations from './views/OverdueGraduations.vue'
import Suspensions from './views/Suspensions.vue'
import MemberAuthors from './views/MemberAuthors.vue'
import PendingAuthors from './views/PendingAuthors.vue'
import Flags from './views/Flags.vue'
import Permissions from './views/Permissions.vue'
import Institutes from './views/Institutes.vue'
import EmeritusNominations from './views/EmeritusNominations.vue'
import Notes from './views/notes/Notes.vue'
import NoteStubs from './views/notes/NoteStubs.vue'
import MyNotes from './views/notes/MyNotes.vue'
import ArcMembers from './views/cadi/ArcMembers.vue'
import Bookings from './views/Bookings.vue'
import Weeks from './views/Weeks.vue'
import Rooms from './views/Rooms.vue'
import CadiLines from './views/cadi/CadiLines.vue'
import Awgs from './views/cadi/Awgs.vue'

Vue.use(Router)

const routerProperties = {
  mode: 'history',
  routes: [
    {
      path: $SCRIPT_ROOT,
      alias: [$SCRIPT_ROOT + '/welcome', $SCRIPT_ROOT + '/tools/welcome'],
      name: 'dashboard',
      component: Dashboard,
      props: true,
    },
    {
      path: $SCRIPT_ROOT + '/users/signedPapers/:cmsId?',
      name: 'al-mentions',
      component: ALMentions,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/people',
      name: 'people-list',
      component: People,
    }, {
      path: $SCRIPT_ROOT + '/authors',
      alias: [$SCRIPT_ROOT + '/data/cmsAuthors'],
      name: 'authors-list',
      component: People,
      props: {
        filterStatus: [],
        filterAuthor: [true],
      }
    }, {
      path: $SCRIPT_ROOT + '/applicants',
      alias: [$SCRIPT_ROOT + '/data/authorApplications'],
      name: 'author-applicants',
      component: AuthorApplicants
    }, {
      path: $SCRIPT_ROOT + '/flags',
      alias: [$SCRIPT_ROOT + '/data/flags'],
      name: 'flags',
      component: Flags
    }, {
      path: $SCRIPT_ROOT + '/users/profile/:cmsId?',
      name: 'user-profile',
      component: UserProfile,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/inst/overdueGraduations',
      alias: [$SCRIPT_ROOT + '/data/overdueGraduations'],
      name: 'overdue-graduations',
      component: OverdueGraduations,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/inst/suspensions',
      name: 'suspensions',
      component: Suspensions,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/cadi/awgs',
      name: 'awgs',
      component: Awgs,
    }, {
      path: $SCRIPT_ROOT + '/cadi/lines',
      name: 'cadilines',
      component: CadiLines,
      props: route => (
        { 
          year: Number(route.query.year) ? Number(route.query.year) : new Date().getFullYear(), 
          awg: route.query.awg || null
        })
    }, {
      path: $SCRIPT_ROOT + '/cadi/arcs/members',
      name: 'arcmembers',
      component: ArcMembers,
    }, {
      path: $SCRIPT_ROOT + '/cms/institutes',
      name: 'institutes',
      component: Institutes,
      props: true,
    }, {
      path: $SCRIPT_ROOT + '/inst/:urlInstCode?',
      alias: [$SCRIPT_ROOT + '/inst/members/:instCode?'],
      name: 'inst-profile',
      component: InstProfile,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/gm/mo/:mode?/:year?',
      name: 'phd-mo-list',  
      component: PhdMoList,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/al/openLists',
      name: 'open-author-lists',
      component: OpenAuthorLists,
    }, {
      path: $SCRIPT_ROOT + '/al/memberAuthors',
      name: 'member-authors',
      component: MemberAuthors,
    }, {
      path: $SCRIPT_ROOT + '/al/pendingAuthors',
      name: 'pending-authors',
      component: PendingAuthors,
    }, {
      path: $SCRIPT_ROOT + '/prevue/personPicker',
      name: 'prevue-person-picker',
      component: VuetifyPersonPicker
    }, {
      path: $SCRIPT_ROOT + '/epr/correctDue',
      name: 'epr-due-corrector',
      component: DueCorrector
    }, {
      path: $SCRIPT_ROOT + '/epr/dueCorrections',
      name: 'epr-due-corrections',
      component: DueCorrections
    }, {
      path: $SCRIPT_ROOT + '/cms/emeriti/:year?',
      name: 'cms-emeriti',
      component: EmeritusNominations,
      props: (route) => {
        var year = Number(route.params.year);
        if (!year) {
          var today = new Date();
          year = today.getMonth() > 6 ? today.getFullYear() + 1 : today.getFullYear();
        }
        return { year: year };
      }
    }, {
      path: $SCRIPT_ROOT + '/cms/tenures',
      name: 'cms-tenures',
      component: Tenures
    }, {
      path: $SCRIPT_ROOT + '/cms/stats',
      name: 'cms-stats',
      alias: [$SCRIPT_ROOT + '/data/cmsStats'],
      component: MemberStats
    }, {
      path: $SCRIPT_ROOT + '/cms/units/:id?',
      name: 'org-chart',
      component: OrgChart,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/cms/exofficio',
      name: 'exofficio-mandates',
      component: ExOfficioMandates
    }, {
      path: $SCRIPT_ROOT + '/cms/positions',
      name: 'org-positions',
      component: OrgPositions
    }, {
      path: $SCRIPT_ROOT + '/cms/voting/delegate/:instCode?',
      alias: [$SCRIPT_ROOT + '/voting/delegate/:instCode?'],
      name: 'vote-delegations',
      component: VoteDelegations,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/cms/voting/:code?',
      alias: [$SCRIPT_ROOT + '/voting/votingList/:code?', $SCRIPT_ROOT + '/voting/new', $SCRIPT_ROOT + '/voting/list'],
      name: 'voting-list',
      component: VotingList,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/cms/tenures_by_unit',
      name: 'tenures-by-unit',
      component: TenuresByUnit
    }, {
      path: $SCRIPT_ROOT + '/secr/mo/:year?',
      name: 'mo-admin',
      component: MoAdmin,
      props: (route) => {
        var year = route.params.year ? Number(route.params.year) : new Date().getFullYear();
        return {year: year};
      }
    }, {
      path: $SCRIPT_ROOT + '/cms/comms',
      name: 'comms-overview',
      component: CommsOverview,
    },
    {
      path: $SCRIPT_ROOT + '/admin/emails',
      name: 'list-emails',
      component: ListEmails,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/admin/send_email',
      name: 'send-emails',
      component: ComposeEmail,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/admin/announcements',
      name: 'announcements',
      component: Announcements,
      props: true
    }, {
      path: $SCRIPT_ROOT + '/admin/markdown',
      name: 'markdown-editor',
      component: MarkdownEditor,
    }, {
      path: $SCRIPT_ROOT + '/admin/permissions',
      name: 'permissions',
      component: Permissions,
    },{
      path: $SCRIPT_ROOT + '/notes/pending/:noteType?/:year?',
      alias: [$SCRIPT_ROOT + '/notes/workflows/:noteType?/:year?', $SCRIPT_ROOT + '/notes/stubs/:noteType?/:year?'],
      name: 'note-stubs',
      component: NoteStubs,
      props: (route) => {
        return {
          noteType: route.params.noteType,
          year: Number(route.params.year) ? Number(route.params.year) : undefined,
        };
      }
    },{
      path: $SCRIPT_ROOT + '/notes/entries/:noteType?/:year?/:index?',
      alias: [$SCRIPT_ROOT + '/notes'],
      name: 'notes',
      component: Notes,
      props: (route) => {
        return {
          noteType: route.params.noteType,
          year: Number(route.params.year) ? Number(route.params.year) : new Date().getFullYear(),
          index: route.params.index
        };
      }
    }, {
      path: $SCRIPT_ROOT + '/notes/mine',
      name: 'notes-mine',
      component: MyNotes,
    },{
      path: $SCRIPT_ROOT + '/notes/admin',
      name: 'notes-admin',
      component: NotesAdmin
    }, {
      path: $SCRIPT_ROOT + '/notes/secretariat',
      name: 'notes-secretariat',
      component: NotesSecretariat,
    }, {
      path: $SCRIPT_ROOT + '/notes/pending',
      name: 'pending-notes',
      component: PendingNotes
    }, {
      path: $SCRIPT_ROOT + '/gm/cms_week_bookings/:weekId?',
      name: 'bookings',
      component: Bookings
    }, {
      path: $SCRIPT_ROOT + '/gm/cms_weeks',
      name: 'weeks',
      component: Weeks
    }, {
      path: $SCRIPT_ROOT + '/gm/cms_week_rooms',
      name: 'rooms',
      component: Rooms
    }, {
      path: $SCRIPT_ROOT + '*',
      name: 'default',
      component: NotFoundComponent,
    },
  ]
};
export default new Router(routerProperties);
export const createRouter = () => new Router(routerProperties);
 