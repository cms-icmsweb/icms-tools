"""New columns and table for voting lists

Revision ID: 009
Revises: 008
Create Date: 2019-11-11 17:57:36.507720

"""

# revision identifiers, used by Alembic.
revision = '009'
down_revision = '008'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('voting_list_entry',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('code', sa.String(length=16), nullable=False),
    sa.Column('cms_id', sa.Integer(), nullable=False),
    sa.Column('weight', sa.FLOAT(), nullable=False),
    sa.Column('represented_inst_code', sa.String(length=32), nullable=True),
    sa.Column('represented_merger_id', sa.Integer(), nullable=True),
    sa.Column('delegated_by_cms_id', sa.Integer(), nullable=True),
    sa.Column('remarks', sa.Text(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=False, server_default=sa.func.now()),
    sa.ForeignKeyConstraint(['cms_id'], ['public.person.cms_id'], name=op.f('fk_voting_list_entry_cms_id_person'), ondelete='CASCADE', onupdate='CASCADE'),
    sa.ForeignKeyConstraint(['code'], ['toolkit.voting.code'], name=op.f('fk_voting_list_entry_code_voting'), ondelete='CASCADE', onupdate='CASCADE'),
    sa.ForeignKeyConstraint(['delegated_by_cms_id'], ['public.person.cms_id'], name=op.f('fk_voting_list_entry_delegated_by_cms_id_person'), ondelete='CASCADE', onupdate='CASCADE'),
    sa.ForeignKeyConstraint(['represented_inst_code'], ['public.institute.code'], name=op.f('fk_voting_list_entry_represented_inst_code_institute'), ondelete='CASCADE', onupdate='CASCADE'),
    sa.ForeignKeyConstraint(['represented_merger_id'], ['toolkit.voting_merger.id'], name=op.f('fk_voting_list_entry_represented_merger_id_voting_merger'), ondelete='CASCADE', onupdate='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_voting_list_entry')),
    schema='toolkit'
    )
    op.add_column('voting', sa.Column('applicable_mo_year', sa.Integer(), nullable=True), schema='toolkit')
    op.add_column('voting', sa.Column('list_closing_date', sa.Date(), nullable=True), schema='toolkit')


def downgrade():
    op.drop_column('voting', 'list_closing_date', schema='toolkit')
    op.drop_column('voting', 'applicable_mo_year', schema='toolkit')
    op.drop_table('voting_list_entry', schema='toolkit')
    # ### end Alembic commands ###
