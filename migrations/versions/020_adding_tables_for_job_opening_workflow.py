"""adding_tables_for_job_opening_workflow

Revision ID: 020
Revises: 019
Create Date: 2022-12-05 11:36:14.326509

"""

# revision identifiers, used by Alembic.
revision = "020"
down_revision = "019"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.create_table(
        "job_questionnaire",
        sa.Column("id", sa.SmallInteger(), autoincrement=True, nullable=False),
        sa.Column("title", sa.String(length=200), nullable=False),
        sa.Column("questions", sa.String(length=4000), nullable=False),
        sa.Column("date_created", sa.DateTime(), nullable=True),
        sa.Column("date_updated", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_job_questionnaire")),
        schema="toolkit",
    )
    op.create_table(
        "job_opening",
        sa.Column("id", sa.SmallInteger(), autoincrement=True, nullable=False),
        sa.Column("title", sa.String(length=200), nullable=False),
        sa.Column("description", sa.String(length=4000), nullable=False),
        sa.Column("requirements", sa.String(length=4000), nullable=False),
        sa.Column("start_date", sa.Date(), nullable=False),
        sa.Column("end_date", sa.Date(), nullable=True),
        sa.Column("nominations_deadline", sa.Date(), nullable=False),
        sa.Column("days_for_nominee_response", sa.Integer(), nullable=True),
        sa.Column("nominee_no_reply", sa.Boolean(), nullable=True),
        sa.Column("status", sa.String(length=80), nullable=False),
        sa.Column("comment", sa.String(length=1000), nullable=True),
        sa.Column("send_mail_to_nominee", sa.Boolean(), nullable=True),
        sa.Column("questionnaire_uri", sa.String(length=500), nullable=True),
        sa.Column("last_modified", sa.DateTime(), nullable=False),
        sa.Column("questionnaire_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["questionnaire_id"],
            ["toolkit.job_questionnaire.id"],
            name=op.f("fk_job_opening_questionnaire_id_job_questionnaire"),
            onupdate="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_job_opening")),
        schema="toolkit",
    )
    op.create_table(
        "job_open_position",
        sa.Column("id", sa.SmallInteger(), autoincrement=True, nullable=False),
        sa.Column("position_id", sa.SmallInteger(), nullable=False),
        sa.Column("job_unit_id", sa.SmallInteger(), nullable=False),
        sa.Column("job_opening_id", sa.SmallInteger(), nullable=False),
        sa.ForeignKeyConstraint(
            ["job_opening_id"],
            ["toolkit.job_opening.id"],
            name=op.f("fk_job_open_position_job_opening_id_job_opening"),
            onupdate="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["job_unit_id"],
            ["public.org_unit.id"],
            name=op.f("fk_job_open_position_job_unit_id_org_unit"),
            onupdate="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["position_id"],
            ["public.position.id"],
            name=op.f("fk_job_open_position_position_id_position"),
            onupdate="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_job_open_position")),
        schema="toolkit",
    )
    op.create_table(
        "job_nomination",
        sa.Column("nomination_id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("open_position_id", sa.Integer(), nullable=False),
        sa.Column("nominee_id", sa.Integer(), nullable=False),
        sa.Column("questionnaire_answers", sa.String(length=4000), nullable=True),
        sa.ForeignKeyConstraint(
            ["nominee_id"],
            ["public.person.cms_id"],
            name=op.f("fk_job_nomination_nominee_id_person"),
            onupdate="CASCADE",
            ondelete="SET NULL",
        ),
        sa.ForeignKeyConstraint(
            ["open_position_id"],
            ["toolkit.job_open_position.id"],
            name=op.f("fk_job_nomination_open_position_id_job_open_position"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("nomination_id", name=op.f("pk_job_nomination")),
        schema="toolkit",
    )
    op.create_table(
        "job_nomination_status",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("actor_id", sa.Integer(), nullable=False),
        sa.Column("nomination_id", sa.Integer(), nullable=False),
        sa.Column("actor_remarks", sa.String(length=1000), nullable=True),
        sa.Column("status", sa.String(length=128), nullable=False),
        sa.Column("last_modified", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["actor_id"],
            ["public.person.cms_id"],
            name=op.f("fk_job_nomination_status_actor_id_person"),
            onupdate="CASCADE",
            ondelete="SET NULL",
        ),
        sa.ForeignKeyConstraint(
            ["nomination_id"],
            ["toolkit.job_nomination.nomination_id"],
            name=op.f("fk_job_nomination_status_nomination_id_job_nomination"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_job_nomination_status")),
        schema="toolkit",
    )
    op.create_table(
        "job_questionnaire_answers",
        sa.Column("id", sa.SmallInteger(), autoincrement=True, nullable=False),
        sa.Column("answers", sa.String(length=10000), nullable=False),
        sa.Column("questionnaire_id", sa.SmallInteger(), nullable=False),
        sa.Column("job_nomination_id", sa.SmallInteger(), nullable=False),
        sa.Column("submitter_id", sa.Integer(), nullable=False),
        sa.Column("submitted_at", sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(
            ["job_nomination_id"],
            ["toolkit.job_nomination.nomination_id"],
            name=op.f("fk_job_questionnaire_answers_job_nomination_id_job_nomination"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["questionnaire_id"],
            ["toolkit.job_questionnaire.id"],
            name=op.f(
                "fk_job_questionnaire_answers_questionnaire_id_job_questionnaire"
            ),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["submitter_id"],
            ["public.person.cms_id"],
            name=op.f("fk_job_questionnaire_answers_submitter_id_person"),
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_job_questionnaire_answers")),
        schema="toolkit",
    )


def downgrade():
    op.drop_table("job_questionnaire_answers", schema="toolkit")
    op.drop_table("job_nomination_status", schema="toolkit")
    op.drop_table("job_nomination", schema="toolkit")
    op.drop_table("job_open_position", schema="toolkit")
    op.drop_table("job_opening", schema="toolkit")
    op.drop_table("job_questionnaire", schema="toolkit")
