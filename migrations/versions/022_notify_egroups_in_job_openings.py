"""Notify egroups in Job openings

Revision ID: 022
Revises: 021
Create Date: 2024-09-12 16:43:21.941127

"""

# revision identifiers, used by Alembic.
revision = "022"
down_revision = "021"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.add_column(
        "job_opening",
        sa.Column("notify_egroups", sa.String(length=1000), nullable=True),
    )


def downgrade():
    op.drop_column("job_opening", "notify_egroups")
