"""Phd Info

Revision ID: 018
Revises: 017
Create Date: 2023-01-19 09:30:54.745309

"""

# revision identifiers, used by Alembic.
revision = "018"
down_revision = "017"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.create_table(
        "phd_info",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("thesis_title", sa.String(length=10000), nullable=True),
        sa.Column("thesis_link", sa.String(length=10000), nullable=True),
        sa.Column("cms_id", sa.SmallInteger(), nullable=False),
        sa.Column("student_institute", sa.String(), nullable=False),
        sa.Column("enrollment_date", sa.Date(), nullable=True),
        sa.Column("dissertation_defense_date", sa.Date(), nullable=True),
        sa.Column("physics_groups", sa.String(length=10000), nullable=True),
        sa.Column("cms_projects", sa.String(length=10000), nullable=True),
        sa.Column("cadi_analyses", sa.String(length=10000), nullable=True),
        sa.Column("cms_notes", sa.String(length=10000), nullable=True),
        sa.Column("remarks", sa.String(length=10000), nullable=True),
        sa.Column("is_active", sa.Boolean(), nullable=False),
        sa.Column(
            "last_updated_at",
            sa.DateTime(),
            server_default=sa.text("timezone('UTC', now())"),
            nullable=False,
        ),
        sa.Column("last_updated_by", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["cms_id"],
            ["public.person.cms_id"],
            name=op.f("fk_phd_info_cms_id_person"),
            onupdate="CASCADE",
            ondelete="RESTRICT",
        ),
        sa.ForeignKeyConstraint(
            ["last_updated_by"],
            ["public.person.cms_id"],
            name=op.f("fk_phd_info_last_updated_by_person"),
            onupdate="CASCADE",
        ),
        sa.ForeignKeyConstraint(
            ["student_institute"],
            ["public.institute.code"],
            name=op.f("fk_phd_info_student_institute_institute"),
            onupdate="CASCADE",
            ondelete="RESTRICT",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_phd_info")),
        schema="toolkit",
    )


def downgrade():
    op.drop_table("phd_info", schema="toolkit")
