"""permissions

Revision ID: 008
Revises: 007
Create Date: 2019-07-17 12:27:32.302735

"""

# revision identifiers, used by Alembic.
revision = '008'
down_revision = '007'

from sqlalchemy.dialects import postgresql
from alembic import op
import sqlalchemy as sa
from icms_orm.toolkit import resource_type_enum, restricted_action_enum


def upgrade():
    resource_type_enum.create(op.get_bind())
    restricted_action_enum.create(op.get_bind())

    op.create_table('restricted_resource',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('type', resource_type_enum, nullable=False),
    sa.Column('key', sa.String(length=128), nullable=False),
    sa.Column('filters', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_restricted_resource')),
    sa.UniqueConstraint('key', name=op.f('uq_restricted_resource_key')),
    schema='toolkit'
    )

    op.create_table('access_class',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(length=128), nullable=False),
    sa.Column('rules', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_access_class')),
    sa.UniqueConstraint('name', name=op.f('uq_access_class_name')),
    schema='toolkit'
    )

    op.create_table('permission',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=True),
    sa.Column('access_class_id', sa.Integer(), nullable=True),
    sa.Column('action', restricted_action_enum, nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['toolkit.restricted_resource.id'], name=op.f('fk_permission_resource_id_restricted_resource'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['access_class_id'], ['toolkit.access_class.id'], name=op.f('fk_permission_access_class_id_access_class'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_permission')),
    schema='toolkit'
    )


def downgrade():
    op.drop_table('permission', schema='toolkit')
    op.drop_table('access_class', schema='toolkit')
    op.drop_table('restricted_resource', schema='toolkit')
    resource_type_enum.drop(op.get_bind())
    restricted_action_enum.drop(op.get_bind())
