"""table for id provider

Revision ID: 011
Revises: 010
Create Date: 2020-11-24 17:21:33.153611

"""

# revision identifiers, used by Alembic.
import sqlalchemy as sa
from alembic import op
revision = '011'
down_revision = '010'


def upgrade():
    op.create_table('provided_identity',
                    sa.Column('id', sa.Integer(),
                              autoincrement=True, nullable=False),
                    sa.Column('timestamp', sa.DateTime(),
                              nullable=False, server_default='now()'),
                    sa.Column('requested_url', sa.Text(), nullable=False),
                    sa.Column('hr_id', sa.Integer(), nullable=False),
                    sa.Column('username', sa.String(length=64), nullable=True),
                    sa.Column('token', sa.String(64), nullable=True),
                    sa.PrimaryKeyConstraint(
                        'id', name=op.f('pk_provided_identity')),
                    schema='toolkit'
                    )


def downgrade():
    op.drop_table('provided_identity', schema='toolkit')
