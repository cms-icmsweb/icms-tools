"""al_fileset table changes

Revision ID: 012
Revises: 011
Create Date: 2021-03-08 23:02:12.127070

"""

# revision identifiers, used by Alembic.
from sqlalchemy.dialects import postgresql
import sqlalchemy as sa
from alembic import op
revision = '012'
down_revision = '011'


def upgrade():
    op.rename_table('author_list_files', 'al_fileset', schema='toolkit')
    op.drop_column('al_fileset', 'status', schema='toolkit')
    op.create_unique_constraint(op.f('uq_al_fileset_paper_code'), 'al_fileset', [
                                'paper_code'], schema='toolkit')


def downgrade():
    op.drop_constraint(op.f('uq_al_fileset_paper_code'),
                       'al_fileset', schema='toolkit', type_='unique')
    op.add_column('al_fileset', sa.Column('status', sa.String(
        length=64), nullable=True, default='closed'), schema='toolkit')
    op.rename_table('al_fileset', 'author_list_files', schema='toolkit')
