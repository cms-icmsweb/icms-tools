#!/bin/bash
# expected args: job_name, [prod/dev, venv_path]
cd "$(dirname "$0")"

#-todo: find a better way than hardcoding the hostname ...
if [ `hostname` != 'vocms0190.cern.ch' ] ; then
    echo "not running on non-production host " `hostname`
    exit 0
fi

JOB=$1
CONFIG=PROD
VNV=./venv-3.9/bin/activate
if [ ! -z "$2" ]; then CONFIG=$2; fi
if [ ! -z "$3" ]; then VNV=$3; fi


source ${VNV}
PYTHONPATH=./venv-3.9/src/icms-orm/:./venv-3.9/src/icms-common/:${PYTHONPATH} TOOLKIT_CONFIG=${CONFIG} python manage.py script $JOB
