import logging
from util import constants as const
import icms_orm


LOGGING_LEVEL = logging.INFO

# THIS MAP CAN BE OVERRIDDEN IN THE INSTANCE CONFIG OR TEMPLATED USING ENV VARIABLES CORRESPONDING TO THE PARAMETER BELOW
SQLALCHEMY_BINDS = {
        icms_orm.cms_people_bind_key(): 'mysql+pymysql://{legacy_user}@{mysql_host}/{old_people_schema}?charset=utf8'.format(
            legacy_user=icms_orm.icms_legacy_user(),
            old_people_schema=icms_orm.cms_people_schema_name(),
            mysql_host=icms_orm.mysql_host()
        ),
        icms_orm.toolkit_bind_key(): 'postgresql+psycopg2://{toolkit_role}:{icms_role}@{postgres_host}/{icms_role}'.format(
            toolkit_role=icms_orm.toolkit_role_name(),
            icms_role=icms_orm.cms_common_role_name(),
            postgres_host=icms_orm.postgres_host()
        ),
        icms_orm.epr_bind_key(): 'postgresql+psycopg2://{epr_role}:{icms_role}@{postgres_host}/{icms_role}'.format(
            epr_role=icms_orm.epr_role_name(), 
            icms_role=icms_orm.cms_common_role_name(),
            postgres_host=icms_orm.postgres_host()
        ),
        icms_orm.cms_common_bind_key(): 'postgresql+psycopg2://{icms_role}:{icms_role}@{postgres_host}/{icms_role}'.format(
            icms_role=icms_orm.cms_common_role_name(),
            postgres_host=icms_orm.postgres_host()
        )
    }

# ALEMBIC MESSES THINGS UP UNLESS THE FOLLOWING IS SPECIFIED (NEEDS TO BE OVERRIDDEN IN instance/config.py AND SHOULD POINT TO THE ADDONS DB)
SQLALCHEMY_DATABASE_URI = SQLALCHEMY_BINDS[const.OPT_NAME_BIND_TOOLKIT]

LDAP_MOCK_ENTRIES = [
    {
        "cmsEgroups": ["cms-award-admins"],
        "accountStatus": "Active",
        # "cernEgroups": "foo",
        "hrId": 422405,
        "familyName": "Pfeiffer",
        "firstName": "Andreas",
        "login": "pfeiffer",
        "mail": "pfeiffer@cern.ch",
        "fullName": "Andreas Pfeiffer",
        "institute": "CERN",
        "department": "EP",
        "gid": 1399,
        "office": "500",
    }
]
