#!/usr/bin/env python3

import os
import re
import pprint
import datetime

def analyseFile( fileNameIn ):
    
    with open( fileNameIn, 'r', encoding='utf-8' ) as iF:
        lines = iF.readlines()
        print( f'found {len(lines)} lines in {fileNameIn}' )
        
        skipRe = re.compile( r'^DEBUG:root:Skipping authorship application check for #(\d+)\s*$' )
        resultRe  = re.compile ( r'^INFO:scripts.check_authorship_application:(?P<name>.*) \[\#(?P<cmsId>\d+), (?P<inst>[A-Z-]+)\]: Application failed \[(?P<reason>.*)\]. (?P<daysApp>\d+) days as applicant, (?P<workApp>[\d.]+) EPR work done.*$' )
        titleRe0 = re.compile( r'^(?P<date>2024-\d\d-\d\d \d\d:\d\d:\d\d)?\,\d+ \[INFO\]: Authorship application check run (?P<run>\d+). Target applicant due: (?P<reqAppDue>[\d.]+) .*$' )
        titleRe1 = re.compile( r'^INFO:root:Authorship application check run (?P<run>\d+). Target applicant due: (?P<reqAppDue>[\d.]+).*$' )

        skippedIds = []
        info = []
        startFound = False
        metaData = {}
        for line in [ x.strip() for x in lines ]:
            if line.startswith( 'INFO:root:Authorship application check run' ): 
                startFound = True
                titleMatch = titleRe0.match(line)
                if titleMatch: metaData = titleMatch.groupdict()
                else:
                    titleMatch = titleRe1.match(line)
                    if titleMatch: metaData = titleMatch.groupdict()
                print( f'starting line found ... match: {titleMatch}')
                
            if not startFound: continue
            if line.strip().startswith('2024'): continue
            
            if 'Skipping authorship application check for ' in line:
                skipMatch = skipRe.match(line)
                if not skipMatch:
                    print( f'ERROR: no match found in line for skipping: {line}')
                skippedIds.append( int(skipMatch.group(1)) )
                continue
            
            resultMatch = resultRe.match( line )
            if resultMatch: 
                # print( resultMatch.groups() )
                info.append( resultMatch.groupdict() )
                continue
            
            # print( line )

        if 'date' not in metaData or not metaData['date'] : metaData['date'] = str(datetime.datetime.now())

        print( metaData )

        return skippedIds, info, metaData

def show( skippedIds, info, metaData ):
    
    print( f'skipped {len(skippedIds)} : ')
    # print( skippedIds )

    # pprint.pprint( info )

    print( f'metadata: {metaData}')


def writePage( info, metaData ):
    
    with open( 'appCheckResults.html', 'w', encoding='utf-8') as hf:
        headers = [ 'id', 'cmsId', 'name', 'inst', 'reason', 'daysApp', 'workApp' ]

        hf.write( '''
<!DOCTYPE html>
<html>
  <head>

    
    <title>
    Results from application check
    </title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.20/af-2.3.4/b-1.6.1/b-colvis-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/fc-3.3.0/fh-3.1.6/kt-2.5.1/r-2.2.3/rr-1.2.6/sc-2.0.1/sp-1.0.1/sl-1.3.1/datatables.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <!-- DataTables -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.1.0/js/dataTables.select.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedheader/3.1.0/js/dataTables.fixedHeader.min.js"></script>


<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/plug-ins/1.10.11/api/sum().js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>

  </head>
  <body>  
''')

        hf.write( f'  <h2> Results from application check -- run {metaData["run"]} on {metaData["date"]}</h2>')
        hf.write( '''
    <table id="resultTable" class="dataTable" cellspacing="0" width="90%">
      <thead>
        <tr>
''')
        for h in headers:
            hf.write( f'     <th> {h} </th>\n' )
        hf.write( f'''
                </thead>
                <tbody>''')

        index = 0
        for item in info:
            hf.write( '<tr>\n' )
            index += 1
            hf.write( f'<td> {index} </td>\n' )
            for h in headers[1:]:
                hf.write( f'     <td> {item[h]} </td>\n' )
            hf.write( '</tr>\n' )

        hf.write( f'''
                </tbody>
                <tfoot>
                    <tr>\n''')
        for h in headers:
            hf.write( f'   <th> {h} </th>\n' )
        hf.write( f'''
                    </tr>
                </tfoot>
            </table>
''')
        hf.write( '''
   <script language="JavaScript"> 
    $(document).ready(function() { 

function setupColSearch(table) {

    // Restore state
    var state = table.state.loaded();
    if ( state ) {
      table.columns().eq( 0 ).each( function ( colIdx ) {
        var colSearch = state.columns[colIdx].search;
        if ( colSearch.search ) {
          $( 'input', table.column( colIdx ).footer() ).val( colSearch.search );
        }
      } );
      table.draw();
    }

    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                // console.log(this);
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $("tfoot input").css( { "width" : "100%", "padding" : "3px", "box-sizing" : "border-box" } );
}
        var resTable = $("#resultTable").DataTable({ 
        "fixedHeader" : true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "dom": '<"top"lifr>t<"bottom"flp>'
        });  

    setupColSearch( resTable );

    }); // end document.ready ...  
   </script>
  </body>
</html>
''')

if __name__ == '__main__':
    skippedIds, info, metaData = analyseFile( 'appCheck.log' )
    # show(skippedIds, info, metaData)
    writePage( info, metaData )
