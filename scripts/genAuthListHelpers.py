# -*- coding: utf-8 -*-

import re

texReplOld = { u"&nbsp;" : "~" ,
            u"&#039" : "'" ,
            # u" "  : "~" ,
            u"&"  : "\\&" ,
            u",~" : "," ,
            u"é"  : "\\'{e}",
            u"Ã©" : "\\'{e}",
            u"è"  : "\\`{e}",
            u"Ã¨" : "\\`{e}",
            u"á"  : "\\'{a}",
            u"Ã¡" : "\\'{a}",
            u"ã"  : "\\~{a}",
            u"Ã£" : "\\~{a}",
            u"ñ"  : "\\~{n}",
            u"Ã±" : "\\~{n}",
            u"í"  : "\\'{i}",
            u"Ã­"  : "\\'{i}",
            u"ç"  : "\\c{c}",
            u"Ã§" : "\\c{c}",
            u"à"  : "\\`{a}",
            u"Ã " : "\\`{a}",
            u"ò"  : "\\`{o}",
            u"Ã²" : "\\`{o}",
            u"ó"  : "\\'{o}",
            u"Ã³" : "\\'{o}",
            u"ä"  : "\\\"{a}",
            u"Ã¤" : "\\\"{a}",
            u"ü"  : "\\\"{u}",
            u"Ã¼" : "\\\"{u}",
            u"ú"  : "\\\'{u}",
            u"Ãº" : "\\\'{u}",
            u"ö"  : "\\\"{o}",
            u"Ã¶" : "\\\"{o}",
            u"Ö"  : "\\\"{O}",
            u"Ã" : "\\\"{O}",
            u"&dagger" : "\\dag",
            u"Ä±" : "\\i",
            }

# Latex turkish characters: https://ergut.wordpress.com/2008/03/16/hello-world/

# Character Entity Reference Chart:
# https://dev.w3.org/html5/html-author/charref

texRepl = { u"&#039;" : "'",
            u"&#171;" : '``',
            u"&#187;" : "''",
            u"&#193;" : "\\'{A}",
            u"&#199;" : '\\c{C}',
            u"&#214;" : '\\"{O}',
            u"&#223;" : '{\\ss}',
            u"&#224;" : "\\`{a}",
            u"&#225;" : "\\'{a}",
            u"&#227;" : "\\~{a}",
            u"&#228;" : '\\"{a}',
            u"&#231;" : '\\c{c}',
            u"&#232;" : "\\`{e}",
            u"&#233;" : "\\'{e}",
            u"&#237;" : "\\'{i}",
            u"&#241;" : "\\~{n}",
            u"&#242;" : "\\`{o}",
            u"&#243;" : "\\'{o}",
            u"&#246;" : '\\"{o}',
            u"&#250;" : "\\'{u}",
            u"&#252;" : '\\"{u}',
            u"&#263;" : "\\'{c}",
            u"&#286;" : '\\u{G}',
            u"&#287;" : '\\u{g}',
            u"&#305;" : "{\\i}",
            u"&#350;" : "\\c{S}",
            u"&quot;" : "'",
            u'\xe4': '\\"{a}',
            u'\xfc': '\\"{u}',
            u"Á"  : "\\'{A}",
            u"á"  : "\\'{a}",
            u"à"  : "\\`{a}",
            u"Ã¡" : "\\'{a}",
            u"Ã " : "\\`{a}",
            u"ä"  : "\\\"{a}",
            u"Ã¤" : "\\\"{A}",
            u"ç"  : "\\c{c}",
            u"ć"  : "\\'{c}",
            u"Ã§" : "\\c{c}",
            u"é"  : "\\'{e}",
            u"Ã©" : "\\'{e}",
            u"è"  : "\\`{e}",
            u"Ã¨" : "\\`{e}",
            u"ã"  : "\\~{a}",
            u"Ã£" : "\\~{a}",
            u"í"  : "\\'{i}",
            u"Ã­" : "\\'{i}",
            u"Ä±" : "{\\i}",
            u"ñ"  : "\\~{n}",
            u"Ã±" : "\\~{n}",
            u"Ö"  : "\\\"{O}",
            u"Ã" : "\\\"{O}",
            u"ö"  : "\\\"{o}",
            u"Ã¶" : "\\\"{o}",
            u"ò"  : "\\`{o}",
            u"Ã²" : "\\`{o}",
            u"ó"  : "\\'{o}",
            u"Ã³" : "\\'{o}",
            u"ü"  : "\\\"{u}",
            u"Ü"  : "\\\"{U}",
            u"Ã¼" : "\\\"{u}",
            u"ú"  : "\\\'{u}",
            u"Ãº" : "\\\'{u}",
            u"Ç" : "\\c{C}",
            u"&dagger" : "\\dag",
            }

def convert( aName ):
    try :
        newName = aName # .replace("&amp;", '&')
        for k, v in texRepl.items():
            newName = newName.replace( k, v )

        return newName
    except :
        print( "convert> got ERROR when fixing: ", aName )
        raise

def utf8ToTex( aNameIn ):

    aName = aNameIn.replace( '&amp;', '&' )  # catch some rare cases ...

    try:
        aName = aName.encode( 'latin-1', 'xmlcharrefreplace' )
    except UnicodeEncodeError as e:
        aName = aName.encode('utf-8', 'xmlcharrefreplace')

    newName = convert( aName.decode('latin-1') )

    return newName


# various FOAF templates

class foafTemplates(object):

    #  { 'creationtime' : '2016-06-11_19:50' }
    foafTop = u"""<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE collaborationauthorlist SYSTEM "authors.dtd">
<collaborationauthorlist
xmlns:foaf="http://xmlns.com/foaf/0.1/"
xmlns:cal="http://www.slac.stanford.edu/spires/hepnames/authors_xml/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<cal:creationdate>%(creationtime)s</cal:creationdate>
<cal:collaborations>
<cal:collaboration>
<foaf:name>CMS</foaf:name>
<cal:experimentNumber></cal:experimentNumber>
</cal:collaboration>
</cal:collaborations>
"""

    # { 'url' : 'http://bg.ac.yu' }
    foafOrgUrl = u'<cal:orgDomain rdf:resource="%(url)s"/>'


    #  { 'spiresICN' : 'VINCA Inst. Nucl. Sci., Belgrade', }
    fofaSpires = u'<cal:orgName source="spiresICN">%(spiresICN)s</cal:orgName>'

    # spires = [ fofaSpires % { 'spiresICN' : 'VINCA Inst. Nucl. Sci., Belgrade', } ]
    # urls = foafOrgUrl % { 'url' : 'http://bg.ac.yu' }
    # { 'orgId' : 'o105',
    #   'url' : '\n'.join(urls),
    #   'name' : 'University of Belgrade, Faculty of Physics and Vinca Institute of Nuclear Sciences, Belgrade, Serbia<',
    #   'spires' : '\n'.join(spires).strip(),
    #   'instId' : 1149,
    #   'status' : 'CMS',
    #   'address' : '',
    #   'member' : 'member', # or: 'nonmember' }
    foafOrganisation = u"""<foaf:Organization id="%(orgId)s">
%(urls)s
<foaf:name>%(name)s</foaf:name>
%(spires)s
<cal:orgName source="InstId">%(instId)i</cal:orgName>
<cal:orgStatus>%(status)s</cal:orgStatus>
<cal:orgAddress>%(address)s</cal:orgAddress>
<cal:orgStatus>%(member)s</cal:orgStatus>
</foaf:Organization>
"""

    foafOrganisationDep = u"""<foaf:Organization id="%(orgId)s">
%(urls)s
<foaf:name>%(name)s</foaf:name>
%(spires)s
<cal:orgName source="InstId">%(instId)s</cal:orgName>
<cal:orgStatus>%(status)s</cal:orgStatus>
<cal:orgAddress>%(address)s</cal:orgAddress>
<cal:group with="%(parentInst)s"/>
<cal:orgStatus>%(member)s</cal:orgStatus>
</foaf:Organization>
"""


    foafAffiliation = u"""<cal:authorAffiliation connection="%(conn)s" organizationid="%(orgId)s"/>"""

    foafID = u"""<cal:authorid source="%(src)s">%(id)s</cal:authorid>"""


    # affils = [ foafAffiliation % { 'orgId' : 'o1',
    #                                'conn' : '' # or: 'AlsoAt'
    #              } ]

    # ids = [ foafID % { 'src' : "INSPIRE", 'id' : "INSPIRE-00314584" },
    #         foafID % { 'src' : "CCID", 'id' : '667227' },
    #         ]

    # { 'status' : '',  # or: 'deceased'
    #   'name' : 'Vardan Khachatryan',
    #   'firstName' : 'Vardan',
    #   'lastName' : 'Khachatryan',
    #   'namePaper' : 'V. Khachatryan',
    #   'authorAffiliations' : '\n'.join(affils),
    #   'authorIDs' : '\n'.join(ids),
    # }
    foafAuthor = u"""<foaf:Person>
<foaf:name>%(name)s</foaf:name>
<foaf:givenName>%(firstName)s</foaf:givenName>
<foaf:familyName>%(lastName)s</foaf:familyName>
<cal:authorNameNative lang=""></cal:authorNameNative>
<cal:authorSuffix>%(suffix)s</cal:authorSuffix>
<cal:authorStatus>%(status)s</cal:authorStatus>
<cal:authorNamePaper>%(namePaper)s</cal:authorNamePaper>
<cal:authorAffiliations>
%(authorAffiliations)s
</cal:authorAffiliations>
<cal:authorids>
%(authorIDs)s
</cal:authorids>
</foaf:Person>
"""
