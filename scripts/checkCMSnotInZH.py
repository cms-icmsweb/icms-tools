
import os
import time, datetime
import json
import logging

from flask import Flask

from icms_orm.cmspeople import Person

from util.factory import WebappOrmManager
from util.EgroupHandler import EgroupHandler

def getZhMembers():
    egh = EgroupHandler()
    zhMembers = egh.getGroupMembers('zh')

    # print( "%s members found: %s " % (len(zhMembers[0]), zhMembers[0][:3]) )

    return zhMembers[0]

def getFromCacheFile():
    cacheFileName = '.zhMemCache'
    print( 'mtime: ', time.time() -os.path.getmtime(cacheFileName), time.time(), os.path.getmtime(cacheFileName) )
    if ( os.path.exists(cacheFileName) and
            (time.time() - os.path.getmtime(cacheFileName) < 1800) ):
        print( "using cache ... " )
        return  json.loads( str( open(cacheFileName, 'r').read() ) )
    else:
        print( "refreshing cache ... " )
        zhMems = getZhMembers()
        with open(cacheFileName,'w') as cFile:
            cFile.write( json.dumps(zhMems) )
        return zhMems

def getDBUsers(db):

    cmsPeople = Person.query.filter( Person.hrId > 0 ).filter( Person.zhFlag == 0 ).filter( Person.status.like('CMS%') ).order_by( Person.cmsId.desc() ).all()
    exPeople  = [] # Person.query.filter( Person.hrId > 0 ).filter( Person.zhFlag == 0 ).filter( Person.status == 'EXMEMBER' ).order_by( Person.cmsId.desc() ).all()

    logging.info( 'found %s active people in DB, %s exmembers w/o zhFlag' % (len(cmsPeople), len(exPeople)) )

    return cmsPeople

def main():

    zhMems = [ int(x.split(';')[1]) for x in getFromCacheFile() ] # extract hrID
    print( "%s members found: %s " % (len(zhMems), zhMems[:3]) )

    theApp = Flask( __name__, instance_relative_config=True )
    theApp.config.from_pyfile( 'config.py' )

    with theApp.app_context() :
        db = WebappOrmManager( theApp )

        isIn = []
        isOut = []
        dbMems = getDBUsers( db )
        for mem in dbMems:
            if int(mem.hrId) in zhMems: isIn.append( mem )
            else: isOut.append( mem )
        print( "found %d in, %d out ... " % (len(isIn), len(isOut)) )
        print( '' )
        for mem in isIn: print( "  + %s (%s):  %s, %s - [%s]" % (mem.cmsId, mem.hrId, mem.lastName.encode( 'ascii', 'xmlcharrefreplace' ).encode( 'utf8' ), mem.firstName.encode( 'ascii', 'xmlcharrefreplace' ).encode( 'utf8' ), mem.dateCreation) )
        print( '' )
        for mem in isOut: print( "  - %s (%s):  %s, %s - [%s]" % (mem.cmsId, mem.hrId, mem.lastName.encode( 'ascii', 'xmlcharrefreplace' ).encode( 'utf8' ), mem.firstName.encode( 'ascii', 'xmlcharrefreplace' ).encode( 'utf8' ), mem.dateCreation) )

main()
