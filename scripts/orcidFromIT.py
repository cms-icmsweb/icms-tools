
# run either via:
# (source ./venv-py3/bin/activate; time  PYTHONPATH=. PYTHONIOENCODING=utf-8 TOOLKIT_CONFIG=TEST python ./scripts/orcidFromIT.py doIt )
# or - for testing:
# (source ./venv-py3/bin/activate; time  PYTHONPATH=. PYTHONIOENCODING=utf-8 TOOLKIT_CONFIG=TEST python ./scripts/orcidFromIT.py dryRun )

import os
import sys
import datetime
import time
import logging
import json

from flask import Flask
from sqlalchemy.orm.exc import  NoResultFound, MultipleResultsFound

from icms_orm.common import UserAuthorData
from icms_orm.cmspeople import Person as PeopleData

sys.path.append( '.' )
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

import webapp
import app_profiles

def getOrcIdInfoFromJson( jsonFileName ) :

    idMap = {}
    noOrcid = 0
    invalid = 0
    total = 0
    with open (jsonFileName, 'r') as jF:
        itemList = json.load( jF )   # ['data']
        total = len(itemList)
        for item in itemList:
            if 'pagination' in item: continue
            try:
                # print( f'item: {item}')
                if 'personId' not in item.keys() or item['personId'] is None:
                    invalid += 1
                    continue
                hrId = int( item['personId'].strip() )
                if 'orcid' not in item.keys():
                    noOrcid += 1
                    continue
                idMap[ hrId ] = item['orcid'].strip()
            except AttributeError:
                print( f'ERROR: invalid item found: {item} - skipping' )
                continue
            
    print( f'\nfound {len(idMap)} valid entries (out of {total}) with OrcIDs -- {noOrcid} w/o OrcIDs (and {invalid} invalid ones (no personID))\n' )

    return idMap


class UserFinder(object):

    def __init__(self, db):
        self.db = db

        self.idMap = {}
        ids = (self.db.session.query( PeopleData.hrId, PeopleData.cmsId).all())
        for h, c in ids:
            self.idMap[ int(h) ] = int(c)


        self.allUsers = { }
        allUsersDB = (self.db.session.query( UserAuthorData.hrid, UserAuthorData.cmsid, UserAuthorData.orcid, UserAuthorData.lastupdate)
                   .filter( UserAuthorData.orcid != '' )
                   .all())

        for hrid, cmsid, orcid, lastupdate in allUsersDB :
            if orcid in self.allUsers.keys( ) : continue
            self.allUsers[ orcid ] = (hrid, cmsid, orcid, lastupdate)

        print( "UserFinder> got %d users with orcids from CMS DB" % len( self.allUsers.keys() ) )

    def findUserByOrcId(self, orcid):

        if orcid.strip( ) == '' :
            return None

        return self.allUsers[ orcid ] if orcid in self.allUsers.keys() else None

    def findUserByHRid(self, hrId):

        for orcId, info in self.allUsers.items():
            if int(hrId) == int(info[0]):
                return info
        return None

def updateOrcIDsFromITdbInfo(inFileName, db):

    orcIdMap = getOrcIdInfoFromJson( inFileName )
    uf = UserFinder(db)

    index = 0
    nNoInfo = 0
    nDone = 0
    nKnown = 0
    conflicts = []
    for hrId, orcId in orcIdMap.items():
        uInfo = uf.findUserByOrcId( orcId )
        if uInfo: # orcId already in DB, check consistency
            index += 1
            # if index<5: print( hrId, orcId, uInfo )
            if orcId.strip() != uInfo[2].strip():
                print( f"inconsistency: json orcId {orcId} maps to already existing {uInfo} in CMS DB " )
        else:
            uByHr = uf.findUserByHRid( hrId )
            if uByHr is None: # clean entry, no hrId nor orcId already known
                if int(hrId) not in uf.idMap.keys():
                    print ( f'user for hrId {hrId} not found in CMS DB ... skipping ')
                    nNoInfo += 1
                    continue
                if len(sys.argv) > 1 and 'dryRun' not in sys.argv[1]:
                    uad = UserAuthorData(hrid=hrId, cmsid=uf.idMap[ int(hrId) ], inspireid='', orcid=orcId)
                    db.session.add( uad )
                nDone += 1
            else:
                if uByHr[2].strip() != orcId.strip():
                    print( f'ERROR: different OrcIDs found for {hrId}: json: {orcId} vs. DB: {uByHr}')
                    conflicts.append( (hrId, orcId, uByHr) )
                nKnown += 1

    if len(sys.argv) > 1 and 'dryRun' not in sys.argv[1]:
        db.session.commit()
        
    print( "found %d users w/o inspId " % nNoInfo )
    print( f"added {nDone} users to CMS DB ({nKnown} already known)" )
    print( f'found {len(conflicts)} conflicting entries' )

def main():

    theApp = webapp.Webapp(__name__, '.', app_profiles.ProfileDev)
    with theApp.app_context():
        db = theApp.db

        # jsonFileName = 'response_1677084745844.json'
        # jsonFileName = 'auth-service-orcids-2023-02-24.json'
        # jsonFileName = 'auth-service-orcids-2023-02-28.json'
        jsonFileName = 'auth-service-orcids-2023-03-06.json'
        # jsonFileName = 'HRdb-OrcIDs-20230220.json' # './HRdb-20221215.json'
        updateOrcIDsFromITdbInfo( jsonFileName, db)

    return 0

if __name__ == '__main__':
    retCode = main()

