"""Generate SQL for renaming a MySQL institute.

Specifically, change its code from old_code to new_code while
preserving most of its history.

Run this script as such:
TOOLKIT_CONFIG=DEV python manage.py script rename-institute-sql \
    --old-inst-code=DEBRECEN-IEP --new-inst-code=DEBRECEN-UD
Replace DEV with PREPROD or PROD as necessary.
"""

from datetime import date

import icms_orm
from icms_orm.cmspeople import (
    Institute,
    InstStatusValues,
    MoData,
    Person,
    PersonHistory,
)
from icmscommon.governors import DbGovernor
from sqlalchemy import func, literal_column, select, update
from sqlalchemy.orm import Session


class InstCodeUpdater:
    """Helper facilitating institute code update SQL generation."""

    def __init__(self, old_code, new_code):
        self.old_code = old_code
        self.new_code = new_code
        cms_people_bind = icms_orm.cms_people_bind_key()
        self.engine = DbGovernor.get_db_manager().get_engine(bind=cms_people_bind)
        self.session = Session(bind=self.engine)
        self.statements = []

    def clone_inst_with_new_code(self):
        inst_table = Institute.__table__
        inst_columns = inst_table.columns
        inst_code_col_name = Institute.code.name
        select_list = [
            literal_column(f"'{self.new_code}'").label(inst_code_col_name)
            if col.name == inst_code_col_name
            else col
            for col in inst_columns
        ]
        select_stmt = (
            select(select_list)
            .select_from(inst_table)
            .where(Institute.code == self.old_code)
        )
        insert_stmt = inst_table.insert().from_select(inst_columns, select_stmt)
        self.statements.append(insert_stmt)

    @staticmethod
    def get_phd_ic_column_for_year(year):
        return getattr(MoData, MoData.phdInstCode2015.key.replace("2015", str(year)))

    def update_recent_mo_data(self):
        current_year = date.today().year
        prev_year = current_year - 1
        phd_ic_cols = [
            self.get_phd_ic_column_for_year(year) for year in (current_year, prev_year)
        ]
        mo_table = MoData.__table__
        phd_ic_updates = [
            update(mo_table)
            .where(col == self.old_code)
            .values({col.name: self.new_code})
            for col in phd_ic_cols
        ]
        self.statements.extend(phd_ic_updates)

    def update_affiliations(self):
        people_data = Person.__table__
        people_history = PersonHistory.__table__
        join_condition = PersonHistory.cmsId == Person.cmsId
        update_stmt = (
            update(people_data.join(people_history, join_condition))
            .values(
                {
                    Person.instCode: self.new_code,
                    PersonHistory.history: func.concat(
                        "cmswww_",
                        # the production version doesn't support to_char
                        # func.to_char(func.current_date(), "DD/MM/YYYY"),
                        func.date_format(func.current_date(), "%d/%m/%Y"),
                        f"-InstCode:{self.old_code}\n",
                        PersonHistory.history,
                    ),
                }
            )
            .where(Person.instCode == self.old_code)
        )
        self.statements.append(update_stmt)

    def deactivate_old_inst(self):
        inst_table = Institute.__table__
        update_stmt = (
            update(inst_table)
            .values(
                {
                    Institute.cmsStatus: InstStatusValues.NO,
                    Institute.cernInstId: -Institute.cernInstId,
                }
            )
            .where(Institute.code == self.old_code)
        )
        self.statements.append(update_stmt)

    def generate_sql(self):
        self.clone_inst_with_new_code()
        self.update_recent_mo_data()
        self.update_affiliations()
        self.deactivate_old_inst()
        # replacing %% as compile duplicates % and production doesn't support to_char
        return [
            str(
                statement.compile(self.engine, compile_kwargs={"literal_binds": True})
            ).replace("%%", "%")
            for statement in self.statements
        ]


def main(old_code, new_code):
    print(f"Compiling SQL for updating the inst code from {old_code} to {new_code}:\n")
    print("\n;\n".join(InstCodeUpdater(old_code, new_code).generate_sql()), end="\n;\n")
