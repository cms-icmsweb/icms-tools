"""Provide simple stats for project members for recent years.

Specifically, provide the bumber of paid M&O members, Doctoral Students
and authors per declared main project since 2015.

If a member changed projects during a year, then double-counting will occur.

Run this script as such:
TOOLKIT_CONFIG=DEV python manage.py script project-member-stats
Replace DEV with PREPROD or PROD as necessary.

The output is stored as an Excel file under /tmp/project-member-stats .
"""

import stat
from collections import defaultdict
from datetime import date
from pathlib import Path

import pyexcel
from icms_orm.cmspeople import MoData
from icms_orm.epr import Category, TimeLineUser


def process_mo_data(year_range):
    paid_mo_cols = [MoData.get_phd_mo_column_for_year(year) for year in year_range]
    mo_data = MoData.session().query(MoData.cmsId, *paid_mo_cols).all()
    paid_mo_by_user = {
        cms_id: [yes_no == "YES" for yes_no in paid_mo_str]
        for cms_id, *paid_mo_str in mo_data
    }
    return paid_mo_by_user


def transform_cms_id_sets_to_counts(project_year_sets):
    for pys in project_year_sets:
        for project in pys:
            pys[project] = [len(year_set) for year_set in pys[project]]


def process_project_stats_for_pyexcel(project_member_stats, start_year, year_range):
    authors, paid_mo_members, doctoral_students = project_member_stats
    all_projects = sorted(
        set(authors.keys())
        | set(paid_mo_members.keys())
        | set(doctoral_students.keys())
    )
    stats_by_year = {
        str(year): [["Project", "N_authors", "N_paid_M&O", "N_doctoral_students"]]
        for year in year_range
    }
    total_str = "Total"
    for stats in stats_by_year.values():
        for project in all_projects:
            stats.append([project, 0, 0, 0])
        stats.append([total_str, 0, 0, 0])
    project_to_index = {
        project: index + 1 for index, project in enumerate(all_projects)
    }
    project_to_index[total_str] = len(all_projects) + 1
    for stat_ind, project_member_stat in enumerate(project_member_stats, 1):
        yearly_sum_across_projects = [0] * len(year_range)
        for project, year_counts in project_member_stat.items():
            for year_offset, count in enumerate(year_counts):
                year_str = str(start_year + year_offset)
                stats_by_year[year_str][project_to_index[project]][stat_ind] = count
                yearly_sum_across_projects[year_offset] += count
        for yearly_stats, yearly_total in zip(
            stats_by_year.values(), yearly_sum_across_projects
        ):
            yearly_stats[project_to_index[total_str]][stat_ind] = yearly_total
    return stats_by_year


def compute_project_member_stats():
    start_year = 2015
    current_year = date.today().year
    year_range = range(start_year, current_year + 1)
    paid_mo_by_user = process_mo_data(year_range)
    timeline_query = (
        TimeLineUser.session()
        .query(
            TimeLineUser.cmsId,
            TimeLineUser.year,
            TimeLineUser.mainProj,
            TimeLineUser.isAuthor,
            Category.name,
        )
        .join(Category, TimeLineUser.category == Category.id)
        .filter(TimeLineUser.cmsId > 0, TimeLineUser.status.like("CMS%"))
    )
    timelines = timeline_query.all()
    year_set_intializer = lambda: [set() for _ in year_range]
    project_authors = defaultdict(year_set_intializer)
    project_paid_mo_members = defaultdict(year_set_intializer)
    project_doctoral_students = defaultdict(year_set_intializer)
    for timeline in timelines:
        cms_id, year, project, is_author, activity = timeline
        if project is None or project == "":
            project = "-"
        if is_author:
            project_authors[project][year - start_year].add(cms_id)
        if activity == "Doctoral Student":
            project_doctoral_students[project][year - start_year].add(cms_id)
        if paid_mo_by_user[cms_id][year - start_year]:
            project_paid_mo_members[project][year - start_year].add(cms_id)
    project_member_stats = (
        project_authors,
        project_paid_mo_members,
        project_doctoral_students,
    )
    transform_cms_id_sets_to_counts(project_member_stats)
    return process_project_stats_for_pyexcel(
        project_member_stats, start_year, year_range
    )


def export_stats(processed_project_member_stats):
    today = date.today()
    filepath = Path("local", f"project-member-stats-{today}.xlsx")
    directory = filepath.parent
    directory.mkdir(mode=stat.S_IRWXU, parents=True, exist_ok=True)
    book = pyexcel.get_book(bookdict=processed_project_member_stats)
    book.save_as(str(filepath))
    return filepath


def main():
    processed_project_member_stats = compute_project_member_stats()
    filepath = export_stats(processed_project_member_stats)
    print(f"Project member stats can be found in {filepath}")
