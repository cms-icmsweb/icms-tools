import sys
import os
import datetime
import logging
import json

for sysPath in ['.', './scripts']:
    if sysPath not in sys.path:
        sys.path.append( sysPath )

from icms_orm.cmspeople import Person
from icmsutils.ldaputils import LdapProxy
from util import accountCheck
from util.EgroupHandler import EgroupHandler
from icmscommon.governors import DbGovernor
theLogger = logging


class ProcInfo(object):

    def __init__(self):

        self.procFileName = 'processedPeople.json'
        self.peopleProcessed = {}
        if os.path.exists( self.procFileName ):
            with open(self.procFileName, 'r') as procFile:
                self.peopleProcessed = json.load(procFile)

    def wasProcessed(self, hrId):
        return str(hrId) in self.peopleProcessed.keys()

    def mailSent(self, hrId, whatFor):
        if whatFor in self.peopleProcessed[str(hrId)]['mailSent']:
            return True
        return False

    def updateInfo(self, hrId, info):
        self.peopleProcessed[str(hrId)] = info

    def write(self):
        with open(self.procFileName, 'w') as procFile:
            json.dump(self.peopleProcessed, procFile)

    def get(self, hrId):
        return self.peopleProcessed[ str(hrId) ]

def getNewUsers(db=None):
    db = db or DbGovernor.get_db_manager()
    startDate = datetime.datetime.utcnow() - datetime.timedelta( days = 1 )
    logging.info("checking accounts registered since: %s " % startDate)
    cmsPeople = Person.query.filter( Person.hrId > 0 )\
                            .filter( Person.dateCreation > startDate )\
                            .order_by( Person.cmsId.desc() ).all()

    logging.info( 'found %s people in DB since %s' % (len(cmsPeople), startDate) )

    procInfo = ProcInfo()

    ldap = LdapProxy.get_instance()

    nProc = -1
    for p in cmsPeople:

        logging.info(" ++++++++++++++++++++ \n")

        fullName = p.lastName+', '+p.firstName

        ldap_user = ldap.get_person_by_hrid( p.hrId )
        if not ldap_user:
            logging.error( f"ERROR: no user in LDAP found for hrId: {p.hrId} ({fullName}) -- skipping check ... ")
            continue

        login = ldap_user.login.strip()

        if login == '':
            logging.info( "no account set yet for cmsId: %s user '%s' -- skipping check " % (p.cmsId, fullName) )
            continue

        ac = accountCheck.AccountChecker( infoOK=True, verbose=False )
        ac.check( p.hrId )

        checkResult = accountCheck.analyse( ac.issues )

        if procInfo.wasProcessed( p.hrId ):
            info = procInfo.get( p.hrId )
            logging.info( 'Skipping processing of cmsId: %s user "%s" -- already processed ... ' % (p.cmsId, fullName))
            continue
        else:
            info = { 'cmsId': p.cmsId, 'hrId': p.hrId, 'user': fullName, 'account': login, 'regDate': p.dateRegistration.isoformat(),
                     'issues': None, 'mailSent': ['zh-OK'] }
            procInfo.updateInfo( p.hrId, info)

        nProc += 1
        if checkResult:
            # logging.info( " " )
            logging.info( "checking cmsId: %s, hrId: %s, user '%s' account '%s' -- OK" % (p.cmsId, p.hrId, fullName, login) )
            # pass
        else:
            # logging.info( " " )
            logging.info( "checking - cmsId: %s user '%s' account '%s' (%s) -- ISSUES found:" % (p.cmsId, fullName, login, p.dateRegistration) )
            info['issues'] = ac.issues['error']

            # set defaults: all OK
            isDisabled = False
            isNotInZH  = False
            isInOtherExpt = False

            # first check what the issues are and set the corresponding flags:
            if ac.issues[ 'error' ] :
                for item in ac.issues[ 'error' ] :

                    isDisabled = 'account disabled' in item[ 'check' ]
                    isNotInZH = 'primary account in zh' in item[ 'check' ]
                    isInOtherExpt = 'secondary account in another experiment' in item[ 'check' ]

                # now act depending on the flags set:
                if isDisabled:
                    # add user to 'zh' eGroup, even if account is disabled
                    egh = EgroupHandler()
                    egh.addMemberPerson( 'zh', p.hrId )
                    logging.info("           ... %s now added to zh eGroup." % login )
                    # flag and send mail to user
                    logging.warning("           --- account '%s' is disabled " % login)
                    if 'disabled' not in info['mailSent']:
                        accountCheck.sendMailInButDisabled(db = db,
                                                            account = login,
                                                            email = ldap_user.mail,
                                                            userFirstName = p.firstName)
                        logging.info("checking - mail for disabled account sent to %s ... \n" % ldap_user.mail)
                        info['mailSent'].append('disabled')
                    else:
                        logging.info("Mail for disabled account was already sent to %s ... \n" % ldap_user.mail)

                if isInOtherExpt:
                    logging.warning("           --- %s " % item['msg'])
                    if 'otherExpt' not in info['mailSent']:
                        otherExpt = item['msg'].split('(')[:-2]
                        accountCheck.sendMailInOther(db = db,
                                                        account = login,
                                                        email = ldap_user.mail,
                                                        userFirstName = p.firstName,
                                                        otherExp = otherExpt
                                                    )
                        logging.info( "checking - mail for account in other expt (%s) sent to %s ... " % (otherExpt, ldap_user.mail) )
                        info['mailSent'].append('otherExpt')
                    else:
                        logging.info( "Mail for account in other expt (%s) was already sent to %s ... " % (otherExpt, ldap_user.mail) )

                if  isNotInZH and not isDisabled:
                    logging.warning("           --- primary account %s not in ZH " % login )
                    if not isInOtherExpt:
                        egh = EgroupHandler()
                        egh.addMemberPerson( 'zh', p.hrId )
                        logging.info("           ... %s now added to zh eGroup." % login )
                        # send mail for this only if there is no mail sent for the account being disabled:
                        if 'addedToZH' not in info['mailSent'] and 'disabled' not in info['mailSent']:
                            accountCheck.sendMailOK(db = db,
                                                    account = login,
                                                    email = ldap_user.mail,
                                                    userFirstName = p.firstName)

                            logging.info("checking - mail for account added to ZH sent to %s ... " % ldap_user.mail)
                            info['mailSent'].append('addedToZH')
                        else:
                            logging.info("Mail for account added to ZH was already sent to %s ... " % ldap_user.mail)

                if not (isDisabled or isNotInZH or isInOtherExpt):
                    print('\n', '-'*42)
                    print(item)
                    print('\n', '-'*42)
                else:
                    logging.info('checking  \n')
            procInfo.updateInfo(p.hrId, info)
    procInfo.write()
    if nProc < 0:
        logging.info( 'checking -- found no new users to be processed.' )
