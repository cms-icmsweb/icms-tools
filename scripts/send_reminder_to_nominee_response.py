""" Send the automatic notification mail to nominee to response whether is he accepted/rejected 
    for the position within the days_for_nominee_reponse. 
Run this script as such:
TOOLKIT_CONFIG=DEV python manage.py script send-reminder-to-nominee-response
Replace DEV with PREPROD or PROD as necessary.

"""

from typing import Any, List
from logging import Logger
import logging
from icms_orm.common import Position, Person, Affiliation, Assignment, Project
from icms_orm.toolkit import (
    JobOpening,
    JobOpenPosition,
    JobNomination,
    JobNominationStatus,
    JobQuestionnaire,
    EmailMessage,
)
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)

from datetime import date, datetime
import sqlalchemy as sa
from sqlalchemy import desc
from sqlalchemy.sql.expression import literal_column
import flask
import sqlalchemy as sa
import flask_login
from flask_login import current_user
from flask import abort
from sqlalchemy.orm import aliased
from util import constants as const
from sqlalchemy import func, select

def nominee_reminder_to_response():
        
    ssn = JobOpening.session()
    current_date = date.today()

    job_openings = ssn.query(JobOpening.id, JobOpening.days_for_nominee_response, JobOpening.title).filter(
        JobOpening.nominee_no_reply == True,
        JobOpening.end_date + JobOpening.days_for_nominee_response == current_date
    ).all()

    job_opening_ids_list = [job_opening.id for job_opening in job_openings]

    q_cols = [
        JobNomination.nomination_id,
        JobOpenPosition.job_opening_id,
        JobNomination.nominee_id,
        JobOpening.id,
        JobOpening.title,
        JobNominationStatus.status,
        Person.first_name,
        Person.last_name,
        Person.email,
    ]
                        
    job_nominations = (
        JobNomination.session.query(*q_cols)
        .filter(JobOpenPosition.job_opening_id.in_(job_opening_ids_list))
        .filter(JobNominationStatus.status == const.JOB_NOMINATION_ACCEPTED)
        .join(JobOpenPosition, JobNomination.open_position_id == JobOpenPosition.id)
        .join(JobOpening, JobOpenPosition.job_opening_id == JobOpening.id)
        .join(Person, JobNomination.nominee_id == Person.cms_id)
        .join(JobNominationStatus, JobNomination.nomination_id == JobNominationStatus.nomination_id)
        .all()
    )
    
    for jn in job_nominations:
        support = 'icms-support@cern.ch'
        secr = 'Cms.Secretariat@cern.ch'
        args = {
        'job_title': jn.title,
        'nominee_name': jn.first_name
        }            
        mail_body = flask.render_template('emails/send_reminder_to_nominee_response.txt', **args)
        EmailMessage.compose_message(
            sender=secr,
            bcc=f'{secr},{support}',
            reply_to=secr,
            subject= "CERN - Reminder information regarding your application for " + jn.title,
            source_app='toolkit',
            to=jn.email,
            body=mail_body,
            db_session=flask.current_app.db.session
        )

if __name__ == '__main__':
   retCode = nominee_reminder_to_response()