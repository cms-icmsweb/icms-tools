
# requirements: 
# -e git+https://:@gitlab.cern.ch:8443/authzsvc/tools/auth-get-sso-cookie.git#egg=auth_get_sso_cookie
# beautifulsoup4

# run as (assuming virtualenv in ./venv/): 
# (source ./venv/bin/activate; time PYTHONPATH=. PYTHONIOENCODING=utf-8 python ./scripts/getPhDStudentInfo.py  )


import csv
import netrc

import requests
import logging
import urllib3
import pprint

import sys
import datetime
import logging

from flask import Flask
from sqlalchemy.orm.exc import  NoResultFound, MultipleResultsFound

from auth_get_sso_cookie.cern_sso import login_with_kerberos

from icms_orm.common import UserAuthorData, Person

from icms_orm.epr import TimeLineUser as TLU, Category, EprUser

from util.database.orm_interface import WebappOrmManager

urllib3.disable_warnings()

sys.path.append( '.' )
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

theLogger = None

from itertools import groupby
from operator import itemgetter

def findLargestSequence( dataIn ):
    
    if not dataIn: return []
    
    data = sorted(dataIn)
    if type(dataIn) == type( set() ):
        data = sorted( list(dataIn) )
    
    new_l = []
    for k, g in groupby(enumerate(data), lambda ix : ix[0] - ix[1]):
        new_l.append(list(map(itemgetter(1), g)))

    return max(new_l, key=lambda x: len(x))

class PhDStudentInfo(object):
    
    def __init__(self, db) -> None:
        
        self.db = db

        self.students = []
        self.stud4yr = {}
        
    def getStudents(self):
        
        sess = self.db.session
        
        self.students = (sess.query(TLU.cmsId, TLU.year, Person.gender )
                   .join( Person, Person.cms_id == TLU.cmsId )
                   .filter( TLU.status.in_(['CMS', 'CMSEXTENDED']) )
                   .filter( TLU.category == 1)
                   # .group_by(TLU.cmsId, TLU.year, Person.gender)
                   .order_by(TLU.cmsId, TLU.year)
                   .all() )

    def analyseStudents(self):

        actId = 0
        actYears = {}
        genders = {}
        index = 0
        for cmsid, year, gender in self.students:
            if cmsid not in actYears.keys():
                actYears[cmsid] = set()
            actYears[cmsid].add( year )
            if cmsid not in genders.keys():
                genders[cmsid] = gender

        print( f'found {len(actYears.keys())} students ({len(genders.keys())})' )

        index = 0
        for cmsid, years in actYears.items():
            seq = findLargestSequence( years )
            if len(seq) < 4: continue
            # if index < 10: print( f'\n ==> {cmsid}: {len(seq)} - {seq} -- {actYears[cmsid]}' )
            self.stud4yr[cmsid] = { "gender": genders[cmsid], "years": sorted(list(seq)) }
            index += 1
  
    def showStudents(self):

        print( f'found {len(self.students)} student timelines' )
        for i in range(10):
            print( self.students[i] )

        sumInfo = {}
        startYearHist = {}
        nYearHist = {}
        print( f'found {len(self.stud4yr)} student timelines with more than three years of studentship' )
        for id, item in self.stud4yr.items():
            val = item['years']
            nYears = max(list(val)) - min(list(val))
            if nYears+1 != len(list(val)):
                print( f'{id} inconsistent years: len={len(list(val))} - min/max: {min(list(val))}/{max(list(val))} -- list is {val} {item["gender"]}')
            # print( f'{id} : {val} - {nYears}' )


            minYear = min(list(val))
            if minYear not in startYearHist:
                startYearHist[minYear] = 0
            startYearHist[minYear] += 1
            if nYears not in nYearHist:
                nYearHist[nYears] = 0
            nYearHist[nYears] += 1
            
            # if nYears > 4: continue 
                                    
            maxYear = max(list(val))
            if maxYear not in sumInfo:
                sumInfo[maxYear] = { 'MALE': 0, 'FEMALE': 0, 'other': 0 }
            sumInfo[maxYear][item['gender']] += 1 

            # if maxYear == 2023:
            #     print( f'2023: {id}: {val} {nYears}' )

        print( '\t year : \tfemale \tmale \tsum ')
        for year in sorted(sumInfo.keys()):
            # the numbers for the actual year contain both, the students who have finished as well as the ones who are still ongoing ones, so we do not display it
            if year == datetime.datetime.today.year : continue
            print( f"\t {year} : \t{sumInfo[year]['FEMALE']} \t {sumInfo[year]['MALE']} \t {sumInfo[year]['FEMALE'] + sumInfo[year]['MALE']}" )

        print ( f'distribution of starting years : {startYearHist}')
        print ( f'distribution of number of years: {nYearHist}')


def main():
    global theLogger

    theApp = Flask( __name__, instance_relative_config=True )
    theApp.config.from_pyfile( 'config.py' )

    with theApp.app_context() :
        db = WebappOrmManager(theApp)
        # db.create_all(bind='icms_common_db')

        dbUrl = str(db.engine.url)
        theLogger = theApp.logger
        theLogger.setLevel( logging.INFO )
        theLogger.info( "\n==> DB at: %s \n" % dbUrl)#[:15] )

        psi = PhDStudentInfo(db)
        psi.getStudents()
        psi.analyseStudents()
        psi.showStudents()
        
        del theLogger

if __name__ == '__main__':
   retCode = main()
