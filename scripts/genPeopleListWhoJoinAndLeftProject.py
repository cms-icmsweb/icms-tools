# Run the script from your local environment: TOOLKIT_CONFIG=DEV python manage.py script genPeopleListWhoJoinAndLeftProject --project '{project_name}'

from icmscommon.governors import DbGovernor

from icms_orm.common import Person
from icms_orm.epr import TimeLineUser

from datetime import date
import logging
import sys
import webapp
import pyexcel
import app_profiles


def main_query():
    db = DbGovernor.get_db_manager()
    return (
        db.session()
        .query(
            TimeLineUser.cmsId.label("cms_id"),
            Person.first_name.label("first_name"),
            Person.last_name.label("last_name"),
            TimeLineUser.instCode.label("inst_code"),
            TimeLineUser.mainProj.label("main_project"),
            TimeLineUser.status.label("status"),
            TimeLineUser.timestamp.label("timestamp"),
        )
        .join(Person, Person.cms_id == TimeLineUser.cmsId)
        .order_by(TimeLineUser.cmsId, TimeLineUser.timestamp)
    )


def get_project_timeline(project):
    return main_query().filter(TimeLineUser.mainProj.ilike(f"{project}%")).all()


def get_timeline_of_people_with_specific_project(cms_ids_with_selected_project_entry):
    return (
        main_query()
        .filter(TimeLineUser.cmsId.in_(cms_ids_with_selected_project_entry))
        .all()
    )


def compute_project_changes(project):
    people_who_joined = []
    cms_ids_with_selected_project_entry = set()
    current_cms_id = 0
    for entry in get_project_timeline(project):
        cms_ids_with_selected_project_entry.add(entry.cms_id)
        if current_cms_id != entry.cms_id:
            people_who_joined.append(entry)
            current_cms_id = entry.cms_id

    people_timeline_with_selected_project = (
        get_timeline_of_people_with_specific_project(
            cms_ids_with_selected_project_entry
        )
    )
    people_who_left = []
    entries = people_timeline_with_selected_project
    for i, entry in enumerate(entries):
        # In the condition we ingore those who have status = null (if entry.status)
        if (
            (i != 0)
            and entry.status
            and entries[i - 1].main_project
            and entries[i - 1].cms_id == entry.cms_id
            and project in entries[i - 1].main_project
            and (project not in entry.main_project or "CMS" not in entry.status)
        ):
            people_who_left.append(entry)

    return people_who_joined, people_who_left


def create_excel(people_who_joined, people_who_left, project):
    main_columns = ["CMS id", "First Name", "Last Name", "Affiliation"]
    joined_columns = main_columns + [
        "Main Project",
        "Status",
        "Joined",
    ]
    left_columns = main_columns + [
        "Main Project After They Left",
        "Status After They Left",
        "Left",
    ]
    book_sheets = {
        "People Who Joined": [joined_columns] + people_who_joined,
        "People Who Left": [left_columns] + people_who_left,
    }

    book = pyexcel.get_book(bookdict=book_sheets)
    book.save_as(f"/tmp/{date.today()}_people_who_joined_and_left_{project}.xlsx")


def main(project):
    people_who_joined, people_who_left = compute_project_changes(project)
    create_excel(people_who_joined, people_who_left, project)


if __name__ == "__main__":
    the_app = webapp.Webapp(__name__, ".", app_profiles.ProfileDev)
    with the_app.app_context():
        db = the_app.db
