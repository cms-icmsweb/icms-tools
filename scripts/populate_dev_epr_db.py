from util import constants as const
import logging
import os
from util import trivial
import adapt_oracle_dump
import subprocess
import config

# This seems to duplicate testutils.py's part :/
# def populate_epr_db(db_name=config.ConfigTest.epr_db_name(), db_user=config.ConfigTest.epr_db_user()):
#     label = 'Populating EPR DB'
#     logging.debug(label)
#     start_time = trivial.get_time()
#
#     rsc_dir = os.path.join(trivial.get_project_root(), const.DIR_NAME_RSC)
#     epr_dir = os.path.join(rsc_dir, const.SUBDIR_NAME_EPR)
#     if not os.path.exists(epr_dir):
#         logging.info('creating dir: %s' % epr_dir)
#         os.mkdir(epr_dir)
#     elif not os.path.isdir(epr_dir):
#         raise Exception('%s already exists and is not a directory!' % epr_dir)
#
#     if not os.listdir(epr_dir):
#         epr_bzip_path = os.path.join(rsc_dir, const.FILE_NAME_EPR_BZIP)
#         if not os.path.exists(epr_bzip_path) or not os.path.isfile(epr_bzip_path):
#             raise Exception('EPR dumps zip not found at: %s' % epr_bzip_path)
#         else:
#             logging.info('decompressing EPR dumps into %s' % epr_dir)
#             subprocess.call('tar xjf %s -C %s' % (epr_bzip_path, epr_dir), shell=True)
#             logging.info('translating Oracle dumps to mySQL')
#             adapt_oracle_dump.main(epr_dir)
#
#     for f in os.listdir(os.path.join(epr_dir, adapt_oracle_dump.SUBDIR_OUTPUT)):
#         if f.endswith('.sql') and '_version_' not in f.lower():
#             filepath = os.path.join(epr_dir, adapt_oracle_dump.SUBDIR_OUTPUT, f)
#             logging.debug('Would like to import data from %s' % filepath)
#             subprocess.call('mysql -u %s %s < %s' % (db_user, filepath, db_name), shell=True)
#             pass
#     logging.debug(trivial.get_exec_time_string(start_time, label))
#
#
# if __name__ == '__main__':
#     logging.debug('Running with predefined development EPR db credentials.')
#     populate_epr_db(db_name=const.DB_DEFAULT_DEV_NAME_EPR, db_user=const.DB_DEFAULT_DEV_USERNAME)