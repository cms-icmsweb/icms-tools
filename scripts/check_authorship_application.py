import datetime
import logging
from collections import OrderedDict as ordict
from typing import List

import flask
import sqlalchemy as sa
from icms_orm.cmspeople import (
    MemberActivity,
    MoData,
    PeopleFlagsAssociation,
    Person,
    PersonHistory,
)
from icms_orm.common import ApplicationAsset as AppAsset
from icms_orm.common import Person as NuPerson
from icms_orm.common import PrehistoryTimeline
from icmsutils.businesslogic import servicework as epr
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from sqlalchemy import func

from models.icms_epr import Category, EprInstitute, EprUser
from models.icms_toolkit import AuthorshipApplicationCheck
from util import constants, glossary


class AuthorApplicationChecker(object):

    app_days_required = 365
    app_days_limit = 730
    ind_work_required = 6.0
    valid_icms_activity_ids = []
    csv_keys = ordict(
        [
            ("user.cmsId", glossary.CMS_ID),
            ("user.name", glossary.USER_FULL_NAME),
            ("inst.code", glossary.INST_CODE),
            ("person.activity.name", glossary.USER_CMS_ACTIVITY),
            (
                "person.modata.mo%d" % datetime.date.today().year,
                "MO%d" % datetime.date.today().year,
            ),
            ("application_passed", glossary.APPLICATION_PASSED),
            ("application_failed", glossary.APPLICATION_FAILED),
            ("reason", glossary.APPLICATION_REASON),
            ("days_as_applicant", glossary.APPLICATION_DAYS),
            ("work_done", glossary.APPLICATION_USER_WORK_DONE),
            ("inst_done", glossary.APPLICATION_INST_WORK_DONE),
            ("inst_required", glossary.APPLICATION_INST_WORK_REQUIRED),
            ("possible_slipthrough", glossary.APPLICATION_SLIPTHROUGH),
        ]
    )

    # reason_success_inst = 'institute did well'
    reason_success_self = "individual work done"
    # reason_failure_timeout = 'application deadline passed'
    reason_negative_time = "not long enough at CMS (1 year required)"
    reason_negative_suspended = "suspended"
    reason_negative_author = "already an author"
    reason_negative_no_start_date = "cannot determine application start date"

    valid_icms_activity_ids = [
        a.id
        for a in Person.session()
        .query(MemberActivity)
        .filter(
            sa.or_(
                MemberActivity.name == "Doctoral Student",
                sa.or_(
                    MemberActivity.name.like("Engineer%"),
                    MemberActivity.name.like("%Physicist%"),
                ),
            )
        )
        .all()
    ]

    def __init__(
        self,
        user=None,
        cmsid=None,
        inst=None,
        cms_people_person=None,
        cms_people_person_history=None,
        applicant_work_needed=None,
    ):
        """
        Some parameters can be passed from the calling method to speed things up (eg when running inst-by-inst)
        :param user: User object
        :param cmsid: can be passed instead of the User object
        :param inst: can be passed to spare the inst's retrieval from DB
        :param user_info_current:
        :param user_info_prev:
        :param summary_prev:
        "param applicant_work_needed:
        """

        self.ind_work_required = (
            applicant_work_needed
            if applicant_work_needed is not None
            else self.__class__.ind_work_required
        )
        self.is_exportable = True
        self.year_now = datetime.date.today().year
        self.user = (
            user
            if user is not None
            else EprUser.query.filter(EprUser.cmsId == cmsid).one_or_none()
        )
        self.person = (
            isinstance(cms_people_person, Person)
            and cms_people_person
            or Person.session().query(Person).filter(Person.cmsId == user.cmsId).one()
        )
        self.history = (
            isinstance(cms_people_person_history, PersonHistory)
            and cms_people_person_history
            or PersonHistory.query.filter(PersonHistory.cmsId == cmsid).one()
        )
        self.ph_model = PrehistoryModel(
            cms_id=None, person=self.person, history=self.history
        )
        self.date_join_cms = self.ph_model.get_last_effective_arrival_date()
        self.inst = inst if inst is not None else EprInstitute.query.get(user.mainInst)

        self.application_passed = False
        self.remarks = ""
        self.days_at_cms = (datetime.date.today() - self.date_join_cms).days
        # failure only in case the deadline has been exceeded
        self.application_failed = False

        # checking both the EprEntry and CMSPEOPLE entry
        _failure_due_to_preliminary_requirements = (
            self._check_preliminary_requirements_for_failure()
        )
        if _failure_due_to_preliminary_requirements:
            self.application_passed = False
            self.reason = _failure_due_to_preliminary_requirements
            self.is_exportable = False
            return

        self.possible_slipthrough = False
        self.date_application_start = (
            self.ph_model.get_authorship_application_start_date()
        )

        _failure_due_to_start_date = self._check_start_date_for_failure()
        if _failure_due_to_start_date:
            self.application_passed = False
            self.reason = _failure_due_to_start_date
            self.is_exportable = False
            return

        self.days_as_applicant = (
            (datetime.date.today() - self.date_application_start).days
            if self.date_application_start is not None
            else self.days_at_cms
        )

        if self.user.authorReq:
            self.application_passed = False
            self.reason = self.__class__.reason_negative_author
            self.is_exportable = False
            return

        self.work_done, self.work_by_year = self._compute_contribution()
        inst_call_params = dict(
            inst_code=self.person.instCode,
            year=self.year_now - 1,
            db_session=Person.session(),
        )
        self.inst_required = epr.get_institute_due(**inst_call_params)
        self.inst_done = epr.get_institute_worked(
            **inst_call_params
        ) + epr.get_institute_shifts_done(**inst_call_params)

        if self.days_at_cms < self.__class__.app_days_required:
            self.application_passed = False
            self.reason = self.__class__.reason_negative_time
            return

        # Prioritize the success conditions to clean up the limbo
        if self.work_done >= self.ind_work_required:
            self.application_passed = True
            self.reason = self.__class__.reason_success_self
            return
        # -ap: as of 2022 this rule is no longer used/valid
        # if self.inst_required <= self.inst_done:
        #     self.application_passed = True
        #     self.reason = AuthorApplicationChecker.reason_success_inst
        #     return

        ### pd: no longer valid rules as of 2022
        # # Then autosuspend everyone who's behind the schedule - and the limbo should be clear
        # if self.date_application_start.year < self.year_now - 3:
        #     self.application_passed = False
        #     self.application_failed = True
        #     self.reason = AuthorApplicationChecker.reason_failure_timeout
        #     return
        # # No success so far. In case this was the last chance a failure should be announced and the candidate
        # # shall get suspended as a result.
        # if self.date_application_start.year == self.year_now - 3:
        #     self.application_failed = True
        #     self.application_passed = False
        #     self.reason = AuthorApplicationChecker.reason_failure_timeout
        #     return

        self.application_passed = False
        self.reason = "not enough work done"

    def _check_preliminary_requirements_for_failure(self):
        if self.user.isSuspended or self.person.isAuthorSuspended:
            return self.__class__.reason_negative_suspended
        return None

    def _check_start_date_for_failure(self):
        if self.date_application_start is None:
            return AuthorApplicationChecker.reason_negative_no_start_date
        return None

    def _compute_contribution(self):
        effective_total = 0
        total_by_year = dict()
        for year in range(self.date_application_start.year - 1, self.year_now + 1):
            year_total = 0
            year_total += epr.get_person_shifts_done(
                cms_id=self.user.cmsId, year=year, db_session=Person.session()
            )
            year_total += epr.get_person_worked(
                cms_id=self.user.cmsId, year=year, db_session=Person.session()
            )
            total_by_year[year] = year_total
            # new rule 2.2 from 2022 on:
            #  max 3 months from the calendar year before application start
            if year == self.date_application_start.year - 1:
                effective_total += min(year_total, 3)
            else:
                effective_total += year_total
        return effective_total, total_by_year

    @classmethod
    def translate_key(cls, key):
        return cls.csv_keys[key]

    def __repr__(self):
        txt = "Application %s [%s]. " % (
            "successful" if self.application_passed else "failed",
            self.reason,
        )
        details = []
        if hasattr(self, "days_as_applicant"):
            details.append("%d days as applicant" % self.days_as_applicant)
        if hasattr(self, "work_done"):
            details.append("%.2f EPR work done" % self.work_done)
        if hasattr(self, "inst_done") and hasattr(self, "inst_required"):
            details.append(
                "%.2f work done (%.2f required) by institute in %d"
                % (self.inst_done, self.inst_required, self.year_now - 1)
            )
        txt += ", ".join(details) + "."
        return txt

    def to_csv(self):
        # Assuming that fields being themselves objects are always present
        vals = []
        for key in AuthorApplicationChecker.csv_keys:
            val = eval("self.%s" % key) if "." in key or hasattr(self, key) else ""
            if isinstance(val, str):
                vals.append(val)
            else:
                vals.append(str(val))
        return ";".join(vals)

    @classmethod
    def csv_header(cls):
        return ";".join(cls.csv_keys)

    def add_to_db_session(self, session, run_number):

        previous = (
            session.query(AuthorshipApplicationCheck)
            .filter(AuthorshipApplicationCheck.cmsId == self.person.cmsId)
            .filter(AuthorshipApplicationCheck.runNumber < run_number - 3)
            .all()
        )
        for p in previous:
            session.delete(p)

        session.add(
            AuthorshipApplicationCheck(
                run_number=run_number,
                cms_id=self.person.cmsId,
                inst_code=self.person.instCode,
                cms_activity=self.person.activityId,
                passed=self.application_passed,
                failed=self.application_failed,
                days=self.days_as_applicant,
                worked_self=self.work_done,
                worked_inst=self.inst_done,
                needed_inst=self.inst_required,
                possible_slipthrough=self.possible_slipthrough,
                mo_status=self.person.activity.name == "Doctoral Student"
                or (
                    eval("self.person.modata.mo%d" % datetime.date.today().year)
                    == "YES"
                ),
                reason=self.reason,
            )
        )


class TotemNewcomerApplicationChecker(AuthorApplicationChecker):
    # setting to 0 so that the un-suspended people are not put on hold here
    app_days_required = 0
    reason_negative_suspended = "suspended (TOTEM member)"
    reason_negative_author = "already an author (TOTEM member)"
    reason_negative_time = "not long enough at CMS (TOTEM member)"
    reason_success_self = "TOTEM member who has completed required work."
    ind_work_required = 4.0
    _late_reg_exceptions = {
        int(_k): _v
        for _k, _v in (AppAsset.retrieve("totem_late_reg_exceptions") or {}).items()
    }

    def _compute_contribution(self):
        year = 2018
        year_total = 0
        year_total += epr.get_person_shifts_done(
            cms_id=self.user.cmsId, year=year, db_session=Person.session()
        )
        year_total += epr.get_person_worked(
            cms_id=self.user.cmsId, year=year, db_session=Person.session()
        )
        total_by_year = {year: year_total}
        return year_total, total_by_year

    def _check_preliminary_requirements_for_failure(self):
        _super_fail = super(
            TotemNewcomerApplicationChecker, self
        )._check_preliminary_requirements_for_failure()
        if _super_fail:
            return _super_fail
        elif (self.person.get(Person.project) or "").upper() != "PPS":
            return "Not a PPS project member, not a TOTEM member then"
        return None

    def _check_start_date_for_failure(self):
        _super_fail = super(
            TotemNewcomerApplicationChecker, self
        )._check_start_date_for_failure()
        if _super_fail:
            return _super_fail
        elif self.ph_model.get_last_effective_arrival_date() > datetime.date(
            2018, 9, 1
        ):
            # here a little grace period is included for the boundary case(s)
            if min(
                [
                    self.person.get(x)
                    for x in [Person.dateCreation, Person.dateRegistration]
                ]
            ).date() > datetime.date(2018, 9, 7):
                # and here we'll take into account the exceptional cases approved later by powers that be
                if (
                    self.person.get(Person.cmsId)
                    not in TotemNewcomerApplicationChecker._late_reg_exceptions
                ):
                    return "TOTEM member joined after the agreed date of 2018/09/01 ({date})".format(
                        date=self.ph_model.get_last_effective_arrival_date()
                    )
        return None


def go_over_all_insts(
    inst_code_set=None, applicant_work_needed=6.0
) -> List[AuthorApplicationChecker]:
    # Only the CMS Member Institutes should be taken into account
    session = Person.session()
    all_insts_query = session.query(EprInstitute).filter(
        EprInstitute.cmsStatus == "Yes"
    )
    if inst_code_set is not None:
        all_insts_query = all_insts_query.filter(EprInstitute.code.in_(inst_code_set))
    all_insts = all_insts_query.all()
    logging.debug("Found %d institutes! " % len(all_insts))
    checkers = []

    # This value is somewhat sensitive:
    # - Too low (14 days or less) and a new application check will run (triggering a new
    # email notification) before the automatic author admitter has a chance to run after
    # 14 days of team leader inactivity.
    # - Too high (or non-existent) and members who become applicants for a second time
    # will have their applications effectively ignored.
    ignore_people_notified_after = datetime.datetime.utcnow() - datetime.timedelta(
        days=30
    )
    recently_notified_cms_ids = {
        x[0]
        for x in (
            session.query(AuthorshipApplicationCheck.cmsId)
            .filter(AuthorshipApplicationCheck.notificationId.isnot(None))
            .filter(AuthorshipApplicationCheck.datetime > ignore_people_notified_after)
            .all()
        )
    }
    noauthor_cms_ids = {
        x[0]
        for x in session.query(PeopleFlagsAssociation.cmsId)
        .filter(PeopleFlagsAssociation.flagId == constants.FLAG_AUTHOR_NO)
        .all()
    }

    logging.debug(
        "Found %d people with %s flag and %d people for whom email had already been sent"
        % (
            len(noauthor_cms_ids),
            constants.FLAG_AUTHOR_NO,
            len(recently_notified_cms_ids),
        )
    )

    fk_sq = EprUser.session().query(NuPerson.cms_id).subquery()

    for inst in all_insts:

        users = (
            EprUser.query.filter(EprUser.mainInst == inst.id)
            .filter(EprUser.authorReq == False)
            .filter(EprUser.isSuspended == False)
            .filter(EprUser.status.like("CMS"))
            .filter(EprUser.creationTime != None)
            .join(Category, EprUser.category == Category.id)
            .filter(
                sa.or_(
                    Category.name == "Doctoral Student",
                    Category.name.ilike("%Physicist%"),
                    Category.name.ilike("%Engineer%"),
                )
            )
            .filter(EprUser.cmsId.in_(fk_sq))
            .all()
        )

        if not users:
            continue

        phd_mo_col = getattr(
            MoData,
            MoData.phdMo2018.key.replace("2018", str(datetime.datetime.now().year)),
        )
        free_mo_col = getattr(
            MoData,
            MoData.freeMo2018.key.replace("2018", str(datetime.datetime.now().year)),
        )

        cms_people_data = (
            Person.session()
            .query(Person, PersonHistory)
            .join(PersonHistory, Person.cmsId == PersonHistory.cmsId)
            .filter(Person.instCode == inst.code)
            .filter(Person.isAuthor == False)
            .filter(Person.isAuthorSuspended == False)
            .filter(Person.isAuthorBlock == True)
            .filter(Person.status == "CMS")
            .join(MemberActivity, Person.activityId == MemberActivity.id)
            .join(MoData, MoData.cmsId == Person.cmsId)
            .filter(
                MemberActivity.name.in_(
                    MemberActivity.get_authorship_eligible_activity_names()
                )
            )
            .all()
        )

        logging.debug(
            "Found %d out of %d people for %s"
            % (len(cms_people_data), len(users), inst.code)
        )

        cms_people_data = {p.cmsId: (p, h) for p, h in cms_people_data}

        for user in users:
            if (
                user.cmsId in cms_people_data
                and user.cmsId not in recently_notified_cms_ids
                and user.cmsId not in noauthor_cms_ids
            ):
                cms_person, cms_person_history = cms_people_data.get(user.cmsId)

                checker = AuthorApplicationChecker(
                    cmsid=user.cmsId,
                    user=user,
                    inst=inst,
                    cms_people_person=cms_person,
                    cms_people_person_history=cms_person_history,
                    applicant_work_needed=applicant_work_needed,
                )

                # pdeligia: TOTEM checks no longer needed as special rules should have long expired
                ## should the first instance of checks fail, let's see if we have a TOTEM person here
                # if (
                #     not checker.application_passed
                #     and (cms_person.get(Person.project) or "").upper() == "PPS"
                # ):
                #     checker = TotemNewcomerApplicationChecker(
                #         cmsid=user.cmsId,
                #         user=user,
                #         inst=inst,
                #         cms_people_person=cms_person,
                #         cms_people_person_history=cms_person_history,
                #     )
                checkers.append(checker)
                logging.getLogger(__name__).info(
                    "%s [#%d, %s]: %s",
                    user.name,
                    user.cmsId,
                    checker.inst.code,
                    str(checker),
                )
            else:
                logging.debug(
                    "Skipping authorship application check for #%d" % user.cmsId
                )
    return checkers


def main(applicant_work_needed=6.0):
    """
    This method is not meant to operate in a stand-alone mode (or shorter: Stalone). Instead, it must be called with
    with application context available. There should be a console launcher for this and perhaps a web link
    will be addded some time as well.
    :return:
    """

    session = Person.session()
    run_number = session.query(func.max(AuthorshipApplicationCheck.runNumber)).first()[
        0
    ]
    run_number = 1 if not run_number else run_number + 1

    logging.info(
        f"Authorship application check run {run_number}. Target applicant due: {applicant_work_needed}"
    )
    checkers = go_over_all_insts(applicant_work_needed=applicant_work_needed)
    date_str = datetime.datetime.now().strftime("%Y.%m.%d-%H:%M")

    for checker in checkers:
        if checker.is_exportable:
            checker.add_to_db_session(Person.session(), run_number)
    Person.session().commit()
