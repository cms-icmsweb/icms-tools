
# create the table (in CMSAnalysis) first, then run the script on prod like this:
# (source ./venv-py3/bin/activate; LANG=en_US.utf8 LC_ALL=en_US.utf8 PYTHONPATH=. TOOLKIT_CONFIG=PROD python3 ./manage.py script fill_fora_only_on_hypernews)

import os
from flask import current_app
from util import constants as const
import sqlalchemy as sa

def main( db=None ):

    dbUrl = current_app.config[ 'SQLALCHEMY_BINDS' ][ const.OPT_NAME_BIND_LEGACY ]
    anaEngine = sa.create_engine( dbUrl.replace( 'CMSPEOPLE', 'CMSAnalysis' ) )

    print( f'running in {os.getcwd()}')
    with open( 'hn_only_fora_list.txt', 'r') as df:
        fora = df.readlines()

    print( f'going to insert {len(fora)} fora into table ' )

    with anaEngine.connect( ) as con :

        # create the table (in CMSAnalysis) first:
        # con.execute( 'drop table if exists fora_only_on_hypernews' )
        # con.execute( 'CREATE table fora_only_on_hypernews (id int NOT NULL AUTO_INCREMENT, code varchar(15), PRIMARY key(id) )' )
        print( 'table fora_only_on_hypernews re-created')

        for f in fora:
            query = f"insert into fora_only_on_hypernews (code) values ('{f.strip()}')"
            # avoid duplicating the table when accidentally re-running the script ... and notify the user ...
            # con.execute(query)
        print( 'cowardly refusing to re-run the script to avoid duplicating the table. Look at the source ... ')
