

import os
import sys
import datetime
import codecs
import re
import time
import glob
import subprocess

import flask
from flask import current_app
import app_profiles

from pyuca import Collator

from flask import Flask
from util.database.orm_interface import WebappOrmManager

from icmscommon.governors import DbGovernor

from util.constants import OPT_NAME_PRODUCTION_HOSTNAMES
from icms_orm.common import UserAuthorData
from icms_orm.toolkit import ALFileset
from icms_orm.cmspeople import User as PeopleUserData, Person as PeopleData, Institute, PeopleFlagsAssociation
from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory
from scripts.genAuthListHelpers import utf8ToTex, foafTemplates
from scripts.JsonDiffer import Comparator

import logging

coll = Collator()

sys.path.append('.')
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

theLogger = logging

def repack(authName, firstName):

    # some people set their signature name as "LastName, <initials>",
    # so we first look for this case, then check for the standard "<initials> LastName"
    if ',' in authName:
        name, initials = authName.split(',')
    else:
        if '. ' in authName:
            initials, name = authName.rsplit('. ', 1)
            initials += '.'
        elif ' ' in authName:
            initials, name = authName.rsplit(' ', 1)
        else:
            theLogger.warning('no space in authName: %s [%s]' % (
                authName, ', '.join(authName.rsplit('.', 1))))
            initials, name = authName.rsplit('.', 1)
            initials += '. '  # restore split away dot and add a space

    return name.strip().replace(' ', '_') + ' |' + initials


def unpack(nSig):
    return nSig.split('|')[1] + ' ' + nSig.split('|')[0]


htmlTemplates = {'nbspVal': '&nbsp;',
                 'orcid': '<a href="https://orcid.org/%s" alt="ORCID icon"><img src="ORCIDiD_iconvector.svg" alt="" width="20rem"/></a>',
                 'multiInstLast': '<font size=-1 color=black><sup>%s</sup></font>',  # a, b, c, d
                 'top': "<font style='font-size: 14px;'>\nAuthor list for %s generated on %s (new tool)<br/><br/>\n",
                 'instTempl': u'<b>%s,&nbsp;%s,&nbsp;%s</b></br>\n',  # instNameSign, City, Country
                 'instSignTempl': u'<b>%s</b></br>\n',  # instNameSign
                 'authTempl': u'<nobr>%s%s,</nobr> ',  # nSign, alsoNowAt
                 'lastAuthTempl': u'<nobr>%s%s</nobr><br/></br>\n',  # nSign, alsoNowAt
                 'authTemplOrcid': u'<nobr>%s%s%s,</nobr> ',  # nSign, alsoNowAt, orcid
                 'lastAuthTemplOrcid': u'<nobr>%s%s%s</nobr><br/></br>\n',  # nSign, alsoNowAt, OrcID
                 'alsoNowAtTempl': '<font size=-1 color=black><sup>%s</sup></font><font size=-1 color=black><sup>,</sup></font>',  # footNoteId
                 'alsoNowAtTemplLast': '<font size=-1 color=black><sup>%s</sup></font>',  # footNoteId
                 # footNoteId, Also|Now, instNameSign, City, Country
                 'alsoNowAtTemplFN': u'%s:&nbsp;%s at&nbsp;%s,&nbsp;%s,&nbsp;%s<br/>',
                 'alsoNowAtTemplFN0': u'%s:&nbsp;%s at&nbsp;%s<br/>', # name has town, country
                 'daggerFN': '<font size=-1 color=black><sup>&dagger;</sup></font>&nbsp;Deceased<br/>\n',
                 }

texTemplates = {'nbspVal': '~',
                # 'orcid': '\\href{https://orcid.org/%s}',
                'orcid': '\\cmsorcid{%s}',
                'multiInstLast': '$^{%s}$',  # a, b, c, d
                'top': None,    # instNameSign, City, Country
                'instTempl': u'\\cmsinstitute{%s, %s, %s}\n',  # instNameSign
                'instSignTempl': u'\\cmsinstitute{%s}\n',
                'authTempl': u'%s%s, ',
                'lastAuthTempl': u'%s%s\n',
                'authTemplOrcid': u'%s%s%s, ', # nSign, alsoNowAt, orcid
                'lastAuthTemplOrcid': u'%s%s%s\n',
                'alsoNowAtTempl': u'\\cmsAuthorMark{%s}$^{, }$',  # footNoteId
                'alsoNowAtTemplLast': u'\\cmsAuthorMark{%s}',  # footNoteId
                # footNoteId, Also|Now, instNameSign, City, Country
                'alsoNowAtTemplFN': u'%s:~%s at %s, %s, %s\\\\\n',
                'alsoNowAtTemplFN0': u'%s:~%s at %s\\\\\n', # name has town, country
                'daggerFN': '\\vskip\\cmsinstskip\n\\dag: Deceased\\\\\n',
                }

revtexTemplates = {'nbspVal': '~',
                   'orcid': '\\cmsorcid{%s}',
                   'multiInstLast': '$^{%s}$',  # a, b, c, d
                   'top': None,
                   'authTempl': u'\\author{%s}\n',  # nSign
                   # instNameSign, City, Country
                   'instTempl': u'\\affiliation{%s, %s, %s}\n',
                   'instSignTempl': u'\\affiliation{%s}\n',  # instNameSign
                   # instNameSign, City, State, Country
                   'instTemplUSA': u'\\affiliation{%s, %s, %s, %s}\n',
                   # instNameSign, City, Country
                   'alsoNowAtTempl': u'\\altaffiliation{%s, %s, %s}\n',
                   'alsoNowAtTempl0': u'\\altaffiliation{%s}\n',  # name has town, country
                   'daggerFN': u'\\thanks{Deceased}\n',
                   }

xmlTemplates = {'nbspVal': ' ',
                'multiInstLast': '',  # repeated instTempl with IDa, IDb, ...
                'top': "<?xml version='1.0' encoding='UTF-8'?>\n<authors>\n",
                # id, instNameSign, City, Country
                'instTempl': u"<affil id='%s'>%s, %s, %s</affil>\n",
                'instSignTempl': u"<affil id='%s'>%s</affil>\n",  # id, instNameSign
                # id(s), nSign/name
                'authTempl': u"<author affil='%s'>%s</author>\n",
                # id(s), nSign, alsoNowAt
                'lastAuthTempl': u"<author affil='%s'>%s</author>\n\n",
                }

xmlNTemplates = {'nbspVal': ' ',
                 'multiInstLast': '',  # repeated instTempl with IDa, IDb, ...
                 'top': foafTemplates.foafTop,
                 'instTempl': foafTemplates.foafOrganisation,
                 'authTempl': foafTemplates.foafAuthor,
                 'lastAuthTempl': foafTemplates.foafAuthor,
                 }


def getOrcIdMapCSV():
    from csv import reader
    result = {}
    with open('dataIn/cms_inspireId-orcId-map-2017-04-28.csv', 'r') as orcidFile:
        lines = reader(orcidFile)
        for row in lines:
            if len(row) == 0:
                continue  # ignore empty lines ...
            result[row[0]] = row[1]

    theLogger.info('found %d ORCIDs in file' % len(result.keys()))

    return result


def getOrcIdMap(db):

    orcIdData = db.session.query(
        UserAuthorData.cmsid, UserAuthorData.orcid).all()
    result = {}
    nInvalid = 0
    for cmsId, orcId in orcIdData:
        if orcId :
            result[int(cmsId)] = orcId
        else:
            # theLogger.warning( f'found invalid ORCID "{orcId}" in DB for {cmsId}' )
            nInvalid += 1
    theLogger.info( f'found {len(result.keys())} ORCIDs in DB (and {nInvalid} invalid ones)' )
    return result


def getInspireIDMap(db):
    inspIds = (db.session.query(PeopleData.hrId, PeopleData.authorId,
                                PeopleData.firstName, PeopleData.lastName)
                 .join()
                 .filter( PeopleData.authorId != '')
                 .all())

    inspireMap = {}
    nameMap = {}
    for hrId, inspireID, fName, lName in inspIds:
        inspireMap[ str(hrId) ] = inspireID
        nameMap[ str(hrId) ] = [fName, lName]
        if  not fName.strip() or not lName.strip():
            theLogger.warning('==> getInspireIDMap> no names found for %s -- %s ' % (hrId, ' - '.join( [hrId, inspireID, fName, lName] ) ) )
    theLogger.info('found %d InspireIDs in DB' % len( inspireMap.keys() ))

    return inspireMap, nameMap


def getFilesFromFS():

    # make sure we have access to the EOS files:
    # according to Jan Iven on MatterMost (2017-06-26 morning)
    assert isinstance(theApp, webapp.Webapp)
    theApp.remount_eos_if_needed()

    now = datetime.datetime.now()

    alDir = os.path.join(os.getcwd(), 'webapp/static/new')
    alFiles = glob.glob(alDir + '/*')
    logging.debug('found %d AL files in %s' % (len(alFiles), alDir))

    if len(alFiles) == 0:  # no files found, try again:
        alFiles = glob.glob(alDir + '/*')
        logging.debug('found %d AL files in %s on second try' %
                      (len(alFiles), alDir))

    return alFiles


def importALFiles():
    alFiles = getFilesFromFS()
    for alFile in alFiles:
        if not 'authorlistN.xml' in alFile:
            continue  # only process one file for each paper
        paperCode = os.path.basename(alFile)[:10]
        timeStamp = os.path.getctime(alFile)
        alf = ALFileset(paperCode, location='eos',
                        last_update=datetime.datetime.fromtimestamp(timeStamp))
        ALFileset.session.add(alf)
        ALFileset.session.commit()


class Author(object):
    def __init__(self, items, rawInfo, peopleUserDataRaw):

        (self.authName,
         self.cmsId,
         self.iCode,
         self.iCodeNow,
         self.iCodeAlso,
         self.iCodeForced,
         self.infn,
         self.univOther,
         self.name,
         self.namf,
         self.status,
         self.instCountry,
         self.instTown,
         self.instNameFull,
         self.instNameSign,
         self.instStatus,
         self.inspireId, 
         self.orcId) = items

        self.rawInfo = rawInfo
        self.peopleUserData = peopleUserDataRaw

        if self.infn is None or u'null' in self.infn or len(self.infn) == 0:
            self.infn = None
        if self.univOther is None or u'null' in self.univOther or len(self.univOther) == 0:
            self.univOther = None

        if self.peopleUserData and self.peopleUserData.nameSignature and self.authName != self.peopleUserData.nameSignature:
            theLogger.warning("authName '%s' differs from nameSign - reset to '%s'" % ( self.authName.encode('utf-8'), self.peopleUserData.nameSignature.encode('utf-8')) )
            self.authName = self.peopleUserData.nameSignature

        if '-' in self.authName and self.name not in self.authName and not '.-' in self.authName:
            newName, initials, inits = getNewNameSig(self.namf, self.name)
            if int(self.cmsId) not in [2639] and 'M. Abdullah Al-Mashad' not in self.authName:
                theLogger.warning("special case found: '%s' reset to '%s'" % ( self.authName.encode('utf-8'), newName.encode('utf-8')) )
                self.authName = newName

        if '.' not in self.authName:
            try:
                newName = self.handleSpecial(self.authName)
            except Exception as e:
                raise e

            if newName:
                theLogger.warning("no '.' found in '%s' reset as special case to '%s'" % ( self.authName, newName) )
                self.authName = newName
                return

            try:
                ints, lastName = self.authName.split(' ')
                newName = '%s. %s' % ( '.'.join( [x for x in ints] ), lastName )
            except ValueError as e:
                theLogger.error( f'ERROR when splitting {self.authName}' )
                newName = self.handleSpecial(self.authName)
            theLogger.warning("no '.' found in '%s' reset to '%s'" % ( self.authName, newName) )
            self.authName = newName

    def handleSpecial(self, authName):
        specialCases = { 'Emine Gurpinar Guler': 'E. G. Guler',
                         'Arun Kumar': 'A. Kumar',
                         'Ashok Kumar': 'A. Kumar',
                         'Aashaq Shah': 'A. Shah',
                         'Ravindra Kumar Verma': 'R. K. Verma',
                         'Gurpreet Singh CHAHAL': 'G. S. Chahal',
                         'Anthony LaTorre': 'A. Latorre',
                          }
        return specialCases[authName]

    def __repr__(self):
        return '<AuthorInfo authName %s cmsId %s instCode %s alsoNowAt %s instNow %s instForced %s infn %s univOther %s name %s firstname %s >' % (self.authName, self.cmsId, self.iCode, self.iCodeAlso, self.iCodeNow, self.iCodeForced, self.infn, self.univOther, self.name, self.namf)

class CMSInst(object):

    def __init__(self, items, rawInfo: Institute) -> None:
        
        (self.iCode, 
         self.instNameFull, 
         self.instTown, 
         self.instCountry) = items
        
        self.rawInfo = rawInfo

        self.instName = self.instNameFull # default

        self.hasNameSign = False
        if self.rawInfo.nameSign != None and self.rawInfo.nameSign.strip() != '':
            self.instName = self.rawInfo.nameSign
            self.hasNameSign = True

        self.styles = ['tex', 'revtex', 'html', 'xml', 'xml2', 'xmlN']

        self.styleNames = {}

        # for the "INFN" like multi-insts
        self.instList = [] 
        self.isMultiInst = False
        self.checkMultiInst()

        pass

    def asList(self):
        return [self.iCode, self.instNameFull, self.instTown, self.instCountry], self.rawInfo

    def checkMultiInst(self):
        # check now and handle multiple instiutes in one town (INFN, Brasilian, ...)
        # do this before replacing spaces with nbspVal, as this introduces a ';' for html ...
        # split only at strings like "); " to avoid false splitting at UTF-8 chars:

        # make sure the INFN institutes have the same structure; add an additional ')' so it's still there after the splitting
        self.prepareStyledNames()

        input = self.styleNames['tex']

        if input.lower().startswith('infn'):
            input = self.instName.replace('),', ');').replace(');', '));')
            
            # Hack, should be fixed in DB (and checked for others like that)
            input = input.replace( 'Pisa Italy', 'Pisa, Italy' )

            # INFN Sezione di Pisa (a); Università di Pisa (b); Scuola Normale Superiore di Pisa (c); Pisa Italy, Università di Siena (d), Siena, Italy
            res = input.split( 'Italy' )
            # if "Napoli" in input:
            #     print()
            #     print('===>>>> instName     ', self.instName)
            #     print('===>>>> res          ', res)

            self.instList = []
            for item in res:
                if item.strip() == '': continue
                items = item.split(');')
                tmpList = []
                tmpTown = '%s,' % self.instTown # set a default in case none is in the string
                for entry in items:
                    if re.search( r'\([abcd]\)', entry): 
                        if entry.startswith(','): entry = entry[1:]
                        tmpList.append( entry.strip() )
                    else:
                        tmpTown = entry.strip()
                for inst in tmpList:
                    if 'Italy' in inst[-9:]:
                        partName = inst
                    else:
                        partName = '%s, %s Italy' % (inst, tmpTown.strip() )
                    self.instList.append( partName.replace( '  ', ' ') )
            # if "Pisa" in input:
            #     print('===>>>> instlist     ', self.instList )
            #     print()

        self.isMultiInst =  len(self.instList) > 1

    def memberStatus(self):
        return 'member' if self.rawInfo.cmsStatus == 'Yes' else 'nonmember'

    def getDomains(self):
        urlList = []

        instUrl = 'n/a'
        if self.rawInfo.domain:
            instUrl = 'http://' + self.rawInfo.domain
        else:
            if self.rawInfo.wwwInst:
                instUrl = self.rawInfo.wwwInst
        urlList.append( instUrl )

        if self.rawInfo.domainA and 'http://' + self.rawInfo.domainA not in urlList: 
            urlList.append( 'http://' + self.rawInfo.domainA )
        else:
            urlList.append( 'n/a' )
        if self.rawInfo.domainB and 'http://' + self.rawInfo.domainB not in urlList: 
            urlList.append( 'http://' + self.rawInfo.domainB )
        else:
            urlList.append( 'n/a' )
        if self.rawInfo.domainC and 'http://' + self.rawInfo.domainC not in urlList: 
            urlList.append( 'http://' + self.rawInfo.domainC )
        else:
            urlList.append( 'n/a' )
        if self.rawInfo.domainD and 'http://' + self.rawInfo.domainD not in urlList: 
            urlList.append( 'http://' + self.rawInfo.domainD )
        else:
            urlList.append( 'n/a' )

        return urlList

    def getSpires(self):
        return (self.rawInfo.spiresIcn, 
                self.rawInfo.spiresIcnA, 
                self.rawInfo.spiresIcnB, 
                self.rawInfo.spiresIcnC, 
                self.rawInfo.spiresIcnD, 
        )

    def getSpiresName(self, style):
        repl = '\\&'
        if 'ml' in style:
            repl = '&amp;'
        sName = self.sanitizeAmpersandInName( self.rawInfo.spiresIcn, replacement=repl )
        return sName

    def sanitizeAmpersandInName(self, name, replacement='\\&'):

        return name.replace(r'A&M', r'A%sM' % replacement) \
                   .replace(r'A&amp;M', r'A%sM' % replacement) \
                   .replace(r' & ', r' %s ' % replacement)\
                   .replace(r'&quot;', "'")\
                   .replace('&#8203', '')  # zero width space

    def prepareStyledNames(self):

        tmpl = { 'tex': { 'nbspVal': '~' },
                 'revtex': { 'nbspVal': '~' },
                 'html': { 'nbspVal': '&nbsp;' },
                 'xml': { 'nbspVal': ' ' },
                 'xml2': { 'nbspVal': ' ' },
                 'xmlN': { 'nbspVal': ' ' },
        }
        for style in self.styles:
            self.styleNames[style] = self.prepInstName(style, tmpl[style])

        # print( "++++>> ", self.styles, self.styleNames)

    def prepInstName(self, style, templates):

        instName = self.instName
        if self.hasNameSign:
            instName = self.rawInfo.nameSign

        if '\\' in instName:
            print( '==> \\: ', self.instName, self.rawInfo.nameSign)

        if instName.strip() == '':
            theLogger.error("ERROR: inst w/o name found - skipping !! code='%s'" % self.iCode)

        if self.isMultiInst:
            self.instName = re.sub(r'\s+', templates['nbspVal'], instName)

        if 'tex' in style:
            if self.isMultiInst:
                instName = ', '.join(
                    [re.sub(r'(.*)\(([a-z])\)(.*)', r'\1$^{\2}$\3', x) for x in self.instList])
            else:
                instName = re.sub(r'([,]+)(?![\s~])', r', ', instName)
            instName = utf8ToTex(instName)

        elif 'xml' in style:
            instName = self.sanitizeAmpersandInName(instName, replacement='&amp;').replace(');', '),')

        elif 'html' in style:
            if self.isMultiInst:
                instName = ', '.join(
                    [re.sub(r'(.*)\(([a-z])\)(.*)', r'\1<sup>\2</sup>\3', x) for x in self.instList])
            else:
                instName = re.sub(r'([,]+)(?![\s&])', r', ', instName)

        return instName
    
    def getFormattedAlsoNowName(self, style, templates):
        repl = '\\&'
        if 'html' in style:
            repl = '&amp;'
        anName = self.sanitizeAmpersandInName( self.getFormattedInstName(style, templates), replacement=repl )

        for sup in ['a', 'b', 'c', 'd']:
            if 'tex' in style:
                anName = anName.replace('~(%s)' % sup, '').replace('~~', '~')
            else:
                anName = anName.replace('%s(%s)' % (templates['nbspVal'], sup), '')

        if 'tex' in style:
            anName = anName.replace('~', ' ') # as discussed with GeorgeA, see if that works 

        return anName

    def getFormattedInstName(self, style, templates):

        instName = self.styleNames[style]

        townName = self.instTown
        countryName = self.instCountry
        if 'tex' in style:
            townName = utf8ToTex(townName)
            countryName = utf8ToTex(countryName)

        nameHasTownCountry = False
        usStateName = self.rawInfo.usState or 'N/A'

        if 'USA' in countryName:
            townCountry = ',%s,%s,%s' % (townName, usStateName, countryName)
        else:
            townCountry = ',%s,%s' % (townName, countryName)

        if instName.startswith('INFN'):
            lastPart = instName[-(1+len(countryName)):].replace(' ', templates['nbspVal']).replace(');', '),')
            # for INFN, check if country name is last, if so, it's OK, otherwise add town, country
            if ( not countryName.replace('~','').replace(' ','').replace(templates['nbspVal'],'') in lastPart ): 
                instName = '%s, %s, %s' % (instName.replace(' ', templates['nbspVal']).replace(');', '),'), townName, countryName)
                nameHasTownCountry = True
                theLogger.warning( f"INFN  W/O country name found: -{countryName.replace('~','').replace(' ','').replace(templates['nbspVal'],'').encode('utf-8')}- IS NOT IN -{lastPart}-- {instName.replace(templates['nbspVal'],' ').encode('utf-8')}" ) 
            else:
                instName = instName.replace(' ', templates['nbspVal']).replace(');', '),')
                nameHasTownCountry = True
                theLogger.warning( f"INFN with country name found: {countryName.replace('~','').replace(' ','').replace(templates['nbspVal'],'').encode('utf-8')} IS IN {lastPart} -- {instName.replace(templates['nbspVal'],' ').encode('utf-8')}" ) 

        else: # other institutes than INFN ones
            if ( townCountry.replace('~','').replace(' ','').replace(templates['nbspVal'],'') in 
                    instName[-(3+len(townCountry)):].replace('~','').replace(' ','').replace(templates['nbspVal'],'') ): 
                nameHasTownCountry = True
                # ensure the "spaces" around town/country are correct:
                ntcRe = re.compile( r'^(.*),[~ ]?(.*),[~ ]?(.*)$' )
                ntcMatch = ntcRe.match(instName)
                if ntcMatch:
                    instNameNoTC, town, cntry = ntcMatch.groups()
                    instName = '%s, %s, %s' % (instNameNoTC.replace(' ', templates['nbspVal']).replace(');', '),'), town, cntry)
            elif ( countryName.replace('~','').replace(' ','').replace(templates['nbspVal'],'') in 
                        instName[-(1+len(countryName)):] ): 
                # print( f"inst with country name found: {countryName.replace('~','').replace(' ','').replace(templates['nbspVal'],'')} IS IN {instName[-(1+len(countryName)):]}" ) 
                instName = '%s, %s, %s' % (instName.replace(' ', templates['nbspVal']).replace(');', '),'), townName, countryName)
                nameHasTownCountry = True
            else:
                instName = instName.replace(' ', templates['nbspVal']).replace(');', '),')

        # print('+++>>1> ', nameHasTownCountry, townCountry, instName[-(3+len(townCountry)):].replace('~','').replace(' ','').replace(templates['nbspVal'],'') )

        sanitizedInstName = instName
        if 'tex' in style:
            sanitizedInstName = utf8ToTex( self.sanitizeAmpersandInName(instName, replacement='\\&' ) )
        if 'xml' in style or 'html' in style:
            sanitizedInstName = self.sanitizeAmpersandInName(instName, replacement='&amp;' )

        if nameHasTownCountry:
            return sanitizedInstName
        else:
            # APS, the (only?) publisher using the revtex file, wants the US institutes
            # to also have the name of the state in the string, so take care of that:
            if 'USA' in countryName:
                stateName = self.rawInfo.usState or 'N/A'
                return '%s, %s, %s, %s' % (sanitizedInstName,
                                            townName,
                                            usStateName,
                                            countryName)
            else:
                return '%s, %s, %s'  % (sanitizedInstName,
                                        townName,
                                        countryName)

        return None
    
    def getFormattedNameList(self, style, templates):
        newList = []
        
        for instName in self.instList:
            if templates['multiInstLast'].strip() != '':
                for sup in ['a', 'b', 'c', 'd']:
                    instName = instName.replace('(%s)' % sup, templates['multiInstLast'] % sup)
            newList.append( utf8ToTex( instName ) )
        return newList



class PaperAuthorListGenerator(object):

    def __init__(self, db):
        self.paperCode = None
        self.isInitialised = True
        self._errMessages = []
        if not self.reset(db):
            self.isInitialised = False

    def error(self, message: str):
        self._errMessages.append(message)

    @property
    def errMessages(self):
        return '\n'.join(self._errMessages)

    @errMessages.setter
    def errMessages(self, messages):
        if isinstance(messages, str):
            self._errMessages = messages.split('\n')
        self._errMessages = list(messages)

    def reset(self, dbIn):

        self.db = dbIn

        self.instAuthors = {}
        self.instInfo = {}
        self.instCodeList = []
        self.sortedAuthors = {}

        self.usaOrder = []

        self.authorInfo = {}
        self.authorInfoRaw = {}

        self.deceased = []
        self.alsoNowAtCodeList = []
        self.alsoNowAtCodeMap = {}
        self.alsoNowAtUser = {}
        self.alsoNowAtFN = {}
        self.instFootnotes = {}

        self.univSuperscript = {}

        self.referenceInsts = {}  # (code : (name, town, country, nAuth) )

        self.orcidMap = getOrcIdMap(self.db)
        self.inspireIdMap, self.nameMap = getInspireIDMap(self.db)

        self.dirMaps = {'ref': flask.current_app.config.get('AL_REF_DIR') if flask.current_app.config.get('AL_REF_DIR') else '/tmp/test-al-refs',
                        'diff': flask.current_app.config.get('AL_DIFFS_DIR') if flask.current_app.config.get('AL_DIFFS_DIR') else '/tmp/test-al-diffs',
                        'new': flask.current_app.config.get('AL_FILES_DIR') if flask.current_app.config.get('AL_FILES_DIR') else '/tmp/test-al-files',
                        }

        for item, path in self.dirMaps.items():
            if not os.path.exists(path):
                self.error("directory for %s at %s is not existing, please make sure it exists ..." % (
                    item, path))
            else:
                if not os.path.isdir(path):
                    self.error("directory for %s at %s already existing, but not a directory ..." % (
                        item, path))

        if self.errMessages:
            theLogger.error(self.errMessages)
            return False

        return True

    def prepare(self):

        alsoNowAtIndex = 0
        for inst in self.instCodeList:

            self.sortedAuthors[inst] = sorted(self.instAuthors[inst], key=coll.sort_key)

            for nSig in self.sortedAuthors[inst]:
                author = self.authorInfo[nSig]

                if author.iCodeAlso:
                    alsoNowAtIndex = self.prepareAltInst(
                        alsoNowAtIndex, nSig, 'Also', author.iCodeAlso)

                if author.iCodeNow:
                    alsoNowAtIndex = self.prepareAltInst(
                        alsoNowAtIndex, nSig, 'Now', author.iCodeNow)

                if author.infn is not None:
                    if nSig not in self.univSuperscript.keys():
                        self.univSuperscript[nSig] = [author.infn]
                    else:
                        self.univSuperscript[nSig].append(author.infn)

                if author.univOther is not None:
                    if nSig not in self.univSuperscript.keys():
                        self.univSuperscript[nSig] = [author.univOther]
                    else:
                        self.univSuperscript[nSig].append(author.univOther)

                if '.' not in author.authName:
                    theLogger.warning("no . found in %s " % str(author))

        # pprint.pprint(self.alsoNowAtFN)
        # pprint.pprint(self.alsoNowAtUser)

        theLogger.info("preps done ... \n")
        return

    def prepareAltInst(self, alsoNowAtIndex, nSig, what, codeIn):
        alsoName = self.db.session.query(Institute).filter_by(code=codeIn).one().name
        if nSig not in self.alsoNowAtUser.keys():
            self.alsoNowAtUser[nSig] = [alsoName]
        else:
            self.alsoNowAtUser[nSig].append(alsoName)
        if alsoName not in self.alsoNowAtFN.keys():
            alsoNowAtIndex += 1
            self.alsoNowAtFN[alsoName] = alsoNowAtIndex
            self.instFootnotes[alsoNowAtIndex] = (what, alsoName, codeIn)
        # update author count for ref inst:
        if codeIn in self.referenceInsts.keys():
            self.referenceInsts[codeIn][3] += 1
        return alsoNowAtIndex

    def getListFromDB(self):

        self.allInsts = (self.db.session.query(Institute.code,
                                               Institute.name,
                                               Institute.nameSign,
                                               Institute.town,
                                               Institute.country,
                                               Institute.cmsStatus,
                                               Institute,
                                               )
                         .all())

        # print '\n\n==> ', self.allInsts, '\n\n'

        self.usaOrder = (self.db.session.query(Institute.code,
                                               Institute.name,
                                               Institute.town,
                                               Institute.country,
                                               Institute.cmsStatus,
                                               Institute.usaOrder,
                                               )
                         .filter(Institute.country == 'USA')
                         .filter(Institute.cmsStatus == 'Yes')
                         .order_by(Institute.usaOrder)
                         .order_by(Institute.name)
                         .all())

        self.usaUnOrder = (self.db.session.query(Institute.code,
                                                 Institute.name,
                                                 Institute.town,
                                                 Institute.country,
                                                 Institute.cmsStatus,
                                                 Institute.usaOrder
                                                 )
                           .filter(Institute.country == 'USA')
                           .filter(Institute.cmsStatus != 'Yes')
                           .filter(Institute.usaOrder == None)
                           .order_by(Institute.usaOrder)
                           .all())

        countries = (self.db.session.query(Institute.country)
                     .filter(PaperAuthor.instCode == Institute.code)
                     .filter(PaperAuthor.code == self.paperCode)
                     .order_by(Institute.country, Institute.usaOrder, Institute.town, Institute.name,
                               PaperAuthor.name, PaperAuthor.namf)
                     .all())

        # | authorID | cmsid | hrid | code       | nameSignature | status | statusCms | activity  | instCode | instCodeNow | instCodeAlso | instCodeForced | infn | univOther | inspireid | name   | namf  |
        # |  1585282 | -1    | -1   | EXO-17-006 | C. Biggio     | in NEW | EXTERNAL  | Undefined | GENOVA   |             |              | NULL           |      |           | 00054613  | Biggio | Carla |

        # noinspection PyUnresolvedReferences
        subQuery1 = (self.db.session.query( PaperAuthor.nameSignature,
                                       PaperAuthor.cmsid,
                                       PaperAuthor.instCode,
                                       PaperAuthor.instCodeNow,
                                       PaperAuthor.instCodeAlso,
                                       PaperAuthor.instCodeForced,
                                       PaperAuthor.infn,
                                       PaperAuthor.univOther,
                                       PeopleData.lastName,
                                       PeopleData.firstName,
                                       PaperAuthor.statusCms,
                                       Institute.country,
                                       Institute.town,
                                       Institute.name,
                                       Institute.nameSign,
                                       Institute.cmsStatus,
                                       PaperAuthor.inspireid,
                                       PaperAuthor,
                                       PeopleUserData,
                                       Institute
                                       )
                        .join( PeopleData, PeopleData.cmsId == PaperAuthor.cmsid )
                        .outerjoin( PeopleUserData, PeopleUserData.cmsId == PaperAuthor.cmsid )
                        .filter(PaperAuthor.status.like('in%') )
                        .filter(PaperAuthor.instCode == Institute.code)
                        .filter(PaperAuthor.code == self.paperCode)
                        )

        resultAll =  (subQuery1.order_by(Institute.country, Institute.town, Institute.name,
                                         PaperAuthor.name, PaperAuthor.namf )
                               .all() )

        resultUSA    =  (subQuery1.filter( Institute.country == 'USA' )
                                  .order_by(Institute.country, Institute.usaOrder, Institute.name, Institute.town,
                                            PaperAuthor.name, PaperAuthor.namf )
                                  .all() )

        theLogger.info( "\n found %s authors in db for %s ...\n" % (len(resultAll+resultUSA), self.paperCode) )

        index = 0
        nowAlso = []
        additionalInstCodes = set()
        directInstCodes = set()
        usaDone = False
        for item in resultAll:

            # first time we see an US institute, handle them all - in their specific order
            if not usaDone and 'USA' in item[11]:
                for item in resultUSA:
                    index += 1
                    self.handleItem(additionalInstCodes,
                                    directInstCodes, item, nowAlso)
                usaDone = True  # ... flag we're done ...
            if 'USA' in item[11]:
                continue  # ... and skip all the other US entries

            index += 1
            self.handleItem(additionalInstCodes,
                            directInstCodes, item, nowAlso)

        for item in self.allInsts:
            (iCode, instNameFull, instNameSign, instTown,
             instCountry, instStatus, InstituteRaw) = item
            if 'ForReference' in instStatus:
                self.referenceInsts[iCode] = [instNameFull,
                                              instTown, instCountry, 0, InstituteRaw]

        anCMindex = 1
        for naC, na in sorted(nowAlso):
            if not na or na.strip() == u'' or na in self.instCodeList:
                continue
            if na not in self.alsoNowAtCodeList:
                self.alsoNowAtCodeList.append(na)
                self.alsoNowAtCodeMap[na] = anCMindex
                anCMindex += 1

        # print "++> found %s for-ref institutes ..." % ( len( self.referenceInsts.keys() ) )
        # print '++> %s additionalInsts : \n\t%s ' % ( len(additionalInstCodes), additionalInstCodes )
        # print '++> direct: ', len(directInstCodes), ' add-dir: ', len( additionalInstCodes-directInstCodes )
        # print '++> add-dir: \n\t%s' % (additionalInstCodes-directInstCodes)
        # print '\n\n++> alsoNowCodeList: len: %s \n %s \n\n' % ( len(self.alsoNowAtCodeList), self.alsoNowAtCodeList)

        return

    def handleItem(self, additionalInstCodes, directInstCodes, item, nowAlso):

        (authName, cmsId, iCode, iCodeNow, iCodeAlso, iCodeForced,
         infn, univOther, name, namf, status, instCountry, instTown,
         instNameFull, instNameSign, instStatus, authInspId,
         paperAuthorRaw, peopleUserDataRaw, InstituteRaw) = item

        # make sure these are set, they are used for the new XML file:
        try:
            if name.strip() == '':
                name = self.nameMap[paperAuthorRaw.hrid][1]
            if namf.strip() == '':
                namf = self.nameMap[paperAuthorRaw.hrid][0]
        except KeyError as e:
            theLogger.error(
                "ERROR: no author found in nameMap for HRId %s " % paperAuthorRaw.hrid)

        if not self.orcidMap:
            self.orcIdMap = getOrcIdMap(self.db)

        authInspId = self.getInspireId(paperAuthorRaw)

        authOrcId = self.orcidMap[int(cmsId)].strip() if int(cmsId) in self.orcidMap.keys() else None

        if int(cmsId) < 0:
            theLogger.warning( f' !!++!! Found negative cmsId for {item} ... ') 
            orcMatch = re.match( r'^[\d-]+$', authInspId)
            theLogger.warning( f" !!++!! inspireID {authInspId} - match: {orcMatch} ... ") 

            if orcMatch : # inspire ID is actually an OrcID, swap:
                authOrcId = authInspId
                authInspId = None
                theLogger.warning( f" !!++!! swapped: {authInspId} - {authOrcId} ... ") 

        # first re-pack the authName from the DB to lastname|initials so we can sort it.
        # also check if we have the same (repacked) authName already in the full list,
        # if so, add the cmsid to make the additional ones unique (and still preserve
        # sortability.
        nSig = repack(authName, namf)
        newAuthor = Author([authName, cmsId, iCode, iCodeNow, iCodeAlso, iCodeForced,
                            infn, univOther, name, namf, status, instCountry, instTown,
                            instNameFull, instNameSign, instStatus, authInspId, authOrcId], paperAuthorRaw, peopleUserDataRaw )

        # make sure nSig is unique as an authorInfo key and still sorts with the name
        nSig += '__' + str(cmsId)
        if nSig in self.authorInfo.keys():
            theLogger.error("ERROR: found %s to be already in authorInfo.keys() !?!?!" % nSig.encode(
                'ascii', 'xmlcharrefreplace'))

        self.authorInfo[nSig] = newAuthor
        self.authorInfoRaw[nSig] = (paperAuthorRaw, InstituteRaw)

        newInst = CMSInst([iCode, instNameFull, instTown, instCountry], InstituteRaw)

        # prepare the institute list
        if iCode not in self.instInfo.keys():
            self.instCodeList.append(iCode)
            self.instInfo[iCode] = newInst
        else:
            if [iCode, instNameFull, instTown, instCountry] != self.instInfo[iCode].asList()[0]:
                theLogger.warning("WARNING: inst code %s already in list and different: %s" % (
                    iCode, self.instInfo[iCode].iCode))
        directInstCodes.add(iCode)
        if not iCode in self.instAuthors:
            self.instAuthors[iCode] = []
        self.instAuthors[iCode].append(nSig)
        for otherInst in [iCodeNow, iCodeAlso]:
            if otherInst not in self.instCodeList:
                nowAlso.append((instCountry, otherInst))
                additionalInstCodes.add(otherInst)

    def getInspireId(self, paperAuthorRaw):
        authInspId = paperAuthorRaw.inspireid
        if authInspId and paperAuthorRaw.hrid in self.inspireIdMap.keys() and authInspId != self.inspireIdMap[paperAuthorRaw.hrid]:
            theLogger.warning("InspireIDs not consistent: PeopleData: %s while in PaperAuthor %s " % (
                self.inspireIdMap[paperAuthorRaw.hrid], authInspId))
        if ((not authInspId or authInspId.strip() == '') and
                (str(paperAuthorRaw.hrid) in self.inspireIdMap.keys())):
            authInspId = self.inspireIdMap[paperAuthorRaw.hrid]
        return authInspId

    def writeFullHTML(self):

        head = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Author Info</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link href="/iCMS/css/mvn_copy.css" rel="stylesheet" type="text/css">
<link href="/iCMS/css/widgets/display/display_mvn.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0">
<script language="JavaScript1.2" src="/iCMS/js/mvn.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/iCMS/js/an.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/iCMS/js/importxml.js" type="text/javascript"></script>
<script language="JavaScript1.2" src="/iCMS/js/utility.js" type="text/javascript"></script>
<font style="font-size: 14px;">
"""

        tail = '''</font>
</body>
</html>
'''

        fullHTMLFileName = '%s-authorlist.html' % self.paperCode
        with codecs.open(os.path.join(self.dirMaps['new'], fullHTMLFileName), "w", "utf-8") as outFile:

            outFile.write(head)

            self.writeBody(outFile, style='html', templates=htmlTemplates)

            # <br/><br/>2264<br/><br/>
            outFile.write('\n<br/><br/>%s<br/><br/>\n' %
                          len(self.authorInfo.keys()))

            # ForReference inst: 46<br>
            outFile.write('ForReference inst: %s\n' % len(
                [v for v in self.referenceInsts.values() if v[3] > 0]))

            # # list of inst:
            # outFile.write('list of inst:\n')

            # ROMA-UNIV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Facolta Ingegneria, Universita di Roma, Roma, Italy<br>
            for code, vals in self.referenceInsts.items():
                if vals[3] > 0:
                    outFile.write('%s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s, %s, %s \n' % (
                        code, vals[0].replace(r'A&M', r'A&amp;M'), vals[1], vals[2]))
            # ...

            # < br >
            outFile.write('<br />\n')
            # Inst UCLA, 132 < br >
            for iCode in self.instCodeList:
                outFile.write('Inst %s, %s <br/>\n' %
                              (iCode, len(self.instAuthors[iCode])))
            # ...
            # RefInst ROMA - UNIV, 1 < br >
            for code, vals in self.referenceInsts.items():
                if vals[3] > 0:
                    outFile.write('RefInst %s, %s <br/>\n' % (code, vals[3]))

            outFile.write(tail)

    def writeBodyHTML(self):

        bodyHTMLFileName = '%s-authorlist_body.html' % self.paperCode
        with codecs.open(os.path.join(self.dirMaps['new'], bodyHTMLFileName), "w", "utf-8") as outFile:
            self.writeBody(outFile, style='html', templates=htmlTemplates)

        return

    def writeTexFile(self):

        bodyTexFileName = '%s-authorlist.tex' % self.paperCode
        with codecs.open(os.path.join(self.dirMaps['new'], bodyTexFileName), "w", "utf-8") as outFile:
            # header = '\\newcommand{\\cmsorcid}[1]{\\href{https://orcid.org/#1}{\\hspace*{0.1em}\\raisebox{-0.45ex}{\\includegraphics[width=1em]{ORCIDiD_iconvector.pdf}}}}'
            # outFile.write(header)
            self.writeBody(outFile, style='tex', templates=texTemplates)

        return

    def writeRevTexFile(self):

        bodyTexFileName = '%s-authorlist.revtex' % self.paperCode
        with codecs.open(os.path.join(self.dirMaps['new'], bodyTexFileName), "w", "utf-8") as outFile:
            self.writeRevTex(outFile, style='tex', templates=revtexTemplates)

        return

    def writeXMLFiles(self):

        # bodyTexFileName = '%s-authorlist.xml' % self.paperCode
        # with codecs.open(os.path.join(self.dirMaps['new'], bodyTexFileName), "w", "utf-8") as outFile:
        #     self.writeXML(outFile, style='xml', templates=xmlTemplates)

        # bodyTexFileName = '%s-authorlist2.xml' % self.paperCode
        # with codecs.open(os.path.join(self.dirMaps['new'], bodyTexFileName), "w", "utf-8") as outFile:
        #     self.writeXML(outFile, style='xml2', templates=xmlTemplates)

        bodyTexFileName = '%s-journal-internal-authorlistN.xml' % self.paperCode
        with codecs.open(os.path.join(self.dirMaps['new'], bodyTexFileName), "w", "utf-8") as outFile:
            self.writeXMLNew(outFile, style='xmlN', templates=xmlNTemplates)

        return

    def writeBody(self, outFile, style, templates):

        if templates['top']:
            outFile.write(templates['top'] % (
                self.paperCode, datetime.date.today().strftime('%d/%m/%Y')))

        daggerSeen = False
        for iName in self.instCodeList:

            cmsInst = self.instInfo[iName]
            if cmsInst.isMultiInst:
                outName = ', '.join( cmsInst.getFormattedNameList(style, templates) )
                outFile.write( templates['instSignTempl'] % outName )
            else:
                outFile.write( templates['instSignTempl'] % cmsInst.getFormattedInstName(style, templates) )

            for nSig in self.sortedAuthors[iName]:
                author = self.authorInfo[nSig]
                lastAuthor = self.authorInfo[self.sortedAuthors[iName][-1]]

                alsoNowAtAct = ''
                # first add the superscripts for the local universities ...
                if cmsInst.isMultiInst and nSig in self.univSuperscript.keys():
                    # theLogger.debug( "inst: %s nsig %s  sup: %s" % (instName, nSig, self.univSuperscript[nSig])
                    lastSup = self.univSuperscript[nSig][-1]
                    for sup in self.univSuperscript[nSig]:
                        # if we already have a chain of superscripts, add a comma next:
                        commaStr = ','
                        if 'tex' in style:
                            commaStr = ', '
                        if alsoNowAtAct != '':
                            alsoNowAtAct += templates['multiInstLast'] % commaStr
                        # now add the new value
                        alsoNowAtAct += templates['multiInstLast'] % sup

                # ... then the nowAt, ... info
                if nSig in self.alsoNowAtUser.keys():
                    for item in self.alsoNowAtUser[nSig]:
                        # if we already have a chain of superscripts, add a comma next:
                        if alsoNowAtAct != '':
                            if 'tex' in style:
                                alsoNowAtAct += '$^{, }$'
                            else:
                                alsoNowAtAct += templates['multiInstLast'] % ','
                        alsoNowAtAct += templates['alsoNowAtTemplLast'] % self.alsoNowAtFN[item]

                if 'deceased' in author.status.lower():
                    alsoNowAtAct = templates['alsoNowAtTempl'] % '&dagger;'
                    if 'tex' in style:
                        alsoNowAtAct = '$^{\\textrm{\\dag}}$'
                    daggerSeen = True

                if 'tex' in style:
                    authorName = utf8ToTex(author.authName)
                else:  # html output ...
                    authorName = author.authName

                authorName = self.reformat(authorName, templates['nbspVal'], style=style)

                # add the OrcID if available
                orcId = author.orcId

                outString = ''
                
                if orcId:
                    templ = templates['authTemplOrcid']
                    if author == lastAuthor:
                        templ = templates['lastAuthTemplOrcid']
                    outString = templ % (authorName, alsoNowAtAct, templates['orcid'] % orcId)
                else:
                    templ = templates['authTempl']
                    if author == lastAuthor:
                        templ = templates['lastAuthTempl']
                    outString = templ % (authorName, alsoNowAtAct)

                # if author != lastAuthor: outString += ','
                outFile.write( outString )

        if daggerSeen:
            outFile.write(templates['daggerFN'])

        # here we need to go back to the full list of institutes, as not all of the referenced ones are members of CMS
        footNotes = ''
        for i in self.instFootnotes.keys():
            alsoNow, iName, iCode = self.instFootnotes[i]
            alsoInst = self.findInst(iCode)
            instName = alsoInst.getFormattedAlsoNowName(style, templates)

            footNotes += templates['alsoNowAtTemplFN0'] % (i, alsoNow, instName)
        outFile.write(footNotes)

        return

    def reformat(self, authName, nbspVal, style=None):

        if '. ' in authName:
            initials, name = authName.rsplit('. ', 1)
            initials += '.'  # add back the stripped '.'
        elif ' ' in authName:
            initials, name = authName.rsplit(' ', 1)
        else:
            theLogger.warning('no space in authName: %s [%s]' % (
                authName, ', '.join(authName.rsplit('.', 1))))
            initials, name = authName.rsplit('.', 1)
            initials += '. '  # restore split away dot and add a space

        swappedRe = re.compile(r'^\s*([A-Z].*),\s*([A-Z.\s]+)\s*$')

        if swappedRe.match(authName):
            name, initials = swappedRe.match(authName).groups()

        # if 'tex' in style and swappedRe.match( authName ):
        #     print "===> swapped name found: '%s '" % authName

        return '%s%s%s' % (initials.strip().replace('. ', '.'), nbspVal, name.strip().replace(' ', nbspVal))

    def writeRevTex(self, outFile, style, templates):

        for iCode in self.instCodeList:

            cmsInst = self.instInfo[iCode]

            for nSig in self.sortedAuthors[iCode]:

                author = self.authorInfo[nSig]

                authorName = utf8ToTex(author.authName)
                authorName = self.reformat(authorName, templates['nbspVal'], style=style)

                authorName = authorName.replace(' ', templates['nbspVal'])

                templ = templates['authTempl']
                outFile.write(templ % (authorName, ))

                alsoNowList = []
                if author.iCodeAlso:
                    alsoNowList.append(author.iCodeAlso)
                if author.iCodeNow:
                    alsoNowList.append(author.iCodeNow)
                for alsoNow in alsoNowList:
                    if alsoNow:
                        alsoInst = self.findInst(alsoNow)
                        instName = alsoInst.getFormattedInstName(style, templates)
                        # remove superscripts in footnotes (alsoAt/NowAt)
                        if alsoInst.isMultiInst:
                            for sup in ['a', 'b', 'c', 'd']:
                                instName = instName.replace('~$^{%s}$' % sup, '')

                        outFile.write(templates['alsoNowAtTempl0'] % instName)
                    else:
                        theLogger.error(
                            "ERROR alsoNow inst code %s not found anywhere ... " % alsoNow)

                if 'deceased' in author.status.lower():
                    alsoNowAtAct = outFile.write(templates['daggerFN'])

            if cmsInst.isMultiInst:
                instNameList = cmsInst.getFormattedNameList(style, templates)
                for instName in instNameList:
                    outFile.write( templates['instSignTempl'] % instName)
            else:
                instName = cmsInst.getFormattedInstName(style, templates)
                outFile.write( templates['instSignTempl'] % instName)

        return

    def writeXML(self, outFile, style, templates):

        outFile.write(templates['top'])

        abcd = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e'}

        instId = 0
        for iCode in self.instCodeList:
            instId += 1
            # handle multiple instiutes in one town (INFN, Brasilian, ...)

            cmsInst = self.instInfo[iCode]

            if cmsInst.isMultiInst:
                # for i in range(len(instList)):                    
                for instName in cmsInst.getFormattedNameList(style, templates):
                    outFile.write( templates['instSignTempl'] % (str(instId), instName ) )
            else:
                outFile.write( templates['instSignTempl'] % (str(instId), cmsInst.getFormattedInstName(style, templates) ) )

            for nSig in self.sortedAuthors[iCode]:

                author = self.authorInfo[nSig]
                lastAuthor = self.authorInfo[self.sortedAuthors[iCode][-1]]

                # handle the last author separately:
                if author == lastAuthor:
                    templ = templates['lastAuthTempl']
                else:
                    templ = templates['authTempl']

                affilId = str(instId)
                if cmsInst.isMultiInst:
                    # set default to be the first institute if multiple
                    affilId = str(instId)+'a'
                    if author.infn:
                        for subInst in author.infn:
                            if subInst == 'a':
                                continue  # this is already set as default
                            if affilId != '' and affilId[-1] != ',':
                                affilId += ','
                            affilId += str(instId)+subInst
                if author.univOther and (str(instId) + author.univOther) not in affilId:
                    if affilId[-1] != ',':
                        affilId += ','
                    affilId += str(instId) + author.univOther

                # note: alsoNowAt seems not to be handled here - is that intentional ? 

                authNameSign = author.authName.replace(' ', templates['nbspVal'])

                if style == 'xml2':
                    authNameSign = u'%s %s' % (author.namf.replace(' ', templates['nbspVal']),
                                               author.name.replace(' ', templates['nbspVal']))

                outFile.write(templ % (affilId, authNameSign))

        # at least properly close the starting tag ...
        outFile.write('</authors>')

        return

    def writeXMLNew(self, outFile, style, templates):

        outFile.write(templates['top'] % {'creationtime': time.strftime(
            '%Y-%m-%d_%H:%M', time.localtime())})

        abcd = {0: 'a', 1: 'b', 2: 'c', 3: 'd'}

        # ===================== Institutes =====================

        outFile.write('<cal:organizations>\n\n')

        instId = 0
        usInstDone = False
        instIdMap = {}
        for iCode in self.instCodeList:
            inst = self.instInfo[iCode]

            instId += 1
            instIdMap[iCode] = instId
            self.handleOrgForInstitute(abcd, iCode, instId, outFile, style, templates)

        # now handle reference-only insts id="roNN'
        refInstId = 1
        for rInstCode in self.alsoNowAtCodeList:
            self.handleRefInstitute(rInstCode, refInstId, outFile, style, templates)
            refInstId += 1

        outFile.write('\n</cal:organizations>\n')

        # ===================== Authors =====================

        outFile.write('\n<cal:authors>\n\n')

        usInstDone = False
        for iCode in self.instCodeList:
            inst = self.instInfo[iCode]
            instId = instIdMap[iCode]

            self.handleAuthForInstitute(iCode, instId, outFile, instIdMap)

        outFile.write('\n</cal:authors>\n')

        # at least properly close the starting tag ...
        outFile.write('\n</collaborationauthorlist>')

        return

    def makeAffilId(self, iCode, conn, instIdMap):

        affilId = ''
        if iCode in self.instCodeList:
            affilId = 'o%s' % instIdMap[iCode]
        if iCode in self.alsoNowAtCodeList:
            affilId = 'ro%s' % self.alsoNowAtCodeMap[iCode]
        return foafTemplates.foafAffiliation % {'orgId': '%s' % affilId, 'conn': conn}

    def handleAuthForInstitute(self, iCode, instId, outFile, instIdMap):

        cmsInst = self.instInfo[iCode]
        
        # make sure we have that info available
        if not self.orcidMap:
            self.orcIdMap = getOrcIdMap(self.db)

        for nSig in self.sortedAuthors[iCode]:
            author = self.authorInfo[nSig]

            paperAuthorRaw, InstituteRaw = self.authorInfoRaw[nSig]

            affilIds = set()
            if cmsInst.isMultiInst:
                # set default to be the first institute if multiple
                affIds = [str(instId) + 'a']
                if author.infn:
                    for subInst in author.infn:
                        if subInst == 'a':
                            continue  # this is already set as default
                        affIds.append(str(instId) + subInst)
                if author.univOther:
                    affIds.append(str(instId) + author.univOther)
                for affilId in affIds:
                    affilIds.add(foafTemplates.foafAffiliation %
                                 {'orgId': 'o%s' % affilId, 'conn': ''})
            else:
                # note: do not use the set literal "{...}" (as recommended by PyCharm), this leads to a crash ...
                affilIds = set([foafTemplates.foafAffiliation %
                                {'orgId': 'o%s' % str(instId), 'conn': ''}])

            affilIdsa = set()
            if author.iCodeAlso:
                affilIdsa.add(self.makeAffilId(
                    author.iCodeAlso, 'AlsoAt', instIdMap))
            if author.iCodeNow:
                affilIdsa.add(self.makeAffilId(
                    author.iCodeNow, 'NowAt', instIdMap))

            inspireId = self.getInspireId(paperAuthorRaw)
            authOrcId = None
            orcMatch = re.match( r'^[\d-]+$', inspireId)
            if orcMatch : # inspire ID is actually an OrcID, swap:
                theLogger.warning( f">>>> inspireID {inspireId} - match: {orcMatch} ... ") 
                authOrcId = inspireId
                inspireId = ''
                theLogger.warning( f">>>> swapped: {inspireId} - {authOrcId} ... ") 

            authIDs = []

            #-ap, 2024-10-11: on req of Paulina, only write InspireID if no OrcID is available:
            authCMSid = int(paperAuthorRaw.cmsid)
            if not ( authCMSid in self.orcidMap.keys() or authOrcId ):
                authIDs.append( foafTemplates.foafID % {'src': "INSPIRE", 'id': inspireId} )
            if int(paperAuthorRaw.hrid) > 0:
                authIDs.append(foafTemplates.foafID %
                               {'src': "CCID", 'id': str(paperAuthorRaw.hrid)})
            # add the OrcID if available, and if inspireID available for mapping:
            if authCMSid in self.orcidMap.keys():
                authIDs += [foafTemplates.foafID %
                            {'src': "ORCID", 'id': self.orcidMap[authCMSid].strip()}]
            elif authOrcId:
                authIDs += [foafTemplates.foafID %
                            {'src': "ORCID", 'id': authOrcId}]
                
            authStat = 'DECEASED' if 'deceased' in author.status.lower() else ''

            if not author.namf and not author.name:
                print( "\n===> ERROR: no name/firstname found for %s: " % author.authName )
                print( "\n ---- \n%s \n" % str(author) )
                print( "\n ---- \n%s \n" % str(paperAuthorRaw) )

            authSuffix = ''
            suffixRe = re.compile( r'^(.*) ([SJ]r\.)$' )
            suffixMatch = suffixRe.match( author.name )
            if suffixMatch:
                # author.name = suffixMatch.group(1) -- for now do not change the author's name, only add the detected suffix. 
                # authSuffix = suffixMatch.group(2)
                print( f'==++==> **IGNORING** found suffix for author: "{author.namf}" "{author.name}" - "{authSuffix}" -- "{author.authName}" ')

            authTempl = foafTemplates.foafAuthor % { 'status'             : authStat,
                                                        'name'               : '%s %s' % (author.namf, author.name),
                                                        'firstName'          : author.namf,
                                                        'lastName'           : author.name,
                                                        'suffix'             : authSuffix,
                                                        'namePaper'          : author.authName,
                                                        'authorAffiliations' : '\n'.join( sorted(list(affilIds))+sorted(list(affilIdsa)) ).strip( ),
                                                        'authorIDs'          : '\n'.join( authIDs ).strip( ),
                                                        }

            outFile.write( self.cleanXml( authTempl ) )

    def findInst(self, iCode):

        for item in self.allInsts:
            if iCode in item.code:
                (iCode, instNameFull, instNameSign, instTown,
                 instCountry, instStatus, InstituteRaw) = item
                return CMSInst( (iCode, instNameFull, instTown, instCountry), InstituteRaw )
        return None

    def handleRefInstitute(self, refInstCode, instId, outFile, style, templates):

        refInst = self.findInst(refInstCode)

        orgId = 'ro%i' % instId

        spires = []
        if refInst.rawInfo.spiresIcn:
            spires.append(foafTemplates.fofaSpires % {'spiresICN': refInst.getSpiresName(style) } )

        urlList = []
        instUrls = refInst.getDomains()
        urlList.append(foafTemplates.foafOrgUrl % {'url': instUrls[0], 'address': 'N/A'})
        urlList = [x.replace('http://http://', 'http://') for x in urlList]

        refInstName = refInst.getFormattedInstName(style, templates)
        # write out the "parent" institute first:
        outString = templates['instTempl'] % {'orgId': orgId,
                                                'urls': '\n'.join(urlList).strip(),
                                                'name': refInstName,
                                                'spires': '\n'.join(spires).strip(),
                                                'instId': refInst.rawInfo.cernInstId or instId,
                                                'status': 'CMS',
                                                'address': '',
                                                'member': 'member',
                                                }
        outFile.write( self.cleanXml(outString) )

    def handleOrgForInstitute(self, abcd, iCode, instId, outFile, style, templates):

        try:
            cmsInst = self.instInfo[iCode]
        except KeyError:
            theLogger.error("ERROR key %s not found in instInfo list - skipping ... " % iCode)
            return False

        # handle multiple institutes in one town (INFN, Brasilian, ...)
        try:
            instList = cmsInst.instList
        except KeyError:
            theLogger.error("ERROR splitting : key %s not found in instInfo list - skipping ... " % iCode)
            return False

        orgId = 'o%i' % instId

        spires = []
        spiresInst = cmsInst.getSpires()
        if spiresInst[0]:
            spires.append(foafTemplates.fofaSpires % {'spiresICN': spiresInst[0]})
        else:
            spires.append( '' )
        member = cmsInst.memberStatus()

        urlList = []
        instUrls = cmsInst.getDomains()

        urlList.append(foafTemplates.foafOrgUrl % {'url': instUrls[0], 'address': 'N/A'})

        if cmsInst.isMultiInst:
            for i in range(len(instList)):
                if spiresInst[i+1]:
                    spires.append(foafTemplates.fofaSpires % {'spiresICN': spiresInst[i+1]})
                else:
                    spires.append( '' )

                if instUrls[i+1]:
                    urlList.append(foafTemplates.foafOrgUrl % {'url': instUrls[i+1],'address': 'N/A'})
                else:
                    urlList.append( '' )

        instName =  cmsInst.getFormattedInstName(style, templates)

        # remove the (a), etc. in case they are still in the name (or NameSign)
        instName = re.sub(r'\([abcd]\)', '', instName)
        instName = re.sub(r'\s+; ', ', ', instName)
        instName = re.sub(r'\s+, ', ', ', instName)

        urlList = [x.replace('http://http://', 'http://') for x in urlList]
        # write out the "parent" institute first:
        outString = templates['instTempl'] % {'orgId': orgId,
                                                'urls': '\n'.join(urlList).strip(),
                                                'name': instName.strip(),
                                                'spires': '\n'.join(spires).strip(),
                                                'instId': cmsInst.rawInfo.cernInstId or instId,
                                                'status': 'CMS',
                                                'address': '',
                                                'member': 'member',
                                                }
        
        outFile.write( self.cleanXml( outString ) )
        parOrgId = orgId
        if cmsInst.isMultiInst:
            for i in range(len(instList)):
                orgId = parOrgId + abcd[i]

                instName = instList[i]
                # remove the (a), etc. in case they are still in the name (or NameSign)
                instName = re.sub(r'\([abcd]\)', '', instName)
                instName = re.sub(r'\s+; ', ', ', instName)
                # remove the (<city>) part if that is still in the name:
                instName = re.sub(r'(.*)\s+\([A-Za-z]+\)\s*$', '\\1', instName)
                outVals = {'orgId': orgId,
                           'parentInst': parOrgId,
                           'urls': '\n'.join([urlList[i]]).strip() if urlList[i] else '',
                           'name': instName.strip(),
                           'spires': '\n'.join([spires[i]]).strip() if spires[i] else '',
                           'instId': '%s%s' % (cmsInst.rawInfo.cernInstId or instId, abcd[i]),
                           'status': 'CMS' if cmsInst.memberStatus() == 'member' else '',
                           'address': '',  # empty for now
                           'member': member
                           }
                outFile.write( self.cleanXml( foafTemplates.foafOrganisationDep % outVals ) )

        return True

    def cleanXml(self, outString):
        # some cleanup for George's diff scripts:
        return outString.replace( '<cal:authorSuffix></cal:authorSuffix>', '<cal:authorSuffix/>')\
                        .replace( '<cal:authorStatus></cal:authorStatus>', '<cal:authorStatus/>' )\
                        .replace( '<cal:authorNameNative lang=""></cal:authorNameNative>', '<cal:authorNameNative lang=""/>' )\
                        .replace( '<cal:orgAddress></cal:orgAddress>', '<cal:orgAddress/>' )        
        
    def checkFilesExist(self, paperCode):

        fList = glob.glob(os.path.join(
            self.dirMaps['new'], '%s-public-authorlist*' % paperCode))
        # print "Found files: ", fList
        if fList:
            return False
        return True

    def generatePaperAuthorListFiles(self, paperCode, forceRewrite=False):

        if not forceRewrite:
            # check if files exist, don't overwrite:
            if not self.checkFilesExist(paperCode):
                return False, "Files for %s already exist and no force-overwrite requested. Nothing done." % paperCode

        self.paperCode = paperCode
        self.getListFromDB()
        self.prepare()

        #-ap: to avoid confusion, as these files will be non-redacted, we won't write them for now (unless clarified where they are needed).
        #-ap: only the (new/structured) XML file is written. 
        #-ap: The tex/revtex, and redacted XML file will be derived from that by "Gautier's scripts", which "transform" the `*-authorlistN.xml` 
        #-ap: to their corresponding `.tex/.revtex` files
        #-ap: while converting the russian, belarussian, and jinr institutes to the "merged" one, following the decision of the collaborations

        # self.writeBodyHTML()
        # self.writeFullHTML()
        # self.writeTexFile()
        # self.writeRevTexFile()
        self.writeXMLFiles()

        logDir = '/tmp/test-al-logs'
        if not os.path.exists( logDir ):
            os.makedirs( logDir )
        logFileName = f'{logDir}/alFileConv-{paperCode}.log'
        cmd = f'./scripts/alFileConverter.py {self.dirMaps["new"]} {self.paperCode} >{logFileName} 2>&1'
        res = doCmd(cmd, verbose=True)
        theLogger.info( f'==> cmd {cmd} returned {res} ' )
        print ( '='*80)
        with open( logFileName, 'r') as lF:
            print( '\n'.join([x.strip() for x in lF.readlines() ]) )
        print ( '='*80)

        return True, "Files for %s generated" % paperCode


    def diff(self, paperCode):

        if not glob.glob(os.path.join(self.dirMaps['ref'], '%s-authorlist*' % paperCode)):
            return False, "No reference files found for %s. Nothing to diff." % paperCode

        topHtml = """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=ISO-8859-1" />
    <title></title>
</head>

<body>
        """
        endHtml = """</body>

</html>
        """

        import difflib

        fileTypes = ['.tex', '.revtex',
                     '.html', '_body.html',
                     '.xml', '2.xml',
                     'N.xml',
                     ]

        with open(os.path.join(self.dirMaps['diff'], 'diff-%s.html' % paperCode), 'w') as htmlFile:
            htmlFile.write(topHtml)
            htmlFile.write(
                '<h2>AuthorList diff info for %s </h2>\n' % paperCode)

            for fType in fileTypes:

                referenceFile = os.path.join(
                    self.dirMaps['ref'], '%s-authorlist%s' % (paperCode, fType))
                if not os.path.exists(referenceFile):
                    theLogger.warning("WARNING: reference file '%s/%s-authorlist%s' not found, skipping comparinson ..." %
                                      (self.dirMaps['ref'], paperCode, fType))
                    continue

                refLines = [x.replace('http://http://', 'http://')
                            for x in open(referenceFile, 'r').readlines()]

                testFile = os.path.join(
                    self.dirMaps['new'], '%s-authorlist%s' % (paperCode, fType))
                testLines = [x for x in open(testFile, 'r').readlines()]

                theLogger.info('found %s lines in new file, and %s lines in reference file.' % (
                    len(testLines), len(refLines)))

                differ = difflib.HtmlDiff(wrapcolumn=100)

                content = differ.make_file(testLines, refLines,
                                           fromdesc='<a href="../new/%s%s"> New </a>' % (
                                               paperCode, '-authorlist.html'),
                                           todesc='<a href="../ref/%s%s"> Reference </a>' % (
                                               paperCode, '-authorlist.html'),
                                           numlines=1, context=True)
                theLogger.info('content created')

                diffFileName = 'diff-%s-%s.html' % (paperCode, fType[1:])
                # theLogger.info ( 'Comparison for  %s (%s diffs)' % ( fType, len(content.split('<tbody>')) ) )
                htmlFile.write('<a href="%s">Comparison for  %s</a> (%s diffs) <br/>\n' %
                               (diffFileName, fType, len(content.split('<tbody>'))))

                with open(os.path.join(self.dirMaps['diff'], diffFileName), 'w') as fullFile:
                    fullFile.write(content)

            htmlFile.write(endHtml)

        return True, 'Successfully created diffs for %s ' % paperCode

    def canStillSign(self, author):
        """
        From CMS Constitution, Annex A6.5 Persons Leaving CMS or Retiring (Rev.: October 9, 2015):

        Authors who have been active in CMS for one year or more continue signing CMS papers for one year after
        leaving the Collaboration, unless the Institution's Team Leader objects. This right of continued signing
        also applies to deceased authors.

        In addition, authors who retire or leave high energy physics can sign CMS physics papers for an additional
        period of four months for every year that they have been signing members of the Collaboration before the
        publication of the first physics paper (defined to be January 1st 2010), unless the Institution's Team
        Leader objects. This also applies to those with CMS Extended status. The additional period cannot exceed
        four years. Any additional period is terminated for authors who become authors of another major LHC experiment.
        The date of commencement in the Collaboration shall be determined from the date of arrival stated in the CMS
        Database. Authors falling in this category shall be exempted from the M&O and obligations for Experimental
        Physics Responsibilities.

        CMS Emeritus members qualify as authors, but are exempted from the M&O and obligations for Experimental
        Physics Responsibilities providing they satisfy all the other conditions for authorship. Years as an
        Emeritus member shall neither be included in the determination of the additional periods mentioned above
        nor be counted against the continued and additional signing periods already accumulated.

        :param author: an object of type "Person" (PersonData in CMSPEOPLE)
        :return: True/False depending on the criteria being fulfilled or not.

        *** ToDo: ***
        - check authorship in other major LHC experiment (q: how to get that info? primary acct. in Expt. group enough ?)
        - handle Emeritus correctly (q: what happens to non-confirmed emeriti ? does an emeritus qualify for
          "additional periods" of 1yr after being emeritus ?)

        """

        if author.status != 'EXMEMBER':
            return True  # not applicable: return OK

        # - datetime.timedelta(6) # simulate date when ref AL was created
        today = datetime.date.today()

        # an explict endDateSign is set and is in the future, so it's OK
        if author.dateEndSign and author.dateEndSign > today:
            return True

        # no explicit endDateSign is set, check the rules:

        # the day the author was set exmember:
        exYear = datetime.datetime.strptime(author.exDate, '%Y-%m-%d').date()

        # check if author was member before 2010-01-01:
        if author.dateRegistration.date() >= datetime.datetime(2010, 1, 1).date():
            # ... no, was not, check if more than a year:
            if author.dateRegistration.date() > (today - datetime.timedelta(365)):
                return False  # registered after the first physics paper, but less than one year ago, not author
            else:  # registered after the first physics paper, and more than one year ago, check exDate
                # still allowed to be author for one more year
                if (exYear + datetime.timedelta(365)) > today:
                    return True
                else:
                    return False

        # ... yes, author was member before 2010-01-01, calculate the additional time the author can stay:
        nDaysIn = (datetime.datetime(2010, 1, 1).date() -
                   author.dateRegistration.date()).days
        nYearsIn = float(nDaysIn) / 365.
        # 4 months per year maximum of four years
        maxYearsAdd = min(4., int(float(nYearsIn)*4./12.))
        maxSignDate = exYear + datetime.timedelta(365*(1. + maxYearsAdd))
        if maxSignDate > today:
            return True

        # theLogger.info( '\n=====> found a case to kick: %s reg: %s nYears: %s, maxYears: %s exYear: %s maxSignDate: %s \n' % ( author.cmsId, author.dateRegistration, nYearsIn, maxYearsAdd, exYear, maxSignDate) )

        # default: can not sign any more
        return False

    def createAuthorListInDB(self, paperCode, cmsId, forceRewrite=False):

        # check if an entry already exists in the PaperAuthor self.db.
        # If requested via forceRewrite, remove the entries in PaperAuthor,
        # flag that in the PaperAuthorHistory, and recreate the entries in PaperAuthor
        # If forceRewrite is False: write an error and return w/o any change in the self.db.

        isInPA = self.db.session.query(PaperAuthor).filter(
            PaperAuthor.code == paperCode).all()
        isInPAH = self.db.session.query(PaperAuthorHistory).filter(
            PaperAuthorHistory.code == paperCode).all()
        if len(isInPA) > 0 or len(isInPAH) > 0:
            if not forceRewrite:
                msg = "ERROR: authorList for %s already exists in DB: %s " % (
                    paperCode, isInPAH[0])
                # print msg
                theLogger.error(msg)
                return (False, msg)

            # we're supposed to overwrite here, so let's delete the old entries first:
            startTime = time.time()
            self.db.session.query(PaperAuthor).filter(
                PaperAuthor.code == paperCode).delete()
            self.db.session.commit()
            stopTime = time.time()
            # ... and make an entry in the history:
            self.addPaperAuthorHistoryEntry(cmsId, paperCode,
                                            action='ListRem',
                                            msg='Author List removed for ancode %s' % paperCode)
            msg = "found forceRewrite flag set: previous authorList for %s removed from DB (in %s sec.)... " % (
                paperCode, stopTime-startTime)
            theLogger.info(msg)

        # map the activities in iCMS to strings ...
        actNameMap = {1: 'Administrative',
                      2: 'Engineer',
                      3: 'Engineer Software',
                      5: 'Other',
                      6: 'Physicist',
                      7: 'Theoretical Physicist',
                      9: 'Doctoral Student',
                      13: 'Non-Doctoral',
                      15: 'Technician',
                      17: 'Engineer Electronics',
                      18: 'Engineer Mechanical',
                      None: 'None',
                      }

        #  authorID | cmsid | hrid   | code       | nameSignature | status | statusCms | activity  | instCode   | instCodeNow | instCodeAlso | instCodeForced | infn | univOther | inspireid | name | namf
        selAuth = (self.db.session.query(PeopleData)
                   .filter(PeopleData.isAuthor == True)
                   # .filter( PeopleData.Author == False )
                   # .filter( PeopleData.AuthorTK == True )
                   # .filter( or_(PeopleData.Author == True, PeopleData.AuthorTK == True) )
                   .all()
                   )

        theLogger.info("found %s authors for code %s " %
                       (len(selAuth), paperCode))

        # speed up things a bit ...
        nameSigAll = (self.db.session.query(PeopleUserData.cmsId, PeopleUserData.nameSignature)
                      .join((PeopleData, PeopleData.cmsId == PeopleUserData.cmsId))
                      .filter(PeopleData.isAuthor == True)
                      # .filter( PeopleData.Author == False )
                      # .filter( PeopleData.AuthorTK == True )
                      # .filter( or_( PeopleData.Author == True, PeopleData.AuthorTK == True ) )
                      .filter(PeopleUserData.nameSignature != '')
                      .all()
                      )
        theLogger.info("got %s total sigs" % len(nameSigAll))
        nameSigMap = {}
        for k, v in nameSigAll:
            if v and v.strip() != '':
                nameSigMap[k] = v

        for id in [4468, 7962, 1531, 9507, 10011]:
            if id in nameSigMap.keys():
                theLogger.debug("%s: %s" % (id, nameSigMap[id]))

        theLogger.debug(
            '==> going to process %d authors from DB' % len(selAuth))

        for author in selAuth:

            if author.status == 'EXMEMBER' and not self.canStillSign(author):
                continue

            nameSig = getDefaultNameSig(author)
            if int(author.cmsId) in nameSigMap.keys():
                if int(author.cmsId) == 10011:
                    theLogger.debug("overriding: %s with %s" %
                                    (nameSig, nameSigMap[int(author.cmsId)]))
                nameSig = nameSigMap[int(author.cmsId)]

            pa = PaperAuthor()
            pa.cmsid = author.cmsId
            pa.hrid = author.hrId

            pa.code = paperCode

            pa.nameSignature = nameSig
            pa.status = 'in'
            pa.statusCms = author.status

            pa.activity = actNameMap[author.activityId]
            pa.instCode = author.instCode

            pa.instCodeNow = author.instCodeNow if author.instCodeNow else ''
            pa.instCodeAlso = author.instCodeOther if author.instCodeOther else ''
            # does not seem to be used heavily, most recent paper code: BPH-13-005
            pa.instCodeForced = None

            pa.infn = author.infn
            pa.univOther = author.univOther

            pa.inspireid = author.authorId
            pa.name = author.lastName
            pa.namf = author.firstName

            self.db.session.add(pa)

        self.db.session.commit()

        theLogger.info('==> stored %d authors for paper %s' %
                       (len(selAuth), paperCode))

        msg = 'Author List generated for ancode %s' % paperCode
        self.addPaperAuthorHistoryEntry(cmsId, paperCode,
                                        action='ListGen',
                                        msg=msg
                                        )

        return (True, msg)

    def addPaperAuthorHistoryEntry(self, cmsId, paperCode, action, msg):
        # ''' ListGen          | UCRIVERSIDE   | 25/08/2016 | Author List generated for ancode HIN-16-004 '''
        person: PeopleData = PeopleData.query.filter(
            PeopleData.cmsId == cmsId).one()
        pah = PaperAuthorHistory.from_ia_dict({
            PaperAuthorHistory.cmsid: cmsId,
            PaperAuthorHistory.code: paperCode,
            PaperAuthorHistory.action: action,
            PaperAuthorHistory.history: msg,
            PaperAuthorHistory.date: datetime.date.today().strftime('%d/%m/%Y'),
            PaperAuthorHistory.hrid: person.hrId,
            PaperAuthorHistory.nicelogin: person.loginId,
            PaperAuthorHistory.instCode: person.instCode,
        })
        # theLogger.info( pah )
        self.db.session.add(pah)
        self.db.session.flush()
        self.db.session.commit()

    def diffDBAL(self, paperCode, refCode='HIG-17-006'):

        alRaw = self.db.session.query(PaperAuthor.cmsid, PaperAuthor).filter(
            PaperAuthor.code == paperCode).all()
        refRaw = self.db.session.query(PaperAuthor.cmsid, PaperAuthor).filter(
            PaperAuthor.code == refCode).all()

        theLogger.info('\n==> found %d entries in new, %d in ref' %
                       (len(alRaw), len(refRaw)))

        al = {}
        for k, v in alRaw:
            al[k] = v

        ref = {}
        for k, v in refRaw:
            ref[k] = v

        plComp = Comparator()
        plComp.excluded_attributes = ['authorID',
                                      'code', 'inspireid', 'name', 'namf']

        nNoRef = 0
        for id, item in al.items():
            if id == 8045:
                theLogger.info('==> 8045 found in new')
            if id not in ref.keys():
                nNoRef += 1
                person = self.db.session.query(PeopleData).filter(
                    PeopleData.cmsId == id).one()
                # personAuthInfo = 'susp:%s - allow:%s - block:%s - auth17: %s - MO: %s' % (person.isAuthorSuspended, person.isAuthorAllowed, person.isAuthorBlock, person.isAuthor2017, person.MOstatus)
                personAuthInfo = 'Flags: %s ' % (
                    '-'.join([fId.flagId for fId in PeopleFlagsAssociation.query.filter_by(cmsId=id).all()]))
                # personAuthInfo = 'exDate %s| statusCms %s| activity %s' % (person.exDate, item.statusCms, item.activity)
                theLogger.error(u"Not found in reference list: cmsID %6s [%s] (%s, %s - %s %s) " % (id,
                                                                                                    personAuthInfo,
                                                                                                    item.name.encode(
                                                                                                        'ascii', 'xmlcharrefreplace'),
                                                                                                    item.namf.encode(
                                                                                                        'ascii', 'xmlcharrefreplace'),
                                                                                                    item.instCode, item.statusCms,
                                                                                                    ))
            else:
                alJson = item.toJson()
                refJson = ref[id].toJson()

                plDiff = plComp.compare_dicts(refJson, alJson)

                if '_update' in plDiff:
                    theLogger.info("\ndifference found for id %s (%s): %s " % (
                        id, item.name, str(plDiff)))
                    theLogger.info("    new : %s " %
                                   alJson[plDiff['_update'].keys()[0]])
                    theLogger.info('    ref : %s' %
                                   refJson[plDiff['_update'].keys()[0]])
                    theLogger.info('    newItem: %s, %s' %
                                   (item.name, item.namf))

        # reverse check ...
        nNoNew = 0
        for id, item in ref.items():
            if id == 8045:
                theLogger.info('==> 8045 found in ref')
            if id not in al.keys():
                nNoNew += 1
                refName = '%s' % (item.nameSignature,) if (
                    item.nameSignature.strip() != '') else item.name+', '+item.namf
                theLogger.error("cmsID %s (%s - %s %s) in reference list, but not in new list " % (
                    id, refName, item.instCode, item.statusCms))
            else:
                alJson = item.toJson()
                refJson = ref[id].toJson()

                plDiff = plComp.compare_dicts(refJson, alJson)

                if '_update' in plDiff:
                    theLogger.info("\ndifference found for id %s (%s): %s " % (
                        id, item.name, str(plDiff)))
                    theLogger.info("    new : %s " %
                                   alJson[plDiff['_update'].keys()[0]])
                    theLogger.info('    ref : %s ' %
                                   refJson[plDiff['_update'].keys()[0]])
                    theLogger.info('    newItem: %s, %s' %
                                   (item.name, item.namf))

        theLogger.info(
            '\n\n==> found %d authors in new but not in ref, %d in ref but not in new ... \n\n' % (nNoRef, nNoNew))


def getNewNameSig(firstName, lastName):

    initials = ''
    inits = ''
    try:
        # inits = re.findall(u'([A-Z])', author.firstName)
        inits = re.split(r'[.\s-]', firstName)
        for i in inits:
            if i:
                # avoid empty strings created by splitting "A.M."
                initials += '%s.' % i[0]
    except:
        theLogger.error('error splitting "%s" for initials, got: "%s" ' % (
            firstName, inits))
        raise

    defNameSig = '%s %s' % (initials, lastName)
    return defNameSig, initials, inits

def getDefaultNameSig(author):

    defNameSig, initials, inits = getNewNameSig(author.firstName, author.lastName)

    for id in [4468, 7962, 1531, 9507]:
        if int(author.cmsId) == id:
            theLogger.info("==> %s: %s - %s (%s %s)" % (int(author.cmsId),
                                                        initials, defNameSig, author.firstName, author.lastName))

    if author.cmsId in [10011]:
        theLogger.debug('++> %s %s %s %s' %
                        (author.firstName, inits, initials, defNameSig))

    return defNameSig


def generatePaperCodes(paperCodeList, db, forceRewrite=False):

    palg = PaperAuthorListGenerator(db)

    if not palg.isInitialised:
        theLogger.error(
            'something went wrong, please check the errors above for details. Aborting. ')
        return

    for paperCode in paperCodeList:
        # (status, msg) = palg.createAuthorListInDB( paperCode, 3040, forceRewrite=True )[1]
        # if not status:
        #     print "ERROR when creating authorlist in DB: %s -- skipping next steps for %s. " % (msg, paperCode)
        #     continue
        # palg.diffDBAL( paperCode, refCode='HIG-17-006' )

        palg.generatePaperAuthorListFiles(paperCode, forceRewrite=forceRewrite)
        # palg.diff(paperCode)


def doCmd(cmd, verbose=False):
    res = ''
    try:
        theLogger.info('doCmd> going to execute:"%s"' % cmd)
        res = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        return res
    except Exception as e:
        theLogger.error("doCmd> ERROR: got: %s" % (str(e),))
        theLogger.error("doCmd>     output: %s " % (str(e.output),))
        return -1, "FAIL"

def main(db=None, code=None, forceRewrite=False):
    
    global theLogger

    theLogger.info('\n\n')

    if code is None:
        print("\nERROR, no code given ... \n")
        return -1

    theLogger.info('\nPreparing AL files for %s\n' % code)

    theApp = Flask( __name__, instance_relative_config=True )
    theApp.config.from_pyfile( 'config.py' )

    ret = -1 
    with theApp.app_context() :
        db = WebappOrmManager(theApp)

        dbUrl = str(db.engine.url)
        theLogger = theApp.logger
        theLogger.setLevel( logging.INFO )
        theLogger.info( "\n==> DB at: %s \n" % dbUrl)#[:15] )

        generatePaperCodes(paperCodeList=[code], db=db, forceRewrite=forceRewrite)
        
        del theLogger

def doOne():
    
    code = sys.argv[1]
    forumRe = re.compile( r'^[A-Z][A-Z0-9][A-Z]-\d{2}-\d{3}$' )
    if not forumRe.match(code):
        print( f'invalid code: {code} -- not matching regex' )
        sys.exit(-1)
    
    retCode = main(code=code, forceRewrite=True)

    return retCode

        
        
def doIt():
    # testing DB and writing files
    # retCode = main(code='FOO-99-042')

    # # testing writing files only:
    # retCode = main(code='EXO-17-006')
    # retCode = main(code='EXO-15-006')
    # retCode = main(code='HIN-16-011')
    # retCode = main(code='HIN-14-009')
    # retCode = main(code='HIN-14-007')
    # retCode = main(code='SUS-16-033')
    # retCode = main(code='SUS-16-035')
    # retCode = main(code='TOP-12-031')
    #
    # retCode = main( code='B2G-16-024')
    #
    # # from mails until Jun 15, 2017 09:43
    # for code in [ 'SUS-16-051', 'EXO-16-030', 'HIN-16-008',
    #               'SMP-15-003', 'SUS-16-043', 'TDR-17-001',
    #               'FSQ-16-004', 'FSQ-12-004', 'PRF-14-001', ] :
    #     retCode = main( code=code )

    # retCode = main(code='TOP-14-008')

    # for code in ['SUS-16-049', 'HIG-16-041', 'SUS-15-009', 'HIG-15-013', 'B2G-17-002', 'SUS-16-039', 'SUS-16-044']:
    #     retCode = main( code=code )

    # retCode = main(code='HIG-17-011')

    # for code in [ 'SMP-16-014', 'EXO-16-051', 'HIN-16-021', 'BPH-15-008', 'SMP-17-004', 'B2G-16-026', 'HIG-17-001', 'HIN-17-001', 'HIN-16-006', 'SMP-17-002', 'HIG-17-010', 'FSQ-16-005', 'SUS-16-041', 'HIG-16-043', 'B2G-17-003', 'B2G-17-001', 'TOP-16-023', 'BPH-15-005', 'HIN-16-017', 'HIN-16-001', 'SUS-16-034', 'SMP-16-017', 'SMP-17-006', 'HIG-15-009', 'HIG-17-006', 'B2G-17-007', 'HIN-16-007', 'EXO-17-006', 'FSQ-14-002', 'FSQ-16-008', 'SUS-16-045', 'SUS-16-032', 'EXO-16-003', 'SUS-16-042', 'TOP-16-007', 'HIN-15-010', 'HIN-15-008', 'SMP-15-009' ] :
    #     retCode = main( code=code, forceRewrite=True )

    # retCode = main( code='SMP-15-009', forceRewrite=True )

    retCode = 0
    # redo all so far:
    # for code in ['B2G-16-024', 'B2G-16-026', 'B2G-17-001', 'B2G-17-002', 'B2G-17-003', 'B2G-17-007', 'BPH-15-005', 'BPH-15-008', 'EXO-15-006', 'EXO-16-003', 'EXO-16-030', 'EXO-16-051', 'EXO-17-006', 'FSQ-12-004', 'FSQ-14-002', 'FSQ-16-004', 'FSQ-16-005', 'FSQ-16-008', 'HIG-15-009', 'HIG-15-013', 'HIG-16-041', 'HIG-16-043', 'HIG-17-001', 'HIG-17-006', 'HIG-17-010', 'HIG-17-011', 'HIN-14-007', 'HIN-14-009', 'HIN-15-008', 'HIN-15-010', 'HIN-16-001', 'HIN-16-006', 'HIN-16-007', 'HIN-16-008', 'HIN-16-011', 'HIN-16-017', 'HIN-16-021', 'HIN-17-001', 'PRF-14-001', 'SMP-15-003', 'SMP-15-009', 'SMP-16-014', 'SMP-16-017', 'SMP-17-002', 'SMP-17-004', 'SMP-17-006', 'SUS-15-009', 'SUS-16-032', 'SUS-16-033', 'SUS-16-034', 'SUS-16-035', 'SUS-16-039', 'SUS-16-041', 'SUS-16-042', 'SUS-16-043', 'SUS-16-044', 'SUS-16-045', 'SUS-16-049', 'SUS-16-051', 'TDR-17-001', 'TOP-12-031', 'TOP-14-008', 'TOP-16-007', 'TOP-16-023','B2G-12-016', 'HIG-17-002', 'SMP-16-005', 'SUS-16-037', 'SUS-16-047', 'TOP-14-013', 'HIG-16-043']:
    #     retCode += main( code=code, forceRewrite=True )

    # retCode = main( code='HIG-16-043', forceRewrite=True )
    # retCode = main( code='B2G-17-007', forceRewrite=True )
    # retCode = main( code='EXO-17-006', forceRewrite=True )
    # retCode = main( code='TDR-17-005', forceRewrite=True )
    # retCode = main( code='SUS-16-048', forceRewrite=True )
    # retCode = main( code='HIG-17-035', forceRewrite=True )
    # retCode = main( code='EXO-18-008', forceRewrite=True )
    # retCode = main( code='BPH-18-007', forceRewrite=True )

    # retCode = main( code='HIG-17-026', forceRewrite=True )

    # retCode = main( code='B2G-18-008', forceRewrite=True )
    # retCode = main( code='EXO-19-001', forceRewrite=True )
    # retCode = main( code='TOP-18-009', forceRewrite=True )
    # retCode = main( code='SUS-18-006', forceRewrite=True )
    # retCode = main( code='TDR-19-002', forceRewrite=True )
    # retCode = main( code='BPH-19-003', forceRewrite=True )

    # retCode = main(code='EXO-19-010', forceRewrite=True)

    # retCode = main(code='EXO-18-014', forceRewrite=True)

    # retCode = main(code='HIG-20-003', forceRewrite=True)
    # retCode = main(code='HIN-18-005', forceRewrite=True)

    # retCode = main(code='HIG-21-013', forceRewrite=True)
    
    # the "Nature paper" -- first one of the embargo ... handled in Feb 2023
    # retCode = main(code='HIG-22-001', forceRewrite=True)

    retCode = main(code='SMP-18-010', forceRewrite=True)

    return retCode


if __name__ == '__main__':
    
    sys.exit( doOne() )
