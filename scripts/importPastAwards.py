
# requirements: 
# -e git+https://:@gitlab.cern.ch:8443/authzsvc/tools/auth-get-sso-cookie.git#egg=auth_get_sso_cookie
# beautifulsoup4

# run as (assuming virtualenv in ./venv/): 
# (source ./venv/bin/activate; time PYTHONPATH=. PYTHONIOENCODING=utf-8 python ./scripts/importPastAwards.py )


import csv
import json

import requests
import logging
import urllib3

import sys
import time
import logging
from datetime import date
import dateutil.parser as dparser

from flask import Flask
from sqlalchemy.orm.exc import  NoResultFound, MultipleResultsFound

from auth_get_sso_cookie.cern_sso import login_with_kerberos

from icms_orm.common import UserAuthorData
from icms_orm.cmspeople import Person as PeopleData

from util.database.orm_interface import WebappOrmManager

from IcmsApiAccess import IcmsApiAccess

urllib3.disable_warnings()

sys.path.append( '.' )
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

theLogger = None

def timing_val(func):
    def wrapper(*arg, **kw):
        '''source: http://www.daniweb.com/code/snippet368.html'''
        t1 = time.time()
        res = func(*arg, **kw)
        t2 = time.time()
        theLogger.debug( '%s ran in %s sec.' % (func.__name__, (t2 - t1)) )
        return (t2 - t1), res, func.__name__
    return wrapper

def makeAwardKey( aw ):
    
    # logging.info( f'makeAwardKey> trying to create key for: {aw}')
    if type(aw) == type({}):
        # logging.info( f'makeAwardKey> trying to create key for: dict {aw}')
        return f'{aw["type"].strip()} | {aw["year"]}'
    elif type(aw) == type([]):
        # logging.info( f'makeAwardKey> trying to create key for: list {aw}')
        return f"{aw[3].strip().replace( ' award', ' Award')} | {aw[2]}"

    return aw

def findInst( iMap, cmsId, year):

# {
# 	"cms_id" : 17,
# 	"start_date" : "2016-08-23",
# 	"end_date" : None,
# 	"inst_code" : "MINSK-INP"
# },


    for item in iMap:
        startDate = dparser.parse( item["start_date"] )
        endDate = dparser.parse( item["end_date"] ) if item["end_date"] is not None else date( startDate.year, 12, 31)
        if int(cmsId) == int(item['cms_id']) and startDate.year == year and endDate.year == year:
            return item["inst_code"]
    return None

def findEprInst( iMap, cmsId, year):
#    {
#         "cms_id" : 17,
#         "year" : 2010,
#         "inst_code" : "MINSK-NCPHEP"
#     },

    for item in iMap:
        if int(cmsId) == int(item['cms_id']) and int(year) == int(item['year']):
            return item['inst_code']

    return None


class PastAwardImporter():
    
    def __init__(self):
        
        self.newAwardTypes = set()
        self.newAwardKeys = set()
        self.newAwardees = []
        
        self.noIDs = []
        self.newProjects = set()

        self.prevAwardTypes = {}
        self.prevAwards = {}
        self.prevAwardees = []

        # initialise known cases, who do not have the inst set in the file:
        self.userInstMap = { 912: 'CERN', # Dirk Samyn
                            3071: 'CERN', # Gilles Raymond
                            5284: 'CERN', # Marco Rovere
                            3767: 'CERN', # Dawn Hudson
                            4377: 'AACHEN-1', # Thomas Maarten
                            }
        
        # self.topUrl = 'https://icms-dev.cern.ch'
        self.topUrl = 'http://localhost:5073'
        self.headers = {}

        if 'localhost' in self.topUrl:
            self.api = None
            self.headers = { "User": "pfeiffer" }
        else:
            self.api = IcmsApiAccess()
            self.headers = {}

    def readNewCSVfile(self, csvFileName): 

        projMap = { 'Admin' : 'Administration',
                    'BRIL'  : 'BRIL',
                    'Commissioning' : 'Run Coordination',
                    'Communication' : 'Outreach and Communication',
                    'DAQ' : 'DAQ',
                    'ECAL'  : 'ECAL',
                    'Endcap Calorimeter'  : 'Endcap Calorimeter',
                    'HCAL'  : 'HCAL',
                    'HGCAL'  : 'HGCAL',
                    'L1 Trigger'  : 'L1 Trigger',
                    'L1TRG & PPD' : 'L1 Trigger and PPD',
                    'MTD'   : 'MTD',
                    'MUONS' : 'Muons',
                    'Mag'   : 'Technical Coordination',
                    'Muons' : 'Muons',
                    'muons' : 'Muons',
                    'Offline & Computing' : 'Offline and Computing',
                    'Outreach'  : 'Outreach and Communication',
                    'PPD' : 'PPD',
                    'PPS' : 'PPS',
                    'RUN COORDINATION'  : 'Run Coordination',
                    'Run Coordination'  : 'Run Coordination',
                    'Run Coordination / Trigger (joint)'  : 'Run Coordination and Trigger (joint)',
                    'TC'  : 'Technical Coordination',
                    'TRACKER' : 'Tracker',
                    'Tracker' : 'Tracker',
                    'TRIDAS'  : 'L1 Trigger and DAQ',
                    'TriDAS'  : 'L1 Trigger and DAQ',
                    'TriDas'  : 'L1 Trigger and DAQ',
                    'Tridas'  : 'L1 Trigger and DAQ',
                    'TRIGGER STUDIES GROUP' : 'Trigger Coordination',
                    'TSG'                   : 'Trigger Coordination',
                    'Trigger'               : 'Trigger Coordination',
                    'Trigger Coordination'  : 'Trigger Coordination',
                    'Upgrade Coordination'  : 'Upgrade Coordination',
        }

        self.noIDs = []
        self.newAwardees = []
        self.newAwardKeys = set()
        self.newAwardTypes = set()
        with open(csvFileName, 'r', encoding='utf-8') as csvFile:
            reader = csv.reader(csvFile, delimiter=',', quotechar='"') 
            # new file (2023-05-25):
            #   0     1     2       3          4          5        6         7      8       9         10
            # Name,Project,Year,Award type,Nomination,Institute,position,InstCode,cmsId,projectName,mappedProject
            index = -1
            for row in reader:
                index += 1
                # if index < 5: 
                #     logging.info( f'{row[8]} - {row}' )
                if index == 0: continue  # ignore header line in CSV file
                
                # replace last column with newly mapped project names:
                try:
                    row[-1] = projMap[ row[-2].strip() ]
                except KeyError:
                    logging.info( f'ERROR: could not map proj "{row[-2].strip()}" for {row}' )
                
                awardKey = makeAwardKey( row )
                
                self.newAwardTypes.add( f"{row[3].strip().replace( ' award', ' Award')}" )
                self.newAwardKeys.add( awardKey ) # store unique award-type/year
                self.newProjects.add( row[-1].strip() )
                
                cmsId = int(row[8])
                if cmsId > 0:
                    self.newAwardees.append( [ x.strip() for x in row ] )
                else: #-ap 2023-06-02 add them now as well
                    self.newAwardees.append( [ x.strip() for x in row ] )
                    self.noIDs.append( [ x.strip() for x in row ] )

        return 

    def getData( self, what, verbose = False):
        if 'localhost' in self.topUrl:
            return self._getData( what, verbose)
        else:
            return self.api.getData( what )
        
    def _getData( self, what, verbose = False):

        url = f'{self.topUrl}/restplus/{what}'

        if verbose: logging.info( f'getData> url: {url}' )
        startTime = time.time()
        data = requests.get(url, allow_redirects=True, verify=False, headers=self.headers)
        if verbose: logging.info( data.status_code )
        if data.status_code != 200:
            logging.info( f'getData> ERROR: {data.text}')

        return data.json(), (startTime, time.time())

    def postData( self, what, payload, verbose=False):
        if 'localhost' in self.topUrl:
            return self._postData( what, payload, verbose)
        else:
            return self.api.postData( what, payload )

    def _postData( self, what, payload, verbose=False ):

        url = f'{self.topUrl}/restplus/{what}'

        if verbose: logging.info( f'post> url: {url}' )
        startTime = time.time()
        data = requests.post( url, allow_redirects=True, verify=False, headers=self.headers, json=payload )
        if verbose: logging.info( data.status_code )
        if data.status_code != 200:
            logging.info( f'postData> ERROR: {data.text}')

        return data, (startTime, time.time())

    def putData( self, what, payload, verbose=False):
        if 'localhost' in self.topUrl:
            return self._putData( what, payload, verbose)
        else:
            return self.api.putData( what, payload )

    def _putData( self, what, payload, verbose=False ):

        url = f'{self.topUrl}/restplus/{what}'

        if verbose: logging.info( f'post> url: {url}' )
        startTime = time.time()
        data = requests.put( url, allow_redirects=True, verify=False, headers=self.headers, json=payload )
        if verbose:
            logging.info( '\n', '-'*42, 'results from put:\n')
            logging.info( 'status code:', data.status_code )
            logging.info( 'text:', data.text )
            logging.info( 'json:', data.json() )
            logging.info( '\n', '-'*42, '\n' )
        if data.status_code != 200:
            logging.info( f'putData> ERROR: {data.text}')

        return data, (startTime, time.time())

    def importPastAwardsFromCSV(self):
        csvFileName = 'All_CMS_awards_2023-05-25.csv'  # './All_CMS_awards.csv'
        self.readNewCSVfile(csvFileName)
        print('\n')
        logging.info( f'found {len(self.newAwardTypes)} awardTypes' )
        [ print(x) for x in self.newAwardTypes ]
        print('\n')
        logging.info( f'found {len(self.newAwardKeys)} awards' )
        [ print(x) for x in self.newAwardKeys ]
        print('\n')
        logging.info( f'found {len(self.newAwardees)} awardees, {len(self.noIDs)} entries w/o cmsId: ' )
        [ print(x) for x in self.noIDs ]
        print('\n')
        logging.info( f'found {len(self.newProjects)} projects')
        [ print(x) for x in sorted(self.newProjects) ]
        print('\n')
        
    def getAwardsFromDB(self):
        
        url = f'/awards/types'
        self.prevAwardTypes, timing = self.getData( url )
        logging.info( f'got {len(self.prevAwardTypes)} award types from DB: {self.prevAwardTypes} ')

        url = f'/awards'
        res, timing = self.getData( url )
        self.prevAwards = res['awards']
        logging.info( f"got {len(self.prevAwards)} awards from DB: ")
        logging.info( f"    {self.prevAwards} ")

        url = f'/awards/awardees'
        res, timing = self.getData( url )
        self.prevAwardees = res # ['awardees']
        logging.info( f"got {len(self.prevAwardees)} awards from DB: ")
        logging.info( f"    {self.prevAwardees} ")

    def findAwardType(self, awTyp ):
        # logging.info( f'checking if {awTyp} is in {self.prevAwardTypes}')
        for at in self.prevAwardTypes:
            if awTyp in at['type']: return True
        return False

    def updateAwardTypes(self):

        logging.info( '\n' )        
        for awTyp in self.newAwardTypes:
            if not self.findAwardType(awTyp):
                logging.info( f'found new award type "{awTyp}" -- adding to DB ... ' )
                payload = { "type": f"{awTyp}"}
                res, timing = self.postData( f'/awards/types', payload )
                logging.info( f'added award type "{awTyp}" ({payload}) to DB: {res} ')
            else:
                logging.info( f'award type "{awTyp}" already in DB ... ' )

    def updateAwards(self):

        #         payload = {   "year": year,
        #   "nominations_open_date": "2023-05-26",
        #   "nominations_deadline": "2023-05-26",
        #   "awardee_publication_date": "2019-06-24",
        #   "remarks": "hello",
        #   "is_active": true
        #  }

        url = f'/awards/types'
        res, timing = self.getData( url )
        awTypeMap = { at['type']: at['id'] for at in res }

        logging.info( '\n' )        
        prevAwardKeys = [ makeAwardKey(x) for x in self.prevAwards ]
        logging.info( f'prevAwardKeys: {prevAwardKeys}')
        
        start = time.time()
        nAdd = 0
        index = -1
        for awardKey in self.newAwardKeys:
            index += 1
            # if index > 5: break
            if awardKey not in prevAwardKeys:
                logging.info( f'found new award "{awardKey}" -- adding to DB ... ' )
                awType, year = awardKey.split( ' | ')
                payload = { "year": int(year), 
                            "award_type_id": awTypeMap[awType],
                            "nominations_open_date": f"{year}-01-21",
                            "nominations_deadline": f"{year}-01-22",
                           }
                res, timing = self.postData( f'/awards', payload )
                logging.info( f'added award "{payload}" to DB: {res} ')
                nAdd += 1
            else:
                logging.info( f'award "{awardKey}" already in DB ... ' )
        logging.info( f'{nAdd} new awards created in {time.time()-start} sec.')

    def getName(self, cmsId, fullName=None):
        
        if cmsId < 0: # no CMSid found. Luckily they are all of type "First Last"
            return ( fullName.split(' ') )
        
        item, timing = self.getData( f'/icms_public/user_info?cms_id={cmsId}' )
        try:
            res = ( item['first_name'], item['last_name'] )
        except Exception as e:
            logging.error( f'Can not split name for {cmsId} from DB: "{item}"' )
            raise e
        return res

    def updateAwardees(self):

        # payload = {
        # "award_id": 0,
        # "cms_id": 0,
        # "activity": "string",
        # "project": "string",
        # "institute": "string",
        # "citation": "string",
        # "remarks": "string"
        # }

        url = f'/awards'
        res, timing = self.getData( url )
        awMap = { makeAwardKey(at): at['id'] for at in res['awards'] }

        logging.info( '\n' )
        prevAwardeeKeys = [ f'{x["award_id"]} | {x["cms_id"]}' for x in self.prevAwardees ]
        logging.info( f'prevAwardeeKeys: {prevAwardeeKeys}')

        res = None
        noInst = []
        start = time.time()
        nAdd = 0
        index = -1
        for awardee in self.newAwardees:
            logging.info( f'processing: {awardee} ...')
            index += 1
            # if index > 5: break
            awKey = makeAwardKey( awardee )
            
            cmsId = int(awardee[-3])
            
            # if cmsId not in [942, 1380, 1528, 2805, 3532, 3767, 10626]: continue
            
            # see if we have a fix for a missing inst:
            if awardee[-4].strip() == '':
                if cmsId in self.userInstMap:
                    inst = self.userInstMap[ cmsId ]
                    awardee[-4] = inst
                    print ( f'updateAwardees> updated institute for new awardee "{awardee}" to {inst}' )
            
            awardeeKey = f'{awMap[ awKey ]} | {awardee[-3]}'
            if awardee[-4].strip() == '':
                noInst.append( awardee)
                #-ap 2023-06-02 add these now as well 
                # logging.info( f'updateAwardees> ERROR: no institute found for new awardee "{awardee}" ' )
                # continue

            try:
                res = self.getName(cmsId, awardee[0])
                (fName, lName) = res
            except Exception as e:
                logging.info( f'ERROR getting names res = {res} for {cmsId}/{awardee} : got {str(e)}')
                raise e
    

            if awardeeKey not in prevAwardeeKeys:
                activity =  'Other'  # not worth mapping the very few entries correctly ...
                # if awardee[6].strip() != '': activity = awardee[6].strip() ## needs cleanup/mapping to go into DB
                logging.info( f'found new awardee "{awardeeKey}" -- adding to DB ... ' )
                payload = { "award_id": awMap[ awKey ],
                            "first_name": fName,
                            "last_name": lName,
                            "activity": activity,
                            "project": awardee[-1].strip(),
                            "citation": awardee[4],
                            "remarks": ""
                           }

                if cmsId > 0:
                    payload[ "cms_id" ] = cmsId
                if awardee[-4].strip() != '':
                    payload[ "institute" ] = awardee[-4].strip()

                res, timing = self.postData( f'/awards/awardees', payload )
                if res.status_code == 400:
                    if 'has already been selected for award' in res.json()['message']: #   { "message": "Member 3532 has already been selected for award 18!"}
                        ## we'll need to update the project list for this award
                        prevList, timing = self.getData( f'/awards/awardees?award={awMap[ awKey ]}&cms_id={cmsId}' )
                        prev = prevList[0]
                        logging.info( f'found prev data for award={awMap[ awKey ]}&cms_id={cmsId}: {prev}')
                        newProj = f'{prev["project"]};{awardee[-1].strip()}'
                        logging.info( f'going to update project for {prev["id"]} to {prev["project"]} -> {newProj} ')
                        
                        payload[ "project" ] = newProj
                        res, timing = self.putData( f'/awards/awardees/{prev["id"]}', payload=payload, verbose=True )

                logging.info( f'added awardee "{payload}" to DB: {res} ({timing[1]-timing[0]}s)')
                nAdd += 1
            else:
                logging.info( f'awardee "{awardeeKey}" already in DB ... ' )
        
        logging.info( '\n' )
        logging.info( f'{nAdd} new awardees created in {time.time()-start} sec.')

        if noInst:
            logging.info( f'found {len(noInst)} entries w/o institute -- written to file noInst-awardees.json ...  ' )
        #    [ print(x) for x in noInst ]
            with open( 'noInst-awardees.json', 'w') as jF:
                json.dump(noInst, jF)

    def analyseMissingInsts(self, verbose=False):
        aList = [
['Vladimir Tchekhovski', 'Muon', '2010', 'Construction', 'For significant contribution to the ME1/1 chambers', '', '', '', '17', 'Muons', 'Muons'],
['Stefanovitch Roman', 'TC', '2010', 'Construction', 'For exceptional effort in the design and assembly of HCAL and in underground assembly', '', '', '', '26', 'TC', 'Technical Coordination'],
['Mikhail Korjik', 'ECAL', '2010', 'Construction', 'For leadership of the Belarus crystal qualification team.', '', '', '', '34', 'ECAL', 'ECAL'],
['Tariel Sakhelashvili', 'ECAL', '2010', 'Construction', 'For contribution to the quality control of the production APDs.', '', '', '', '174', 'ECAL', 'ECAL'],
['Giuliano Parrini', 'TRK', '2010', 'Construction', 'For the contribution to the CMS Tracker Power System.', '', '', '', '276', 'Tracker', 'Tracker'],
['Pasquale Fabbricatore', 'Mag', '2010', 'Construction', 'For outstanding contribution to the engineering of the Coil and Winding', '', '', '', '280', 'Mag', 'Technical Coordination'],
['Bruno Checcucci', 'TRK', '2010', 'Construction', 'For the Tracker Inner Barrel OptoHybrids.', '', '', '', '306', 'Tracker', 'Tracker'],
['Kudla Maciek', 'TRIDAS', '2010', 'Construction', 'For outstanding contribution to the RPC Trigger system', '', '', '', '345', 'TRIDAS', 'L1 Trigger and DAQ'],
['Dmitri Smolin', 'Muon', '2010', 'Construction', 'For outstanding contribution to the ME1/1 muon chambers', '', '', '', '382', 'Muons', 'Muons'],
['Yuri Musienko', 'ECAL', '2010', 'Construction', 'For leadership of the CERN APDlab Team.', '', '', '', '396', 'ECAL', 'ECAL'],
['Nikolai Bondar', 'Muon', '2010', 'Construction', 'For outstanding contribution to the CSC anode wire front end electronics', '', '', '', '471', 'Muons', 'Muons'],
['Horvath Istvan', 'Mag', '2010', 'Construction', 'For outstanding contribution to the CMS Magnet Coil conductor', '', '', '', '544', 'Mag', 'Technical Coordination'],
['Marcus French', 'TRK', '2010', 'Construction', 'For the CMS Tracker VLSI engineering.', '', '', '', '587', 'Tracker', 'Tracker'],
['Eric Hazen', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the HCAL data concentrator cards', '', '', '', '604', 'HCAL', 'HCAL'],
['Igor Churin', 'HCAL', '2010', 'Construction', 'For outstanding contribution to HCAL mechanical design', '', '', '', '666', 'HCAL', 'HCAL'],
['Maurizio Bertoldi', 'HCAL', '2010', 'Construction', 'For the high standards used in the supervision of HCAL production', '', '', '', '692', 'HCAL', 'HCAL'],
['Ben Bylsma', 'Muon', '2010', 'Construction', 'For outstanding contributions to the CSC cathode strip front-end electronics', '', '', '', '765', 'Muons', 'Muons'],
['Feyzi Farshid', 'Mag', '2010', 'Construction', 'For outstanding contribution to the engineering of the CMS Endcaps', '', '', '', '807', 'Mag', 'Technical Coordination'],
['Marie-Claude Pelloux', 'Admin', '2010', 'Construction', 'For leadership of the CMS Secretariat and support of the Spokesperson', '', '', '', '881', 'Admin', 'Administration'],
['Hubert Gerwig', 'Mag', '2010', 'Construction', 'For outstanding contribution to the engineering of the Barrel and the Lowering', '', '', '', '886', 'Mag', 'Technical Coordination'],
['Sandro Marchioro', 'TRK', '2010', 'Construction', 'For the contribution to the electronic systems of CMS.', '', '', '', '901', 'Tracker', 'Tracker'],
['Dirk Samyn', 'SWC', '2010', 'Construction', 'For leadership in the development and support of CMS Web systems', '', '', '', '912', 'Offline & Computing', 'Offline and Computing'],
['Gerhard Waurick', 'TC', '2010', 'Construction', 'For outstanding contribution to the moving and heavy lifting systems', '', '', '', '919', 'TC', 'Technical Coordination'],
['Raffaelli Fabrizio', 'TRK', '2010', 'Construction', 'For the contribution to the mechanics of the CMS TIB', '', '', '', '931', 'Tracker', 'Tracker'],
['Jose Carlos Da Silva', 'TRIDAS', '2010', 'Construction', 'see Tracker', '', '', '', '942', 'TRIDAS', 'L1 Trigger and DAQ'],
['John Coughlan', 'TRK', '2010', 'Construction', 'For the CMS Tracker off-detector electronics.', '', '', '', '954', 'Tracker', 'Tracker'],
['Pascal Petiot', 'TC', '2010', 'Construction', 'For exceptional support in testing and installing CMS subdetectors', '', '', '', '1042', 'TC', 'Technical Coordination'],
['Francois Vasey', 'TRK', '2010', 'Construction', 'For the contribution to the optical link technologies of CMS.', '', '', '', '1060', 'Tracker', 'Tracker'],
['Dominique Gigi', 'TRIDAS', '2010', 'Construction', 'For outstanding contribution to the electronics for the central DAQ', '', '', '', '1062', 'TRIDAS', 'L1 Trigger and DAQ'],
['Madeleine Azeglio', 'Admin', '2010', 'Construction', 'For outstanding support and helpfulness under challenging conditions', '', '', '', '1105', 'Admin', 'Administration'],
['Benoit Cure', 'Mag', '2010', 'Construction', 'For outstanding contribution to the CMS Magnet and Coil', '', '', '', '1240', 'Mag', 'Technical Coordination'],
['Massimo Benettoni', 'Muon', '2010', 'Construction', 'For outstanding contribution to the DT mechanical engineering design', '', '', '', '1270', 'Muons', 'Muons'],
['Jean Pothier', 'TC', '2010', 'Construction', 'For outstanding contribution to test beam set-up and general safety', '', '', '', '1329', 'TC', 'Technical Coordination'],
['Gabriele Kogler', 'Admin', '2010', 'Construction', 'see ECAL', '', '', '', '1330', 'Admin', 'Administration'],
['Janos Ero', 'TRIDAS', '2010', 'Construction', 'For outstanding contribution to the DT Muon Track Finder electronics', '', '', '', '1344', 'TRIDAS', 'L1 Trigger and DAQ'],
['Wim Beaumont', 'TRK', '2010', 'Construction', 'For the construction and testing of the CMS Tracker.', '', '', '', '1380', 'Tracker', 'Tracker'],
['Wim Beaumont', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the CASTOR electronics', '', '', '', '1380', 'HCAL', 'HCAL'],
['Nikolay Golubev', 'ECAL', '2010', 'Construction', 'For leadership of the Russian Supercrystal Team.', '', '', '', '1383', 'ECAL', 'ECAL'],
['Marc Anfreville', 'ECAL', '2010', 'Construction', 'For leadership of the Saclay laser monitoring team', '', '', '', '1424', 'ECAL', 'ECAL'],
['Manfred Pernicka', 'TRK', '2010', 'Construction', 'For the contribution to the CMS Tracker readout electronics.', '', '', '', '1490', 'Tracker', 'Tracker'],
['Kirsti Aspola', 'Admin', '2010', 'Construction', 'see Tracker', '', '', '', '1528', 'Admin', 'Administration'],
['Kirsti Aspola', 'RC', '2010', 'Construction', 'see tracker', '', '', '', '1528', 'Run Coordination', 'Run Coordination'],
['Daniel Berst', 'TRK', '2010', 'Construction', 'For the CMS Tracker Front End Hybrids.', '', '', '', '1681', 'Tracker', 'Tracker'],
['Angelo Vicini', 'Muon', '2010', 'Construction', 'For major contribution to the RPC production and test at Pavia', '', '', '', '1699', 'Muons', 'Muons'],
['Domenico Dattola', 'Muon', '2010', 'Construction', 'For leadership in the construction and testing of the MB4 chambers at Torino', '', '', '', '1714', 'Muons', 'Muons'],
['Peter Zalan', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the Forward HCAL component production', '', '', '', '1744', 'HCAL', 'HCAL'],
['Jean-Pierre Girod', 'TC', '2010', 'Construction', 'For important contributions to CMS assembly and resource management at point 5', '', '', '', '1783', 'TC', 'Technical Coordination'],
['Nelson Chester', 'Muon', '2010', 'Construction', 'For leadership as CSC project engineer for construction', '', '', '', '2020', 'Muons', 'Muons'],
['Enrique Calvo', 'Muon', '2010', 'Construction', 'For leadership in the tracker/muon optical alignment system', '', '', '', '2321', 'Muons', 'Muons'],
['Sanjay Chendvankar', 'HCAL', '2010', 'Construction', 'For the high standards used in the performance of the HO components', '', '', '', '2503', 'HCAL', 'HCAL'],
['Arjan Heering', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the HCAL photodetectors', '', '', '', '2505', 'HCAL', 'HCAL'],
['Ianna Osborne', 'SWC', '2010', 'Construction', 'For outstanding contribution to the event display programs', '', '', '', '2628', 'Offline & Computing', 'Offline and Computing'],
['Suresh Kalmani', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the Outer HCA system', '', '', '', '2656', 'HCAL', 'HCAL'],
['Ianos Schmidt', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the Forward HCAL electronics', '', '', '', '2732', 'HCAL', 'HCAL'],
['Shahzad Muzaffar', 'SWC', '2010', 'Construction', 'For outstanding contribution to the visualization and build system', '', '', '', '2745', 'Offline & Computing', 'Offline and Computing'],
['Vladimir Smetannikov', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the YE1/HE interface', '', '', '', '2751', 'HCAL', 'HCAL'],
['Franco Gonella', 'Muon', '2010', 'Construction', 'For leadership of the DT front end ASIC, the MAD and minicrate testing', '', '', '', '2817', 'Muons', 'Muons'],
['Tony Wildish', 'SWC', '2010', 'Construction', 'For outstanding contribution to data and workflow management', '', '', '', '2844', 'Offline & Computing', 'Offline and Computing'],
['Jean-Paul Chatelain', 'TC', '2010', 'Construction', 'For outstanding supervision and design, notably for BRM, ECAL and RPC', '', '', '', '2855', 'TC', 'Technical Coordination'],
['Lassi Tuura', 'SWC', '2010', 'Construction', 'For outstanding contribution to data quality monitoring and visualization', '', '', '', '2977', 'Offline & Computing', 'Offline and Computing'],
['Tami Kramer', 'Admin', '2010', 'Construction', 'For outstanding contribution to support of CMS visiting physicists', '', '', '', '2986', 'Admin', 'Administration'],
['Grassi Tullio', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the HCAL Trigger and Receiver Boards', '', '', '', '3026', 'HCAL', 'HCAL'],
['Raymond Gilles', 'SWC', '2010', 'Construction', 'For major contribution to user support and CMS Centers Worldwide', '', '', '', '3071', 'Offline & Computing', 'Offline and Computing'],
['Konstantin Abadjiev', 'Muon', '2010', 'Construction', 'For outstanding contribution to RPC production and CMS underground installation', '', '', '', '3110', 'Muons', 'Muons'],
['Pier Paolo Trapani', 'TC', '2010', 'Construction', 'see ECAL', '', '', '', '3157', 'TC', 'Technical Coordination'],
['Pier Paolo Trapani', 'RC', '2010', 'Construction', 'see ECAL', '', '', '', '3157', 'Run Coordination', 'Run Coordination'],
['Guenter Hilgers', 'Muon', '2010', 'Construction', 'For outstanding contribution to the DT HV boards and to the VDC system', '', '', '', '3257', 'Muons', 'Muons'],
['Natalia Ratnikova', 'SWC', '2010', 'Construction', 'For outstanding contribution as a software librarian', '', '', '', '3265', 'Offline & Computing', 'Offline and Computing'],
['Michael Case', 'SWC', '2010', 'Construction', 'For outstanding contribution to the detector description geometry', '', '', '', '3281', 'Offline & Computing', 'Offline and Computing'],
['David Bailleux', 'ECAL', '2010', 'Construction', 'For the contribution to the ECAL monitoring lasers.', '', '', '', '3413', 'ECAL', 'ECAL'],
['John Hill', 'ECAL', '2010', 'Construction', 'For outstanding contribution as project engineer for the ECAL Endcaps.', '', '', '', '3425', 'ECAL', 'ECAL'],
['Dan Wenman', 'Muon', '2010', 'Construction', 'For leadership in the CSC mechanical systems', '', '', '', '3463', 'Muons', 'Muons'],
['Bruno Levesy', 'Mag', '2010', 'Construction', 'For outstanding contribution to the general engineering of the Coil', '', '', '', '3496', 'Mag', 'Technical Coordination'],
['Silverio Dos Santos', 'TC', '2010', 'Construction', 'For crucial contribution to the CMS power distribution systems', '', '', '', '3515', 'TC', 'Technical Coordination'],
['Silverio Dos Santos', 'RC', '2010', 'Construction', 'see TC', '', '', '', '3515', 'Run Coordination', 'Run Coordination'],
['Clemente Adamantonio', 'Muon', '2010', 'Construction', 'For outstanding contribution to the RPC mechanical design', '', '', '', '3565', 'Muons', 'Muons'],
['Francesco Palmonari', 'TRK', '2010', 'Construction', 'For the contribution to the integration of the CMS Tracker.', '', '', '', '3642', 'Tracker', 'Tracker'],
['Vincenzo Giordano', 'Muon', '2010', 'Construction', 'For outstanding contributions to the production of DT cathodes', '', '', '', '3696', 'Muons', 'Muons'],
['Dawn Hudson', 'TC', '2010', 'Construction', 'For outstanding contribution in support of CMS construction and commissioning', '', '', '', '3767', 'TC', 'Technical Coordination'],
['Dawn Hudson', 'Admin', '2010', 'Construction', 'see TC', '', '', '', '3767', 'Admin', 'Administration'],
['Dawn Hudson', 'RC', '2010', 'Construction', 'see TC', '', '', '', '3767', 'Run Coordination', 'Run Coordination'],
['Sandro Di Vincenzo', 'TC', '2010', 'Construction', 'For sustained contribution to the barrel muon detector and beampipe', '', '', '', '3781', 'TC', 'Technical Coordination'],
['Sandro Di Vincenzo', 'RC', '2010', 'Construction', 'see TC', '', '', '', '3781', 'Run Coordination', 'Run Coordination'],
["Mariarosaria D'Alfonso", 'HCAL', '2020', 'CMS award', 'For sustained contributions to a series of critical improvements in HCAL local reconstruction over many years,and for general leadership in HCAL detector performance.', '', '', '', '3795', 'HCAL', 'HCAL'],
['Osman Zorba', 'TRK', '2010', 'Construction', 'For the contribution to the Tracker DAQ and Control Electronics.', '', '', '', '3891', 'Tracker', 'Tracker'],
['Dean White', 'TRK', '2010', 'Construction', 'For the contribution to the modules construction of the CMS Tracker.', '', '', '', '3960', 'Tracker', 'Tracker'],
['Olga Vlassova', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the YE1/HE interface', '', '', '', '4244', 'HCAL', 'HCAL'],
['Thomas Maarten', 'Software, Computing', '2007', 'Achievement', 'leadership in the CMS Monte Carlo production', 'Aachen', '', '', '4377', 'Offline & Computing', 'Offline and Computing'],
['Irakli Mandjavidze', 'ECAL', '2010', 'Construction', 'For contribution to the ECAL off-detector electronics.', '', '', '', '4424', 'ECAL', 'ECAL'],
['Herve Sauce', 'TC', '2010', 'Construction', 'For outstanding commitment during the assembly and maintenance of CMS', '', '', '', '4566', 'TC', 'Technical Coordination'],
['Martin Wensveen', 'TC', '2010', 'Construction', 'For outstanding contribution to the CMS cabeling effort', '', '', '', '4603', 'TC', 'Technical Coordination'],
['Michel Chauvey', 'TC', '2010', 'Construction', 'For outstanding commitment during the assembly and maintenance of CMS', '', '', '', '4712', 'TC', 'Technical Coordination'],
['Marc Favre', 'TC', '2010', 'Construction', 'For outstanding commitment during the assembly and maintenance of CMS', '', '', '', '4715', 'TC', 'Technical Coordination'],
['Johnson Marvin', 'TRK', '2010', 'Construction', 'For the commissioning of the CMS Tracker.', '', '', '', '4763', 'Tracker', 'Tracker'],
['Paul Debbins', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the HCAL source drivers', '', '', '', '4890', 'HCAL', 'HCAL'],
['Marco Rovere', 'Tracker', '2020', 'CMS award', 'For his leadership in the HGCAL DPG group and for major contributions to the clustering algorithms in the reconstruction software development.', '', '', '', '5284', 'Tracker', 'Tracker'],
['Jonathan Jason Hollar', 'PPS', '2020', 'CMS award', 'In his role as Deputy Project Manager he has followed the data-taking, the calibration, as well as the ongoing analyses with inexhaustible energy, competence, and tact.', '', '', '', '5942', 'PPS', 'PPS'],
['John Osborne', 'TC', '2010', 'Construction', 'For outstanding contribution to the Civil Engineering of CMS', '', '', '', '6099', 'TC', 'Technical Coordination'],
['Cristina Botta', 'Trigger Coordination', '2020', 'CMS award', 'For her outstanding contributions in developing and coordinating the physics trigger menu for the Level-1 Phase-2 Trigger Upgrade', '', '', '', '6218', 'Trigger Coordination', 'Trigger Coordination'],
['Vladimir Rekovic', 'L1TRG', '2020', 'CMS award', 'For his excellent coordination, crucial contributions, and exceptional commitment to the offline software of the Level-1 Trigger Project.', '', '', '', '6399', 'L1 Trigger', 'L1 Trigger'],
['Cathy Farrow', 'BRIL', '2020', 'CMS award', 'For her service and technical expertise in CMS detector construction, with particular dedication to the Pixel Luminosity Telescope assembly for both Run 2 and Run 3.', '', '', '', '6550', 'BRIL', 'BRIL'],
['Felipe Torres da Silva', 'Muon', '2020', 'CMS award', 'For his significant contribution to the development of a new version of the online software for control, monitoring and efficiency performance of the phase 1 RPC and for the development of a new RPC system for the LHC Phase 2.', '', '', '', '6623', 'Muons', 'Muons'],
['Georg Auzinger', 'BRIL', '2020', 'CMS award', 'For building and steering the collaboration for the BRIL Phase II Upgrade and in coordinating the rebuild effort for the BCM1F detector for Run 3.', '', '', '', '7624', 'BRIL', 'BRIL'],
['Chayanit Asawatangtrakuldee', 'PPD', '2020', 'CMS award', 'For her leadership of and crucial contributions to the Physics Dataset and Monte Carlo Validation group, and in particular her invaluable impact on software release validation, data re-reconstruction and Monte Carlo production.', '', '', '', '7838', 'PPD', 'PPD'],
['Mantas Stankevicius', 'Run Coordination', '2020', 'CMS award', 'For his dedicated work on the Web Based Monitoring System and the design and implementation of the new Online Monitoring System.', '', '', '', '7866', 'Run Coordination', 'Run Coordination'],
['Niels Dupont', 'Technical Coordination', '2020', 'CMS award', 'For his outstanding work in the safety area of technical coordination during the past ten years.', '', '', '', '7895', 'TC', 'Technical Coordination'],
['Silvan Streuli', 'TRK', '2010', 'Construction', 'For the contribution to the construction of the CMS pixel detector', '', '', '', '7900', 'Tracker', 'Tracker'],
['Dylan Rankin', 'L1TRG', '2020', 'CMS award', 'For his outstanding contribution to the development of the first hardware implementation of the particle flow algorithm for the Level-1 Trigger Upgrade', '', '', '', '8658', 'L1 Trigger', 'L1 Trigger'],
['Isabelle De Bruyn', 'Muon', '2020', 'CMS award', 'For her outstanding role in coordinating the commissioning activities of the CSC.', '', '', '', '8872', 'Muons', 'Muons'],
['Atanu Modak', 'Tracker', '2020', 'CMS award', 'For his efforts that enabled significant improvements in the configuration time of the pixel detector.', '', '', '', '9000', 'Tracker', 'Tracker'],
['Ivan Shvetsov', 'Tracker', '2020', 'CMS award', 'For his excellent work in Strip Tracker operations, including in his current role of StripTracker run coordinator.', '', '', '', '9194', 'Tracker', 'Tracker'],
['Cristian Pena', 'MTD', '2020', 'CMS award', 'For his critical and extensive work in developing and demonstrating the precision timing detector designs for both the barrel and endcap regions of MTD.', '', '', '', '9505', 'MTD', 'MTD'],
['Marino Missiroli', 'Trigger', '2020', 'CMS award', 'For his exceptional contributions to the trigger in almost every single area: online operations, data quality monitoring, Run2/Run3 preparation, and the jet/MET effort for Phase-II.', '', '', '', '9734', 'Trigger', 'Trigger Coordination'],
['Mei Hualin', 'Muon', '2020', 'CMS award', 'For his impressive knowledge of hardware, software, electronics and performance analysis Hualin Mei gave important and significant contributions to CSC upgrade program and in chamber tests at GIF++.', '', '', '', '9968', 'Muons', 'Muons'],
['Martha Estefany Nunez Ornelas', 'Muon', '2020', 'CMS award', 'For her outstanding contribution to the CSC LS2 testing of the post-refurbishment electronics  and of the new generation trigger mother board. Her contribution was crucial to avoid any delay in the CSC LS2 upgrade that was the main driver of the whole CMS LS2 schedule.', '', '', '', '10004', 'Muons', 'Muons'],
['Giorgio Cotto', 'Muon', '2020', 'CMS award', 'For his sound technical contributions to the installation of the DT MB4 shielding in LS2. He proposed innovative solutions and tools that substantially helped the timely and  safe installation of the  shielding.', '', '', '', '10296', 'Muons', 'Muons'],
['Valerio Pettinacci', 'ECAL', '2020', 'CMS award', 'For his outstanding commitment and successful coordination of the design of the new ECAL enfourneur, the machine for the insertion and extraction of the ECAL supermodules in CMS.', '', '', '', '10298', 'ECAL', 'ECAL'],
['Austin Baty', 'Tracker', '2020', 'CMS award', 'For his major developments in the heavy ions tracking reconstruction and research activity aiming to evolve the tracker raw storage scheme.', '', '', '', '10343', 'Tracker', 'Tracker'],
['Pramod Pathare', 'HCAL', '2010', 'Construction', 'For outstanding contribution to the Outer HCAL system', '', '', '', '10384', 'HCAL', 'HCAL'],
['Kenneth Long', 'Computing and Offline', '2020', 'CMS award', 'For his outstanding contributions to the MC generator group.', '', '', '', '10546', 'Offline & Computing', 'Offline and Computing'],
['Patrick Connor', 'Tracker', '2020', 'CMS award', 'For his excellent work leading the large and complex alignment campaign of the tracker following the 2018 data taking and the alignment effort for the legacy reprocessing of the CMS Run 2 data.', '', '', '', '11113', 'Tracker', 'Tracker'],
['Matteo Cremonesi', 'Computing and Offline', '2020', 'CMS award', 'For his methodical approach and dedication to solving the problem of speeding up future HEP analyses, co-creating the Coffea analysis framework and exploring innovative infrastructures such as Apache Spark combined with the SWAN service.', '', '', '', '11123', 'Offline & Computing', 'Offline and Computing'],
['Grace Cummings', 'HCAL', '2020', 'CMS award', 'For outstanding contributions to the assembly and testing of Next-Generation Clock and Control Modules for the Phase 1 upgrade of the HCAL Barrel and Endcap front-end electronics', '', '', '', '11369', 'HCAL', 'HCAL'],
['Shubham Pandey', 'HGCAL', '2020', 'CMS award', 'For his key role in the HGCAL test beam program, including construction, operation, and data analyses to which he made major contributions.', '', '', '', '11492', 'Endcap Calorimeter', 'Endcap Calorimeter'],
['Frederic Dulucq', 'HGCAL', '2020', 'CMS award', 'For his leadership and technical expertise in designing the readout ASIC for HGCAL, including prototype developments for the test beam program and the final ASIC, the HGROC.', '', '', '', '11575', 'Endcap Calorimeter', 'Endcap Calorimeter'],
['Soham Bhattacharya', 'Trigger Coordination', '2020', 'CMS award', 'For his huge and significant contributions in the reconstruction and identification of E/gamma objects in the HGCal for the HLT TDR', '', '', '', '11642', 'Trigger Coordination', 'Trigger Coordination'],
['Stefan Maier', 'Tracker', '2019', 'Detector', 'For outstanding contributions to the upgrade of the Outer Tracker, including development of procedures and systems in 2S module assembly and qualification and the development of a high-rate test stand for the module readout chain.', 'KIT', '', '', '11700', 'Tracker', 'Tracker'],
['Inseok Yoon', 'Muon', '2020', 'CMS award', 'For his intensive work to ensure Korean foil production and certification towards GE21, through the setup of a test facility in Korea for ageing studies and the crucial establishment of the certification process with all relevant tests.', '', '', '', '12000', 'Muons', 'Muons'],
['Pieter David', 'Tracker', '2020', 'CMS award', 'For his excellent work as convener of the Strip Tracker Local Reco group for the past several years.', '', '', '', '12021', 'Tracker', 'Tracker'],
['Minseok Oh', 'Trigger', '2020', 'CMS award', 'For his critical work in muon trigger development for the HLT Phase-II TDR and his outstanding contributions to the Run2 muon HLT over many years.', '', '', '', '12360', 'Trigger', 'Trigger Coordination'],
['Joscha Knolle', 'BRIL', '2020', 'CMS award', 'For his critical contributions to the Run 2 luminosity analysis and its publication.', '', '', '', '12551', 'BRIL', 'BRIL'],
['Giovanni Mocellin', 'Muon', '2020', 'CMS award', 'For many substantial and original contributions to the GE1/1 system, from simulation studies to chamber testing and characterization and commissioning at CERN. These achievements were instrumental to reach the GEM detectors required performance.', '', '', '', '12769', 'Muons', 'Muons'],
['Wassef Karimeh', 'Tracker', '2021', 'CMS award', 'For outstanding work on the CMS Strip Tracker detector control system.', 'Universite Saint-Joseph de Beyrouth', '', '', '13527', 'Tracker', 'Tracker'],
['Francesco Bianchi', 'Tracker', '2020', 'CMS award', 'For his significant contributions to the thermal simulations for the Tracker upgrade.', '', '', '', '13555', 'Tracker', 'Tracker'],
['Inna Makarenko', 'Tracker', '2020', 'CMS award', 'For her crucial contributions to the Data Acquisition for the Outer Tracker, enabling critical tests for ASICs and hybrids.', '', '', '', '13707', 'Tracker', 'Tracker'],
['Federico Siviero', 'MTD', '2020', 'CMS award', 'For his critical work on the design and characterization of the silicon sensors for the MTD Endcap Timing Layer, which is guiding the final sensor design.', '', '', '', '14163', 'MTD', 'MTD'],
['Natalia Koss', 'MTD', '2020', 'CMS award', 'For her essential contributions to the cooling, mechanical and module designs for the MTD Endcap Timing Layer.', '', '', '', '14185', 'MTD', 'MTD'],
['Congqiao Li', 'Computing and Offline', '2020', 'CMS award', 'For allowing the Legacy processing to benefit from the most recent framework developments in the area of parallelisation of event generators.', '', '', '', '14760', 'Offline & Computing', 'Offline and Computing'],
['Stefano Caregari', 'HGCAL', '2020', 'CMS award', 'For his excellent work on the HGCAL electronics development, including the design of DCDC ASICs and associated components.', '', '', '', '14858', 'Endcap Calorimeter', 'Endcap Calorimeter']
        ]

        with open( 'instMapPrehistory.json', 'r') as jF:
            instMapPreHistory = json.load(jF)
        
        with open( 'eprAgg.json', 'r') as jF:
            eprAgg = json.load(jF)
        
        users = set()
        noInstUsers = []
        userMap = {}
        for row in aList:
            name, proj, yearStr = row[:3]
            year = int( yearStr )
            cmsId = row[-3]
            
            if int(cmsId) in self.userInstMap.keys(): continue  # skip the users we already know of
            
            users.add(name)
            if name not in userMap:
                userMap[name] = []
            userMap[name].append( year )
            if year > 2014:
                # get info from API for EPR time line:
                url = f'/epr/timeline?year={year}&cms_id={cmsId}'
                res, timing = self.getData( url )
                inst = res[0]["instCode"]
                if verbose: logging.info( f'analyseMissingInsts> Found Inst for {name} ({cmsId}) in {year}: {inst}' )
                self.userInstMap[ int(cmsId) ] = inst
            elif year > 2000:
                inst = findInst( instMapPreHistory, cmsId, year)
                if inst:
                    if verbose: logging.info( f'analyseMissingInsts> Found Inst via "pre-History" info for {name} ({cmsId}) in {year} : {inst}' )
                    self.userInstMap[ int(cmsId) ] = inst
                else:
                    inst = findEprInst( eprAgg, cmsId, year )
                    if inst:
                        if verbose: logging.info( f'analyseMissingInsts> Found Inst via EPR aggregate info for {name} ({cmsId}) in {year} : {inst}' )
                        self.userInstMap[ int(cmsId) ] = inst
                    else:
                        if verbose: logging.info( f'++analyseMissingInsts++> Found NO Inst for {name} ({cmsId}) in {year} ' )
                        noInstUsers.append( row )
            else:
                if verbose: logging.info( f'==analyseMissingInsts==> Found NO Inst for {name} ({cmsId}) in {year} ' )
                noInstUsers.append( row )
        
        print()
        logging.info( f'analyseMissingInsts> found {len(users)} unique initial names w/o inst')
        if verbose: [ logging.info( f'{k}: {v}') for k, v in userMap.items() ]
        logging.info( f'analyseMissingInsts> found {len(noInstUsers)} users w/o inst from before 2015 ')
        if verbose: [ logging.info( x ) for x in noInstUsers ]

        logging.info( f'analyseMissingInsts> found overall {len(self.userInstMap.keys())} institutes for users ' )

    def importPastAwards(self):
        
        self.importPastAwardsFromCSV()
        self.getAwardsFromDB()

        self.updateAwardTypes()
        self.updateAwards()

        self.analyseMissingInsts()

        self.updateAwardees()
        
def main():
    global theLogger

    theApp = Flask( __name__, instance_relative_config=True )
    theApp.config.from_pyfile( 'config.py' )

    with theApp.app_context() :
        db = WebappOrmManager(theApp)
        db.create_all(bind='icms_common_db')

        dbUrl = str(db.engine.url)
        theLogger = theApp.logger
        theLogger.setLevel( logging.INFO )
        theLogger.info( "\n==> DB at: %s \n" % dbUrl[:25] )

        pai = PastAwardImporter()
        pai.importPastAwards()
        
        del theLogger

    return 0

if __name__ == '__main__':
    retCode = main()
