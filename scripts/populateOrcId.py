
import os
import sys
import datetime
import time
import logging
import pyexcel as pe
import json

from flask import Flask
from sqlalchemy.orm.exc import  NoResultFound, MultipleResultsFound

from icms_orm.common import UserAuthorData
from icms_orm.cmspeople import Person as PeopleData

from util.factory import WebappOrmManager

sys.path.append( '.' )
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

theLogger = None

def timing_val(func):
    def wrapper(*arg, **kw):
        '''source: http://www.daniweb.com/code/snippet368.html'''
        t1 = time.time()
        res = func(*arg, **kw)
        t2 = time.time()
        theLogger.debug( '%s ran in %s sec.' % (func.__name__, (t2 - t1)) )
        return (t2 - t1), res, func.__name__
    return wrapper

def getExcelInfo( fileName ) :

    book_dict = pe.get_book_dict( file_name=fileName )

    fn, itemList = book_dict.popitem()

    idMap = {}
    for inspId, orcId in itemList:
        idMap[ inspId ] = orcId

    return idMap

class UserFinder(object):

    def __init__(self, db):
        self.db = db

        self.allUsers = { }

        allUsersDB = (self.db.session.query( PeopleData.hrId, PeopleData.cmsId, PeopleData.authorId)
                   .filter( PeopleData.authorId != '' )
                   .all())

        for hrId, cmsid, inspId in allUsersDB :
            if inspId in self.allUsers.keys( ) : continue
            self.allUsers[ inspId ] = (hrId, cmsid, inspId)

        print( "UserFinder> got %d users from DB" % len( self.allUsers.keys() ) )

    def findUser(self, inspId):

        if inspId.strip( ) == '' :
            return None

        return self.allUsers[ inspId ] if inspId in self.allUsers.keys() else None

def importOrcIdData(inFileName, db):

    orcIdMap = getExcelInfo( inFileName )
    uf = UserFinder(db)

    index = 0
    nNoInfo = 0
    nDone = 0
    for inspId, orcId in orcIdMap.items():
        uInfo = uf.findUser(inspId)
        if not uInfo:
            nNoInfo += 1
            continue
        index += 1
        if index<5: print( inspId, orcId, uInfo )
        if inspId != uInfo[2]:
            print( "inconsistency: inspId %s maps to %s " % (inspId, uInfo) )

    #     uad = UserAuthorData(hrid=uInfo[0], cmsid=uInfo[1], inspireid=inspId, orcid=orcId)
    #     db.session.add( uad )
    #     nDone += 1
    #
    # db.session.commit()

    print( "found %d users w/o inspId " % nNoInfo )
    print( "added %d users to DB" % nDone )

def main():

    global theLogger

    theApp = Flask( __name__, instance_relative_config=True )
    theApp.config.from_pyfile( 'config.py' )

    with theApp.app_context() :
        db = WebappOrmManager(theApp)
        db.create_all(bind='icms_common_db')

        dbUrl = str(db.engine.url)
        theLogger = theApp.logger
        theLogger.setLevel( logging.INFO )
        theLogger.info( "\n==> DB at: %s \n" % dbUrl)#[:15] )

        importOrcIdData( './cms_inspireId-orcId-map-2017-08-04.csv', db)

        del theLogger

    return 0

if __name__ == '__main__':
    retCode = main()
