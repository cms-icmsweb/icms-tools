#!/usr/bin/env python3

# A wrapper for the various do-*-svn* scripts in /afs/cern.ch/cms/PAS/
# See how the script was called (via the symlink in the dir), then
# call the latex build with the appropriate arguments/options
#
# Author: Andreas.Pfeiffer@cern.ch
#
# Copyright CERN, 2018
#

import sys
import os
import re
import shutil, socket

from .doLatexBuild import CMSLatexBuildChecker

class PasPaperBuildWrapper(object):

    def __init__(self):
        self.argMap = { 'do-paper-svn-nodraft' : [ "--wrap", "--nodraft" ],
                        'do-paper-svn'         : [ "--export" ],
                        'do-pas-svn-nodraft'   : [ "--wrap", "--nodraft" ],
                        'do-pas-svn-preview'   : [ "--preview", "--nodraft" ],
                        'do-pas-svn-upload'    : [ "--nodraft", "--wrap", "--upload" ],
                        'do-pas-svn'           : [ "--wrap" ],
                      }

    def process( self ):

        calledAs = os.path.split( sys.argv[0] )[-1]
        args = self.argMap[ calledAs ] if calledAs in self.argMap else []

        print( "\npasPaperWrapper> argsIn: ", sys.argv[1:] )

        if not args:
            print( "\nERROR: pasPaperWrapper was called as '%s' -- no args available, can not process ! Aborting. \n" % calledAs )
            return -1

        cadiType = ''
        if calledAs.startswith( 'do-paper-' ): cadiType = 'paper'
        if calledAs.startswith( 'do-pas-' )  : cadiType = 'pas'

        if not cadiType:
            print( "\n==> ERROR: pasPaperWrapper no style (paper or pas) given ! Aborting. \n" )
            return -2

        cadiLine = sys.argv[1]
        if len(sys.argv) > 2: # treat remaining args as options to pass on, ignore the obsolete -vN one
            for argIn in sys.argv[1:-2]:
               args.append( argIn.replace('-reload=', '--reload=') if argIn.startswith('-reload') else argIn )
            cadiLine = sys.argv[-2]

        checkRe = r'^[A-Z0-9]{3}-\d{2}-\d{3}$'
        if not re.match( checkRe, cadiLine ):
            print( "\n==> ERROR: pasPaperWrapper found illegal cadiLine '%s', not matching regex '%s'\n" % (cadiLine, checkRe) )

        if not cadiLine:
            print( "\n==> ERROR: pasPaperWrapper no cadiLine given ! Aborting. \n" )
            return -3

        print( "pasPaperWrapper was called as '%s' - type: %s, args: '%s', cadiLine '%s' " % (calledAs, cadiType, ' '.join( args ), cadiLine) )

        lbc = CMSLatexBuildChecker()
        lbc.keepFiles = True # for now do not remvoe the created dirs/files
        log, (runTime, buildTime) = lbc.processOne( args + [cadiType, cadiLine] )
        for cmd, res in log.items():
            print( '[%s]\n%s\n' % (cmd, res) )
        lbc.showBuildLog()

        return 0


def test():

    if 'vocms0233' in socket.gethostname():
        print( "Refusing to run tests on active production system ! " )
        return -1

    ppbw = PasPaperBuildWrapper()
    aliases = ppbw.argMap.keys()
    del ppbw

    tmpDir = './fooTmp'
    os.makedirs( tmpDir )
    os.chdir( tmpDir )
    for item in aliases:
        cmd = 'ln -s ../scripts/pasPaperWrapper.py %s && ./%s XXX-08-000' % (item, item)
        os.system( cmd )
    os.chdir('..')
    shutil.rmtree( tmpDir )


def main():
    ppbw = PasPaperBuildWrapper()
    ppbw.process()

if __name__ == '__main__':
    if '--test' in ' '.join( sys.argv ):
        sys.argv.remove('--test')
        print( "remaining argv: ", sys.argv )
        test()
    else:
        sys.exit( main() )


