from subprocess import run, CalledProcessError, PIPE, STDOUT
import getpass
import json
import requests
import logging
import time
from netrc import netrc

from auth_get_sso_cookie.cern_sso import login_with_kerberos

import urllib3
urllib3.disable_warnings()


class IcmsApiBaseAccess():
    def __init__(self):
        self.krbTokenOK = False

        # self.user = os.getenv('USER')

        self.netrcKey = 'icms-api-cmswww.cern.ch'
        (login, account, password) = netrc().authenticators( self.netrcKey )

        self.pwd = password
        self.user = login

        self.loggedIn = False
        
    def _checkKrbToken(self, verbose=False):
        cmd = 'klist' # 'klist -l && klist'
        out = None
        try:
            res = run(cmd, shell=True, check=True, stdout=PIPE, stderr=STDOUT)
            out = res.stdout.decode()
            if verbose: [ logging.info( f'_checkKrbToken> {x}' ) for x in out.split("\n")]
        except CalledProcessError as e:
            logging.info( f'_checkKrbToken> {e}' )
            return False, None
        except Exception as e:
            logging.info( f'_checkKrbToken> ERROR when CHECKING kerberos token !' )
            return False, None
        return True, out
    
    def _getKrbToken(self, verbose=False):
        cmd = f'kinit {self.user}@CERN.CH'
        try:
            res = run(cmd, input=self.pwd, shell=True, check=True, stdout=PIPE, stderr=STDOUT)
            out = res.stdout
            if verbose: [ logging.info( f'_getKrbToken> {x}' ) for x in out.split("\n")]
        except CalledProcessError as e:
            logging.info( f'_getKrbToken> { str(e) }' )
            return False, None
        except Exception as e:
            logging.info( f'_getKrbToken> ERROR: { str(e) }' )
            return False, None
        return True, out
    
    def login(self, verbose=False):
        target0 = f'Default principal: {self.user}@CERN.CH'
        target1 = 'krbtgt/CERN.CH@CERN.CH'
        success, out = self._checkKrbToken(verbose)
        if not out or (target0 not in out or target1 not in out):
            logging.info( f'login> no kerberos token found for {self.user} ' )

            if self.netrcKey:
                (login, acct, passwd) = netrc().authenticators(self.netrcKey)
                self.pwd = passwd
                self.user = login

            if not self.pwd: 
                self.pwd = getpass.getpass( f'Enter lxplus password for user {self.user}:')
            success, out = self._getKrbToken(verbose)
            if not success:
                logging.info( f'login> ERROR when getting kerberos token !' )
            success, out = self._checkKrbToken(verbose)
            if not success or (target0 not in out or target1 not in out):
                logging.info( f'login> ERROR when checking kerberos token !' )
                logging.info( f'login> success: {success}' )
                logging.info( f'login> target0 not in out: {target0 not in out}' )
                logging.info( f'login> target1 not in out: {target1 not in out}' )
                [ logging.info( f'login> {x}' ) for x in out.split("\n")]
            self.loggedIn = True
            logging.info( f'login> successfully logged in as {self.user}' )
        else:
            self.loggedIn = True
            logging.info( f'login> already logged in as {self.user}' )

    def logout(self, verbose=False):
        cmd = '/usr/bin/kdestroy'
        try:
            res = run(cmd, shell=True, capture_output=True, check=True)
            out = res.stdout.decode().split("\n")
            self.loggedIn = False
            
            if verbose: 
                logging.info( f'logout> {out}' )
                success, out = self._checkKrbToken(verbose)
                if out: [ logging.info( f'{x}' ) for x in out.split("\n")]
        except Exception as e:
            logging.info( f'logout> ERPOR when logging out ... ' )
            raise e


class IcmsApiAccess(IcmsApiBaseAccess):
    
    def __init__(self):
        super(IcmsApiAccess, self).__init__()

        self.memberData = {}

        self.topUrl = 'https://localhost:5073'
        # self.topUrl = 'https://icms-dev.cern.ch'
        authUrl = self.topUrl + '/tools/'
        
        logging.info( f'iaa:init> user: {self.user} topURL: {self.topUrl} ... ' )

        self.verify_cert = False

        logging.info( f'iaa:init> going to get kerberos ticket ... ' )

        try:
            self.login(verbose=True)
        except Exception as e:
            logging.info( f'iaa:init> ERPOR when logging in ... ' )
            raise e

        if not self.loggedIn:
            logging.info( f'iaa:init> ERPOR not logged in after login !?!? ' )
            return 

        logging.info( f'iaa:init> going to get cookies ... ' )

        try:
            self.authCookies = self.getAuthCookies( authUrl )
        except Exception as e:
            if 'Matching credential not found' in str(e):
                logging.info( f'ERROR when trying to get auth cookies !!! {self.authCookies}' )
        if not self.authCookies:
            logging.info( f'ERROR when trying to get auth cookies !!! {self.authCookies}' )                
                
    def log(self, lvl, msg):
        logging.info(msg)

    def getAuthCookies(self, url):

        try:
            auth_hostname='auth.cern.ch'
            session, response = login_with_kerberos(url, self.verify_cert, auth_hostname, silent=False)
            if response.status_code == 302:
                redirect_uri = response.headers["Location"]
                self.log( lvl='info', msg = "Logged in. Fetching redirect URL to get application cookies")
                session.get(redirect_uri, verify=self.verify_cert)
            self.log( lvl='info', msg = "Returning cookies")
            return session.cookies
        except Exception as e:
            self.log( lvl='error', msg = "An error occurred while trying to log in and get cookies for %s, %s" % (url, str(e)) )

        return None

    def getData(self, what):

        if not self.loggedIn: 
            logging.info( f'getData> not logged in, can not do any work ... ' )
            return None, (0,0)
        if not self.authCookies:
            logging.info( f'getData> no auth cookies, can not do any work ... ' )
            return None, (0,0)
        
        url = f'{self.topUrl}/tools-api/restplus/{what}'

        # logging.info( f'url: {url}' )
        startTime = time.time()
        data = requests.get(url, allow_redirects=True, cookies=self.authCookies, verify=self.verify_cert)
        if data.status_code != 200:
            logging.info( 'getData> ERROR:', data.status_code )
            logging.info( 'getData>  ... :', data.text )
        try:
            res = data.json()
        except json.decoder.JSONDecodeError:
            logging.info( 'getData> ERROR: no json found: ', data.text )

        return data.json(), (startTime, time.time())

    def postData( self, what, payload):

        if not self.loggedIn: 
            logging.info( f'putData> not logged in, can not do any work ... ' )
            return None, (0,0)
        if not self.authCookies:
            logging.info( f'putData> no auth cookies, can not do any work ... ' )
            return None, (0,0)

        url = f'{self.topUrl}/tools-api/restplus/{what}'

        # logging.info( f'url: {url}' )
        startTime = time.time()
        res = requests.post(url, allow_redirects=True, cookies=self.authCookies, verify=self.verify_cert, json=payload)
        if res.status_code != 200: logging.info( res.status_code )

        return res, (startTime, time.time())
  
    
    def putData( self, what, payload):

        if not self.loggedIn: 
            logging.info( f'putData> not logged in, can not do any work ... ' )
            return None, (0,0)
        if not self.authCookies:
            logging.info( f'putData> no auth cookies, can not do any work ... ' )
            return None, (0,0)

        url = f'{self.topUrl}/tools-api/restplus/{what}'

        # logging.info( f'url: {url}' )
        startTime = time.time()
        res = requests.put(url, allow_redirects=True, cookies=self.authCookies, verify=self.verify_cert, json=payload)
        if res.status_code != 200: logging.info( res.status_code )

        return res, (startTime, time.time())
  
    
