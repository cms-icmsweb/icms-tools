from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, populate_dbs
from tests.fixtures.authorization_fixtures import grant_supervisor_write, grant
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application
from icms_orm.toolkit import RestrictedActionTypeValues as Action
from webapp.permissions.access_class_definitions import AccessClassDefinition


class TestPath(PseudoEnum):
    impersonation = '/restplus/admin/impersonate'
    def impersonation_put(x): return f'/restplus/admin/impersonate/{x}'
    user_info = '/restplus/icms_public/user_info'


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'insert_access_classes', 'grant')
@pytest.mark.parametrize('grant', [(Action.DELETE, TestPath.impersonation, AccessClassDefinition.anyone.name), ], indirect=True)
class TestImpersonationEndpoints():

    @classmethod
    def assert_real_and_active_cms_ids(cls, client: IcmsTestClient, user: FakePerson, expected_cms_id: int):
        from blueprints.api_restplus.api_units.admin_units.impersonate_units import ImpersonateUserCall
        resp: Response = client.request(
            TestPath.impersonation).auth_header(user.email).get()
        assert 200 == resp.status_code
        assert user.cms_id == resp.get_json().get(
            ImpersonateUserCall.FIELD_REAL_CMS_ID.name)
        assert expected_cms_id == resp.get_json().get(
            ImpersonateUserCall.FIELD_ACTIVE_CMS_ID.name)

        resp = client.request(TestPath.user_info).auth_header(user.email).get()
        assert 200 == resp.status_code
        assert expected_cms_id == resp.get_json().get('cms_id')

    @classmethod
    def impersonate(cls, client: IcmsTestClient, user: FakePerson, target_user: FakePerson, expected_code: int = 200):
        resp: Response = client.request(
            TestPath.impersonation_put(target_user.cms_id)).auth_header(user.email).put()
        assert expected_code == resp.status_code, f'Impersonation attempt yielded code {resp.status_code}'

    @classmethod
    def stop_impersonating(cls, client: IcmsTestClient, user: FakePerson, expected_code: int = 200):
        resp = client.request(
            TestPath.impersonation).auth_header(user.email).delete()
        assert expected_code == resp.status_code, f'Dropping the disguise did not work ({resp.status_code})'

    @pytest.mark.parametrize('user', [FakePeople.jdoe, FakePeople.grossi])
    def test_non_impersonated_user_info(self, client: IcmsTestClient, user: FakePerson):
        self.assert_real_and_active_cms_ids(client, user, user.cms_id)

    def test_non_authenticated_info_access(self, client: IcmsTestClient):
        resp: Response = client.request(
            TestPath.impersonation).get()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('user', [FakePeople.jdoe, FakePeople.grossi])
    def test_non_admin_cannot_impersonate(self, client: IcmsTestClient, user: FakePerson):
        resp: Response = client.request(
            TestPath.impersonation_put(FakePeople.tbawej.cms_id)).auth_header(user.email).put()
        assert 403 == resp.status_code
        self.assert_real_and_active_cms_ids(client, user, user.cms_id)

    @pytest.mark.parametrize('user', [FakePeople.tbawej])
    def test_admin_can_impersonate(self, client: IcmsTestClient, user: FakePerson):
        self.impersonate(client, user, FakePeople.jdoe)
        self.assert_real_and_active_cms_ids(
            client, user, FakePeople.jdoe.cms_id)

    @pytest.mark.parametrize('user', [FakePeople.tbawej])
    def test_admin_impersonating_themselves(self, client: IcmsTestClient, user: FakePerson):
        self.impersonate(client, user, user)
        self.assert_real_and_active_cms_ids(
            client, user, user.cms_id)
        self.stop_impersonating(client, user, 409)


    @pytest.mark.parametrize('user', [FakePeople.tbawej])
    def test_admin_can_stop_impersonating(self, client: IcmsTestClient, user: FakePerson, grant):
        resp: Response = client.request(
            TestPath.impersonation_put(FakePeople.jdoe.cms_id)).auth_header(user.email).put()
        assert 200 == resp.status_code

        self.assert_real_and_active_cms_ids(
            client, user, FakePeople.jdoe.cms_id)

        self.stop_impersonating(client, user)

        self.assert_real_and_active_cms_ids(
            client, user, FakePeople.tbawej.cms_id)

    @pytest.mark.parametrize('user', [FakePeople.jdoe, FakePeople.grossi])
    def test_non_admin_cannot_stop_impersonating(self, client: IcmsTestClient, user: FakePerson):
        self.stop_impersonating(client, user, 409)
        self.assert_real_and_active_cms_ids(client, user, user.cms_id)
