from blueprints.api_restplus.api_units.voting_units.voting_event_api_unit import VotingEventsApiUnit
from blueprints.api_restplus.api_units.voting_units.vote_delegations_api_unit import VoteDelegationsApiUnit
from webapp.permissions.access_class_definitions import AccessClassDefinition
from blueprints.api_restplus.api_units.voting_units.voting_participants_api_unit import VotingParticipantsApiUnit
from blueprints.api_restplus.api_units.voting_units.voting_lists_api_unit import VotingListsApiUnit
import logging
from icms_orm.toolkit import RestrictedResource, RestrictedResourceTypeValues, Status, Voting
from typing import Any, Dict, Optional
from icms_orm.custom_classes import PseudoEnum
import pytest
from tests.fixtures.authorization_fixtures import grant_write
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
from tests.fixtures.fakedata_fixtures import FakeCountries, FakeInsts, FakePerson, FakePeople, FakeInst, FakeCountry
from datetime import date, datetime
from collections import defaultdict


log: logging.Logger = logging.getLogger(__name__)


class TestUri(PseudoEnum):
    events = '/restplus/voting/events'
    participants = '/restplus/voting/participants'
    lists = '/restplus/voting/lists'
    delegations = '/restplus/voting/delegations'
    delegation = '/restplus/voting/delegations/<int:id>'

    @staticmethod
    def participants_of(code: str):
        return f'{TestUri.participants}?{VotingParticipantsApiUnit.ARG_NAME_VOTING_KEY}={code}'


def as_voting(instance: Any) -> Optional[Voting]:
    '''just a twiddle-silencer for the IDE'''
    return isinstance(instance, Voting) and instance or None


class TestEvent(PseudoEnum):
    '''
    Some of the tests below are a little rough and should either use a set of 
    cascading fixtures before verifying individual assumptions or more DB-reloads.
    Eventually there are quite a bit assertions within each of the test cases 
    but hopefully that's fine enough to help pinpointing any potential regressions
    '''
    FOO42: Optional[Voting] = as_voting(Voting.from_ia_dict({
        Voting.code: 'FOO42',
        Voting.list_closing_date: date(2031, 1, 17),
        Voting.delegation_deadline: datetime(2031, 2, 17, 23, 59, 59),
        Voting.start_time: datetime(2031, 1, 18, 0, 0, 0),
        Voting.end_time: datetime(2031, 1, 19, 0, 0, 0),
        Voting.title: 'FooBar voting',
        Voting.type: Voting.Type.VOTE,
        Voting.applicable_mo_year: 2021
    }))
    ELEC27: Optional[Voting] = as_voting(Voting.from_ia_dict({
        Voting.code: 'ELEC27',
        Voting.list_closing_date: date(2027, 1, 17),
        Voting.delegation_deadline: datetime(2027, 2, 17, 23, 59, 59),
        Voting.start_time: datetime(2027, 1, 18, 0, 0, 0),
        Voting.end_time: datetime(2027, 1, 19, 0, 0, 0),
        Voting.title: 'Election 2027',
        Voting.type: Voting.Type.ELECTION,
        Voting.applicable_mo_year: 2021
    }))


@pytest.fixture(scope='class')
def insert_votings():
    ssn = Voting.session()
    _ = [ssn.add(event) for event in TestEvent.values()]
    ssn.commit()


@pytest.fixture(scope='class')
def set_up_permissions(insert_access_classes):
    '''
    Filtering on the resource level so that:
    - it's only matched when the requestor is the relevant team leader
    - and so a simple access class (anyone) is good
    - also, when a resource is NOT found, the default will be applied (eg. allowing admin write)
    '''
    ssn = RestrictedResource.session()

    ssn.add(RestrictedResource.from_ia_dict({
        RestrictedResource.key: TestUri.delegations,
        RestrictedResource.type: RestrictedResourceTypeValues.ENDPOINT,
        RestrictedResource.filters: [
            {"r.inst_code": "u.represented_institutes"}]
    }))
    ssn.add(RestrictedResource.from_ia_dict({
        RestrictedResource.key: TestUri.delegation,
        RestrictedResource.type: RestrictedResourceTypeValues.ENDPOINT,
        RestrictedResource.filters: [
            {"r.inst_code": "u.represented_institutes"}]
    }))
    ssn.commit()
    grant_write(TestUri.delegations, AccessClassDefinition.anyone.name)
    grant_write(TestUri.delegation, AccessClassDefinition.anyone.name)


@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db', 'insert_votings', 'set_up_permissions')
class TestVotingEndpoints():

    @staticmethod
    def get_participants(client: IcmsTestClient, code: str) -> Response:
        return client.request(TestUri.participants_of(code)).auth_header(FakePeople.tbawej.login).get()

    @staticmethod
    def get_delegation_payload(voting: Optional[Voting], principal: FakePerson, delegate: FakePerson) -> Dict[str, Any]:
        retval: Dict[str, Any] = {
            VoteDelegationsApiUnit.FIELD_INST_CODE.name: principal.inst.code,
            VoteDelegationsApiUnit.FIELD_DELEGATE_CMS_ID.name: delegate.cms_id,
            VoteDelegationsApiUnit.FIELD_PRINCIPAL_CMS_ID.name: principal.cms_id,
        }
        if voting:
            retval[VoteDelegationsApiUnit.FIELD_VOTING_CODE.name] = voting.code
        else:
            retval[VoteDelegationsApiUnit.FIELD_IS_PERENNIAL.name] = True
        return retval

    @pytest.mark.parametrize('principal,delegate', [(FakePeople.grossi, FakePeople.jdoe)])
    def test_perennial_delegation(self, client: IcmsTestClient, principal: FakePerson, delegate: FakePerson):
        payload = TestVotingEndpoints.get_delegation_payload(None, principal, delegate)
        # establish
        resp: Response = client.request(TestUri.delegations).auth_header(
            principal.login).payload(payload).post()
        assert 200 == resp.status_code
        delegation_id = resp.json['id']
        # test it does not affect elections
        resp = TestVotingEndpoints.get_participants(client, TestEvent.ELEC27.code)
        assert 200 == resp.status_code
        assert True is any([r['cms_id'] == principal.cms_id for r in resp.json]), 'Elections: principal should remain despite perennial delegation'
        assert False is any([r['cms_id'] == delegate.cms_id for r in resp.json]), 'Elections: perennial delegation should remain ignored'
        # test it does show up elsewhere
        resp = TestVotingEndpoints.get_participants(client, TestEvent.FOO42.code)
        assert 200 == resp.status_code
        assert True is any([r['cms_id'] == delegate.cms_id for r in resp.json]), 'Delegate should be present barring the elections'
        assert False is any([r['cms_id'] == principal.cms_id for r in resp.json]), 'Principal should have delegated the vote away by now'
        # cancel
        cancellation_payload: Dict[str, Any] = {VoteDelegationsApiUnit.FIELD_STATUS.name: Status.CANCELLED}
        cancellation_payload.update(payload)
        resp = client.request(TestUri.delegation.replace('<int:id>', str(
            delegation_id))).auth_header(principal.login).payload(cancellation_payload).put()
        # test that the delegate shows up nowhere anymore
        assert 200 == resp.status_code
        resp = TestVotingEndpoints.get_participants(client, TestEvent.FOO42.code)
        assert 200 == resp.status_code
        assert False is any([r['cms_id'] == delegate.cms_id for r in resp.json]), 'Delegate should be OUT of the list now'
        assert True is any([r['cms_id'] == principal.cms_id for r in resp.json]), 'Principal should be back IN now'

    @pytest.mark.parametrize('event', list(TestEvent.values()))
    def test_voting_participants(self, client: IcmsTestClient, event: Voting):
        leaders = [
            p
            for p in FakePeople.values()
            if p.led_insts
        ]
        exp_voter_cms_ids = set(
            leader.cms_id
            for leader in leaders
        )
        exp_inst_votes = defaultdict(int)
        for fake_person in FakePeople.values():
            if fake_person.led_insts:
                primary_led_inst_code = fake_person.led_insts[0].code
                exp_inst_votes[primary_led_inst_code] += 1
        fake_inst_authors = defaultdict(int)
        for fake_person in FakePeople.values():
            if fake_person.is_author:
                fake_inst_authors[fake_person.inst.code] += 1
        small_inst_codes = []
        for inst_code, author_count in fake_inst_authors.items():
            if author_count < 3:
                exp_inst_votes[None] += exp_inst_votes[inst_code]
                small_inst_codes.append(inst_code)
        for inst_code in small_inst_codes:
            del exp_inst_votes[inst_code]

        resp: Response = (
            client.request(TestUri.participants_of(event.code))
            .auth_header(FakePeople.tbawej.login)
            .get()
        )
        assert 200 == resp.status_code
        assert resp.json is not None, 'Expected some JSON in response'
        voters = resp.json
        assert len(leaders) == len(voters)
        voter_cms_ids = set(
            voter['cms_id']
            for voter in voters
        )
        assert voter_cms_ids == exp_voter_cms_ids, f'Expected voters {exp_voter_cms_ids}'
        returned_inst_votes = defaultdict(int)
        for voter in voters:
            returned_inst_votes[voter['unit']] += voter['votes']
        assert returned_inst_votes == exp_inst_votes, f'Expected {exp_inst_votes}'


    @pytest.mark.parametrize('principal, delegate', [(FakePeople.grossi, FakePeople.tbawej)])
    def test_one_off_delegation(self, client: IcmsTestClient, principal: FakePerson, delegate: FakePerson):
        payload = TestVotingEndpoints.get_delegation_payload(
            TestEvent.ELEC27, principal, delegate)

        resp: Response = client.request(TestUri.delegations).auth_header(
            principal.login).payload(payload).post()
        assert 200 == resp.status_code

        delegation_id = resp.json['id']

        # assert that the delegate now shows up in the list of voters
        resp = TestVotingEndpoints.get_participants(
            client, TestEvent.ELEC27.code)
        assert 200 == resp.status_code
        assert False is any(
            [r['cms_id'] == principal.cms_id for r in resp.json])
        assert True is any([r['cms_id'] == delegate.cms_id for r in resp.json])

        # make sure that a repeated delegation attempt would now fail
        resp = client.request(TestUri.delegations).auth_header(
            principal.login).payload(payload).post()
        assert 409 == resp.status_code, f'Repeated delegation attempt should have returned 409, not {resp.status_code}'

        # cancel the delegation
        cancellation_payload: Dict[str, Any] = {
            VoteDelegationsApiUnit.FIELD_STATUS.name: 'cancelled'}
        cancellation_payload.update(payload)
        resp = client.request(f'{TestUri.delegations}/{delegation_id}').auth_header(
            principal.login).payload(cancellation_payload).put()
        assert 200 == resp.status_code, f'Cancelling delegation #{delegation_id} yielded {resp.status_code}'

        # assert that the delegate no longer shows in the list of voters
        resp = TestVotingEndpoints.get_participants(
            client, TestEvent.ELEC27.code)
        assert 200 == resp.status_code
        assert True is any(
            [r['cms_id'] == principal.cms_id for r in resp.json])
        assert False is any(
            [r['cms_id'] == delegate.cms_id for r in resp.json])

    def test_catching_attempts_to_delegate_to_ppl_already_voting(self, client: IcmsTestClient):
        payload = TestVotingEndpoints.get_delegation_payload(
            TestEvent.ELEC27, FakePeople.grossi, FakePeople.tmuller)
        resp = client.request(TestUri.delegations).auth_header(
            FakePeople.grossi.login).payload(payload).post()
        assert 409 == resp.status_code    

    @pytest.mark.parametrize('user, exp_code', [(FakePeople.jdoe, 403), (FakePeople.tbawej, 200)])
    def test_delegation_access_rights(self, client: IcmsTestClient, user: FakePerson, exp_code: int):
        '''This one sits towards the end as it messes up the DB a little'''
        payload = TestVotingEndpoints.get_delegation_payload(
            TestEvent.FOO42, FakePeople.grossi, FakePeople.jdoe)
        resp = client.request(TestUri.delegations).auth_header(
            user.login).payload(payload).post()
        assert exp_code == resp.status_code, f'Delegation attempt should have returned {exp_code}, not {resp.status_code}'

    def test_listing_voting_lists_does_not_break(self, client: IcmsTestClient):
        resp: Response = client.request(
            TestUri.lists).auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code

    def test_submitting_new_voting_event(self, client: IcmsTestClient):
        from blueprints.api_restplus.api_units.basics.model_field_types import DateTime
        from icmsutils.icmstest.assertables import count_in_db
        payload = {
            VotingEventsApiUnit.Field.TYPE.name: Voting.Type.VOTE,
            VotingEventsApiUnit.Field.CODE.name: 'XMAS28',
            VotingEventsApiUnit.Field.TITLE.name: 'Dishes&Carols',
            VotingEventsApiUnit.Field.START_TIME.name: DateTime().format(datetime(2028, 12, 24, 20, 00)),
            VotingEventsApiUnit.Field.END_TIME.name: DateTime().format(datetime(2028, 12, 24, 21, 00)),
            VotingEventsApiUnit.Field.DELEGATION_DEADLINE.name: DateTime().format(datetime(2028, 12, 23, 20, 00)),
        }
        resp: Response = client.request(TestUri.events).auth_header(
            FakePeople.tbawej.login).payload(payload).post()
        assert 200 == resp.status_code
        assert 1 == count_in_db(
            Voting, {Voting.code: payload[VotingEventsApiUnit.Field.CODE.name], Voting.title: payload[VotingEventsApiUnit.Field.TITLE.name]})

    def test_persisting_voting_lists_and_subsequent_delegation_attempts(self, client: IcmsTestClient, insert_votings):
        '''This one also better sit towards the end of the suite'''
        resp: Response = client.request(TestUri.lists).auth_header(
            FakePeople.tbawej.login).payload({VotingListsApiUnit.FIELD_EVENT_CODE.name: TestEvent.FOO42.code}).post()
        assert 200 == resp.status_code
        payload = TestVotingEndpoints.get_delegation_payload(TestEvent.FOO42, FakePeople.grossi, FakePeople.jdoe)
        resp = client.request(TestUri.delegations).payload(payload).auth_header(FakePeople.grossi.login).post()
        assert 409 == resp.status_code
        assert 'cannot be modified' in resp.json['message']
