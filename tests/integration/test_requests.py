from flask.wrappers import Response
from icms_orm.common.common_schema_requests_tables import RequestStatusValues
import pytest
from icms_orm.custom_classes import PseudoEnum
from tests.fixtures.testkit.test_client import IcmsTestClient
from tests.fixtures.fakedata_fixtures import FakeStatuses, populate_icms_db, populate_people_db, FakePeople, FakePerson
from icmsutils.icmstest.assertables import count_in_db
from tests.fixtures.authorization_fixtures import grant_write
from webapp.permissions import AccessClassDefinition
from icms_orm.cmspeople import MemberStatusValues as Status
from blueprints.api_restplus.api_units.requests_api_units import PersonStatusRequestsApiUnit
from blueprints.api_restplus.api_units.requests_api_units import PersonStatusRequestsApiUnit
from icmsutils.businesslogic.requests import PersonStatusChangeRequestDefinition
from icms_orm.common import Request
from blueprints.api_restplus.api_units.requests_api_units import RequestStepsApiUnit
from icms_orm.common import RequestStepStatusValues as StepStatus, RequestStatusValues as RequestStatus
from icms_orm.cmspeople import Person


class TestUri(PseudoEnum):
    uri_requests = '/restplus/requests'
    uri_status_requests = '/restplus/requests/person/status'
    uri_request_steps = '/restplus/requests/steps'
    uri_request_step = '/restplus/requests/steps/<int:id>'


@pytest.fixture(scope='class')
def setup_permissions(insert_access_classes):
    grant_write(TestUri.uri_request_step, AccessClassDefinition.anyone.name)
    grant_write(TestUri.uri_status_requests, AccessClassDefinition.anyone.name)


@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db', 'setup_permissions')
class TestRequestsApi():

    @classmethod
    def request_status_change(cls, client: IcmsTestClient, actor: FakePerson, cms_id: int, status: str) -> Response:
        # TODO: construct the payload, assure that the naming of params is the same across Unit and Processor
        # WATCH OUT: the processing rules can interfere with permissions management
        payload = {
            PersonStatusRequestsApiUnit.FIELD_NEW_STATUS.name: status,
            PersonStatusRequestsApiUnit.FIELD_CMS_ID.name: cms_id
        }
        return client.request(TestUri.uri_status_requests).auth_header(actor.login).payload(payload).post()

    @classmethod
    def fetch_status_requests(cls, client: IcmsTestClient, actor: FakePerson, status: str = None) -> Response:

        uri = TestUri.uri_status_requests
        if status:
            uri = f'{uri}?{PersonStatusRequestsApiUnit.FIELD_STATUS.name}={status}'
        return client.request(uri).auth_header(actor.login).get()

    @pytest.mark.parametrize('actor,cms_id,status', [
        (FakePeople.grossi, FakePeople.tbawej.cms_id, FakeStatuses.exmember.name),
        (FakePeople.tmuller, FakePeople.aschmidt.cms_id, FakeStatuses.emeritus.name)
    ])
    def test_status_change_request(self, client: IcmsTestClient, actor: FakePerson, cms_id: int, status: str):
        resp: Response = self.request_status_change(
            client, actor, cms_id, status)
        assert 200 == resp.status_code
        assert cms_id == resp.json[PersonStatusRequestsApiUnit.FIELD_CMS_ID.name]
        assert actor.cms_id == resp.json[PersonStatusRequestsApiUnit.FIELD_CREATOR_CMS_ID.name]
        assert PersonStatusChangeRequestDefinition.get_name() == resp.json[
            PersonStatusRequestsApiUnit.FIELD_TYPE.name]

        assert 1 == count_in_db(
            Request, {Request.id: resp.json[Request.id.key]})

    @pytest.mark.parametrize('actor,cms_id,status', [(FakePeople.ksmith, FakePeople.jdoe.cms_id, FakeStatuses.exmember.name)])
    def test_fail_unauthorized_status_change_request(self, client: IcmsTestClient, actor: FakePerson, cms_id: int, status: str):
        resp: Response = self.request_status_change(
            client, actor, cms_id, status)
        assert 409 == resp.status_code
        assert 'cannot perform action' in resp.json['message']

    @pytest.mark.parametrize('actor, requests_status, exp_count', [
        (FakePeople.jdoe, None, 2),
        (FakePeople.ptgreat, RequestStatusValues.PENDING_APPROVAL, 2),
        (FakePeople.tbawej, RequestStatusValues.EXECUTED, 0)
    ])
    def test_listing_person_status_requests(self, client: IcmsTestClient, actor: FakePerson, requests_status: str, exp_count: int):
        resp: Response = self.fetch_status_requests(
            client, actor, requests_status)
        assert 200 == resp.status_code
        assert exp_count == len(resp.json)

    @pytest.mark.parametrize('actor, actionable', [(FakePeople.ptgreat, True), (FakePeople.jdoe, False), (FakePeople.grossi, False)])
    def test_fetching_actionable_steps(self, client: IcmsTestClient, actor: FakePerson, actionable: bool):
        """
        TODO: this test requires requests to be submitted. 
        It currently relies on execution order but a class-scoped fixture should be employed ionstead.
        """
        resp: Response = client.request(
            TestUri.uri_request_steps).auth_header(actor.login).get()
        assert 200 == resp.status_code
        assert 2 == len(resp.json)
        assert actionable == resp.json[0]['is_actionable_upon']
        assert actionable == resp.json[1]['is_actionable_upon']

    @pytest.mark.parametrize('actor', [FakePeople.grossi, FakePeople.tmuller])
    def test_fail_illegal_approval_of_status_change(self, client: IcmsTestClient, actor: FakePerson):
        """
        TODO: this test requires requests to be submitted. 
        It currently relies on execution order but a class-scoped fixture should be employed ionstead.
        """
        resp: Response = client.request(
            TestUri.uri_request_steps).auth_header(actor.login).get()
        step_ids = [e['id'] for e in resp.json]
        assert 0 < len(step_ids), 'The response was expected to contain some step ids'
        for id in step_ids:
            payload = {
                RequestStepsApiUnit.FIELD_STATUS.name: StepStatus.APPROVED}
            resp = client.request(
                f'{TestUri.uri_request_steps}/{id}').auth_header(actor.login).payload(payload).put()
            assert 409 == resp.status_code

    @pytest.mark.parametrize('actor', [FakePeople.ptgreat])
    def test_secr_approval(self, client: IcmsTestClient, actor: FakePerson):
        """
        TODO: this test requires requests to be submitted. 
        It currently relies on execution order but a class-scoped fixture should be employed ionstead.
        """
        resp: Response = client.request(
            TestUri.uri_request_steps).auth_header(actor.login).get()
        steps = resp.json
        assert 0 < len(steps)
        for step in steps:
            step_id = step[RequestStepsApiUnit.FIELD_ID.name]
            request_id = step[RequestStepsApiUnit.FIELD_REQUEST_ID.name]
            payload = {
                RequestStepsApiUnit.FIELD_STATUS.name: StepStatus.APPROVED}
            resp = client.request(
                f'{TestUri.uri_request_steps}/{step_id}').auth_header(actor.login).payload(payload).put()
            assert 200 == resp.status_code

            resp = client.request(
                f'{TestUri.uri_requests}/{request_id}').auth_header(actor.login).get()
            cms_id = resp.json['processing_data']['cms_id']
            new_status = resp.json['processing_data']['new_status']
            assert 1 == count_in_db(
                Person, {Person.cmsId: cms_id, Person.status: new_status})

            resp = client.request(
                f'{TestUri.uri_status_requests}/{request_id}').auth_header(actor.login).get()
            assert 200 == resp.status_code
            assert RequestStatus.EXECUTED == resp.json[RequestStepsApiUnit.FIELD_STATUS.name]
