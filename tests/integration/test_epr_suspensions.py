from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, populate_dbs
from tests.fixtures.authorization_fixtures import grant_supervisor_write
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application


class TestPath(PseudoEnum):
    suspensions = '/restplus/inst/suspensions'


@pytest.mark.parametrize("grant_supervisor_write", [(TestPath.suspensions), ], indirect=True)
@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'grant_supervisor_write')
class TestSuspensionManagement():

    @pytest.mark.parametrize('user, member, input, exp_code', [
        (FakePeople.tmuller, FakePeople.aschmidt, False, 200),
        (FakePeople.tmuller, FakePeople.aschmidt, True, 200),
    ])
    def test_suspension_toggling(self, client: IcmsTestClient, user: FakePerson, member: FakePerson, input: bool, exp_code: int):
        resp: Response
        resp = client.request(TestPath.suspensions).auth_header(user.email).payload({
            'cmsId': member.cms_id, 'isAuthorSuspended': input
        }).put()
        assert exp_code == resp.status_code

        from icms_orm.cmspeople import Person
        person: Person
        person = Person.session().query(Person).filter(Person.cmsId == member.cms_id).one()
        assert input == person.isAuthorSuspended

    @pytest.mark.parametrize('user, member, input, exp_code', [
        (FakePeople.tmuller, FakePeople.trice, True, 403),
        (FakePeople.tmuller, FakePeople.trice, False, 403),
    ])
    def test_illegal_inputs(self, client: IcmsTestClient, user: FakePerson, member: FakePerson, input, exp_code):
        resp = client.request(TestPath.suspensions).auth_header(user.email).payload(
            {'cmsId': member.cms_id, 'isAuthorSuspended': input}).put()
        assert exp_code == resp.status_code
