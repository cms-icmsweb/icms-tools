from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, populate_dbs
from tests.fixtures.authorization_fixtures import grant_supervisor_write
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application


class TestPath(PseudoEnum):
    overdue_graduations = '/restplus/inst/overdue-graduations'
    overdue_status = f'{overdue_graduations}/status'


@pytest.mark.parametrize('grant_supervisor_write', [TestPath.overdue_status], indirect=True)
@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'grant_supervisor_write')
class TestOverdueGraduationsLogic():
    def test_that_the_page_loads(self, client: IcmsTestClient):
        resp: Response
        resp = client.request(TestPath.overdue_graduations).auth_header(
            FakePeople.tbawej.email).get()
        assert 200 == resp.status_code
        print(resp.get_json())

    def test_cross_inst_write_fails_with_403(self, client: IcmsTestClient):
        actor = FakePeople.tmuller
        student: FakePerson
        student = FakePeople.ksmith
        resp = client.request(TestPath.overdue_status).auth_header(
            actor.email).payload({'cmsId': student.cms_id, 'status': 'confirmed'}).post()
        assert 403 == resp.status_code

    def test_inst_leader_writes_pass(self, client: IcmsTestClient):
        actor = FakePeople.grossi
        student: FakePerson
        student = FakePeople.ksmith
        resp = client.request(TestPath.overdue_status).auth_header(
            actor.email).payload({'cmsId': student.cms_id, 'status': 'confirmed'}).post()
        assert 200 == resp.status_code
