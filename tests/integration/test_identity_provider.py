from uuid import uuid1
from flask.wrappers import Response
from icms_orm.custom_classes import PseudoEnum
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from tests.fixtures.fakedata_fixtures import FakePerson, populate_dbs, FakePeople
from tests.fixtures.authorization_fixtures import grant_self_write, grant_supervisor_write
from flask.testing import FlaskClient
from datetime import datetime, timedelta
import uuid
import sqlalchemy as sa


class TestPath(PseudoEnum):
    demand = lambda url: f'/idProvider/demand/{url}'
    redeem = lambda token: f'/idProvider/redeem/{token}'


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs')
class TestIdentityProvider():

    req_url = 'http://fakecms.cern.ch/iCMS/somePage'

    def test_that_anonymous_users_cannot_demand_a_token(self, client: IcmsTestClient):
        resp: Response = client.request(TestPath.demand(self.req_url)).get()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('user', [FakePeople.grossi, FakePeople.tbawej])
    def test_that_authenticated_users_can_demand_a_token(self, client: IcmsTestClient, user: FakePerson):
        resp: Response = client.request(TestPath.demand(
            self.req_url)).follow_redirects(False).auth_header(user.login).get()
        assert 302 == resp.status_code
        body: str = resp.data.decode('utf8')
        assert "http://fakecms.cern.ch/iCMS/analysisadmin/loginsso?token=" in body

    @pytest.mark.parametrize('token', ['nonsense', 'halftruth'])
    def test_redeeming_invalid_token(self, token: str, client: IcmsTestClient):
        resp: Response = client.request(TestPath.redeem(token)).get()
        assert 404 == resp.status_code

    def test_redeeming_existing_token(self, client: IcmsTestClient):
        from icms_orm.toolkit import ProvidedIdentity
        ssn = ProvidedIdentity.session()
        
        token: str = 'secret'
        username: str = 'tbawej'
        hr_id: int = 123

        id = ProvidedIdentity.from_ia_dict({
            ProvidedIdentity.hr_id: hr_id,
            ProvidedIdentity.requested_url: self.req_url,
            ProvidedIdentity.token: token,
            ProvidedIdentity.username: username,
            ProvidedIdentity.timestamp: datetime.utcnow()
        })
        ssn.add(id)
        ssn.commit()

        resp: Response = client.request(TestPath.redeem(token)).get()
        assert 200 == resp.status_code

        body = resp.data.decode('utf8')
        assert f'{hr_id},{username},{self.req_url}' in body

    @pytest.mark.parametrize('request_ages', [
        [142],
        [142, 4242]
    ])
    def test_redeeming_outdated_token(self, client: IcmsTestClient, request_ages: list):
        from icms_orm.toolkit import ProvidedIdentity
        ssn = ProvidedIdentity.session()

        for age in request_ages:
            token: str = str(uuid.uuid1())
            username: str = 'tbawej'
            hr_id: int = 123

            id = ProvidedIdentity.from_ia_dict({
                ProvidedIdentity.hr_id: hr_id,
                ProvidedIdentity.requested_url: self.req_url,
                ProvidedIdentity.token: token,
                ProvidedIdentity.username: username,
                ProvidedIdentity.timestamp: datetime.utcnow() - timedelta(seconds=age)
            })
            ssn.add(id)
            ssn.commit()

        resp: Response = client.request(TestPath.redeem(token)).get()
        assert 410 == resp.status_code


    @pytest.mark.parametrize('input_url, expected_url', [
        (req_url, req_url),
        ('https:/cms.cern.ch/iCMS/analysisadmin', 'https://cms.cern.ch/iCMS/analysisadmin'),
        ('http:/cms.cern.ch/iCMS', 'http://cms.cern.ch/iCMS'),
        ('http://cms.cern.ch/iCMS/fooPage?bar=42&truth=false', 'http://cms.cern.ch/iCMS/fooPage?bar=42&truth=false'),
        ('http://cms.cern.ch/iCMS/fooPage?bar=42\';drop table X--', 'http://cms.cern.ch/iCMS/fooPage?bar=42\';drop table X--')
        ])
    def test_preserving_the_url(self, client: IcmsTestClient, input_url: str, expected_url: str):
        """
        The double slash in http(s):// sometimes gets slashed along the way. 
        """
        from icms_orm.toolkit import ProvidedIdentity
        resp: Response = client.request(TestPath.demand(input_url)).follow_redirects(
            False).auth_header(FakePeople.tbawej.login).get()
        assert 302 == resp.status_code

        ssn = ProvidedIdentity.session()
        entry: ProvidedIdentity = ssn.query(ProvidedIdentity).order_by(
            sa.desc(ProvidedIdentity.id)).first()
        assert expected_url == entry.requested_url
