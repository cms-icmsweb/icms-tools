import json
import logging
from io import BytesIO
from typing import Any, Dict, Optional

import pytest
from flask.wrappers import Response
from icms_orm import PseudoEnum
from mockito import unstub, when
from mockito.matchers import captor
from requests import Response as ReqResponse
from requests import Session
from requests.sessions import PreparedRequest
from tests.fixtures.fakedata_fixtures import FakePeople
from tests.fixtures.testkit.test_client import IcmsTestClient, RequestBuilder


log: logging.Logger = logging.getLogger(__name__)


class TestUri(PseudoEnum):
    pbk_notes = '/restplus/relay/piggyback/notes'


class FakeResponse(ReqResponse):
    def __init__(self, request: PreparedRequest, body: str, headers: Optional[Dict[str, str]] = None, status_code: int = 200) -> None:
        super().__init__()
        self.status_code = status_code
        self.raw = BytesIO(body.encode())
        for k, v in (headers or {}).items():
            self.headers[k] = v
        self.request = request


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs')
class TestApiRelayEndpoints():

    default_headers = {'content-type': 'application/json',
                       'content-disposition': 'inline', 'location': 'https://icms.cern.ch'}
    default_body = dict(name='foo', quality='splendid', quantity=42)

    @classmethod
    def teardown_class(cls):
        unstub()

    @pytest.mark.parametrize('uri, headers, body, status_code', [
        (TestUri.pbk_notes, default_headers, default_body, 200),
        ('/restplus/relay/piggyback/a/b/c?queryString=1or2or3',
         default_headers, {'foo': 213}, 301),
    ])
    def test_get_relay(self, client: IcmsTestClient, uri: str, headers: Dict[str, str], body: Dict[str, Any], status_code: int):
        request_captor = captor()
        when(Session).send(request_captor, ...).thenReturn(FakeResponse(
            request=request_captor.value, body=json.dumps(body), status_code=status_code, headers=headers))

        resp: Response = client.request(uri).auth_header(
            FakePeople.tbawej.login).follow_redirects(False).get()
        assert status_code == resp.status_code

        for header_name, header_value in headers.items():
            assert header_value == resp.headers[header_name]

        for body_key, body_value in body.items():
            assert body_value == resp.json[body_key]

        assert isinstance(request_captor.value, PreparedRequest)
        assert uri.replace('/restplus/relay/piggyback',
                           '') in str(request_captor.value.url)

    def test_delete_relay(self, client: IcmsTestClient):
        request_captor = captor()
        when(Session).send(request_captor, ...).thenReturn(FakeResponse(
            request=request_captor.value, body='deleted', status_code=200, headers=dict()))
        resp: Response = client.request(TestUri.pbk_notes).auth_header(
            FakePeople.tbawej.login).follow_redirects(False).delete()
        assert 200 == resp.status_code
        assert isinstance(request_captor.value, PreparedRequest)
        assert 'DELETE' == request_captor.value.method

    @pytest.mark.parametrize('method', [RequestBuilder.post, RequestBuilder.put])
    def test_post_put_relay(self, client: IcmsTestClient, method):
        request_captor = captor()
        when(Session).send(request_captor, ...).thenReturn(FakeResponse(
            request=request_captor.value, body=json.dumps({'?': 'ok'}), status_code=201, headers=dict()))

        resp: Response = method(client.request(TestUri.pbk_notes).auth_header(
            FakePeople.tbawej.login).follow_redirects(False).payload(self.default_body))
        assert 201 == resp.status_code
        assert isinstance(request_captor.value, PreparedRequest)
        captured_payload = json.loads(request_captor.value.body.decode())
        assert self.default_body == captured_payload
        assert method.__name__ == request_captor.value.method.lower()

    def test_thwarting_anonymous_requests(self, client: IcmsTestClient):
        resp: Response = client.request(
            TestUri.pbk_notes).follow_redirects(True).get()
        assert 403 == resp.status_code
