import logging
from icms_orm.toolkit import ALFileset
from blueprints.api_restplus.api_units.authorlist_units.authorlist_files_api_units import ALFileApiUnit
from typing import Dict
from util import constants
from _pytest.fixtures import SubRequest
from icms_orm.custom_classes import PseudoEnum
import pytest
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
from tests.fixtures.basic_fixtures import app, client, sign_in, reset_dbs_icms, reset_db_people
from tests.fixtures.fakedata_fixtures import FakePerson, populate_icms_db, populate_people_db, FakePeople
from tests.fixtures.basic_fixtures import TestappSingletonFactory
from icmsutils.businesslogic.flags import Flag as FlagDef
import flask
import time
from datetime import date
from os import listdir, makedirs, path
import shutil
from icmsutils.icmstest.assertables import count_in_db
from blueprints.api_restplus.api_units.authorlist_units import AuthorListUnit
from blueprints.api_restplus.api_units.authorlist_units import ALFileSetApiCall
from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory
import pathlib
from blueprints.author_lists.gen_al_views import AuthorListFileType


log: logging.Logger = logging.getLogger(__name__)


class TestData(PseudoEnum):
    al_entries = '/restplus/authorlists/entries'
    al_lists = '/restplus/authorlists/lists'
    al_filesets = '/restplus/authorlists/filesets'
    al_files = '/restplus/authorlists/files'
    # to be appended with code/type
    al_file = '/al/getFile/'
    @staticmethod
    def al_entry(id): return f'/restplus/authorlists/entries/{id}'
    @staticmethod
    def al_list(code): return f'/restplus/authorlists/lists/{code}'
    @staticmethod
    def al_fileset(id): return f'/restplus/authorlists/filesets/{id}'
    code_tst: str = 'TST-20-042'
    code_foo: str = 'FOO-21-019'
    code_bar: str = 'FOO-19-018'
    code_mee: str = 'MEE-22-222'


@pytest.fixture()
def sign_in_with_rights(client):
    yield client.post('/users/signin', data=dict(login='tbawej'), follow_redirects=True)
    client.get('/users/signout')


@pytest.fixture()
def generate_al_in_db(client: IcmsTestClient, request: SubRequest):
    code: str = request.param
    return TestAuthorListGenerationTests.generate_al_in_db(client, code)


@pytest.fixture()
def generate_al_files(client: IcmsTestClient, request: SubRequest):
    code: str = request.param
    return TestAuthorListGenerationTests.generate_al_files(client, code)


@pytest.fixture()
def generate_al_in_db_and_files(client: IcmsTestClient, request: SubRequest):
    code: str = request.param
    response: Response = TestAuthorListGenerationTests.generate_al_in_db(
        client, code)
    assert 200 == response.status_code, f'Initial in-DB AL generation for {code} should have gone smoothly'
    return TestAuthorListGenerationTests.generate_al_files(client, code)


@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db')
class TestAuthorListGenerationTests():

    _created_dirs = []

    @classmethod
    def generate_al_in_db(cls, client: IcmsTestClient, code: str) -> Response:
        payload = {}
        if code is not None:
            payload[AuthorListUnit.FIELD_PAPER_CODE.name] = code
        return client.request(TestData.al_lists).auth_header(FakePeople.tbawej.login).payload(payload).post()

    @classmethod
    def generate_al_files(cls, client: IcmsTestClient, code: str) -> Response:
        payload = {ALFileSetApiCall.FIELD_CODE.name: code}
        return client.request(TestData.al_filesets).auth_header(FakePeople.tbawej.login).payload(payload).post()

    @classmethod
    def delete_db_al(cls, client: IcmsTestClient, code: str) -> Response:
        payload = {AuthorListUnit.FIELD_PAPER_CODE.name: code}
        return client.request(TestData.al_list(code)).auth_header(FakePeople.tbawej.login).payload(payload).delete()

    @classmethod
    def close_indb_al(cls, client: IcmsTestClient, code: str) -> Response:
        payload = {AuthorListUnit.FIELD_PAPER_CODE.name: code,
                   AuthorListUnit.FIELD_IS_CLOSED.name: True}
        return client.request(TestData.al_list(code)).auth_header(FakePeople.tbawej.login).payload(payload).put()

    @classmethod
    def setup_class(cls):
        config = TestappSingletonFactory.get_web_app().config
        for key in ['AL_FILES_DIR', 'AL_DIFFS_DIR', 'AL_REF_DIR']:
            _target_dir = config.get(key)
            if path.exists(_target_dir):
                raise ValueError(
                    f'{_target_dir} alrteady exists. It should not so that it can be safely disposed of after the tests.')
            makedirs(_target_dir)
            cls._created_dirs.append(_target_dir)

    @classmethod
    def teardown_class(cls):
        for _dir in cls._created_dirs:
            shutil.rmtree(_dir, ignore_errors=True)

    @pytest.mark.parametrize('paper_code', [TestData.code_mee])
    def test_in_db_al_generation(self, client: IcmsTestClient, paper_code: str):
        resp = self.generate_al_in_db(client, paper_code)
        assert '200 OK' == resp.status
        from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory
        # todo: need to set up the DB correctly / pass proper `post` parameters to trigger the right chain of events...
        assert FakePeople.count_authors(
        ) == flask.current_app.db.session.query(PaperAuthor).count()
        assert 1 == flask.current_app.db.session.query(
            PaperAuthorHistory).count()
        pah: PaperAuthorHistory = PaperAuthorHistory.query.filter(
            PaperAuthorHistory.code == paper_code).one()
        assert paper_code == pah.code
        assert date.today() == pah.date

    @pytest.mark.skip(reason="needs reworking with new tool-chain for this")
    @pytest.mark.parametrize('generate_al_in_db_and_files', [TestData.code_tst], indirect=True)
    def test_files_generation(self, generate_al_in_db_and_files, client: IcmsTestClient):
        resp = generate_al_in_db_and_files
        assert 200 == resp.status_code, 'Expected file generation to go smoothly'
        al_files_dir = flask.current_app.config.get('AL_FILES_DIR')
        matching_files = [f for f in listdir(
            al_files_dir) if f.startswith('TST-20-042')]
        assert 1 < len(matching_files), 'Only found {0} matching files under {1}'.format(
            len(matching_files), al_files_dir)
        resp_file = client.request(f'{TestData.al_file}{TestData.code_tst}/authorlist.html').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp_file.status_code
        author: FakePerson
        text: str = resp_file.data.decode('utf8')
        for author in FakePeople.authors():
            assert author.last_name in text
        
    @pytest.mark.parametrize('code', ['HIG-19-025'])
    def test_existing_entries_error(self, client: IcmsTestClient, code: str):
        resp: Response = self.generate_al_in_db(client, code)
        assert 200 == resp.status_code
        resp = self.generate_al_in_db(client, code)
        assert 409 == resp.status_code
        assert f'{code} already exists' in resp.json['message']

    @pytest.mark.skip(reason="needs reworking with new tool-chain for this")
    @pytest.mark.parametrize('code', ['HIG-19-026'])
    def test_existing_files_error(self, client: IcmsTestClient, code: str):
        resp: Response = self.generate_al_in_db(client, code)
        assert 200 == resp.status_code
        resp = self.generate_al_files(client, code)
        assert 200 == resp.status_code
        resp = self.generate_al_files(client, code)
        assert 409 == resp.status_code
        assert f'Files for {code} already exist' in resp.json['message']

    @pytest.mark.skip(reason="needs reworking with new tool-chain for this")
    @pytest.mark.parametrize('code', ['B2G-17-026'])
    def test_rewriting_files(self, client: IcmsTestClient, code: str):
        resp: Response = self.generate_al_in_db(client, code)
        assert 200 == resp.status_code
        resp = self.generate_al_files(client, code)
        assert 200 == resp.status_code

        al_files_dir = flask.current_app.config.get('AL_FILES_DIR')
        al_file_path: pathlib.Path = pathlib.Path(
            f'{al_files_dir}/{code}-authorlist.html')
        assert al_file_path.exists()
        initial_ctime = al_file_path.stat().st_ctime

        paths: Dict[str, pathlib.Path] = {e.value: pathlib.Path(
            f'{al_files_dir}/{code}-{e.value}') for e in AuthorListFileType}
        assert len(paths) > 3
        initial_ctimes = dict()
        path: pathlib.Path
        for list_type, path in paths.items():
            assert path.exists()
            initial_ctimes[list_type] = path.stat().st_ctime

        log.debug(f'Making it look as if {code} files had been generated long ago.')
        fileset: ALFileset = ALFileset.query.filter(
            ALFileset.paper_code == code).one()
        fileset.last_update = date(2019, 11, 23)
        fileset.session().add(fileset)
        fileset.session().commit()

        time.sleep(1.0)

        resp = client.request(TestData.al_fileset(code)).payload(
            {ALFileApiUnit.FIELD_PAPER_CODE.name: code}).auth_header(FakePeople.tbawej.login).put()
        assert 200 == resp.status_code, f'Request to re-generate files yieled {resp.status_code}'

        fileset: ALFileset = ALFileset.query.filter(
            ALFileset.paper_code == code).one()
        assert date.today(
        ) == fileset.last_update, f'Expected the fileset update time to be {date.today()}, not {fileset.last_update}'

        for list_type, path in paths.items():
            assert path.exists()
            assert path.stat().st_ctime > initial_ctimes[list_type]


    @pytest.mark.parametrize('code', ['HIG-19-027'])
    def test_deleting_entries(self, client: IcmsTestClient, code: str):
        resp: Response = self.generate_al_in_db(client, code)
        assert 200 == resp.status_code
        assert 0 < count_in_db(PaperAuthor, {PaperAuthor.code: code})
        assert 1 == count_in_db(PaperAuthorHistory, {
                                PaperAuthorHistory.code: code})
        resp = self.delete_db_al(client, code)
        assert 200 == resp.status_code, f'Delete yielded response code {resp.status_code}'
        assert 0 == count_in_db(PaperAuthor, {PaperAuthor.code: code})
        assert 0 == count_in_db(PaperAuthorHistory, {
                                PaperAuthorHistory.code: code})

    @pytest.mark.parametrize('code', ['XYZ-19-028'])
    def test_deleting_nonexistent_error(self, client: IcmsTestClient, code: str):
        resp: Response = self.delete_db_al(client, code)
        assert 404 == resp.status_code

    # Eventually it might not be that big of a deal to delete something from the DB despite having some files for it. Commenting out for now
    # @pytest.mark.parametrize('code', ['HIG-19-028'])
    # def test_deleting_with_existing_files_error(self, client: IcmsTestClient, code: str):
    #     resp: Response = self.generate_al_in_db(client, code)
    #     assert 200 == resp.status_code
    #     resp = self.generate_al_files(client, code)
    #     assert 200 == resp.status_code
    #     resp = self.delete_db_al(client, code)
    #     assert 409 == resp.status_code

    @pytest.mark.parametrize('code', ['HIG-19-028'])
    def test_closing_authorlist(self, client: IcmsTestClient, code: str):
        resp: Response = self.generate_al_in_db(client, code)
        assert 200 == resp.status_code
        resp = self.close_indb_al(client, code)
        assert 200 == resp.status_code
        resp = client.request(TestData.al_list(code)).auth_header(
            FakePeople.tbawej.login).get()
        _json = resp.json
        assert True == _json[AuthorListUnit.FIELD_IS_CLOSED.name]

    @pytest.mark.parametrize('code', ['HIG-19-029'])
    def test_closing_closed_error(self, client: IcmsTestClient, code: str):
        resp: Response = self.generate_al_in_db(client, code)
        assert 200 == resp.status_code
        resp = self.close_indb_al(client, code)
        assert 200 == resp.status_code
        resp = self.close_indb_al(client, code)
        assert 409 == resp.status_code

    @pytest.mark.parametrize('paper_code, exp_status_code, exp_message, exp_errors', [
        (None, 400, 'Input payload validation failed', {
         AuthorListUnit.FIELD_PAPER_CODE.name: 'is a required'}),
        ('XY-1245-AAA', 400, 'Input payload validation failed',
         {AuthorListUnit.FIELD_PAPER_CODE.name: 'does not match'})
    ])
    def test_indb_al_failing_with_erroneous_input(self, client: IcmsTestClient, paper_code: str, exp_status_code: int, exp_message: str, exp_errors: Dict[str, str]):
        resp: Response = self.generate_al_in_db(client, paper_code)
        assert exp_status_code == resp.status_code
        assert exp_message in resp.json['message']
        for field, message in exp_errors.items():
            assert field in resp.json['errors']
            assert message in resp.json['errors'][field]
