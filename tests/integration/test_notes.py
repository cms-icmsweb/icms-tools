import pytest
from tests.fixtures import basic_fixtures
from icmsutils.icmstest import mock
from datetime import date


@pytest.fixture
def add_some_notes_to_db():
    from icms_orm.old_notes import Note, NoteAuthors, NoteCategory, NoteCategoriesRel, NoteReferees, NoteFile
    from icms_orm.old_notes import WorkflowProcess, WorkflowData, WorkflowRecord
    from icms_orm.cmspeople import Person

    entry_data = {
        Note.id: 389,
        Note.cmsNoteId: 'XYZ',
        Note.abstr: 'Too abstract',
        Note.title: 'Something',
        Note.submitter: 9981,
        Note.type: 'XY',
        Note.URL: 'go',
        Note.authors: 'Wise Guys',
        NoteFile.fileName: 'somenote.pdf',
    }
    ssn = Person.session()
    for factory in [mock.MockOldNoteFactory, mock.MockOldNoteFileFactory]:
        ssn.add(factory.create_instance(entry_data))
        ssn.flush()
    ssn.commit()
