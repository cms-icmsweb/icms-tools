from tests.fixtures.fakedata_fixtures import FakePeople, FakePerson, FakeProject, FakeProjects
from typing import List
from dataclasses import dataclass, field
from icms_orm.custom_classes import PseudoEnum
import pytest

from sqlalchemy.sql.operators import desc_op
from sqlalchemy.sql.sqltypes import Boolean
from tests.fixtures.basic_fixtures import recycle_dbs, reset_db_icms_public, reset_db_people, reset_dbs_icms, reset_old_cadi_db
from flask.testing import FlaskClient
from icmsutils.businesslogic.flags import Flag as FlagDef
from datetime import date, timedelta, datetime
from os import listdir
import time


@dataclass
class FakeWeek():
    """
    We need a mix of past and future, local and remote weeks...
    """
    id: int
    title: str
    date_start: date
    date_end: date
    is_external: bool = field(default=False)

    @classmethod
    def create(cls, id: int, title: str, days_ago: int, is_external: bool = False):
        start_date = (datetime.now() - timedelta(days=days_ago)).date()
        end_date = start_date + timedelta(days=4)
        return FakeWeek(id=id, title=title, date_start=start_date, date_end=end_date, is_external=is_external)


class FakeWeeks(PseudoEnum):
    past_local = FakeWeek.create(1, 'CMS WEEK', 28, False)
    past_remote = FakeWeek.create(2, 'CMS remote WEEK I', 56, True)
    upcoming_local = FakeWeek.create(3, 'CMS WEEK', -28, False)
    upcoming_remote = FakeWeek.create(4, 'CMS remote WEEK II', -56, True)


@dataclass
class FakeRoom():
    id: int
    indico_id: int
    building: str
    floor: str
    number: str
    name: str
    is_at_cern: bool = field(default=True)
    specific_week: FakeWeek = field(default=None)


class FakeRooms(PseudoEnum):
    local_one_off = FakeRoom(1, None, 'P.32', 'RdC',
                             '-', 'CMS Tent', True, FakeWeeks.upcoming_local)
    local_500_r_012 = FakeRoom(2, 1, '500', 'R', '012', 'Fake room 1', True)
    remote_one = FakeRoom(3, None, '12', 'RdC', '123',
                          'The one without view', False, FakeWeeks.past_remote)
    remote_two = FakeRoom(4, None, 'XII', '-2', '32',
                          'The hidden one', False, FakeWeeks.upcoming_remote)
    remote_three = FakeRoom(5, None, '42', '7', '13',
                            'The one with a view', False, FakeWeeks.upcoming_remote)
    local_500_r_013 = FakeRoom(6, 2, '500', 'R', '013', 'Fake room 2', True)


@dataclass
class FakeBooking:
    id: int
    booked_by: FakePerson
    booked_for: FakePerson
    week: FakeWeek
    room: FakeRoom
    project: FakeProject
    status: str = field(default='pending')
    start_day: int = field(default=0)
    start_hour: int = field(default=12)
    start_min: int = field(default=0)
    duration: int = field(default=120)


class FakeBookings(PseudoEnum):
    one = FakeBooking(1001, FakePeople.grossi, FakePeople.grossi,
                      FakeWeeks.upcoming_local, FakeRooms.local_500_r_012, FakeProjects.daq)
    two = FakeBooking(1002, FakePeople.aschmidt, FakePeople.aschmidt,
                      FakeWeeks.upcoming_local, FakeRooms.local_500_r_012, FakeProjects.ecal, 'done', 1)
    three = FakeBooking(1003, FakePeople.ptgreat, FakePeople.ptgreat,
                        FakeWeeks.upcoming_local, FakeRooms.local_500_r_012, FakeProjects.ecal, 'done', 2)
    four = FakeBooking(1004, FakePeople.tmuller, FakePeople.tmuller, FakeWeeks.upcoming_local,
                       FakeRooms.local_500_r_013, FakeProjects.hcal, 'done', 2, 14, 30, 120)


@pytest.fixture(scope="class")
def insert_cms_weeks():
    from icms_orm.toolkit import CmsWeek
    week: FakeWeek
    for week in FakeWeeks.values():
        ia_dict = {
            CmsWeek.id: week.id,
            CmsWeek.title: week.title,
            CmsWeek.date: week.date_start,
            CmsWeek.date_end: week.date_end,
            CmsWeek.is_external: week.is_external
        }
        CmsWeek.session.add(CmsWeek.from_ia_dict(ia_dict))
    CmsWeek.session().commit()


@pytest.fixture(scope="class")
def insert_rooms():
    from icms_orm.toolkit import Room
    room: FakeRoom
    for room in FakeRooms.values():
        ia_dict = {
            Room.id: room.id,
            Room.indico_id: room.indico_id,
            Room.building: room.building,
            Room.floor: room.floor,
            Room.room_nr: room.number,
            Room.custom_name: room.name,
            Room.at_cern: room.is_at_cern
        }
        Room.session().add(Room.from_ia_dict(ia_dict))
    Room.session().commit()


@pytest.fixture(scope='class')
def insert_bookings(insert_cms_projects, insert_cms_weeks, insert_rooms):
    from icms_orm.toolkit import RoomRequest, Status
    ssn = RoomRequest.session()
    e: FakeBooking
    for e in FakeBookings.values():
        ssn.add(RoomRequest.from_ia_dict({
            RoomRequest.id: e.id,
            RoomRequest.cms_id_by: e.booked_by.cms_id,
            RoomRequest.cms_id_for: e.booked_for.cms_id,
            RoomRequest.title: 'TESTmeet',
            RoomRequest.project: e.project.code,
            RoomRequest.webcast: False,
            RoomRequest.time_start: e.week.date_start + timedelta(days=e.start_day, hours=e.start_hour, minutes=e.start_min),
            RoomRequest.duration: e.duration,
            RoomRequest.cms_week_id: e.week.id,
            RoomRequest.room_id_preferred: e.room.id,
            RoomRequest.room_id: e.room.id,
            RoomRequest.capacity: None,
            RoomRequest.password: None,
            RoomRequest.official: False,
            RoomRequest.agenda_url: None,
            RoomRequest.remarks: 'Thank you',
            RoomRequest.reason: 'To thank you',
            RoomRequest.status: e.status,
        }))
    ssn.commit()
