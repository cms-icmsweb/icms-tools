
from typing import List
from dataclasses import dataclass, field
from _pytest.fixtures import SubRequest
from icms_orm.custom_classes import PseudoEnum
import pytest
from enum import Enum
from tests.fixtures.basic_fixtures import reset_db_icms_public, reset_db_people, reset_dbs_icms, reset_old_cadi_db
from flask.testing import FlaskClient
from icmsutils.businesslogic.flags import Flag as FlagDef
import flask
from datetime import date
from os import listdir
import sqlalchemy as sa
import logging


@pytest.fixture(scope='class')
def insert_access_classes():
    from webapp.permissions import AccessClassDefinition
    AccessClassDefinition.insert_missing_classes()


@pytest.fixture(scope='class')
def insert_dummy_resources():
    from icms_orm.toolkit import RestrictedResource as RR, RestrictedResourceTypeValues as Type
    for id in range(1, 7):
        ia_dict = {RR.type: Type.ENDPOINT,
                   RR.key: f'/restplus/fake{id}', RR.filters: {}}
        RR.session().add(RR.from_ia_dict(ia_dict))
    RR.session().commit()


def grant_write(endpoint: str, access_class_name: str):
    from icms_orm.toolkit import RestrictedActionTypeValues as Action
    grant_action(Action.WRITE, endpoint, access_class_name)


def grant_action(action, endpoint: str, access_class_name: str):
    from icms_orm.toolkit import RestrictedResource as RR, AccessClass as AC, Permission as P
    from icms_orm.toolkit import RestrictedActionTypeValues as Action, RestrictedResourceTypeValues as Type

    session = P.session()

    rr = RR.session().query(RR).filter(RR.key == endpoint).one_or_none()
    if not rr:
        rr = RR.from_ia_dict({
            RR.key: endpoint, RR.type: Type.ENDPOINT, RR.filters: {}
        })
    assert isinstance(rr, RR)

    ac: AC
    ac = session.query(AC).filter(
        AC.name == access_class_name).one()

    p = P.from_ia_dict({
        P.access_class_id: ac.id, P.resource_id: rr.id, P.action: action, P.access_class: ac, P.resource: rr
    })

    session.add(p)
    session.commit()


@pytest.fixture(scope='class')
def grant(request: SubRequest, insert_access_classes):
    logging.info(f'Request to grant: {request.param}')
    grant_action(*request.param)

@pytest.fixture(scope='class')
def grant_self_write(request: SubRequest, insert_access_classes):
    from webapp.permissions import AccessClassDefinition
    grant_write(request.param, AccessClassDefinition.self.name)


@pytest.fixture(scope='class')
def grant_self_path_write(request: SubRequest, insert_access_classes):
    from webapp.permissions import AccessClassDefinition
    grant_write(request.param, AccessClassDefinition.self_path.name)


@pytest.fixture(scope='class')
def grant_supervisor_write(request: SubRequest, insert_access_classes):
    from webapp.permissions import AccessClassDefinition
    grant_write(request.param, AccessClassDefinition.inst_supervisor.name)


@pytest.fixture(scope='class')
def grant_booking_owner_write(request: SubRequest, insert_access_classes):
    from webapp.permissions import AccessClassDefinition
    grant_write(request.param, AccessClassDefinition.booking_owner.name)


@pytest.fixture(scope='class')
def grant_anyone_write(request: SubRequest, insert_access_classes):
    from webapp.permissions import AccessClassDefinition
    grant_write(request.param, AccessClassDefinition.anyone.name)

