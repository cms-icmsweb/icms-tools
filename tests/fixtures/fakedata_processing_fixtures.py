from tests.fixtures.fakedata_fixtures import FakePerson
import pytest
from datetime import datetime
from stringcase import snakecase


@pytest.fixture(scope="class")
def bypass_application(request):
    """
    :param request: expecting to find a FakePerson instance under request.param
    """
    from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
    person: FakePerson
    person = request.param

    aac = AAC(run_number=42, cms_activity=person.activity.old_id, cms_id=person.cms_id, passed=True, failed=False, inst_code=person.inst.code,
              days=420, worked_inst=199, worked_self=7.5, needed_inst=180, possible_slipthrough=False, mo_status=None, reason='Test')

    AAC.session.add(aac)
    AAC.session.commit()
