from typing import List
from dataclasses import dataclass, field
from icms_orm.cmspeople.people import MoData, User
from icms_orm.custom_classes import PseudoEnum
import pytest
from icmsutils.businesslogic.flags import Flag as FlagDef
from datetime import date, datetime
import time


@dataclass
class FakeOrgUnitType():
    id: int
    last_modified: datetime
    name: str
    level: int
    is_enclosed: bool
    is_active: bool

class FakeOrgUnitTypes(PseudoEnum):
    board = FakeOrgUnitType(1, datetime.utcnow(), "Board", 0, False, True)
    committee = FakeOrgUnitType(2, datetime.utcnow(), "committee", 1, False, True)
    office = FakeOrgUnitType(3, datetime.utcnow(), "Office", 1, False, True)
    coordinationArea = FakeOrgUnitType(4, datetime.utcnow(), "Coordination Area", 1, False, True)
    subdetector = FakeOrgUnitType(5, datetime.utcnow(), "Subdetector", 1, False, True)
    fundingAgency = FakeOrgUnitType(6, datetime.utcnow(), "Funding Agency", 10, False, True)
    institute = FakeOrgUnitType(7, datetime.utcnow(), "Institute", 10, False, True)
    physicsObjectGroup = FakeOrgUnitType(8, datetime.utcnow(), "Physics Object Group", 2, False, True)
    physicsAnalysisGroup = FakeOrgUnitType(9, datetime.utcnow(), "Physics Analysis Group", 2, False, True)
    level2Entry = FakeOrgUnitType(10, datetime.utcnow(), "Level 2 Entry", 2, False, True)
    editorialBoard = FakeOrgUnitType(11, datetime.utcnow(), "Editorial Board", 1, True, True)
    region = FakeOrgUnitType(14, datetime.utcnow(), "Region", 1, True, True)
    advisors = FakeOrgUnitType(15, datetime.utcnow(), "Advisors", 2, True, True)


@dataclass
class FakeOrgUnit():
    id: int
    last_modified: datetime
    type_id: int
    domain: str
    superior_unit_id: int
    enclosing_unit_id: int
    outermost_unit_id: int
    is_active: bool

class FakeOrgUnits(PseudoEnum):
    executive = FakeOrgUnit(374, datetime.utcnow(), 1, "Executive", None, None, None, True)
    collaboration = FakeOrgUnit(1, datetime.utcnow(), 1, "Collaboration", None, None, None, True)
    publications = FakeOrgUnit(7, datetime.utcnow(), 2, "Publications", 1, None, None, True)
    diversity = FakeOrgUnit(2, datetime.utcnow(), 3, "Diversity", 1, None, None, True)
    management = FakeOrgUnit(366, datetime.utcnow(), 1, "Management", None, None, None, True)
    trigger = FakeOrgUnit(364, datetime.utcnow(), 4, "Trigger", 366, None, None, True)
    muon = FakeOrgUnit(326, datetime.utcnow(), 5, "MUON", 366, None, None, True)
    ireland = FakeOrgUnit(35, datetime.utcnow(), 6, "Ireland", None, None, None, True)
    mit = FakeOrgUnit(92, datetime.utcnow(), 7, "MIT", None, None, None, True)
    physics = FakeOrgUnit(17, datetime.utcnow(), 4, "Physics", 366, None, None, True)
    egm = FakeOrgUnit(322, datetime.utcnow(), 8, "EGM", 17, None, None, True)
    bph = FakeOrgUnit(358, datetime.utcnow(), 9, "BPH", 17, None, 17, True)
    offlineAndComputing = FakeOrgUnit(349, datetime.utcnow(), 4, "Offline & Computing", 366, None, None, True)
    wbm = FakeOrgUnit(340, datetime.utcnow(), 10, "WBM", 349, None, None, True)
    hig = FakeOrgUnit(371, datetime.utcnow(), 11, "HIG", 7, 7, 7, True)
    cern = FakeOrgUnit(383, datetime.utcnow(), 14, "CERN", 366, None, 366, True)
    spokespersonAndTC = FakeOrgUnit(382, datetime.utcnow(), 15, "Spokesperson & Technical Coordination", 366, None, 366, True)

@dataclass
class FakePosition():
    id: id
    last_modified: datetime
    name: str
    unit_type_id: int
    level: int
    is_active: bool

class FakePositions(PseudoEnum):
    chairperson = FakePosition(5, datetime.utcnow(), 'Chairperson', 1, 2, True)
    leader = FakePosition(9, datetime.utcnow(), 'Leader', 1, 7, True)
    deputyLeader = FakePosition(10, datetime.utcnow(), 'Deputy Leader', 2, 7, True)
    secretary = FakePosition(7, datetime.utcnow(), 'Secretary', 2, 1, True)
    technicalCoordination = FakePosition(3, datetime.utcnow(), 'Technical Coordination', 3, 5, True)
    resourcesManager = FakePosition(34, datetime.utcnow(), 'Resources Manager', 3, 4, True)
    projectPlanningManager = FakePosition(15, datetime.utcnow(), 'Project Planning Manager', 4, 4, True)
    member = FakePosition(42, datetime.utcnow(), 'Member', 4, 2, True)

@dataclass
class FakeTenure():
    last_modified: datetime
    cms_id: int
    position_id: int
    unit_id: int
    start_date: datetime
    end_date: datetime

class FakeTenures(PseudoEnum):
    tenure1 = FakeTenure(datetime.utcnow(), 9981, 5, 1, datetime.strptime('01/01/10 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure2 = FakeTenure(datetime.utcnow(), 1234, 9, 374, datetime.strptime('01/01/08 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure3 = FakeTenure(datetime.utcnow(), 3306, 10, 7, datetime.strptime('01/01/15 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure4 = FakeTenure(datetime.utcnow(), 3307, 7, 2, datetime.strptime('01/01/12 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure5 = FakeTenure(datetime.utcnow(), 9090, 3, 366, datetime.strptime('01/01/18 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure6 = FakeTenure(datetime.utcnow(), 2341, 34, 364, datetime.strptime('01/10/19 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure7 = FakeTenure(datetime.utcnow(), 7788, 15, 326, datetime.strptime('01/09/13 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure8 = FakeTenure(datetime.utcnow(), 8080, 42, 35, datetime.strptime('01/02/14 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure9 = FakeTenure(datetime.utcnow(), 5432, 5, 92, datetime.strptime('01/02/20 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure10 = FakeTenure(datetime.utcnow(), 5984, 9, 17, datetime.strptime('01/02/21 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure11 = FakeTenure(datetime.utcnow(), 2856, 10, 322, datetime.strptime('01/05/19 00:00:00', '%d/%m/%y %H:%M:%S'), None)
    tenure12 = FakeTenure(datetime.utcnow(), 9101, 7, 358, datetime.strptime('01/05/19 00:00:00', '%d/%m/%y %H:%M:%S'), datetime.strptime('01/01/24 00:00:00', '%d/%m/%y %H:%M:%S'))
    tenure13 = FakeTenure(datetime.utcnow(), 1213, 3, 349, datetime.strptime('01/10/23 00:00:00', '%d/%m/%y %H:%M:%S'), datetime.strptime('01/05/25 00:00:00', '%d/%m/%y %H:%M:%S'))

@dataclass
class FakeExOfficioMandate():
    last_modified: datetime
    start_date: datetime
    src_unit_id: int
    dst_unit_id: int
    src_position_level: int
    dst_position_level: int
    src_position_name: str
    dst_position_name: str
    end_date: datetime
    id: int

class FakeExOfficioMandates(PseudoEnum):
    exOfficioMandate1 = FakeExOfficioMandate(
        datetime.utcnow(),
        datetime.strptime('01/01/10 00:00:00', '%d/%m/%y %H:%M:%S'),
        1, 374, 1, 1, 'Chairperson', None, None, 1
    )

@pytest.fixture(scope='class')
def insert_org_unit_types():
    from icms_orm.common import OrgUnitType
    ssn = OrgUnitType.session()
    orgUnitType: FakeOrgUnitType
    for orgUnitType in FakeOrgUnitTypes.values():
        db_OrgUnitType = OrgUnitType.from_ia_dict({
            OrgUnitType.last_modified: orgUnitType.last_modified,
            OrgUnitType.name: orgUnitType.name,
            OrgUnitType.level: orgUnitType.level,
            OrgUnitType.is_enclosed: orgUnitType.is_enclosed,
            OrgUnitType.is_active: orgUnitType.is_active,
            OrgUnitType.id: orgUnitType.id
        })
        ssn.add(db_OrgUnitType)
    ssn.commit()

@pytest.fixture(scope='class')
def insert_org_units():
    from icms_orm.common import OrgUnit
    ssn = OrgUnit.session()
    orgUnit: FakeOrgUnit
    for orgUnit in FakeOrgUnits.values():
        db_OrgUnit = OrgUnit.from_ia_dict({
            OrgUnit.last_modified: orgUnit.last_modified,
            OrgUnit.type_id: orgUnit.type_id,
            OrgUnit.domain: orgUnit.domain,
            OrgUnit.is_active: orgUnit.is_active,
            OrgUnit.superior_unit_id: orgUnit.superior_unit_id,
            OrgUnit.enclosing_unit_id: orgUnit.enclosing_unit_id,
            OrgUnit.outermost_unit_id: orgUnit.outermost_unit_id,
            OrgUnit.id: orgUnit.id
        })
        ssn.add(db_OrgUnit)
    ssn.commit()

@pytest.fixture(scope='class')
def insert_positions():
    from icms_orm.common import Position
    ssn = Position.session()
    position: FakePosition
    for position in FakePositions.values():
        db_Position = Position.from_ia_dict({
            Position.last_modified: position.last_modified,
            Position.name: position.name,
            Position.unit_type_id: position.unit_type_id,
            Position.level: position.level,
            Position.is_active: position.is_active,
            Position.id: position.id
        })
        ssn.add(db_Position)
    ssn.commit()

@pytest.fixture(scope='class')
def insert_tenures():
    from icms_orm.common import Tenure
    ssn = Tenure.session()
    tenure: FakeTenure
    for tenure in FakeTenures.values():
        db_Tenure = Tenure.from_ia_dict({
            Tenure.last_modified: tenure.last_modified,
            Tenure.cms_id: tenure.cms_id,
            Tenure.position_id: tenure.position_id,
            Tenure.unit_id: tenure.unit_id,
            Tenure.start_date: tenure.start_date,
            Tenure.end_date: tenure.end_date
        })
        ssn.add(db_Tenure)
    ssn.commit()

@pytest.fixture(scope='class')
def insert_ex_officio_mandates():
    from icms_orm.common import ExOfficioMandate
    ssn = ExOfficioMandate.session()
    exOfficioMandate: FakeExOfficioMandate
    for exOfficioMandate in FakeExOfficioMandates.values():
        db_ExOfficioMandate = ExOfficioMandate.from_ia_dict({
            ExOfficioMandate.last_modified: exOfficioMandate.last_modified,
            ExOfficioMandate.start_date: exOfficioMandate.start_date,
            ExOfficioMandate.src_unit_id: exOfficioMandate.src_unit_id,
            ExOfficioMandate.dst_unit_id: exOfficioMandate.dst_unit_id,
            ExOfficioMandate.src_position_level: exOfficioMandate.src_position_level,
            ExOfficioMandate.dst_position_level: exOfficioMandate.dst_position_level,
            ExOfficioMandate.src_position_name: exOfficioMandate.src_position_name,
            ExOfficioMandate.dst_position_name: exOfficioMandate.dst_position_name,
            ExOfficioMandate.end_date: exOfficioMandate.end_date,
            ExOfficioMandate.id: exOfficioMandate.id
        })
        ssn.add(db_ExOfficioMandate)
    ssn.commit()


# @pytest.mark.usefixtures('insert_org_unit_types', 
#                             'insert_org_units',
#                             'insert_positions',
#                             'insert_tenures',
#                             'insert_ex_officio_mandates')
@pytest.fixture(scope='class')
def insert_tenure_related_data(
    insert_org_unit_types,
    insert_org_units,
    insert_positions,
    insert_tenures,
    insert_ex_officio_mandates
):
    pass
