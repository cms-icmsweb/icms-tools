from typing import List, Optional
from dataclasses import dataclass, field
from icms_orm.cmspeople.people import MoData, User
from icms_orm.custom_classes import PseudoEnum
import pytest
from icmsutils.businesslogic.flags import Flag as FlagDef
from datetime import date, datetime
import time


@dataclass
class FakeProject:
    code: str
    name: str

    @classmethod
    def for_code(cls, code: str) -> "FakeProject":
        return FakeProject(code, code)


class FakeProjects(PseudoEnum):
    daq = FakeProject.for_code("DAQ")
    ecal = FakeProject.for_code("ECAL")
    hcal = FakeProject.for_code("HCAL")


@dataclass
class FakeActivity:
    name: str
    old_id: int


class FakeActivities(PseudoEnum):
    physicist = FakeActivity("Physicist", 6)
    phd_student = FakeActivity("Doctoral Student", 9)
    non_phd_student = FakeActivity("Non-Doctoral Student", 13)
    administrative = FakeActivity("Administrative", 1)
    engineer = FakeActivity("Engineer", 2)
    software_engineer = FakeActivity("Engineer Software", 3)
    other = FakeActivity("Other", 5)
    technician = FakeActivity("Technician", 15)
    theoretical_physicist = FakeActivity("Theoretical Physicist", 7)


@dataclass
class FakeOldStatus:
    status: str
    status_id: int
    is_author: bool
    start_date: date = field(default=date(2017, 12, 1))
    end_date: date = field(default=date(2018, 12, 1))
    is_primary: bool = field(default=False)


@dataclass
class FakeRegion:
    name: str
    code: str


@dataclass
class FakeCountry:
    name: str
    code: str
    region_code: str


@dataclass
class FakeInstStatus:
    name: str


class FakeInstStatuses(PseudoEnum):
    MEMBER = FakeInstStatus("Yes")
    FOR_REFERENCE = FakeInstStatus("ForReference")
    NON_MEMBER = FakeInstStatus("No")
    COOPERATING = FakeInstStatus("Cooperating")
    ASSOCIATED = FakeInstStatus("Associated")
    LEAVING = FakeInstStatus("Leaving")


@dataclass
class FakeInst:
    name: str
    code: str
    country: FakeCountry
    town: str
    status: FakeInstStatus = field(default=FakeInstStatuses.MEMBER)
    date_in: date = field(default=date(2000, 1, 1))

    @property
    def leaders(self) -> List["FakePerson"]:
        return [p for p in FakePeople.values() if self in p.led_insts]

    @property
    def members(self) -> List["FakePerson"]:
        return [p for p in FakePeople.values() if self == p.inst]


@dataclass
class FakeFlag:
    code: str


@dataclass
class FakeStatus:
    name: str


@dataclass
class FakeStatuses:
    cms = FakeStatus("CMS")
    exmember = FakeStatus("EXMEMBER")
    emeritus = FakeStatus("CMSEMERITUS")


@dataclass
class FakeOldAffiliation:
    name: str
    code: str
    start_date: date = field(default=date(2017, 12, 1))
    end_date: date = field(default=date(2018, 12, 1))
    is_primary: bool = field(default=True)


@dataclass
class FakePerson:
    login: str
    cms_id: int
    is_author: bool
    inst: FakeInst
    old_affiliation: List[FakeOldAffiliation] = field(default_factory=list)
    start_date: date = field(default=date(2017, 1, 1))
    flags: List[FakeFlag] = field(default_factory=list)
    led_insts: List[FakeInst] = field(default_factory=list)
    is_blocked: bool = field(default=False)
    activity: FakeActivity = field(default=FakeActivities.physicist)
    old_status: List[FakeOldStatus] = field(default_factory=list)
    is_suspended: bool = field(default=False)
    status: str = field(default=FakeStatuses.cms.name)

    @property
    def last_name(self) -> str:
        return self.login[1:].capitalize()

    @property
    def first_name(self) -> str:
        return f"{self.login[:1]}oobar".capitalize()

    @property
    def email(self):
        return f"{self.login}@cern.ch"

    @property
    def mo(self) -> List["FakeMoYear"]:
        """
        Retrieves the FakeMoYear data defined for this person or GENERATES DEFAULT DATA
        if nothing is found and person matches the criteria (author and not phd student)
        """
        mo_data = []
        mo_year: FakeMoYear
        for mo_year in FakeMoData.values():
            if mo_year.person == self:
                mo_data.append(mo_year)
        if (
            not mo_data
            and self.is_author
            and self.activity != FakeActivities.phd_student
        ):
            for year in range(max(2015, self.inst.date_in.year), date.today().year + 1):
                mo_data.append(FakeMoYear(self, self.inst, year, free=False))
        return mo_data

    @property
    def hr_id(self):
        return self.cms_id * 3

    @classmethod
    def applicant(
        cls,
        login: str,
        cms_id: int,
        inst: FakeInst,
        activity: FakeActivity = FakeActivities.physicist,
    ):
        return cls(
            login, cms_id, False, inst, [], date(2015, 12, 1), [], [], True, activity
        )


class FakeRegions(PseudoEnum):
    cern = FakeRegion("CERN", "CERN")
    france = FakeRegion("France", "FR")
    germany = FakeRegion("Germany", "DE")
    switzerland = FakeRegion("Switzerland", "CH")
    italy = FakeRegion("Italy", "IT")
    rdms = FakeRegion("Russia and Dubna Member States", "RDMS")
    uk = FakeRegion("United Kingdom", "UK")
    usa = FakeRegion("United States of America", "USA")
    ocms = FakeRegion("Other CERN Member States", "OCMS")
    osa = FakeRegion("Other States A", "OSA")
    osb = FakeRegion("Other States B", "OSB")


class FakeCountries(PseudoEnum):
    ch = FakeCountry("Switzerland", "CH", "CH")
    fr = FakeCountry("France", "FR", "FR")
    de = FakeCountry("Germany", "DE", "DE")
    it = FakeCountry("Italy", "IT", "IT")
    us = FakeCountry("United States of America", "US", "USA")
    ru = FakeCountry("Russia", "RU", "RDMS")
    am = FakeCountry("Armenia", "AM", "RDMS")
    by = FakeCountry("Belarus", "BY", "RDMS")
    ge = FakeCountry("Georgia", "GE", "RDMS")
    ua = FakeCountry("Ukraine", "UA", "RDMS")
    uz = FakeCountry("Uzbekistan", "UZ", "RDMS")
    uk = FakeCountry("United Kingdom", "UK", "UK")
    at = FakeCountry("Austria", "AT", "OCMS")
    be = FakeCountry("Belgium", "BE", "OCMS")
    bg = FakeCountry("Bulgaria", "BG", "OCMS")
    hu = FakeCountry("Hungary", "HU", "OCMS")
    pl = FakeCountry("Poland", "PL", "OCMS")
    pt = FakeCountry("Portugal", "PT", "OCMS")
    rs = FakeCountry("Serbia", "RS", "OCMS")
    es = FakeCountry("Spain", "ES", "OCMS")
    fi = FakeCountry("Finland", "FI", "OCMS")
    gr = FakeCountry("Greece", "GR", "OCMS")
    cn = FakeCountry("China", "CN", "OSA")
    ind = FakeCountry("India", "IN", "OSA")
    ir = FakeCountry("Iran", "IR", "OSA")
    my = FakeCountry("Malaysia", "MY", "OSA")
    nz = FakeCountry("New Zealand", "NZ", "OSA")
    pk = FakeCountry("Pakistan", "PK", "OSA")
    lk = FakeCountry("Sri Lanka", "LK", "OSA")
    tw = FakeCountry("Taiwan", "TW", "OSA")
    kr = FakeCountry("Korea", "KR", "OSA")
    th = FakeCountry("Thailand", "TH", "OSA")
    bh = FakeCountry("Bahrain", "BH", "OSB")
    br = FakeCountry("Brazil", "BR", "OSB")
    co = FakeCountry("Colombia", "CO", "OSB")
    hr = FakeCountry("Croatia", "HR", "OSB")
    cy = FakeCountry("Cyprus", "CY", "OSB")
    ec = FakeCountry("Ecuador", "EC", "OSB")
    eg = FakeCountry("Egypt", "EG", "OSB")
    ee = FakeCountry("Estonia", "EE", "OSB")
    ie = FakeCountry("Ireland", "IE", "OSB")
    kw = FakeCountry("Kuwait", "KW", "OSB")
    lb = FakeCountry("Lebanon", "LB", "OSB")
    lv = FakeCountry("Latvia", "LV", "OSB")
    lt = FakeCountry("Lithuania", "LT", "OSB")
    mx = FakeCountry("Mexico", "MX", "OSB")
    me = FakeCountry("Montenegro", "ME", "OSB")
    om = FakeCountry("Oman", "OM", "OSB")
    qa = FakeCountry("Qatar", "QA", "OSB")
    sa = FakeCountry("Saudi Arabia", "SA", "OSB")
    tr = FakeCountry("Turkey", "TR", "OSB")


class FakeFlags(PseudoEnum):
    icms_root = FakeFlag(FlagDef.ICMS_ROOT)
    icms_admin = FakeFlag(FlagDef.ICMS_ADMIN)
    icms_sec = FakeFlag(FlagDef.ICMS_SECR)
    misc_authorno = FakeFlag(FlagDef.MISC_AUTHORNO)


class FakeOldStatuses(PseudoEnum):
    physicist = FakeOldStatus("Physicist", 6, True, date(2016, 12, 1))
    phd_student = FakeOldStatus("Doctoral Student", 9, True)
    non_phd_student = FakeOldStatus("Non-Doctoral Student", 13, False)
    administrative = FakeOldStatus(
        "Administrative", 1, False, date(2019, 12, 1), date(2020, 12, 1)
    )
    engineer = FakeOldStatus("Engineer", 2, False)
    software_engineer = FakeOldStatus("Engineer Software", 3, False)
    other = FakeOldStatus("Other", 5, False)
    technician = FakeOldStatus("Technician", 15, False)
    theoretical_physicist = FakeOldStatus("Theoretical Physicist", 7, True)


class FakeInsts(PseudoEnum):
    cern = FakeInst("CERN", "CERN", FakeCountries.ch, "Meyrin")
    desy = FakeInst("DESY", "DESY", FakeCountries.de, "Hamburg")
    fermilab = FakeInst("FERMILAB", "FERMILAB", FakeCountries.us, "Batavia")
    void_inst = FakeInst("VOIDINST", "Void Institute", FakeCountries.ch, "Vernier")


class FakeOldAffiliations(PseudoEnum):
    cern = FakeOldAffiliation("CERN", "CERN")
    desy = FakeOldAffiliation("DESY", "DESY", date(2016, 12, 1), date(2017, 12, 1))
    fermilab = FakeOldAffiliation("FERMILAB", "FERMILAB")
    void_inst = FakeOldAffiliation(
        "VOIDINST", "Void Institute", date(2019, 12, 1), date(2021, 12, 1)
    )


class FakePeople(PseudoEnum):
    tbawej = FakePerson(
        "tbawej",
        9981,
        False,
        FakeInsts.cern,
        [],
        date(2017, 12, 1),
        [FakeFlags.icms_admin, FakeFlags.icms_root, FakeFlags.icms_sec],
    )
    grossi = FakePerson(
        "grossi",
        1234,
        True,
        FakeInsts.cern,
        [],
        date(2017, 12, 1),
        [],
        [FakeInsts.cern],
    )
    jdoe = FakePerson("jdoe", 3306, True, FakeInsts.cern, [], date(2017, 12, 1))
    wdoe = FakePerson("wdoe", 3307, True, FakeInsts.cern, [], date(2016, 12, 1))
    trice = FakePerson.applicant(
        "trice", 9090, FakeInsts.cern, FakeActivities.physicist
    )
    ksmith = FakePerson.applicant(
        "ksmith", 2341, FakeInsts.cern, FakeActivities.phd_student
    )
    ppending = FakePerson(
        "ppending",
        7788,
        False,
        FakeInsts.cern,
        [],
        date(2015, 12, 1),
        [],
        [],
        True,
        FakeActivities.engineer,
        [],
        True,
    )
    ptgreat = FakePerson(
        "ptgreat",
        8080,
        False,
        FakeInsts.cern,
        [],
        date(2015, 12, 1),
        [FakeFlags.icms_sec],
        [],
        True,
        FakeActivities.administrative,
        [],
        True,
    )
    tmuller = FakePerson(
        "tmuller",
        5432,
        True,
        FakeInsts.desy,
        [],
        date(2020, 12, 1),
        [],
        [FakeInsts.desy],
    )
    aschmidt = FakePerson(
        "aschmidt",
        5984,
        False,
        FakeInsts.desy,
        [],
        date(2019, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [],
        True,
    )
    tholland = FakePerson(
        "tholland", 2856, False, FakeInsts.fermilab, [], date(2021, 12, 1)
    )
    pparker = FakePerson("pparker", 9101, False, FakeInsts.cern, [], date(2016, 12, 1))
    mjane = FakePerson("mjane", 1213, False, FakeInsts.cern, [], date(2020, 12, 1))
    atest = FakePerson(
        "atest",
        6587,
        True,
        FakeInsts.cern,
        [FakeOldAffiliations.desy],
        date(2016, 12, 1),
    )
    btest = FakePerson(
        "btest",
        9538,
        False,
        FakeInsts.cern,
        [FakeOldAffiliations.desy],
        date(2018, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [FakeOldStatuses.physicist],
        False,
    )
    ctest = FakePerson(
        "ctest",
        2028,
        True,
        FakeInsts.desy,
        [],
        date(2018, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [FakeOldStatuses.physicist],
        False,
    )
    dtest = FakePerson(
        "dtest",
        7645,
        True,
        FakeInsts.desy,
        [FakeOldAffiliations.fermilab],
        date(2019, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [FakeOldStatuses.physicist],
        False,
    )
    etest = FakePerson(
        "etest",
        2058,
        False,
        FakeInsts.fermilab,
        [],
        date(2022, 12, 1),
        [],
        [FakeInsts.fermilab],
        True,
        FakeActivities.engineer,
        [FakeOldStatuses.physicist],
        True,
    )
    ftest = FakePerson(
        "ftest",
        4517,
        True,
        FakeInsts.fermilab,
        [FakeOldAffiliations.cern],
        date(2022, 12, 1),
        [],
        [],
        True,
        FakeActivities.physicist,
        [FakeOldStatuses.phd_student],
        False,
    )
    gtest = FakePerson(
        "gtest",
        3789,
        False,
        FakeInsts.void_inst,
        [],
        date(2021, 12, 1),
        [],
        [],
        False,
        FakeActivities.technician,
        [FakeOldStatuses.administrative],
        False,
    )
    htest = FakePerson(
        "htest",
        4634,
        True,
        FakeInsts.desy,
        [],
        date(2022, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [FakeOldStatuses.physicist],
        False,
    )
    itest = FakePerson(
        "itest",
        3985,
        True,
        FakeInsts.fermilab,
        [],
        date(2021, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [FakeOldStatuses.administrative],
    )
    jtest = FakePerson(
        "jtest",
        1076,
        False,
        FakeInsts.void_inst,
        [FakeOldAffiliations.cern],
        date(2022, 12, 1),
        [],
        [],
        False,
        FakeActivities.engineer,
        [FakeOldStatuses.administrative],
    )
    ktest = FakePerson(
        "ktest",
        4260,
        False,
        FakeInsts.desy,
        [],
        date(2016, 12, 1),
        [],
        [],
        False,
        FakeActivities.administrative,
        [FakeOldStatuses.software_engineer],
    )
    ltest = FakePerson(
        "ltest",
        5173,
        True,
        FakeInsts.cern,
        [FakeOldAffiliations.cern],
        date(2015, 12, 1),
        [],
        [],
        False,
        FakeActivities.software_engineer,
        [FakeOldStatuses.technician],
    )
    mtest = FakePerson(
        "mtest",
        6302,
        True,
        FakeInsts.cern,
        [FakeOldAffiliations.void_inst],
        date(2015, 12, 1),
        [],
        [],
        False,
        FakeActivities.physicist,
    )

    @classmethod
    def count_authors(cls):
        return len([p for p in cls.values() if p.is_author == True])

    @classmethod
    def count_team_members(cls, code: str):
        return len(
            [
                p
                for p in cls.values()
                if isinstance(p, FakePerson) and p.inst.code == code
            ]
        )

    @classmethod
    def authors(cls) -> List[FakePerson]:
        return [p for p in cls.values() if p.is_author == True]


@dataclass
class FakeAssignment:
    cms_id: int
    project_code: str
    fraction: float
    start_date: date
    end_date: Optional[date]


class FakeAssignments(PseudoEnum):
    assignment_1 = FakeAssignment(
        FakePeople.tbawej.cms_id, FakeProjects.daq.code, 0, date.today(), None
    )
    assignment_2 = FakeAssignment(
        FakePeople.tbawej.cms_id, FakeProjects.ecal.code, 0.3, date.today(), None
    )
    assignment_3 = FakeAssignment(
        FakePeople.aschmidt.cms_id, FakeProjects.daq.code, 0, date.today(), None
    )
    assignment_4 = FakeAssignment(
        FakePeople.aschmidt.cms_id, FakeProjects.ecal.code, 0.4, date.today(), None
    )
    assignment_5 = FakeAssignment(
        FakePeople.tholland.cms_id, FakeProjects.ecal.code, 0, date.today(), None
    )


@dataclass
class FakeMoYear:
    person: FakePerson
    inst: FakeInst
    year: int = field(default=date.today().year)
    free: bool = field(default=False)


class FakeMoData(PseudoEnum):
    pass


@pytest.fixture(scope="class")
def populate_people_db():
    from icmsutils.icmstest import mock
    from icms_orm.cmspeople import (
        Person,
        Flag,
        PeopleFlagsAssociation as PFA,
        Institute as Inst,
        MemberActivity,
    )

    session = Flag.session()

    fake_activity: FakeActivity
    for fake_activity in FakeActivities.values():
        session.add(
            mock.MockActivityFactory.create_instance(
                instance_overrides={
                    MemberActivity.id: fake_activity.old_id,
                    MemberActivity.name: fake_activity.name,
                }
            )
        )
    session.commit()

    fake_inst: FakeInst
    for fake_inst in FakeInsts.values():
        inst = mock.MockInstFactory.create_instance(
            {
                Inst.code: fake_inst.code,
                Inst.country: fake_inst.country.name,
                Inst.name: fake_inst.name,
                Inst.town: fake_inst.town,
                Inst.dateCmsIn: fake_inst.date_in,
            }
        )
        Flag.session().add(inst)
    Flag.session().commit()

    fake_flag: FakeFlag
    for fake_flag in FakeFlags.values():
        Flag.session.add(
            Flag.from_ia_dict(
                {Flag.id: fake_flag.code, Flag.desc: "FAKE {}".format(fake_flag.code)}
            )
        )
    Flag.session().commit()

    fake_person: FakePerson
    for fake_person in FakePeople.values():
        mock_overrides = {
            Person.hrId: fake_person.hr_id,
            Person.loginId: fake_person.login,
            Person.cmsId: fake_person.cms_id,
            Person.isAuthor: fake_person.is_author,
            Person.instCode: fake_person.inst.code,
            Person.isAuthorBlock: fake_person.is_blocked,
            Person.activityId: fake_person.activity.old_id,
            Person.isAuthorSuspended: fake_person.is_suspended,
            Person.status: fake_person.status,
            Person.lastName: fake_person.last_name,
            Person.firstName: fake_person.first_name,
            User.email1: fake_person.email,
            User.emailCern: fake_person.email,
        }

        mo_year: FakeMoYear
        for mo_year in fake_person.mo:
            (
                mo_col,
                phd_mo_col,
                free_mo_col,
                phd_ic_col,
                phd_fa_col,
            ) = MoData.get_columns_for_year(mo_year.year)
            mock_overrides[mo_col] = "YES"
            if mo_year.free:
                mock_overrides[free_mo_col] = "YES"
            else:
                mock_overrides[phd_mo_col] = "YES"
                mock_overrides[phd_ic_col] = mo_year.inst.code
                mock_overrides[phd_fa_col] = "DefaultFA"
        person = mock.MockPersonFactory.create_instance(
            instance_overrides=mock_overrides
        )
        mo = mock.MockPersonMoFactory.create_instance(
            person=person, instance_overrides=mock_overrides
        )
        user: User = mock.MockUserFactory.create_instance(
            person=person, instance_overrides=mock_overrides
        )

        Flag.session().add(person)
        Flag.session().add(user)
        Flag.session().add(mo)
        Flag.session().commit()
        held_flag: FakeFlag
        for held_flag in fake_person.flags:
            Flag.session().add(
                PFA.from_ia_dict(
                    {PFA.cmsId: fake_person.cms_id, PFA.flagId: held_flag.code}
                )
            )
        for led_team in fake_person.led_insts:
            inst: Inst
            inst = session.query(Inst).filter(Inst.code == led_team.code).one()
            inst.cbiCmsId = fake_person.cms_id
            session.add(inst)
        Flag.session().commit()


@pytest.fixture(scope="class")
def populate_icms_db():
    """
    the MO info is not synchronized as of yet, neither is the inst creation date
    """
    from icmsutils.icmstest import mock
    from icms_orm.common import (
        Person,
        Affiliation,
        Institute,
        PersonStatus,
        LegacyFlag,
        InstituteLeader,
        InstituteStatus,
        Country,
        Region,
    )
    from icms_orm.toolkit import Status

    _ = [
        Status.session().add(Status(_code))
        for _code in [
            Status.ACTIVE,
            Status.CANCELLED,
            Status.DELETED,
            Status.DISABLED,
            Status.PENDING,
            Status.ACCEPTED,
            Status.REJECTED,
            Status.DONE,
            Status.SUSPENDED,
        ]
    ]

    fake_region: FakeRegion
    for fake_region in FakeRegions.values():
        db_region = Region.from_ia_dict(
            {
                Region.name: fake_region.name,
                Region.code: fake_region.code,
            }
        )
        Region.session().add(db_region)
    Region.session().flush()
    fake_country: FakeCountry
    for fake_country in FakeCountries.values():
        db_country = Country.from_ia_dict(
            {
                Country.name: fake_country.name,
                Country.code: fake_country.code,
                Country.region_code: fake_country.region_code,
            }
        )
        Country.session().add(db_country)
    Country.session().flush()
    fake_inst: FakeInst
    for fake_inst in FakeInsts.values():

        db_inst = Institute.from_ia_dict(
            {
                Institute.name: fake_inst.name,
                Institute.code: fake_inst.code,
                Institute.country_code: fake_inst.country.code,
            }
        )
        db_inst_status = InstituteStatus.from_ia_dict(
            {
                InstituteStatus.code: fake_inst.code,
                InstituteStatus.status: fake_inst.status.name,
                InstituteStatus.start_date: datetime.today(),
                InstituteStatus.end_date: None,
            }
        )
        Institute.session().add(db_inst)
        Institute.session().flush()
        Institute.session().add(db_inst_status)
    Institute.session().commit()

    _fake_flag_remote_id = 332

    fake_person: FakePerson
    for fake_person in FakePeople.values():
        for factory in [
            mock.MockNewPersonFactory,
            mock.MockAffiliationFactory,
            mock.MockNewPersonStatusFactory,
        ]:
            Person.session.add(
                factory.create_instance(
                    {
                        Affiliation.is_primary: True,
                        Affiliation.inst_code: fake_person.inst.code,
                        Person.hr_id: fake_person.hr_id,
                        Person.login: fake_person.login,
                        Person.cms_id: fake_person.cms_id,
                        Person.last_name: fake_person.last_name,
                        Person.first_name: fake_person.first_name,
                        Person.email: fake_person.email,
                        PersonStatus.is_author: fake_person.is_author,
                        PersonStatus.author_block: fake_person.is_blocked,
                        PersonStatus.epr_suspension: fake_person.is_suspended,
                        PersonStatus.status: fake_person.status,
                        PersonStatus.activity: fake_person.activity.name,
                        PersonStatus.start_date: fake_person.start_date,
                    }
                )
            )
            Person.session().commit()
        fake_flag: FakeFlag
        for fake_flag in fake_person.flags:
            Person.session.add(
                LegacyFlag.from_ia_dict(
                    {
                        LegacyFlag.cms_id: fake_person.cms_id,
                        LegacyFlag.flag_code: fake_flag.code,
                        LegacyFlag.start_date: date(2010, 1, 1),
                        LegacyFlag.remote_id: _fake_flag_remote_id,
                    }
                )
            )
            _fake_flag_remote_id += 1
        led_inst: FakeInst
        for led_inst in fake_person.led_insts:
            Person.session.add(
                InstituteLeader.from_ia_dict(
                    {
                        InstituteLeader.is_primary: True,
                        InstituteLeader.start_date: date(2010, 1, 1),
                        InstituteLeader.inst_code: led_inst.code,
                        InstituteLeader.cms_id: fake_person.cms_id,
                    }
                )
            )
        old_affiliation: FakeOldAffiliation
        for old_affiliation in fake_person.old_affiliation:
            Person.session.add(
                Affiliation.from_ia_dict(
                    {
                        Affiliation.cms_id: fake_person.cms_id,
                        Affiliation.inst_code: old_affiliation.code,
                        Affiliation.is_primary: old_affiliation.is_primary,
                        Affiliation.start_date: old_affiliation.start_date,
                        Affiliation.end_date: old_affiliation.end_date,
                    }
                )
            )
        old_status: FakeOldStatus
        for old_status in fake_person.old_status:
            Person.session.add(
                PersonStatus.from_ia_dict(
                    {
                        PersonStatus.cms_id: fake_person.cms_id,
                        PersonStatus.activity: old_status.status,
                        PersonStatus.start_date: old_status.start_date,
                        PersonStatus.end_date: old_status.end_date,
                        PersonStatus.is_author: old_status.is_author,
                    }
                )
            )
    Person.session().commit()


@pytest.fixture(scope="class")
def insert_cms_projects():
    from icms_orm.toolkit import CmsProject

    ssn = CmsProject.session()
    project: FakeProject
    for project in FakeProjects.values():
        ssn.add(CmsProject(project.code, project.name))
    ssn.commit()


@pytest.fixture(scope="class")
def insert_projects():
    from icms_orm.common import Project

    ssn = Project.session()
    project: FakeProject
    for fake_project in FakeProjects.values():
        ia_dict = {Project.code: fake_project.code, Project.name: fake_project.name}
        project = Project.from_ia_dict(ia_dict)
        ssn.add(project)
    ssn.commit()


@pytest.fixture(scope="class")
def insert_cms_assignments():
    from icms_orm.common import Assignment

    ssn = Assignment.session()
    assignment: FakeAssignment
    for fake_assignment in FakeAssignments.values():
        ia_dict = {
            Assignment.cms_id: fake_assignment.cms_id,
            Assignment.project_code: fake_assignment.project_code,
            Assignment.fraction: fake_assignment.fraction,
            Assignment.start_date: fake_assignment.start_date,
            Assignment.end_date: fake_assignment.end_date,
        }
        assignment = Assignment.from_ia_dict(ia_dict)
        ssn.add(assignment)
    ssn.commit()


@pytest.fixture(scope="class")
def populate_dbs(
    populate_people_db,
    populate_icms_db,
    insert_cms_weeks,
    insert_projects,
    insert_cms_assignments,
):
    return time.time()
