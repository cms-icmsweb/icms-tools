"""
The initial purpose of this file was to register a temporary config governor that would already indicate that the test mode is active.
That allows the icmsutils.icmstest package's safeguard to be satisfied before the Governor is automatically replaced with the one
created during webepp's creation.
"""

import flask
import logging
from icmscommon.governors import ConfigGovernor


def pytest_load_initial_conftests(args, early_config, parser):
    try:
        app = flask.current_app
        logging.info('Current app already present: {0} - startup governot should not be needed'.format(app))
    except Exception as _e:
        logging.info(
            'It looks like the current_app could not be retrieved. Registerting a startup governot for testing.')

        class StartupConfigGovernor(ConfigGovernor):
            """
            Bridges the gap between pytest startup and app's creation (the latter creates and registers a valid governor)
            """

            def _check_test_mode(self):
                return True

            def _get_db_owner_pass(self):
                return super()._get_db_owner_pass()

            def _get_db_string(self, db):
                return super()._get_db_string(db)

            def _get_db_owner_user(self):
                return super()._get_db_owner_user()

            def _get_epr_db_string(self):
                return super()._get_epr_db_string()

        ConfigGovernor.set_instance(StartupConfigGovernor())