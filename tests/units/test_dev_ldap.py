from app_profiles import ProfileDev
import pytest
import json
import logging
from io import BytesIO
from typing import Any, Dict, List, Optional
from flask.wrappers import Response
from icms_orm import PseudoEnum
from mockito import unstub, when
from mockito.matchers import captor
from requests import Response as ReqResponse
from requests import Session
from requests.sessions import PreparedRequest
from tests.fixtures.fakedata_fixtures import FakePeople, FakePerson
from tests.fixtures.testkit.test_client import IcmsTestClient, RequestBuilder
from icmsutils.icmstest.assertables import count_in_db
from icmsutils.ldaputils import LdapPerson, LdapProxy
from util.ldap import DevLdapProxy

log: logging.Logger = logging.getLogger(__name__)


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs')
class TestDevLdap():

    ldap: Optional[DevLdapProxy] = None

    @classmethod
    def setup_class(cls):
        cls.ldap = DevLdapProxy()

    @classmethod
    def assert_ldap_result_matches_expectations(cls, person: FakePerson, match: Optional[LdapPerson]):
        assert isinstance(
            match, LdapPerson), "Result was expected to be an LdapPerson instance"
        assert person.hr_id == match.hrId
        assert 'zh' in match.cernEgroups
        assert 'cms-web-access' in match.cmsEgroups
        assert person.inst.code == match.institute

    @pytest.mark.parametrize('person', list(FakePeople.values()))
    def test_get_person_by_hrid(self, person: FakePerson):
        assert self.ldap is not None
        match: Optional[LdapPerson] = self.ldap.get_person_by_hrid(
            person.hr_id)
        self.assert_ldap_result_matches_expectations(person, match)

    @pytest.mark.parametrize('person', list(FakePeople.values()))
    def test_get_person_by_login(self, person: FakePerson):
        assert self.ldap is not None
        match: Optional[LdapPerson] = self.ldap.get_person_by_login(
            person.login)
        self.assert_ldap_result_matches_expectations(person, match)

    @pytest.mark.parametrize('person', list(FakePeople.values()))
    def test_get_person_by_email(self, person: FakePerson):
        assert self.ldap is not None
        match: Optional[LdapPerson] = self.ldap.get_person_by_mail(
            person.email)
        self.assert_ldap_result_matches_expectations(person, match)

    @pytest.mark.parametrize('person', list(FakePeople.values()))
    def test_find_people_by_family_name(self, person: FakePerson):
        assert self.ldap is not None
        matches: List[LdapPerson] = self.ldap.find_people_by_family_name(
            person.last_name)
        assert len(matches) > 0
        for m in matches:
            assert person.last_name == m.familyName

    @pytest.mark.parametrize('gid, expected_matches', [(1399, len(FakePeople.values())), (1400, 0)])
    def test_find_people_by_gid(self, gid: int, expected_matches: int):
        assert self.ldap is not None
        matches: List[LdapPerson] = self.ldap.find_people_by_gid(gid)
        assert expected_matches == len(matches)

    @pytest.mark.parametrize('people', [
        list(FakePeople.values())[:4],
        list(FakePeople.values())[3:]
    ])
    def test_find_people_by_hr_ids(self, people: List[FakePerson]):
        assert self.ldap is not None
        hr_ids = {p.hr_id for p in people}
        matches: List[LdapPerson] = self.ldap.find_people_by_hrids(
            list(hr_ids))
        assert len(people) == len(matches)
        for match in matches:
            assert match.hrId in hr_ids, f'HR ID {match.hrId} should be in {hr_ids}'

    def test_adding_mock_entries(self):
        self.ldap.register_mocks(ProfileDev.LDAP_MOCK_ENTRIES)
        match: LdapPerson
        for match in [self.ldap.get_person_by_login('pexter'), self.ldap.get_person_by_mail('pexter@cern.ch')]:
            assert match.firstName == 'Pierre'
            assert match.familyName == 'External'
            assert match.gid == 42
