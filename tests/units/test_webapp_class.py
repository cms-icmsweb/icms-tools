from logging import log
import logging
from typing import List
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from tests.fixtures.basic_fixtures import client
import re

log: logging.Logger = logging.getLogger(__name__)


@pytest.fixture(scope="class")
def alter_app_config(app):
    config_backup = {}
    with app.app_context():
        config_backup.update(app.config)
        app.config.get('DEPRECATION_FALLBACK_SWAPTOKENS')[
            'localhost'] = 'foobarhost'
        app.config.get('DEPRECATED_ROUTES')['/foo/bar'] = None
        app.config.get('DEPRECATED_ROUTES')[
            '/api/cadiLines'] = '/restplus/cadi/lines'
    yield
    log.info("Now may some cleanup happen")
    with app.app_context():
        app.config = config_backup


@pytest.mark.usefixtures("alter_app_config")
class TestWebappClass():
    @pytest.mark.parametrize('origin', ['http://cms.cern.ch', 'https://icms-dev.cern.ch/', 'https://icms.cern.ch/tools'])
    def test_header_access_control_allow_origin_is_present(self, client: IcmsTestClient, origin: str):
        resp: Response = client.request(
            "/restplus").header('Origin', origin).get()
        assert 'access-control-allow-origin' in resp.headers
        assert origin == resp.headers.get('access-control-allow-origin')

    @pytest.mark.parametrize('origin', ['https://cms.cern.chh', 'http://icms.cern.ch.foo'])
    def test_header_access_control_allow_origin_is_absent(self, client: IcmsTestClient, origin: str):
        resp: Response = client.request(
            "/restplus").header('Origin', origin).get()
        assert 'access-control-allow-origin' not in resp.headers

    @pytest.mark.parametrize('requested_path, expected_replacement, message', [
        ('/api/cadiLines', '/restplus/cadi/lines',
         'A direct PATH remapping should exist'),
        ('/api/cadiLines', 'http://localhost/restplus/cadi/lines',
         'A direct PATH remapping: hostname should remain in place'),
        ('/api/cadiLines?awg=XYZ', '/restplus/cadi/lines',
         'Query string parameters should be tolerated'),
        ('/foo/bar', 'http://foobarhost/foo/bar',
         'No direct remapping - the full URL should be patched')
    ])
    def test_deprecation_announcement(self, client: IcmsTestClient, requested_path: str, expected_replacement: str, message: str):
        resp: Response = client.request(requested_path).get()
        assert 410 == resp.status_code
        text = resp.data.decode(resp.charset)
        region: List[str] = re.findall(r'in favor of [\S]+', text)
        assert 1 == len(region)
        assert expected_replacement in region[0], message
