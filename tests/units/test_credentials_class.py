import pytest
from util.structs import Credentials


class TestCredentials():
    @pytest.mark.parametrize('input, exp_email, exp_username', [
        ('noah', None, 'noah'), 
        ('someone.with.their.name@cern.ch', 'someone.with.their.name@cern.ch', None)
    ])
    def test_from_string_factory(self, input, exp_email, exp_username):
        creds: Credentials = Credentials.from_string(input)
        assert creds.password is None
        assert exp_email == creds.email
        assert exp_username == creds.username

    @pytest.mark.parametrize('input', ['no-reply@cern.ch'])
    def test_for_email_factory_method(self, input):
        creds: Credentials = Credentials.for_email(input)
        assert creds.password is None
        assert creds.username is None
        assert input == creds.email

    @pytest.mark.parametrize('input, exp_password, exp_username', [
        (['potatoe', '123'], '123', 'potatoe'), 
        (['carrot'], None, 'carrot')
    ])
    def test_for_username_factory_method(self, input, exp_password, exp_username):
        creds: Credentials = Credentials.for_username_password(*input)
        assert creds.email is None
        assert exp_password == creds.password
        assert exp_username == creds.username
