import pytest
from flask import Flask
import logging
from io import BytesIO
from typing import Any, Dict, Optional
from flask.wrappers import Response
from icms_orm import PseudoEnum
from mockito import unstub, when
from mockito.matchers import captor
from requests import Response as ReqResponse
from requests import Session
from requests.sessions import PreparedRequest
from tests.fixtures.fakedata_fixtures import FakePeople
from tests.fixtures.testkit.test_client import IcmsTestClient, RequestBuilder
from icmsutils.icmstest.assertables import count_in_db
from scripts.cadiReminders import main as cadi_reminders_main

log: logging.Logger = logging.getLogger(__name__)


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs')
class TestScripts():
    # Dummy invocations for now
    def test_cadi_reminders(self, app: Flask, client: IcmsTestClient):
        with app.app_context():
            cadi_reminders_main()

    def test_authorship_applications_script(self, app: Flask):
        from scripts.check_authorship_application import main as aa_main
        with app.app_context():
            aa_main()
