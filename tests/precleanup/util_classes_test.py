from icms_orm.common.common_schema_misc_tables import EmailMessage
from icmscommon.governors import ConfigGovernor, DbGovernor
from util.webapp_governors import WebappConfigGovernor
import zeep
from requests import auth
import netrc
from util.EgroupHandler import EgroupHandler
from util.accountCheck import AccountChecker, send_mail_db
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
import logging
from icmsutils.ldaputils import LdapProxy
from mockito import unstub, when, any
import tempfile
from tests.fixtures.fakedata_fixtures import FakeInsts, FakePeople, FakePerson
from datetime import date


log: logging.Logger = logging.getLogger(__name__)


@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db', 'client')
class TestAccountChecker:
    @classmethod
    def teardown_class(cls):
        unstub()

    @classmethod
    def setup_class(cls):
        when(LdapProxy).find_people_by_filter(any()).thenReturn([])
        when(LdapProxy).find_people_by_hrids(any()).thenReturn([])

    @pytest.mark.parametrize('user, in_db, in_ldap, in_zh', [
        (FakePeople.jdoe, True, False, False),
        (FakePerson('planetix', -23214, False, FakeInsts.cern, [], date(2017, 12, 1)), False, False, False)
    ])
    def test_dummy_account_check(self, user: FakePerson, in_db: bool, in_ldap: bool, in_zh: bool):
        checker: AccountChecker = AccountChecker()
        checker.check(user.hr_id)
        log.debug(checker.issues)

        assert in_db == ('was found in CMS DB' in str(checker.issues))
        assert in_zh != ('NOT in zh or zj' in str(checker.issues))
        assert in_ldap != ('not found in LDAP' in str(checker.issues))

    @pytest.mark.parametrize('exp_in_output, stubbed', [('Error', True), ('OK', False), ('Warning', False)])
    def test_mail_sending_helper(self, exp_in_output: str, stubbed: bool):
        db: DbGovernor = DbGovernor.get_db_manager()
        if stubbed:
            when(db.session).add(any()).thenRaise(Exception('Suprize!'))
        result = send_mail_db(db, 'me@he.re', 'you@the.re',
                              '', 'Draft#12', '14, really')
        assert exp_in_output in str(result)
        unstub(db.session)


class FakeResult():
    def __init__(self):
        self.result = 'OK'


class FakeMember(FakeResult):
    def __init__(self, type='StaticEgroup', primary_account='Whatever', email='tbawej@cern.ch', id=47):
        self.Type = type
        self.PrimaryAccount = primary_account
        self.Email = email
        self.ID = id
        self.Name = primary_account


class FakeGroup(FakeResult):
    def __init__(self):
        self.result = self
        self.Members = [FakeMember(), FakeMember(
            'DynamicEgroup', id=42, email='bestpizza@not.really')]


class FakeMethod():
    def __init__(self, result: FakeResult):
        self._result = result

    def __call__(self, *args, **kwargs):
        return self._result


class FakeZeep():
    class Service():
        def __init__(self):
            self.FindEgroupByName = FakeMethod(FakeGroup())
            self.DeleteEgroup = FakeMethod(FakeResult())

    @property
    def service(self):
        return FakeZeep.Service()


class TestEgroupHandler:

    @classmethod
    def teardown_class(cls):
        unstub()

    @classmethod
    def setup_class(cls):
        mockfile = tempfile.mkstemp()
        mockrc: netrc.netrc = netrc.netrc(mockfile[1])
        when(netrc).netrc('/data/secrets/.netrc').thenReturn(mockrc)
        when(netrc).netrc('/Users/ap/.netrc').thenReturn(mockrc)
        when(mockrc).authenticators(any()).thenReturn(
            ('foo', 'bar', 'barefoot'))
        when(auth).HTTPBasicAuth(any(), any()).thenReturn(None)
        when(zeep).Client(any(), transport=any(),
                          plugins=any()).thenReturn(FakeZeep())
        cls.h: EgroupHandler = EgroupHandler()

    def test_group_search(self):
        result = self.h.findGroup('grroupp')
        log.debug(result)

    @pytest.mark.parametrize('groupname', [('grrr', )])
    def test_group_members_retrieval(self, groupname: str):
        members, memberNoAccount, egMemberList, accountList, emailList = self.h.getGroupMembers('grroupp')
        print( '\n===>>', members, memberNoAccount, egMemberList, accountList, '\n')
        assert 2 == len(members) + len(egMemberList) + len(accountList)
        # somehow the two member-eGroups are both "whatever"s, no pizza found :(
        # assert 'pizza' in str( egMemberList )
        assert 'Whatever' in str( ','.join(members+egMemberList+accountList) )
        log.debug(members)

    @pytest.mark.parametrize('method, params, exp_result', [
        (EgroupHandler.getGroupMembers, [None], ([], [], [], [], [])),
        (EgroupHandler.accountInEgroup, [None, None], (False, None)),
        (EgroupHandler.deleteGroup, [None], (False, None)),
        (EgroupHandler.findGroup, [None], (False, 'no group-name given'))
    ])
    def test_missing_params(self, method, params, exp_result):
        result = method(self.h, *params)
        assert exp_result == result

    @pytest.mark.parametrize('input, exp_output', [
        ({}, True),
        ({'warnings': [{'Message': 'Last warning!'}]}, False),
        ({'error': {'Message': 'This cannot be!'}}, False)
    ])
    def test_check_ok(self, input, exp_output):
        assert exp_output == self.h.checkOK(input)

    # @pytest.mark.parametrize('input, exp_output', [
    #     ({'error': {'Message': 'This cannot be!'}}, False),
    # ])
    # def test_check_ok_fails(self, input, exp_output):
    #     with pytest.raises(Exception) as e_info:
    #         exp_output == self.h.checkOK(input)
